<?php //$Id: forgot_password_form.php,v 1.7.2.1 2007/11/23 22:12:35 skodak Exp $

require_once $CFG->libdir.'/formslib.php';

class enrol_unasus_profile_form extends moodleform {

    function definition() {
    	global $profile;
    	$fields= $profile->get_fields_user_profile();
        
        $seleted_fields_filter=$profile->get_profile_config_in_array(enrol_unasus_userprofile::$USER_PROFILE_FILTER);
      
       
    	$seleted_fields_reporter=$profile->get_profile_config_in_array(enrol_unasus_userprofile::$USER_PROFILE_REPORTER);
    	
    		$mform    =& $this->_form;
        $renderer =& $mform->defaultRenderer();
         
        $mform->addElement('header', '', get_string('config_enable_field_filter','enrol_unasus'), '');
        foreach ($fields as $field){
         	 $mform->addElement('checkbox', 'F_'.$field->id, $field->name);
         	if(isset($seleted_fields_filter[$field->id])) {$mform->setDefault('F_'.$field->id, 1);}
         	 
         }
         
         $mform->addElement('header', '', get_string('config_enable_field_reporter','enrol_unasus'), '');
        foreach ($fields as $field){
         	 $mform->addElement('checkbox', 'R_'.$field->id, $field->name);
         	if(isset($seleted_fields_reporter[$field->id])) {$mform->setDefault('R_'.$field->id, 1);}
         	 
         }
        $this->add_action_buttons(true, get_string('save','enrol_unasus'));
    }

    function validation($data, $files) {
        global $CFG;

         $errors = parent::validation($data, $files);
         
      
     return $errors;
    }

}

?>