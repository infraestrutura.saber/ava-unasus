<?php

   require("../../../../config.php");
   require("$CFG->dirroot/enrol/unasus/role/lib.php");
   require("$CFG->dirroot/enrol/unasus/lib/context.php");
   require("$CFG->dirroot/enrol/unasus/tab.php");

 require_login();
 
  // Acesso permitido apenas ao usu�rio admin
    require_capability('enrol/unasus:roleview', get_context_instance(CONTEXT_SYSTEM), NULL, false);
     add_to_log(1, 'enrol_unasus', 'roleview', "enrol/unasus/role/index.php", getremoteaddr());    
     
   
   
    $navigation = build_navigation(array(array('name' => get_string('enrolname','enrol_unasus'), 'link' => "$CFG->wwwroot/enrol/unasus/index.php", 'type' => 'misc'),
                                     array('name' => get_string('role_manege','enrol_unasus'), 'link' => null, 'type' => 'misc')));
   
   
   
    $context =new context();
    $role =new role();
   $config=$role->get_contextid_config();
   
   $displaylist = array();
    $parentlist = array();
    make_categories_list($displaylist, $parentlist);
    
    print_header(get_string('enrolname','enrol_unasus'),get_string('applicationsenrolment','enrol_unasusem'), $navigation);
    $currenttab = 'role';
    
    print_tabs($tabs, $currenttab, $inactive, $activated);
 	get_sistemacontext_view();
   get_course_category_view();
     get_course_view();
     print_footer();
    
     function get_course_view(){
   		global $context,$config,$displaylist;
   		 $rows = $context->get_courses();
   				 
   		$output = "<center><table   border=\"1\"   width=\"95%\" cellspacing=\"1\" cellpadding=\"5\">";
    	$output .= "<tr>";
                $output .= "<th align=\"left\">".get_string('id','enrol_unasus')."</th>";
				$output .= "<th align=\"left\">".get_string('course','enrol_unasus')."</th>";
				$output .= "<th align=\"left\">".get_string('category','enrol_unasus')."</th>";
				$output .= "<th align=\"left\">".get_string('role','enrol_unasus')."</th>";
				$output .= "</tr>";
    
    	if(!empty($rows)){
	    	foreach ($rows as $row){
	    		$exist_conf=FALSE;
	    		if(isset($config[$row->contextid]))$exist_conf=$config[$row->contextid];
	    		$link="<a href='add.php?contextid=".$row->contextid."'>".get_string('add','enrol_unasus')."</a>";
	    		if($exist_conf){
	    			$link="<a href='edit.php?contextid=".$row->contextid."'>".get_string('edit','enrol_unasus')."</a>";
	    			$link .=" | <a href='view.php?contextid=".$row->contextid."'>".get_string('view','enrol_unasus')."</a>";
	    		}
	    		$output .= "<tr>";
			 	$output .=  "<td>".$row->id."</td>"; 
            	$output .=  "<td>".$row->fullname."</td>";
            	$output .=  "<td>".$displaylist[$row->category]."</td>";
            	$output .=  "<td>$link</td>";
            	$output .= "</tr>";    
    		}
    	}    
    	$output .= "</table></center>";
    	echo $output;
   }
   
   function get_course_category_view(){
   		global $context,$config,$displaylist;
   		 $rows = $context->get_courses_category();
   				 
   		$output = "<center><table   border=\"1\"   width=\"95%\" cellspacing=\"1\" cellpadding=\"5\">";
    	$output .= "<tr>";
                $output .= "<th align=\"left\">".get_string('id','enrol_unasus')."</th>";
				$output .= "<th align=\"left\">".get_string('course_category','enrol_unasus')."</th>";
				$output .= "<th align=\"left\">".get_string('role','enrol_unasus')."</th>";
				$output .= "</tr>";
    
    	if(!empty($rows)){
	    	foreach ($rows as $row){
	    		$exist_conf=FALSE;
	    		if(isset($config[$row->contextid]))$exist_conf=$config[$row->contextid];
	    		$link="<a href='add.php?contextid=".$row->contextid."'>".get_string('add','enrol_unasus')."</a>";
	    		if($exist_conf){
	    			$link="<a href='edit.php?contextid=".$row->contextid."'>".get_string('edit','enrol_unasus')."</a>";
	    			$link .=" | <a href='view.php?contextid=".$row->contextid."'>".get_string('view','enrol_unasus')."</a>";
	    		}
	    		$output .= "<tr>";
			 	$output .=  "<td>".$row->id."</td>"; 
            	$output .=  "<td>".$displaylist[$row->id]."</td>";
            	$output .=  "<td>$link</td>";
            	$output .= "</tr>";    
    		}
    	}    
    	$output .= "</table></center>";
    	$output .= "<br><br>";
    	echo $output;
   }
   
   function get_sistemacontext_view(){
   		global $config;
   		$output = "<center><table   border=\"1\"   width=\"95%\" cellspacing=\"1\" cellpadding=\"5\">";
    	$output .= "<tr>";
               
				$output .= "<th align=\"left\">".get_string('context_system','enrol_unasus')."</th>";
				$output .= "<th align=\"left\">".get_string('role','enrol_unasus')."</th>";
				$output .= "</tr>";
    			$exist_conf=FALSE;
    			if(isset($config[1]))$exist_conf=$config[1];
	    		
	    		$link="<a href='add.php?contextid=1'>".get_string('add','enrol_unasus')."</a>";
	    		if($exist_conf){
	    			$link="<a href='edit.php?contextid=1'>".get_string('edit','enrol_unasus')."</a>";
	    			$link .=" | <a href='view.php?contextid=1'>".get_string('view','enrol_unasus')."</a>";
	    		}
	    		$output .= "<tr>";
		
            	$output .=  "<td>".get_string('config_global','enrol_unasus')."</td>";
            	$output .=  "<td>$link</td>";
            	$output .= "</tr>";    
    		
    	$output .= "</table></center>";
    	$output .= "<br><br>";
    	echo $output;
   }
?>