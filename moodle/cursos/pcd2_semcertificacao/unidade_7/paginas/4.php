<?php
               
    $pg_atual=4;
     registro($id,unid7_pg,$pg_atual,unid7_ev);
?>

<style type="text/css">

#lista li{
            list-style-type: disc;   
       margin-left: 30px;

        
}
</style> 

   <h4 class="titulo">Cuidados durante a execução do procedimento</h4>

   De acordo com Barros et al (2002), o paciente em oxigenoterapia deve ser monitorizado adequadamente, lembrando que o exame físico é o primeiro passo para a realização da monitorização respiratória. Portanto, é imprescindível que a equipe de saúde oriente o cuidador e sempre atente para:
<ul id="lista">
<li>padrão respiratório;</li>
   
<li>sinais vitais, principalmente a frequência respiratória;
<li>oximetria de pulso, conforme as condições clínicas do paciente;
<li>fluxo de O<sub>2</sub> fornecido;</li>
    <li>manutenção de vias aéreas sempre pérveas, através do posicionamento correto da cânula, realizando aspiração destas quando necessário;</li>
<li>umidificação das vias aéreas com soro fisiológico a 0,9%, sempre que necessário;</li>
<li>manutenção de boa vedação e adaptação à face do paciente, do dispositivo utilizado;</li>
<li>substituição da máscara facial por cateter nasal durante as refeições;</li>
    <li>prevenção de lesões em narinas.</li>
    </ul>
 
   
  