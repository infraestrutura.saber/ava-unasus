<?php 

require_once $CFG->libdir.'/formslib.php';

class form_add extends moodleform {

    function definition() {
    	global $param;
    
        $mform    =& $this->_form;
        $renderer =& $mform->defaultRenderer();

        $mform->addElement('header', '', get_string('role_add','enrol_unasus'), '');
        
    
        
         $mform->addElement('hidden', 'contextid', $param->contextid);
        $mform->setType('contextid', PARAM_INT);
        
         $mform->addElement('hidden', 'type','CBO');
        $mform->setType('type', PARAM_TEXT);
        
         $mform->addElement('hidden', 'name', 'CBO');
        $mform->setType('name', PARAM_TEXT);
        
       $mform->addElement('text', 'title', get_string('role_title','enrol_unasus'));
        $mform->setType('title', PARAM_TEXT);
      	$mform->addRule('title', get_string('requiredfield','enrol_unasus'), 'required', null, 'cliente');
         $mform->setHelpButton('title', array('role_title', get_string('role_title', 'enrol_unasus'), 'enrol_unasus'));
     
       
        $mform->addElement('textarea', 'value', get_string('cbo','enrol_unasus'),'wrap="virtual" rows="4" cols="40"');
        $mform->setType('value', PARAM_TEXT);
      	$mform->addRule('value', get_string('requiredfield','enrol_unasus'), 'required', null, 'cliente');
         $mform->setHelpButton('value', array('role_cbo', get_string('cbo', 'enrol_unasus'), 'enrol_unasus'));
         
        $mform->addElement('textarea', 'description', get_string('descriptiont','enrol_unasus'),'wrap="virtual" rows="7" cols="40"');
        $mform->setType('description', PARAM_TEXT);
         $mform->setHelpButton('description', array('role_description', get_string('descriptiont', 'enrol_unasus'), 'enrol_unasus'));
        $this->add_action_buttons(true,get_string('save','enrol_unasus'));
    }

    function validation($data, $files) {
        global $CFG;

         $errors = parent::validation($data, $files);
        
      
     return $errors;
    }

}

?>