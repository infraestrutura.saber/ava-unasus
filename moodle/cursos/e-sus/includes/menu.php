<?php
require_once '../../../config.php';
include '../functions/conectar.php';
require_login();
$USUARIO = &$_SESSION['USER'];
conectar();
include '../functions/functions.php'; //funcoes de conteudo, cadastro de topicos...


$unidade = $_GET ['view'];
$topico = $_GET ['topico'];
$id =$USER->id;
$cpf = $USER->username;
session_start('config');
//verificando autenticacao
if($_SESSION['logado'] != 1){
	header ( 'Location: https://ufpe.unasus.gov.br/moodle_unasus/cursos/e-sus/index.php' );
}else{

$_SESSION['firstname'] = $USER->firstname;
$_SESSION['id'] = $id;
$_SESSION['cpf'] = $cpf;
$_SESSION['topico'] = $topico;
$_SESSION['unidade'] = $unidade;


validar_usuario($cpf,$id);



?>

<div id="menu" class="visible-desktop">
	<ul>
		<li>
			 	<?php
					
					if (end ( explode ( "/", getcwd () ) ) == "apresentacao") {
						
						$nivel = "../";
					} else {
						
						$nivel = "../../../";
					}
					
					// echo '<a href="'.$nivel.'apresentacao/index.php"><img id="icone_home" src="../../../imagens/home.png"> Início</a>';
					echo '<a href="index.php"><img id="icone_home" src="../imagens/home.png"> Início</a>';
					?>

			 </li>
		<li><a>Unidades Didáticas<b class="caret"></b></a>
			<ul>
				<!--NAVEGAÇÃO-->
				<li><a href="index.php?view=menu">Acesso às unidades</b></a>
					<!--UNIDADE 1-->
				
				<li><a href="index.php?view=unidade1&topico=0&resource=13">Unidade 1</b></a>

				</li>
				<!--UNIDADE 2-->
				<li><a href="index.php?view=unidade2&topico=0&resource=23">Unidade 2</b></a>

				</li>
				<!--UNIDADE 3-->
				<li><a href="index.php?view=unidade3&topico=0&resource=28">Unidade 3</b></a>

				</li>
				<!--UNIDADE 4-->


			</ul></li>
		<li><a href="index.php?view=forum">Fórum de discussões<?php alert_postage($id,'desktop');?> </a></li>
		<li><a href="index.php?view=certificacao">Certificação</a></li>
		<li><a href="index.php?view=perguntas">Perguntas frequentes</a></li>
		<li><a href="index.php?view=opiniao">Perfil e opinião</a></li>
		<li><a href="index.php?view=creditos">Créditos</a></li>
	</ul>
	<div id="sair"><?php echo "Olá <b>".$_SESSION['firstname']."</b>! | ";?>Sair</div>
	<div id="fechar">
		<p>
			<?php
			include ('sair.php');
			?>
			<img id="icone_sair" src="../imagens/icn_sair.png"></a>
		</p>
	</div>

</div>


<script type="text/javascript">

							  		function abreModal () {
							  			
							  			 $(".inline").colorbox({inline:true, width:"60%"});
							  		}

							  </script>




<div style='display: none'>
	<div id="modal_creditos" style='padding: 10px; background: #fff;'
		style="font-size: 14"></div>

</div>


<div class="modal" style="display: none" id="modal_res" tabindex="10"
		role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"
				aria-hidden="true">×</button>
			<h3 id="myModalLabel">Acesso negado!</h3>
		</div>
		<div class="modal-body">
			<p>Esse conteúdo está indisponível.</p>
		</div>
		<div class="modal-footer">
			<a href="../views/index.php" class="btn" aria-hidden="true">Voltar ao 
				curso</a>

		</div>
	</div>


<!-- fim -->
<div class="saudacao hidden-desktop">
	Olá, <b><?php echo $USER->firstname; ?></b> !
</div>
<div id="menu_responsivo" class="hidden-desktop">

	


	<img class="icon_menu" src="../imagens/icn_menu_responsivo.svg" />
	<!--<div class="menu_reponsive_container">
  	
</div>-->
</div>

<style type="text/css">
#right {
	width: 48%;
	float: right;
	font-size: 12px !important;
	margin-left: 10px;
}

#left {
	width: 46%;
	float: left; 
	font-size: 12px !important;
}

#modal_creditos b {
	font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans",
		"Helvetica Neue", Arial, sans-serif;
	!
	important;
}
</style>
<script>
				$('.dropdown-toggle').dropdown();
			</script>
<script type="text/javascript">
							$(document).ready(function() {
								$("#menu li a").mouseover(function() {
									var index = $("#menu li a").index(this);
									$("#menu li").eq(index).children("ul").slideDown(100);
									if ($(this).siblings('ul').size() > 0) {
										return false;
									}
								});
								$("#menu li").mouseleave(function() {
									var index = $("#menu li").index(this);
									$("#menu li").eq(index).children("ul").slideUp(100);
								});
							});

		</script>
<?php
}
?>
