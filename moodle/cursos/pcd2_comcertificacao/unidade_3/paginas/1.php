﻿<?php
               
    $pg_atual=1;
     //registro($id,unid3_pg,$pg_atual,unid3_ev);
?>

<style type="text/css">

		#lista li{
			list-style-type: disc;   
       margin-left: 30px;

		}

		.img_3{
			margin-left: 10px;
			
		}
		
		.img_4{
			margin-left: 170px;
			margin-top: 5px;
		}
		
</style> 
<!-- <script src = "http://www.youtube.com/apiplayer"></script> -->
   <h4 class="titulo">O que é enteroclisma?</h4>
  Enteroclisma  é a introdução de 
   uma solução medicamentosa (ou não) no intestino grosso com o objetivo de 
   promover a retirada de resíduos fecais, os quais são fontes de 
   processos intoxicantes do corpo, podendo ser feita através da sonda retal, 
   sendo indicado nas seguintes situações (BRUNI et al 2004; SÃO PAULO, 2010; 
   RIO GRANDE DO NORTE, 2013):

   
<ul id="lista">
<li>preparo de exames gastrointestinais;</li>
<li>cirurgias abdominais gerais;</li>
<li>indicações clínicas:</li>
<ul id="lista">
    <li>eliminar ou evitar a distensão abdominal e flatulência;</li>
<li>facilitar a eliminação de fezes;</li>
<li>remover sangue no caso de melena;</li>
<li>aliviar sinais e sintomas de constipação intestinal.</li></ul>
</ul>
   
<div class="box">
       <img src="../images/img_vocesabiaque_ad.png" >
        <span class="titulo_box">Você sabia que...</span>
        <br>
        <hr/>
Pacientes portadores de doenças 
crônicas, com mobilidade prejudicada e em uso de medicamentos à base de opiáceos,
 inibidores de canais de cálcio ou à base de cálcio, têm mais predisposição para
  impactação das fezes. 
</div>
<br>
A lavagem intestinal pode ser 
executada por meio do enteroclisma, na posição de Sims, e geralmente 
são utilizadas soluções hiperosmolares, como a solução glicerinada ou óleo mineral, 
atraindo água para dentro da luz intestinal, acelerando o trânsito intestinal.
 Neste caso, o volume indicado está entre 500 ml ou mais, enquanto que no enema 
 ou clister utiliza-se cerca de 150 ml ou menos de volume. Logo, é possível supor 
 que o enteroclisma pode promover maior esvaziamento retal (LOPES et al, 2001).
</ul>

<br><br>
<div class="box" style="position:relative;">
<h5>Posição de Sims <i class=" icon-hand-up" title="Essa figura possui interação."></i></h5>
  A posição é ideal para exames e para a realização de medicação retal,
  pois facilita a introdução da sonda retal no trânsito intestinal, auxiliando 
  na absorção dos medicamentos e diminuindo o desconforto intestinal.  
    <br><br>
       
        <img class="img_3" src="paginas/images/posicao_sims.jpg" width="660px" /><br>
        <div class="um_action"></div>
        <div class="dois_action"></div>
        <div class="um"><img src="paginas/images/quadro-perna-direita.png"></div>
        <div class="dois"><img src="paginas/images/quadro-perna-esquerda.png"></div>
        <b>Fonte:</b> (UNA-SUS UFPE, 2014).
</div>

<br>
<div id="player_video" class="box">
      
       <img src="../images/img_saibamais_ad.png" >
        <span class="titulo_box">Saiba mais</span>
        <br>
        <hr/>
        No vídeo a seguir é apresentado o procedimento de enteroclisma. Confira. 
        <br><br>
        <table width="100%" ><td align="center">
		
		<div id="player"></div>
        </iframe>
		</td></table><br><br>
      </div>
<br>


<style type="text/css">
.um_action{
background-color: #009EC8;
position: absolute;
height: 7px;
left: 164px;
top: 271px;
width: 7px;
z-index: 3;
border: 2px solid #fff;
border-radius: 30px 30px 30px 30px;
cursor: pointer;
}
.um{

position: absolute;
left: 172px;
top: 160px;
z-index: 3;
display: none;



}
.dois_action{
background-color: #009EC8;
position: absolute;
height: 7px;
left: 134px;
top: 318px;
width: 7px;
z-index: 3;
border: 2px solid #fff;
border-radius: 30px 30px 30px 30px;
cursor: pointer;
}
.dois{

position: absolute;
left: 143px;
top: 327px;
z-index: 3;
display: none;



}

</style>
<script type="text/javascript">
$('.um_action').click(function(){
  $('.um').fadeIn(1000);
});
$('.dois_action').click(function(){
  $('.dois').fadeIn(1000);
});

//onYouTubePlayerAPIReady 
/* $.post( "../apresentacao/functions/chamaCadVideo.php", { video: 1}, function() {
						
				});
		MPQ6G01SauM
		*/		
// 2. This code loads the IFrame Player API code asynchronously.
      var tag = document.createElement('script');

      tag.src = "https://www.youtube.com/iframe_api";
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

      // 3. This function creates an <iframe> (and YouTube player)
      //    after the API code downloads.
      var player;
      function onYouTubeIframeAPIReady() {
        player = new YT.Player('player', {
          height: '360',
          width: '640',
          videoId: 'Orquw-CzoH0',
          events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
          }
       
        });
      }
      // 4. The API will call this function when the video player is ready.
      function onPlayerReady(event) {
        //event.target.playVideo();
      }

      // 5. The API calls this function when the player's state changes.
      //    The function indicates that when playing a video (state=1),
      //    the player should play for six seconds and then stop.
      var done = false;
      function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.PLAYING && !done) {
       $.post( "../apresentacao/functions/chamaCadVideo.php", { video: 1}, function() {
						
				});
          done = true;
        }
      }
</script>

	<?php

	if ($_GET['video']==1){ 

		echo '<script type="text/javascript">$("html,body").animate({scrollTop: $("#player_video").offset().top}, 1500);</script>';
	}
?>
   
