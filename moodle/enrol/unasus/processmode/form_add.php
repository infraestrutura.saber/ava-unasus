<?php

require_once $CFG->libdir.'/formslib.php';

class form_add extends moodleform {

    function definition() {
    	global $param;
    	global $processmode;
    	 $options_mode=$processmode->get_cofig_html_option();
        $mform    =& $this->_form;
        $renderer =& $mform->defaultRenderer();

        $mform->addElement('header', '', get_string('processmode_config_add','enrol_unasus'), '');
         
         $mform->addElement('hidden', 'contextid', $param->contextid);
        $mform->setType('contextid', PARAM_INT);
        
        $mform->addElement('select', 'processing_mode', get_string('processmode','enrol_unasus'),$options_mode);
        $mform->setType('processing_mode', PARAM_TEXT);
         $mform->setHelpButton('processing_mode', array('processing_mode', get_string('processmode', 'enrol_unasus'), 'enrol_unasus'));
        
     
        $this->add_action_buttons(true,get_string('save','enrol_unasus'));
    }

    function validation($data, $files) {
       $errors = parent::validation($data, $files);
     
     return $errors;
    }

}

?>