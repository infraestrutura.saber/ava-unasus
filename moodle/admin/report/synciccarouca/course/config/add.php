<?php

      require("../../../../../config.php");
   require("$CFG->dirroot/admin/report/synciccarouca/course/config/lib.php");
   require("$CFG->dirroot/admin/report/synciccarouca/course/config/form_add.php");
   require("$CFG->dirroot/admin/report/synciccarouca/course/lib.php");
   require_login();
   
//permissão
    if(!isadmin()) require_capability('report/synciccarouca:configcreate', get_context_instance(CONTEXT_SYSTEM), NULL, false);
    add_to_log(1, 'report_synciccarouca', 'syncoursecreate', "admin/report/synciccarouca/course/config/add.php", getremoteaddr());    
    //Navegação
   $navigation = build_navigation(array(array('name' => get_string('synciccarouca','report_synciccarouca'), 'link' => "$CFG->wwwroot/admin/report/synciccarouca/index.php", 'type' => 'misc'),
                                     array('name' => get_string('manegewscoursesync','report_synciccarouca'), 'link' => "$CFG->wwwroot/admin/report/synciccarouca/course/config/index.php", 'type' => 'misc'), array('name' => get_string('addnewcoursesync','report_synciccarouca'), 'link' => null, 'type' => 'misc')));
    
    
    //form
     $course_sync_config =new course_sync_config();
     $syc_ing_conc=new ingr_conc_sync_couse();
    $form= new form_add();
   
   
    
    //parâmetros
    $param=  new object();
    
     if ($form->is_cancelled()) {
        redirect($CFG->httpswwwroot.'/admin/report/synciccarouca/course/config/index.php') ;

    } else if ($formdata = $form->get_data()) {
    	//$formdata->filekey= $course_sync_config->create_folder_file_upload($formdata->systemname);
    	$course_sync_config->save($formdata);
    	redirect($CFG->httpswwwroot.'/admin/report/synciccarouca/course/config/index.php') ;
     }
     
   
     print_header(get_string('synciccarouca','report_synciccarouca'),get_string('synciccarouca','report_synciccarouca'), $navigation);
   
 	
    $form->display();
  
    print_footer();
?>