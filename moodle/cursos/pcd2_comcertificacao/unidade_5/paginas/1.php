﻿<?php
               
  $pg_atual=1;
  registro($id,unid5_pg,$pg_atual,unid5_ev);
?>

    <style type="text/css">

		#lista li{
			list-style-type: disc;   
       margin-left: 30px;

		}
	
	</style> 

   <h4 class="titulo" style="margin-top: -15px;">Conceito, indicações e finalidades das sondagens</h4><br>
   
Várias evidências científicas comprovam que o estado nutricional interfere diretamente na evolução clínica do paciente. As pessoas que sofrem de doenças crônicas, inanição ou têm sinais de desnutrição estão mais suscetíveis a infecções, necessitam de maior tempo para cicatrização, consomem maior quantidade de medicamentos e apresentam um maior risco de morte. Sendo assim, quando não se pode utilizar a via oral para alimentar os pacientes, a terapia nutricional se torna essencial para a sobrevida do indivíduo (SMELTIZER; BARE, 2005; SOCIEDADE BRASILEIRA DE NUTRIÇÃO PARENTAL E ENTERAL; FEDERAÇÃO BRASILEIRA DE GASTROENTEROLOGIA; ASSOCIAÇÃO BRASILEIRA DE NUTROLOGIA, 2011).
<br/>
   <br>A sondagem enteral é a introdução de uma sonda de silicone ou poliuretano, através da nasofaringe, no sistema digestório. Ela pode possuir um peso na extremidade distal (auxilia no posicionamento pelo peristaltismo), podendo ser inserida até o estômago (90 cm), duodeno (110 cm) ou, ainda, até o jejuno (120 cm). Por meio da sondagem enteral, podem ser administradas dietas industrializadas ou artesanais (KNOBEL, 2006). O procedimento pode ser usado também para:
   
   
<ul id="lista">
<li>promover suporte calórico para evitar catabolismo;</li>
<li>adequar as demandas metabólicas às necessidades clínicas do paciente;</li>
<li>administrar medicamentos, líquidos (hidratação do paciente) e dieta por períodos curtos.</li>
</ul>

Além do suporte nutricional, a sondagem enteral pode ser indicada quando (KNOBEL, 2006):

<ul id="lista">
<li>o trato gastrointestinal estiver parcialmente funcionante;</li>
<li>existirem fístulas do intestino delgado distal ou do cólon;</li>
<li>houver a necessidade de aumentar as demandas nutricionais;</li>
<li>houver anorexia persistente por neoplasias, doenças infecciosas crônicas, 
  depressão, trauma, acidente vascular encefálico, etc.;</li>
<li>o paciente estiver inconsciente ou apresentar traumas faciais;</li>
<li>o paciente apresentar dor ou desconforto em decorrência de pancreatite, quimioterapia 
  e radioterapia;</li>
<li>houver distúrbios relacionados à má absorção, como ocorre na doença de Crohn, 
  que é a inflamação subaguda e crônica que se estende através de todas as camadas 
  da parede intestinal, a partir da mucosa intestinal. A doença caracteriza-se por 
  períodos de exacerbações e remissões.</li>
</ul>

<br>

<div class="box">
        <img src="../images/img_vocesabiaque_ad.png" alt="Você sabia" >
        <span class="titulo_box">Você sabia que...</span>
       
        <hr/>
        A má absorção refere-se à incapacidade do sistema digestivo para absorver
         uma ou mais das principais vitaminas (principalmente a vitamina B 12), 
         minerais e nutrientes. As doenças do intestino delgado constituem a causa 
         mais comum de má absorção e depleção de nutrientes (SMELTIZER; BARE, 2005). </div>

        