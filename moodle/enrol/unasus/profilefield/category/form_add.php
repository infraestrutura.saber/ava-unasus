<?php

require_once $CFG->libdir.'/formslib.php';

class form_add extends moodleform {

    function definition() {
    	global $param;
    	global $profilefield_category;
        $mform    =& $this->_form;
        $renderer =& $mform->defaultRenderer();

        $mform->addElement('header', '', get_string('profilefieldcategory_add','enrol_unasus'), '');
        $mform->addElement('hidden', 'contextid', $param->contextid);
        $mform->setType('contextid', PARAM_INT);
        
       $mform->addElement('text', 'name', get_string('profilefieldcategory_name','enrol_unasus'));
        $mform->setType('name', PARAM_TEXT);
       $mform->addRule('name', get_string('requiredfield','enrol_unasus'), 'required', null, 'cliente');
        $this->add_action_buttons(true,get_string('save','enrol_unasus'));
    }

    function validation($data, $files) {
    	global $profilefield_category;
       $errors = parent::validation($data, $files);
       
       if($profilefield_category->exist_create_name($data['contextid'],$data['name'])){
       		$errors['name'] = get_string('duplication_record_db','enrol_unasus');
       }
     return $errors;
    }

}

?>