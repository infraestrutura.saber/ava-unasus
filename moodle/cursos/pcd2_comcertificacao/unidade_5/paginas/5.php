<?php
               
    $pg_atual=5;
    registro($id,unid5_pg,$pg_atual,unid5_ev);
?>
  <style type="text/css">
    #lista li{
      list-style-type: disc;
       margin-left: 30px;
    }
  
  </style>
  <!-- <script src = "http://www.youtube.com/player_api"></script> -->
   <h4 class="titulo">Cuidados no preparo e administração das dietas</h4>
   <br>
	É importante que todo o processo de preparação e envase da dieta seja realizado em um ambiente higienizado. Portanto, alguns cuidados devem ser observados ou reforçados no que se refere à manipulação e à administração de dietas via enteral (DREYER et al, 2011):
	<ul id="lista">
		<li>o local de preparo das dietas (pias, bancadas) deve ter boa iluminação, ser limpo antes e após o preparo;</li>
		<li>a pessoa responsável pela manipulação da dieta deve considerar os cuidados descritos na figura a seguir.</li>
	</ul>
    <br><br>
    <b>Cuidados na manipulação da dieta</b><i class=" icon-hand-up" title="Essa figura possui interação."></i>
    <div id="background">
 <img class="imagem_base" src="images/fig01-base.jpg" alt="Cuidados na manipulação da dieta" >
 <b>Fonte:</b> (SANTOS; BOTTONI; MORAIS, 2013; ATZINGEN; SILVA, 2007, adaptado).

 <div class="um_action"></div>
 <div class="um"><img src="images/fig02.5.png"></div>
 

  
<div class="dois"><img src="images/fig02.2.png"></div>
<div class="dois_action"></div>

<div class="tres"><img src="images/fig02.1.png"></div>
<div class="tres_action"></div>

<div class="quatro"><img src="images/fig02.3.png"></div>
<div class="quatro_action"></div>

<div class="cinco"><img src="images/fig02.4.png"></div>
<div class="cinco_action"></div>

 </div>

	<br/><br/>
	Podemos classificar as dietas segundo a padronização do preparo da dieta. Nesses casos, temos as dietas que são produzidas em laboratório (industrializadas) ou dietas preparadas no domicílio pelos cuidadores e familiares (dieta artesanal). <br/>
	<br>A dieta industrializada é um produto oferecido pelos fabricantes como uma opção balanceada e pode ser personalizada para cada tipo de necessidade, de acordo com a condição clínica do paciente. Pode ser encontrada na forma líquida (pronta para administrar) ou liofilizada, devendo ser diluída segundo as recomendações do fabricante. <br/>
	<br>A dieta caseira é aquela preparada na casa do paciente, devendo ser batida no liquidificador e coada para não obstruir a sonda de alimentação. Durante o preparo, é importante que os cuidadores sigam as orientações fornecidas pela equipe de saúde.<br/>
  
	<br/>
	
<div id="player_video" class="box">
        <img src="../images/img_saibamais_ad.png" alt="Saiba mais">
        <span class="titulo_box">Saiba mais</span>
        <br>
        <hr/>
       Veja a seguir uma sugestão de passo a passo para a produção da dieta artesanal.<br>
<br>
       <table width="100%" ><td align="center"><div id="player"></div></td></table><br><br>
        Para desinfecção de alimentos, superfícies e objetos inanimados, você pode orientar o paciente e/ou 
        cuidador a utilizar a água sanitária (solução de água + hipoclorito, com concentração 2,0 a 2,5%) 
        como alternativa ao hipoclorito. <br><br>
        De acordo com a Agência Nacional de Vigilância Sanitária  - ANVISA  (2008):
        <ul id="lista">
          <li>para a desinfecção de água para consumo humano, adicione 2 gotas (0,1 mL) de água sanitária por litro de água, misture bem e aguarde 30 minutos antes de utilizar;</li>
          <li>para desinfecção de caixas d’água, adicione 1 litro de água sanitária para cada 1000 litros de água, aguarde uma hora, drene toda a água do reservatório e efetue pelo menos um enxágue antes de utilizar;</li>
          <li>para desinfecção de hortifrutícolas, adicione 1 colher de sopa (0,8 mL) de água sanitária para cada litro de água, faça a imersão dos hortifrutícolas, aguarde 30 minutos e enxágue em água corrente potável;</li>
          <li>o tempo de contato que deve constar no modo de uso do rótulo do produto para desinfecção de ambientes e superfícies inanimadas é de no mínimo 10 minutos.</li>
        </ul>
</div>

	<br/><br/>
<div class="box">
        <img src="../images/img_atencao_ad.png" alt="Atenção" >
        <span class="titulo_box">Atenção!</span>
        <br>
        <hr/>
       <p>A dieta nunca deve ser aquecida nem congelada após o seu preparo.
          Ao final do dia, o resto da dieta preparada deve ser jogado fora. 
          Nunca reaproveitá-la

(BENTO; JORDÃO JÚNIOR; GARCIA, 2011).</p><br/>
</div>

<style type="text/css">
/* animacao ponto ---------------------------------*/
.imagem_base{
  position: relative;
}
.um{
	
    position: absolute;
     display: none;
   left: -2px;
    position: absolute;
    top: 68px;
    z-index: 2;
}
.um_2{
    
    position: absolute;
     
   left: 4px;
   
    top: 198px;
    z-index: 2;
    display: none;
}
.um_action{
   background-color: #0F8A44;
position: absolute;
height: 7px;
left: 273px;
  
    top: 237px;
width: 7px;
z-index: 3;
border: 2px solid #fff;
border-radius: 30px 30px 30px 30px;
cursor: pointer;
}
.dois_action{
background-color: #0F8A44;
position: absolute;
height: 7px;

left: 287px;
   
    top: 288px;
width: 7px;
z-index: 2;
border: 2px solid #fff;
border-radius: 30px 30px 30px 30px;
cursor: pointer;
}

.dois{
    
   position: absolute;
left: 32px;
    display: none;
    top: 240px;
    z-index: 2;
   
}
.tres_action{
background-color: #0F8A44;
    border: 2px solid #FFFFFF;
    border-radius: 30px 30px 30px 30px;
    cursor: pointer;
    height: 7px;
    left: 386px;
    position: absolute;
    top: 353px;
    width: 7px;
    z-index: 2;
}

.tres{
    
 display: none;
    left: 397px;
    position: absolute;
    top: 180px;
    z-index: 2;

   
}
.quatro_action{
background-color: #0F8A44;
    border: 2px solid #FFFFFF;
    border-radius: 30px 30px 30px 30px;
    cursor: pointer;
    height: 7px;
    left: 423px;
    position: absolute;
    top: 354px;
    width: 7px;
    z-index: 2;
  
}

.quatro{
    
display: none;
    left: 433px;
    position: absolute;
    top: 305px;
    z-index: 2;

   
}
.cinco_action{
background-color: #0F8A44;
position: absolute;
height: 7px;
left: 400px;
   
    top: 63px;
width: 7px;
z-index: 2;
border: 2px solid #fff;
border-radius: 30px 30px 30px 30px;
cursor: pointer;
}

.cinco{
    
  left: 421px;
    position: absolute;
    top: 71px;
z-index: 2;
display: none;


  }
  #background{
    position: relative;
  }
   




</style>
<script type="text/javascript">
$(".um_action").click(function(){
   
    $(".um").fadeIn(200);
    $(".um_2").fadeIn(500);
    $(".um_2").animate({"top":"320px"},1600);
   
});
$(".dois_action").click(function(){
   
    $(".dois").fadeIn(300);
    
   
});
$(".tres_action").click(function(){
    
    $(".tres").fadeIn(300);
});
$(".quatro_action").click(function(){
    
    $(".quatro").fadeIn(300);
    
});
$(".cinco_action").click(function(){
    
    $(".cinco").fadeIn(300);
    
   
});

function sumir(){
    $(".um").fadeOut(100);
     $(".um_2").fadeOut(100);
    $(".dois").fadeOut(100);
}


//onYouTubePlayerAPIReady
/*
 * QDpOcqYPwWc
 * $.post( "../apresentacao/functions/chamaCadVideo.php", { video: 7}, function() {
						
						});
 */
// 2. This code loads the IFrame Player API code asynchronously.
      var tag = document.createElement('script');

      tag.src = "https://www.youtube.com/iframe_api";
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

      // 3. This function creates an <iframe> (and YouTube player)
      //    after the API code downloads.
      var player;
      function onYouTubeIframeAPIReady() {
        player = new YT.Player('player', {
          height: '250',
          width: '400',
          videoId: 'OQPs6JqHm74',
          events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
          }
       
        });
      }
      // 4. The API will call this function when the video player is ready.
      function onPlayerReady(event) {
        //event.target.playVideo();
      }

      // 5. The API calls this function when the player's state changes.
      //    The function indicates that when playing a video (state=1),
      //    the player should play for six seconds and then stop.
      var done = false;
      function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.PLAYING && !done) {
   $.post( "../apresentacao/functions/chamaCadVideo.php", { video: 7}, function() {
						
						});
          done = true;
        }
      }



</script>

	<?php

	if ($_GET['video']==1){ 

		echo '<script type="text/javascript">$("html,body").animate({scrollTop: $("#player_video").offset().top}, 1500);</script>';
	}
	
?>

   
