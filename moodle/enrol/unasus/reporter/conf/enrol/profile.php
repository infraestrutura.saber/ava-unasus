<?php
  require("../../../../../config.php");
   require("$CFG->dirroot/enrol/unasus/lib/userprofile.php");
   require("$CFG->dirroot/enrol/unasus/reporter/conf/enrol/profile_form.php");
   require("$CFG->dirroot/enrol/unasus/profilefield/lib.php");
   require("$CFG->dirroot/enrol/unasus/lib/configcontext.php");
   require("$CFG->dirroot/enrol/unasus/lib/context.php");
 
require_login();
   //permiss�o
    require_capability('enrol/unasus:configreporterenrol', get_context_instance(CONTEXT_SYSTEM), NULL, false);
   
    
    $param=  new object();
     
     $param->contextid= required_param('contextid',PARAM_INT);
     $context =new context();
     $context_name=$context->get_name($param->contextid);   
     
      add_to_log(1, 'enrol_unasus', 'configreporterenrol', "enrol/unasus/reporter/conf/enrol/profile.php?contextid=$param->contextid", getremoteaddr());    
    //Navega��o
    //form
   $profilefield_field = new profilefield_field(); 
   $configcontext =new configcontext();
    $profile =new enrol_unasus_userprofile();
    $form= new enrol_unasus_profile_enrol_form();
   
   
    
    if ($form->is_cancelled()) {
        redirect($CFG->httpswwwroot.'/enrol/unasus/reporter/conf/enrol/index.php') ;

    } else if ($formdata = $form->get_data()) {
    	$fields= $profilefield_field->get_by_contextid($param->contextid);
    	$seleted_fields_filter=$profile->get_selected_profile_field($fields,"F_");
    	$seleted_fields_reporter=$profile->get_selected_profile_field($fields,"R_");
    	
    	$dtof=  new object();
    	$dtof->contextid=$param->contextid;
    	$dtof->name=configcontext::$USER_PROFILE_ENROL_FILTER;
    	$dtof->value=$seleted_fields_filter;
    	if(!empty($dtof->value))$configcontext->update($dtof);
    	
    	$dtor=  new object();
    	$dtor->contextid=$param->contextid;
    	$dtor->name=configcontext::$USER_PROFILE_ENROL_REPORTER;
    	$dtor->value=$seleted_fields_reporter;
    	if(!empty($dtor->value))$configcontext->update($dtor);
    	
    	redirect($CFG->httpswwwroot.'/enrol/unasus/reporter/conf/enrol/index.php') ;
     }
     
    //Navega��o
   $navigation = build_navigation(array(array('name' => get_string('enrolname','enrol_unasus'), 'link' => "$CFG->wwwroot/enrol/unasus/index.php", 'type' => 'misc'),
                                     array('name' => get_string('config_reporter_profile_enrol_conf','enrol_unasus'), 'link' => "$CFG->wwwroot/enrol/unasus/reporter/conf/enrol/index.php", 'type' => 'misc'), array('name' => $context_name, 'link' => null, 'type' => 'misc'), array('name' => get_string('cron_config_add','enrol_unasus'), 'link' => null, 'type' => 'misc')));

 
     print_header(get_string('enrolname','enrol_unasus'),get_string('applicationsenrolment','enrol_unasusem'), $navigation);
   
 	
    $form->display();
  
    print_footer();
?>