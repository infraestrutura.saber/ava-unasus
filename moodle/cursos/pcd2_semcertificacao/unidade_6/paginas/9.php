<?php
               
    $pg_atual=9;
     registro($id,unid6_pg,$pg_atual,unid6_ev);
?>

<style type="text/css">
		#lista{
            list-style-type: disc;  
            margin-left: 30px;
        }
		
		table tr td{
			
			padding-left: 10px;
		}
		.txt_localizacao{
            	left: 490px;
				position: absolute;
				top: 56px;
				z-index: 20;
				width: 250px;
				text-align: center;
            }
</style>
<h4 class="titulo">Estoma de Eliminação</h4>
   <h5 >Cistostomia</h5><br/>
    
   
  

</span>
  <ul style="width: 443px; float:left;">
  A cistostomia é uma derivação vesical suprapúbica, na qual, cirurgicamente, é colocado, no interior da 
  bexiga, um cateter, criando um trajeto alternativo para a saída da urina. Dependendo ou não do uso de cateteres, 
  a derivação vesical pode ser dividida em entubadas (cistostomia) e não entubadas (vesicostomia). 
  A cistostomia é indicada em diversas situações clínicas, como (COLOGNA, 2011):<br/><br/>
<li id="lista">obstrução do colo vesical;</li>
<li id="lista">trauma vesical;</li>
<li id="lista">trauma uretral;</li>
<li id="lista">pós-uretroplastia;</li>
<li id="lista">pós-cistoplastias.</li>
</ul>

	<div class="txt_localizacao"><b>Derivação urinária a partir da bexiga</b><br>
<img  src="images/Fig10_cistostomia.jpg" alt="Cistostomia"><br>
<b>Fonte:</b> (UNA-SUS UFPE, 2014).
</div>
	
	
	
	 <span style="clear:both">&#160</span>
<div style="width: 310px;float:left; clear:both; margin-top: 58px;">

	<div id="hide1" style="float:left;">
<table style="background-color:#FFE4CA" class="table table-bordered " style="width: 310px; margin-left: 20px; float:left;">


	
	<tr bgcolor="#ccc">
		<td><b>Contraindicações da Cistostomia</b></td>
	
	<tr>
		<tr>
		<td>Tumores malignos da bexiga</td>
	
	<tr>
	
	<tr >
		<td>Acentuada redução da capacidade vesical</td>
	
	<tr>
	
	<tr  >
		<td>Pacientes submetidos à radioterapia</td>
	
	<tr>
	
	<tr>
		<td>Pacientes submetidos à cirurgias pélvicas</td>
	
	<tr>
	

</table>	
<span style="margin-left: 0px;float:left;"><b>Fonte:</b> (COLOGNA, 2011, adaptado).</span></div></div>

<div style="width: 384px; float:right; margin-right: 21px;margin-top: 58px;">

	<div id="hide2" style="float:right;">
<table style="background-color:#FFE4CA" class="table table-bordered " style="width: 300px; margin-left: 20px; float:right;">
<tr bgcolor="#ccc">
		<td><b>Complicações da Cistostomia</b></td>
	
	</tr>

	<tr >
		<td>Infecção no local da punção</td>
	
	</tr>
	
	<tr >
		<td>Infecção urinária</td>
	
	</tr>
	
	<tr  >
		<td>Perda de urina ao redor do cateter</td>
	
	</tr>
	
	<tr >
		<td>Obstrução ou deslocamento do cateter</td>
	
	</tr>
	
	<tr >
		<td>Perfuração do peritônio e/ou alça intestinal</td>
	
	</tr>
	
	<tr >
		<td>Incrustrações calcárias ao redor do cateter</td>
	
	</tr>
	
	<tr >
		<td>Perfuração da parede posterior da bexiga</td>
	
	</tr>
	
	<tr >
		<td>Extravazamento de urina no tecido peri-vesical e/ou subcutâneo</td>
	
	</tr>

</table>	
<span style="margin-left: 0px;float:left;"><b>Fonte:</b> (COLOGNA, 2011, adaptado).</span></div></div>
<div style="clear: both; height: 20px; display: block"></div>

	
 
  <h5>Cuidados com cistostomia</h5>
  Os cuidados com a cistostomia não diferem dos das estomas intestinais, entretanto é importante redobrá-los, pois a urina é um efluente estéril, e qualquer contaminação pode levar a infecções sistêmicas mais sérias (SILVA; HUGA-TANIGUCHI, 2010; SMELTZER; BARE, 2005). Sendo assim, fique atento(a) aos seguintes cuidados:</br>
 <ul>
<li id="lista">é muito importante manter limpa e seca a pele em torno da área do estoma;</li>
<li id="lista">inspecionar a pele ao redor do estoma, os sinais de irritação e sangramento na mucosa;</li>
<li id="lista">lavar as mãos antes e depois de qualquer cuidado com o cateter, curativo e bolsa de drenagem, 
a fim de evitar infecções;</li>
<li id="lista">orientar o paciente e o cuidador a não dobrar o cateter nem deitar-se sobre ele, pois isso obstrui 
a livre passagem da urina que estiver sendo eliminada pela bexiga; </li>
<li id="lista">não elevar a bolsa coletora para não haver retorno da diurese na bexiga. Orientar o cuidador que, 
em caso de decúbito dorsal, a bolsa coletora deve ser posicionada em um  nível abaixo do paciente;</li>
<li id="lista">esvaziar a bolsa coletora quando estiver cheia, porque o peso da urina fará com que a bolsa se 
separe da pele, caso ela encha demais;</li>
<li id="lista">o curativo deve ser trocado diariamente ou com maior frequência, caso fique sujo ou se solte. Orientar o cuidador a comunicar para a equipe de atenção domiciliar qualquer alteração, como: coloração, volume de drenagem ou presença de grumos.</li>
 </ul>
<br><br>
<!--<a href="javascript:history.go(-1)" class="btn">Voltar</a>-->
<script>
	
		
		function esconder(id){
		
			if ($("#hide" + id).is(":hidden")) {
				$("#hide" + id).slideDown("");
				
			} else{
			$('#hide' + id).hide("");
			}
	
		}
 
	</script>

   
