
function frmDownloadSubmit(frm) {
    var licence = '';
    if (document.getElementById('licence-0').checked == true) {
        licence = 'non-commercial';
    }  else if (document.getElementById('licence-1').checked == true) {
        licence = 'commercial';
    }  
    if (licence == '') {    
        alert ('Please select your intended use');
        return false;   
    }
    else if (licence == 'commercial') {
        alert ('Remember to come back and pay using the Paypal button above');
    }
    return true;
}
function pre(obj) {
    document.getElementById('debug').innerHTML += obj +', ';
}

var print_r_path = Array(); // avoids recursion
var print_r_depth = 5;
function print_r(obj, depth) {
	var s = print_r_level(obj, 0);
	print_r_depth = depth;
	var debug = document.getElementById('debug');
	if (!debug) debug = document.body.appendChild();
	debug.innerHTML += '<pre style="border: 1px solid black; padding: 2px; background-color: white">'+ s +'</pre>';
}


function print_r_level(obj, level) {
	
	print_r_path[level] = obj;
	var s = '';
	var tab = '\t';
	var nl = '\n';
	var indent = '';
	for (i = 0; i < level; i++) {
		indent += tab;
	}
	if (typeof obj == 'string' && obj.indexOf('\n') > -1) {
		return '['+ typeof obj +'] :<br/><div style="border: 1px dotted black; padding: 2px">'
			+ obj.replace(/</g, '&lt;').replace(/>/g, '&gt;') +'</div>'+ nl;
				
	} else if (typeof obj == 'string' || typeof obj == 'number') {	
		return '['+ typeof obj +'] '+ obj + nl;

	} else if (typeof obj == 'function') {	
		return '[function]'+ nl;
	
	} else if (typeof obj == 'object' && level >= print_r_depth) {	
		return '[object: max depth reached]'+ nl;
	
	} else if (typeof obj == 'object') {
		try {
			for (var x in obj) {
				var rec = false;
				for (i = level - 1; i >= 0; i--) {
					if (obj[x] && print_r_path[i] == obj[x]) {
						s += indent + tab + x +': [Recursion]' + nl;
						rec = true;
						break;
					}
							
				}
		
				if (!rec) {
					if (typeof obj[x] == 'object') {
						if (obj[x] && obj[x].nodeName) { 
							s += indent + tab + x +': [DOM element]'+ nl;
						} else {
							s += indent + tab + x +': '+ print_r_level(obj[x], level + 1);
						}
					} else if (typeof obj[x] == 'function') {
						s += indent + tab + x +': [function]'+ nl;
					} else if (typeof obj[x] == 'string' && (obj[x].indexOf('\n') > -1 || obj[x].indexOf('<') > -1)) {
						s += indent + tab + x +':<br/><div style="border: 1px dotted black; padding: 2px">'
							+ obj[x].replace(/</g, '&lt;').replace(/>/g, '&gt;') +'</div>'+ nl;
					} else  {
						s += indent + tab + x +': '+ obj[x] + nl;
					}
				}
			}
		} catch (e) {
			return '(Exeption: '+ e.message +')'+ nl;
			
		}
	}
	if (s == '') return '['+ typeof obj +'] null' + nl;
	else return '['+ typeof obj +']' + nl + indent +'{'+ nl + s + indent +'}'+ nl;
}

var tick = new Date().getTime();
function debugMark (funcName) {
	var now = new Date().getTime();
	 
	if (document.getElementById('debug')) {		
		if (now - tick > 100) document.getElementById('debug').innerHTML += '<hr/>';
		document.getElementById('debug').innerHTML += (now - tick) +': '+ funcName +'<br/>';
	}
	tick = now;
}

var debug = {
	write: function(s) {
        if (!document.getElementById('debug')) {
            var debug = document.createElement("div");
            debug.id = "debug";
            document.body.appendChild(debug);
        }
		document.getElementById('debug').innerHTML += '<pre style="border: 1px solid black;padding:2px;background:white; margin: 1px 0">'+ s +'</pre>';
	}
}

// create a shorthand function so we don't need to put all this in the opener's onclick
function openYouTube(opener) {
	var returnValue;
	
	// Safari Mobile doesn't have Flash, so we just let the device use the built-in 
	// YouTube viewer.
	if (/(iPhone|iPod|iPad)/.test(navigator.userAgent)) returnValue = true;

	else returnValue = hs.htmlExpand(opener, { 
		objectType: 'swf', 
		objectWidth: 480, 
		objectHeight: 385, 
		transitions: ['fade'],
		width: 480, 
		outlineType: 'rounded-white',
		outlineWhileAnimating: true,
		allowSizeReduction: false,
		// always use this with flash, else the movie will be stopped on close:
		preserveContent: false,
		wrapperClassName: 'draggable-header no-footer',
		swfOptions: { 
			params: { 
				allowfullscreen: 'true' 
			}
		}, 
		maincontentText: 'You need to upgrade your Flash player' 
	});
	
	return returnValue;
}  
