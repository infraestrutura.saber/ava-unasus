
<div style='display:none'>
			<div id="content_color1" style='padding:10px; background:#fff;'>

			<div id="texto_color"></div>
			<br><br>
			<a class="btn inline" href="index.php?pagina=0" onClick="apagar_confirmar();" >Sim, desejo recomeçar os estudos.</a>
			<a class="btn" href="index.php?pagina=0">Não, quero continuar.</a>
				
			<br><br>
			
			</div>
</div>

<style type="text/css">
        #lista{
            list-style-type: disc; 
             margin-left: 30px;
        }
    </style>
        <p>
            <h4 class='titulo'>Apresentação da Unidade</h4>
        
        </p>
		A eliminação de resíduos pela urina é uma necessidade básica que depende
         do correto funcionamento do sistema urinário, que acontece de forma 
         sincronizada. Por vezes, este sincronismo encontra-se prejudicado, 
         fazendo com que o paciente domiciliado necessite do procedimento de 
         sondagem vesical. O acompanhamento do débito urinário é de responsabilidade 
         da equipe de enfermagem. <br><br>
         Nesta unidade, conheceremos as indicações e as finalidades da sondagem vesical, 
        os tipos de sonda, os cuidados necessários no manuseio dos equipamentos 
        utilizados na execução da sondagem vesical e as responsabilidades da equipe 
        de enfermagem na execução do procedimento.<br>
        
		<br>
       
        <b>Objetivos educacionais da unidade </b>
           <li id="lista">Listar as indicações e finalidades da sondagem vesical, os tipos de sonda vesical e as responsabilidades da equipe na execução do procedimento; </li>
           <li id="lista">Relacionar os cuidados necessários no manuseio dos equipamentos necessários à execução do procedimento de sondagem vesical.</li>
           
           <br><br>
          


        
        <br>
        <img src="images/abertura.png" alt="Imagem de abertura Unidade 4">
		
		<script type="text/javascript">
			
		function apagar(){
			$("#texto_color").html("<h4>Confirmação</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, você tem certeza que deseja recomeçar os estudos desta unidade? Todos os seus registros serão zerados.");
            $(".inline").colorbox({inline:true, width:"50%",onClosed:function(){}});
		}
		
		function apagar_confirmar(){
			
			$.post( "../apresentacao/functions/apagar_tudo.php", { unidade: 4 }, function() {
						location.reload();
				});
			
		
		}
		
		</script>
