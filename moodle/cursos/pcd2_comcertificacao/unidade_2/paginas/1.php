<?php
               
    $pg_atual=1;
     registro($id,unid2_pg,$pg_atual,unid2_ev);
?>
    
   <div style='display:none'>
			<div id="content_color" style='padding:10px; background:#fff;'>

			<div id="texto_color"></div>
			<br><br>
			<a class="btn inline" href="index.php?pagina=0" onClick="apagar_confirmar();" >Sim, desejo recomeçar os estudos.</a>
			<a class="btn" href="index.php?pagina=0">Não, quero continuar.</a>
				
			<br><br>
			
			</div>
			</div>

   <h4 class="titulo">Ambiente domiciliar: o que observar?</h4>
   Durante o reconhecimento do espaço físico, onde se busca encontrar 
    o mínimo de condições para a realização dos procedimentos técnicos, 
    é importante que a equipe de saúde atente para a avaliação do ambiente 
    domiciliar como sendo parte integrante do processo do cuidado, que 
    busca como resposta a melhoria da saúde do paciente.
    <br><br>Em outras palavras, o domicílio, além de ser o local de vivência/moradia 
    do paciente, deve ser entendido como um espaço que pode propiciar 
    saúde e atenção adequadas. A visita da equipe multiprofissional 
    proporciona o acesso a informações detalhadas sobre o ambiente 
    domiciliar e o cotidiano do paciente. Todas as informações são 
    importantes para determinar um plano de cuidados, oferecendo, assim, 
    uma boa qualidade de assistência dentro do real contexto no qual o 
    paciente se insere. Alguns aspectos importantes a serem observados 
    na avaliação do ambiente domiciliar são apresentados na figura a seguir.
<br><br>
<br>
<b>Aspectos importantes na avaliação do ambiente domiciliar</b> <i class=" icon-hand-up" title="Essa figura possui interação."></i>
<br><br>
<table class="table table-bordered"  style="background-color:#EFE5D8" width="700px">
<tr >
  <td rowspan="5" width="300" style="background-color:#785F41"><img style="margin-top: 48px; width: 300px !important" src="images/Fig-base.jpg" class="imagem" alt="Ambiente domiciliar"></td>
</tr>
<tr>
    <td  class="box_table um">Disponibilidade de rede elétrica e  iluminação adequada
</td>
    <td >Garante o funcionamento de alguns equipamentos e traz  a segurança  de uma boa visibilidade na realização do(s) procedimento(s).
</td>
</tr>
<tr>
<td class="box_table dois">Boa ventilação
</td>
<td> Ambientes arejados ajudam a prevenir doenças.
</td>
</tr>
<tr>
<td class="box_table tres">Disposição <br>das mobílias
</td>
<td> Disposição adequada das mobílias podem evitar acidentes e quedas, além de 
    facilitar o uso de determinados equipamentos, como o de oxigenoterapia.

</td>
</tr>
<tr>
<td class="box_table quatro">Higienização <br>do ambiente
</td>
<td> Ajuda a prevenir contaminações.

</td>
</tr>
</table>
<b>Fonte:</b> (OLIVEIRA, 2010, adaptado).
   <br><br>
Caso seja preciso realizar mudanças e adaptações para adequar o ambiente domiciliar (instalação de corrimãos e barras no(s) banheiro(s), retirada de tapetes, construção 
de rampas como alternativa às escadas, dentre outros), é necessário sempre realizá-las
em parceria com os familiares e/ou cuidador, respeitando a dinâmica familiar e de acordo 
com as necessidades do paciente. Lembre-se: quanto maior for o conhecimento sobre o ambiente, maior será a
 compreensão sobre os elementos facilitadores e dificultadores do trabalho da 
 equipe no domicílio (SANTOS, 2010).
<br><br>
 <div class="box">
        <img src="../images/img_atencao_ad.png" alt="Atenção" >
        <span class="titulo_box">Atenção!</span>
        <br>
        <hr/>
           
            Para a realização do cuidado no domicílio pela equipe multiprofissional,
             é importante que as condições ambientais e os aspectos socioculturais
              do paciente e da família sejam considerados.
<br>
</div>
<br><br>
Por fim, lembre-se de que as condutas referentes ao tratamento do paciente 
devem possuir as melhores evidências científicas e de que os conhecimentos 
devem ser compartilhados em linguagem acessível com o paciente, os familiares 
e/ou cuidador, sendo consideradas as peculiaridades do ambiente domiciliar 
(IZZO, 2010; BRASIL, 2013).
<br><br>
<div class="box">
        <img src="../images/img_atencao_ad.png" alt="Atenção" >
        <span class="titulo_box">Atenção!</span>
        <br>
        <hr/>
           
            A proposta da assistência prestada no domicílio não deve ser somente 
            a de tratar a doença, mas, sim, a de promover a saúde favorecendo o 
            desempenho nas Atividades da Vida Diária (AVDs), reinserindo, assim, o 
            indivíduo na sociedade.
<br>
</div>

   <style type="text/css">
   td {
  padding-top: 5px;
  padding-bottom: 5px;
  word-break: normal;
}
   .box_table{
    
    background-color: #C1976A;
  
    padding: 3px;

    text-align: center;
    height: 50px;
    cursor: pointer;
   }
   .box_table:hover{
    background-color: #785F41;
   } 
   .interacao{
    width: 400px;
   }
   
   </style>
   <script type="text/javascript">
   $(".um").mouseover(function(){
      $(".imagem").attr("src","images/fig1.2.jpg");
   });
   $(".um").mouseleave(function(){
      $(".imagem").attr("src","images/Fig-base.jpg");
   });  
//-----------------------
   $(".dois").mouseover(function(){
      $(".imagem").attr("src","images/fig1.3.jpg");
   });
   $(".dois").mouseleave(function(){
      $(".imagem").attr("src","images/Fig-base.jpg");
   });  
//-----------------------
   $(".tres").mouseover(function(){
      $(".imagem").attr("src","images/fig1.4.jpg");
   });
   $(".tres").mouseleave(function(){
      $(".imagem").attr("src","images/Fig-base.jpg");
   });  
//-----------------------
   $(".quatro").mouseover(function(){
      $(".imagem").attr("src","images/fig1.5.jpg");
   });
   $(".quatro").mouseleave(function(){
      $(".imagem").attr("src","images/Fig-base.jpg");
   });  

    

   </script>
