<?php
      require("../../../../../config.php");
    require("$CFG->dirroot/admin/report/synciccarouca/course/config/lib.php");
   require_login();
   $param=  new object();
     $param->id= required_param('id',PARAM_INT);

if(!isadmin()) require_capability('report/synciccarouca:configdelete', get_context_instance(CONTEXT_SYSTEM), NULL, false);
    add_to_log(1, 'report_synciccarouca', 'configdeleteconfirmation', "admin/report/synciccarouca/course/config/delete.php?id=".$param->id, getremoteaddr());    
    //Navegação
   $navigation = build_navigation(array(array('name' => get_string('synciccarouca','report_synciccarouca'), 'link' => "$CFG->wwwroot/admin/report/synciccarouca/index.php", 'type' => 'misc'),
                                     array('name' => get_string('manegewscoursesync','report_synciccarouca'), 'link' => "$CFG->wwwroot/admin/report/synciccarouca/course/config/index.php", 'type' => 'misc'), array('name' => get_string('deletecoursesync','report_synciccarouca'), 'link' => null, 'type' => 'misc')));

  
    
    //form
   $course_sync_config =new course_sync_config();
   
  
       
    $dto=$course_sync_config->get_by_id($param->id);
    
    
   
    //parâmetros
   $param->op= optional_param('op',0,PARAM_INT);
    
     print_header(get_string('synciccarouca','report_synciccarouca'),get_string('synciccarouca','report_synciccaroucaem'), $navigation);
   
 	
    get_table_view();
  
    print_footer();
    
    function get_table_view(){
   		global $dto,$course_sync_config;
   		$course_name=$course_sync_config->get_course_name($dto->mdlcourseid);
                 $txt_yesnot=$course_sync_config->get_name_option_yes_not($dto->concluinteferta);
   	 	$output = "<center><table class=\"generaltable\"   border=\"1\"   width=\"95%\" cellspacing=\"1\" cellpadding=\"5\">";
    	$output .= "<tr>";
    	$output .= "<th align=\"center\" COLSPAN=2><h3><b>".get_string('deletecoursesync','report_synciccarouca')."</h3></th>";
     	$output .= "</tr>";
     	
     	$output .= "<tr>";
     	$output .= "<td align=\"left\" width=\"25%\"><b>".get_string('id','report_synciccarouca')."</b></td>";
     	$output .=  "<td>".$dto->id."</td>"; 
     	$output .= "</tr>";
     	
     	$output .= "<tr>";
		$output .= "<td align=\"left\" width=\"25%\"><b>".get_string('mdlcoursesync','report_synciccarouca')."</b></td>";
		$output .=  "<td>".$course_name."</td>"; 
		$output .= "</tr>";
		
		$output .= "<tr>";
		$output .= "<td align=\"left\" width=\"25%\"><b>".get_string('aroucacoursesync','report_synciccarouca')."</b></td>";
		$output .=  "<td>".$dto->arcidcurso."</td>";
		$output .= "</tr>";
		
		$output .= "<tr>";
		$output .= "<td align=\"left\" width=\"25%\"><b>".get_string('aroucaofertaync','report_synciccarouca')."</b></td>";
		$output .=  "<td>".$dto->arcidoferta."</td>";
		$output .= "</tr>";
		
                $output .= "<tr>";
		$output .= "<td align=\"left\" width=\"25%\"><b>".get_string('coursesync_aroucaconcluinteoferta','report_synciccarouca')."</b></td>";
		$output .=  "<td>".$txt_yesnot."</td>";
		$output .= "</tr>";
		
                
		$output .= "<tr>";
		$output .= "<td align=\"left\" width=\"25%\"><b>".get_string('approvedgrade','report_synciccarouca')."</b></td>";
		$output .=  "<td>".$dto->gradeapproved."</td>";
		$output .= "</tr>";
		
		$output .= "<tr>";
		$output .= "<td align=\"left\" width=\"25%\"><b>".get_string('sendmailcoursesync','report_synciccarouca')."</b></td>"; 
		$output .=  "<td>".$course_sync_config->certificate_send_mail_text_format($dto)."</td>";
		$output .= "</tr>";
		
		$output .= "<tr>";
		$output .= "<td align=\"center\"  COLSPAN=2>  <input type=button onClick=\"location.href='delete.php?id=".$dto->id."'\" value='".get_string('confirm','report_synciccarouca')."'> &nbsp;&nbsp;<input type=button onClick=\"location.href='index.php'\" value='".get_string('cancel','report_synciccarouca')."'> </td>";
		
		$output .= "</tr>";			
    	$output .= "</table></center>";
    	echo $output;
   }
?>
