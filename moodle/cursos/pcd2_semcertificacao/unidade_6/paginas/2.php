<?php
               
    $pg_atual=2;
    registro($id,unid6_pg,$pg_atual,unid6_ev);
?>
    <style type="text/css">
	
		#caixa1{
			border: 1px solid #F68C23;
			width: 705px;
			margin-left: 10px;
			background-color: #FFE4CA;
			border-radius: 10px;
			height: 430px;
		
		}
		
		#caixa1 h4{
			margin-left: 100px;
			color:#F78D25;
		
		}
		
		#linha1, #linha2, #linha3{
			height: 272px;
			margin-top: 25px;
			float: left;
			width: 230px;
			
		}
		
		#linha1 p, #linha2 p, #linha3 p{
			float: left;
			margin-left: 60px;
			width: 122px;
					
		}
		
		#linha1 p a, #linha2 p a, #linha3 p a{
			color: rgb(201, 80, 0);
			text-align: center;
			margin-left: 28px;
			font-weight: bold;
					
		}
		
		#linha1 img, #linha2 img, #linha3 img{
			float: right;
			margin-right: 19px;
			border: 2px solid #F68C23;
			width: 70%;
		}
		
		#title_est{
			padding: 10px;
			background-color: #F78D25;
			text-align: center;
			color: white;	
		}
		/* new css----------------------*/
		 .table td {
                border-top: 1px solid #ffe4ca;
                line-height: 20px;
                padding: 8px;
                text-align: center;
                vertical-align: middle;
            }
            .titulos{
            	background-color: #F68C23;
            	color: #fff;
            	padding: 7px;
            	border-radius: 4px;
            }

            .box2{
	background-color: #ffe4ca;
	padding: 10px;
	border-radius: 10px;
	border: 1px solid #F68C23;

}
.borda{
	border: 1px solid #F68C23;
}
         
		
	</style>
 

   <h4 class="titulo">Tipos de estomas quanto à origem
</h4><br/>
  <div class="box2">
  <table class="table">
  	<tr>
  		<td><div class="titulos"><b>Estoma de respiração</b></div></td>
  		<td><div class="titulos"><b>Estomas de alimentação</b></div></td>
  		<td><div class="titulos"><b>Estomas de eliminação</b></div></td>
  	</tr>	
  	<tr>
  		<td><img class="borda"  src="images/Fig01.2_ap_respiratorio.jpg" alt="Estoma de respiração"></td>
  		<td><img  class="borda" src="images/Fig01.1_ap_digestivo.jpg" alt="Estoma de alimentação"></td>
  		<td><img class="borda" src="images/Fig01.2_ap_urinario.jpg" alt="Estoma de eliminação" ></img></td>
  	</tr>	
  	<tr>
  		<td><a href="index.php?pagina=3">Traqueostomia</a></td>
  		<td>
  			<a href="index.php?pagina=4">Gastrostomia<br></a>
  			<a href="index.php?pagina=5">Jejunostomia <br></a>

  		</td>
  		<td><a href="index.php?pagina=8">Urostomia</a><br><a href="index.php?pagina=9">Cistostomia</a><br><a href="index.php?pagina=6">Colostomia</a><br><a href="index.php?pagina=7">Ileostomia</a></td>
  	</tr>	
  </table>
</div>

  <br><br>
<div class="box">
        <img src="../images/img_vocesabiaque_ad.png" alt="Você sabia" >
        <span class="titulo_box">Você sabia que...</span>
         <hr>
        <br>
        Ao enfermeiro compete a troca de sondas de gastrostomia, jejunostomia, bem como de cistostomia 
        (sondagem vesical suprapúbica) e de traqueóstomos, desde que tenha segurança na realização do 
        procedimento, avaliando criteriosamente na sua competência técnica, cientifica e ética, para que não 
        venha lesar o paciente por imperícia, negligência ou imprudência (RONDÔNIA, 2012; CEARÁ, 2007).
        </div>
