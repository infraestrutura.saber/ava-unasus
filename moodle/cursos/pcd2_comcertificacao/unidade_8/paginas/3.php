<?php
               
    $pg_atual=3;
    registro($id,unid8_pg,$pg_atual,unid8_ev);
?>
    <style type="text/css">
        #lista li, .box ul li{
            list-style-type: disc; 
             margin-left: 30px;
        }
	
		
		
		
    </style>
   

   <h4 class="titulo">Descarte dos resíduos</h4>
   <br>
   Gerenciar os resíduos de serviços de saúde é importante, pois garante a qualidade da saúde coletiva e a preservação do meio ambiente.


<br><br>
<div class="box">
        <img src="../images/img_atencao_ad.png" alt="Atenção" >
        <span class="titulo_box">Atenção!</span>
        <br>
        <hr/>
		Todos que fazem parte desta cadeia são responsáveis pelo correto manejo dos resíduos de serviços de saúde, desde sua geração até o descarte. 
			</div>
<br>

Existe um potencial de risco aumentado quando os Resíduos de Serviço de Saúde (RSS) não são
 manuseados de maneira adequada ou deixam de ser acondicionados e descartados corretamente.
  Para minimizar este risco, deve-se (ABNT, 1997; ABNT, 1993):<br>
<ul id="lista">
<li>não reencapar, entortar, quebrar ou retirar manualmente as agulhas das seringas;</li>
<li>colocar de forma acessível ao local onde é realizado o procedimento os recipientes coletores para o descarte de material perfuro cortante;</li>
<li>utilizar recipiente exclusivo, resistente à punctura, à ruptura e com tampa, para descarte de todo resíduo perfuro cortante e abrasivo, inclusive os que não foram usados, sem ultrapassar o limite de 2/3 da capacidade total;</li>
<li>usar sempre, no manuseio dos RSS, equipamentos de proteção individual.</li>
</ul>

	<div class="box">
        <img src="../images/img_saibamais_ad.png" alt="Saiba mais">
        <span class="titulo_box">Saiba mais...</span>
        <br>
        <hr/>
		Para mais informações sobre o gerenciamento de resíduos de serviços de saúde, 
        recomendamos o acesso do Manual de Gerenciamento de Resíduos de Serviços de Saúde, 
        da Agência Nacional de Vigilância Sanitária,
         <a href="https://www.anvisa.gov.br/servicosaude/manuais/manual_gerenciamento_residuos.pdf" target="_blank">clique aqui</a> (ANVISA,2006).


</div>
   
