﻿<!DOCTYPE html!>
<meta charset="utf-8">
<html lang="pt-br">
<head>

		
		<link rel="stylesheet" type="text/css" href="../css/style.css">
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<link rel="stylesheet" type="text/css" href="../lib/bootstrap/css/bootstrap-responsive.css">
		<link rel="stylesheet" type="text/css" href="../lib/bootstrap/css/bootstrap-responsive.min.css">
		<link rel="stylesheet" type="text/css" href="../lib/bootstrap/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="../lib/bootstrap/css/bootstrap.min.css">
		<script type="text/javascript" src="../lib/bootstrap/js/bootstrap.js"></script>
		<script type="text/javascript" src="../lib/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="../lib/bootstrap/js/bootstrap-dropdown.js"></script>
		<script type="text/javascript" src="../lib/bootstrap/js/jquery-1.10.1.min.js"></script>
				<link rel="stylesheet" type="text/css" href="../lib/colorbox/colorbox.css">
		<script type="text/javascript" src="../lib/colorbox/jquery.colorbox.js"></script>
		<title>Oxigenoterapia Domiciliar</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>




	
			<body>
				<div style='display:none'>
			<div id="content_color" style='padding:10px; background:#fff;'>

			<div id="texto_color"></div>
			<br><br>
			<a class="btn inline" href="index.php?pagina=0" onClick="apagar_confirmar();" >Sim, desejo recomeçar os estudos.</a>
			<a class="btn" href="index.php?pagina=0">Não, quero continuar.</a>
				
			<br><br>
			
			</div>
</div>
						<script type="text/javascript">
			$(document).ready(function(){
			 $("#menu li a").mouseover(function(){
			 var index = $("#menu li a").index(this);
			 $("#menu li").eq(index).children("ul").slideDown(100);
			 if($(this).siblings('ul').size() > 0){
			 return false;
			 }
			 });
			 $("#menu li").mouseleave(function(){
			 var index = $("#menu li").index(this);
			 $("#menu li").eq(index).children("ul").slideUp(100);
			 });
			});


		</script>
				<?php 
				
				
				require_once('../../../config.php');
				require_once('../apresentacao/functions/conectar.php');	require_once('../apresentacao/functions/functions.php');conectar();
				$id=$USER->id; 
				$cpf=$USER->username;
				
				session_start();

				if ($_SESSION['logado'] == 1) {
					//echo "<script> alert('vc esta logado na unidade'); </script>";
				}else{
					//echo "<script> alert('NAO logado'); </script>";
				}

				function b($id){
					echo "ID: " .$id;
				}
				
				//redirecionar caso nao tenha permissao
				if($id==""){
					header('Location: http://ufpe.unasus.gov.br/moodle_unasus/login/index.php');
					//header('Location: http://ufpe.unasus.gov.br/adteste/login/index.php');
				}
				
				if(empty($_GET['pagina'])){
					//se variavél pagina não existir 
					$temp = '0';
					

				}

				if ($_GET['pagina']==1){ 
					$temp = 1;
					
				}
				if ($_GET['pagina']==2){ 
					$temp = 2;
					
				}
				if ($_GET['pagina']==3){ 
					$temp = 3;
					
				}
				if ($_GET['pagina']==4){ 
					$temp = 4;
					
				}
				if ($_GET['pagina']==5){ 
					$temp = 5;
					
				}
				if ($_GET['pagina']==6){ 
					$temp = 6;
					
				}
				if ($_GET['pagina']==7){ 
					$temp = 7;
					
				}
				if ($_GET['pagina']==8){ 
					$temp = 8;
					
				}
				?>
				<div id="geral">
					<div id="fundo">
							<div id="barra_topo">
								<?php
										include('../includes/menu.php'); 

								?>
					</div>

					<div id="disciplina">
						<p>Oxigenoterapia Domiciliar</p>
						<div class="info"><img src="../images/info.png" title="Título da unidade didática." alt="Título da unidade didática."></div>
					</div>
					
					<div id="usuario">
							  		<p>Olá, <b><?php echo ($USER->firstname ."</b>"." |  "); ?></p>
					</div>

			<div id="barra_evolucao">
				<div id="evolucao">
					<p class="descricao">Evolução<img src="../images/info.png" title="Contabiliza a quantidade de páginas visualizadas. A proposta é lhe dar 
a visão do percentual de conteúdos já acessados." alt="Contabiliza a quantidade de páginas visualizadas. A proposta é lhe dar 
a visão do percentual de conteúdos já acessados."></p>
					
					  <div class="navbar">
			          
			             <div class="progress evolucao">
			                 <div class="bar" style="width: <?php porcentagem_unid(7,$cpf); ?> "></div>
			            </div>
			          

			      </div>
				</div>
					


				<!--<div id="videos">
					<p class="descricao">Vídeo(s)<img src="../images/info.png" title="Acesso rápido ao(s) vídeo(s) disponível(is) nesta unidade didática." alt="Acesso rápido ao(s) vídeo(s) disponível(is) nesta unidade didática."></p>
					<ul>
							<li><img src="../images/video.png"></li>
							<li><img src="../images/video.png"></li>
							<li><img src="../images/video.png"></li>
					</ul>
				</div>-->
					


				<div id="exercicios">
					<p class="descricao">Exercício(s)<img src="../images/info.png" title="Atividade(s) formativa(s) relacionada(s) a esta unidade didática. 
Servem para testar seus conhecimentos, mas não valem nota." alt="Atividade(s) formativa(s) relacionada(s) a esta unidade didática. 
Servem para testar seus conhecimentos, mas não valem nota."></p>
				<ul>
							<li>
								<?php
								  verificaExercicio($cpf, 9, 7, 1);
								?>
							</li>
							<li>
								<?php
								  verificaExercicio($cpf, 10, 7, 2);
								?>
							</li>
							
						</ul>	
							
				</div>
					

					
				</div>


			</div> <!--daf-->


			<div id="central">
				
				<div id="conteudo">
					<?php
						if($temp==0){
							include('paginas/0.php'); 
						}

						if($temp == 1){
							include('paginas/1.php');
						}
						if($temp == 2){
							include('paginas/2.php');
						}
						if($temp == 3){
							include('paginas/3.php');
						}
						if($temp == 4){
							include('paginas/4.php');
						}
						if($temp == 5){
							include('paginas/5.php');
						}
						if($temp == 6){
							include('paginas/6.php');
						}
						if($temp == 7){
							include('paginas/7.php');
						}
						if($temp == 8){
							include('paginas/8.php');
						}
					?>
				</div>

				<div id="indice">
					<div id="titulo">
						<h3>Índice</h3>
						<div class="info"><img src="../images/info.png" title="Acesso ao(s) conteúdos(s) disponível(is) nesta unidade didática." alt="Acesso ao(s) conteúdos(s) disponível(is) nesta unidade didática."></div>
					</div>

					<ul>
					      	<li><?php verificarTopico($cpf, 0, 7); ?><a href="index.php?pagina=0">Apresentação da unidade</a>
							</li>
							<?php
					      			//require_once('../apresentacao/functions/functions.php');
					      			if($_GET['pagina'] == 0) {
					      				//$id = ID do usuario , $id_unidade = ID da unidade
					      				cadastrarTopico($cpf, 7);
					      			}
					      		?>
							
							
					      	<li><?php verificarTopico($cpf, 1, 7); ?><a href="index.php?pagina=1">Importância, indicações e finalidades do procedimento</a>
							</li>
							<?php
							    	require_once('../apresentacao/functions/functions.php');							
									if($_GET['pagina'] == 1) {
										cadastrarTopico($cpf, 7);
									}
					      		?>
							
							
							
							<li><?php verificarTopico($cpf, 2, 7); ?><a href="index.php?pagina=2">Responsabilidades da equipe na execução do procedimento</a>
							</li>
							<?php
							    	require_once('../apresentacao/functions/functions.php');							
									if($_GET['pagina'] == 2) {
										cadastrarTopico($cpf, 7);
									}
					      		?>
							
							
							
					      	<li><?php verificarTopico($cpf, 3, 7); ?><a href="index.php?pagina=3">Utilização e cuidados no manuseio dos equipamentos de oxigenoterapia</a>
							</li>
							<?php
							    	require_once('../apresentacao/functions/functions.php');							
									if($_GET['pagina'] == 3) {
										cadastrarTopico($cpf, 7);
									}
					      		?>
							
							
							
					      	<li><?php verificarTopico($cpf, 4, 7); ?><a href="index.php?pagina=4">Cuidados durante a execução do procedimento</a>
							</li>
							<?php
							    	require_once('../apresentacao/functions/functions.php');							
									if($_GET['pagina'] == 4) {
										cadastrarTopico($cpf, 7);
									}
					      		?>
							
							
							
							<li><?php verificarTopico($cpf, 5, 7); ?><a href="index.php?pagina=5">Equipamentos necessários para a realização do procedimento</a>
							</li>
							<?php
							    	require_once('../apresentacao/functions/functions.php');							
									if($_GET['pagina'] == 5) {
										cadastrarTopico($cpf, 7);
									}
					      		?>
							
							
							
							<li><?php verificarTopico($cpf, 6, 7); ?><a href="index.php?pagina=6">Intercorrências e contraindicações para a administração do oxigênio domiciliar</a>
							</li>
							<?php
							    	require_once('../apresentacao/functions/functions.php');							
									if($_GET['pagina'] == 6) {
										cadastrarTopico($cpf, 7);
									}
					      		?>
							
							
							
					      	<li><?php verificarTopico($cpf, 7, 7); ?><a href="index.php?pagina=7">Exercício(s) </a>
							</li>
							<?php
							    	require_once('../apresentacao/functions/functions.php');							
									if($_GET['pagina'] == 7) {
										cadastrarTopico($cpf, 7);
									}
					      		?>
							
							
							
					      	<li><?php verificarTopico($cpf, 8, 7); ?><a href="index.php?pagina=8">Referências </a>
							</li>
							<?php
							    	require_once('../apresentacao/functions/functions.php');							
									if($_GET['pagina'] == 8) {
										cadastrarTopico($cpf, 7);
									}
					      		?>

								
					</ul>
					<div id="indicefuncional">
						<?php ultimoAcesso($id, 7); ?>
						<!-- <a class="btn-danger btn-block btn btn_recomecar inline " href="#content_color1" onClick="apagar();">Recomeçar os estudos</a> -->
						<!-- <a class="btn-small btn-danger btn_recomecar inline" href="#content_color1" onClick="apagar();">Recomeçar os estudos</a> -->
						<a class="btn-block btn btn-danger btn_recomecar inline" href="#content_color" onClick="apagar();">Recomeçar os estudos</a>
					</div>
					
<script>
					function apagar(){
															
					$("#texto_color").html("<h4>Confirmação</h4> <br><br> <b> <?php echo $USER->firstname.', ';?></b>você tem certeza que deseja recomeçar os estudos desta unidade? Se confirmar a opção, todos os registros de navegação pelas páginas desta unidade serão eliminados e você não saberá quais páginas já visitou.");
					$(".inline").colorbox({inline:true, width:"50%",onClosed:function(){}});
				}

				function apagar_confirmar(){

				$.post( "../apresentacao/functions/apagar_tudo.php", { unidade: 7 }, function() {
				location.reload();
				});

				}
</script>
					<style>
						#indicefuncional {
							float: left;
							width: 250px;
							position: relative;
							margin-top: 35px;
						}
					</style>

				</div>

			</div>


		</div>

	</body>

	</html>
