<?php 

require_once $CFG->libdir.'/formslib.php';

class couse_search_form extends moodleform {

    function definition() {
    	$util = new enrol_unasus_report_util();
    
    	global $param;
 		$mform    =& $this->_form;
        $renderer =& $mform->defaultRenderer();

        $mform->addElement('header', '',get_string('filter_enrol_data','enrol_unasus'), '');
        
        $mform->addElement('select', 'userid', get_string('user','enrol_unasus'),$util->get_user_html_select());
        $mform->setType('userid', PARAM_INT);
        $mform->setDefault('userid', $param->userid);
        
        $mform->addElement('text', 'username', get_string('user','enrol_unasus'),'size="20"');
        $mform->setType('username', PARAM_TEXT);
        $mform->setDefault('username', $param->username);
     
     
        $mform->addElement('select', 'statusinfo', get_string('type','enrol_unasus'),$util->get_status_type_domain_table());
        $mform->setType('statusinfo', PARAM_INT);
          $mform->setDefault('statusinfo', $param->statusinfo);
        
    
        
        $mform->addElement('header', 'filter_profile_data',get_string('filter_profile_data','enrol_unasus'), '');
         $mform->setAdvanced('filter_profile_data');
         
        global $profile,$fields,$seleted_fields_filter,$profilefield_field;
    	$options_yes_not=$profilefield_field->get_general_html_option_yes_not_filter();
     foreach ($fields as $field){
    	if(isset($seleted_fields_filter[$field->id])) {
    		if($field->datatype=='menu'){
    			$mform->addElement('select', 'F_'.$field->id,$field->name,$profile->convert_menu_feild_data_to_array_for_form($field->param1));
        		$mform->setType('F_'.$field->id, PARAM_TEXT);
    		}
    			else if($field->datatype=='checkbox'){
    								$mform->addElement('select', 'F_'.$field->id,$field->name,$options_yes_not);
        							$mform->setType('F_'.$field->id, PARAM_TEXT);
        							$mform->setDefault('F_'.$field->id,optional_param('F_'.$field->id,NULL,PARAM_TEXT));
    						}
    		else{
    			 $mform->addElement('text', 'F_'.$field->id, $field->name);
    			 $mform->setType('F_'.$field->id, PARAM_TEXT);
    		}
         	 
    	}
      }	
      
     
          //data
        $this->add_action_buttons(true,get_string('research','enrol_unasus'));
    }

    function validation($data, $files) {
        global $CFG;

         $errors = parent::validation($data, $files);
         
      
     return $errors;
    }

}

?>