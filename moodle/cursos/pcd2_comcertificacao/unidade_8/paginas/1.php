<?php
               
    $pg_atual=1;
    registro($id,unid8_pg,$pg_atual,unid8_ev);
?>
    <style type="text/css">
    #lista li, .box ul li{
            list-style-type: disc;   
       margin-left: 15px;

        }
        .sub{
			margin-left: 25px;        	
        }
		
		
		#quadro_geral{
			height: 240px;
		
		}
    </style>
   

   <h4 class="titulo">Cuidados na execução do procedimento de coleta, dispositivos 
   	e acessórios necessários</h4>
    Os procedimentos de coleta de material humano no domicílio são executados por 
    profissionais de saúde legalmente habilitados, com finalidades de investigação 
    clínica e epidemiológica, de diagnose ou apoio diagnóstico, de terapêutica e 
    de acompanhamento clínico.<br><br>  
	 O serviço de apoio laboratorial no domicílio deve ser orientado por protocolos 
	 e normas de <b>biossegurança</b> no tocante aos procedimentos de coleta, ao modo 
	 de acondicionamento de material biológico e de seu transporte, a fim de 
	 estabelecer uma assistência qualificada ao usuário.<br><br>
	 Toda coleta de material biológico realizada no domicílio, assim como nas 
	 unidades de serviços de saúde, deve respeitar o princípio da biossegurança. 
	 Para tanto, é necessário que os profissionais envolvidos no processo façam 
	 uso de Equipamentos de Proteção Individual – EPIs, que se 
	 destinam à redução do risco de transmissão de microorganismos, bem como a 
	 protegê-los durante o exercício das suas atividades, <b>minimizando o risco 
	 de contato com sangue e fluídos corpóreos (secreções e excretas)  e de 
	 contato com lesões de pele e membranas mucosas. O uso de EPIs destina-se 
	 ainda aos cuidados envolvendo procedimentos invasivos</b> (ANVISA, 2009).<br><br>
	 <br>
	<div id="quadro_geral">
	 <b>Manuseio dos EPIs</b><i class=" icon-hand-up" title="Esse quadro possui interação."></i>
	 <br><br>
	 <div class="quadro1">
 	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Procedimentos para o uso de EPIs</b><br>
	<span class="um"><img  style="margin-left: 27px;" src="images/fig02_icone_usoepi.jpg" alt="Procedimento Uso EPI" ></span><br>
	<div class="descricao1">
	<ul id="lista" >
			<li>Reunir todo equipamento necessário;</li>
			<li>Prender os cabelos;</li>
			<li>Retirar os adornos (pulseiras, anel, relógio, etc.);</li>
			<li>Lavar as mãos ou friccionar alcool etílico a 70% por 30 segundos;</li>
			<li>Colocar os EPIs:</li>
			<li>Vestir o avental/capote;</li>
			<li>Colocar o respirador paticulado;</li>
			<li>Colocar óculos de proteção;</li>
			<li>Colocar luvas de procedimentos;</li>
			<li>Iniciar atividade designada.</li>
		</ul>
		<br><br>
	</div>
	</div>
   <div class="quadro2">
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Procedimentos para a retirada de EPIs</b><br>
    	<span class="dois"><img style="margin-left: 27px;" src="images/fig02_icone_retiradaepi.jpg" alt="Procedimento Retirada EPI"></span><br>
    	<div class="descricao2">
    	<ul id="lista">
    	 <li>Remover os EPIs de forma a evitar autocontaminação 
	ou autoinoculação com mãos ou EPIs contaminados. É necessário:</li>

<!--------------- -->
<ul id="lista">
    <li>retirar luvas (devem ser removidas durante a remoção do avental descartável);</li>
<li>higienizar as mãos;</li>
<li>remover óculos de proteção;</li>
<li>remover respirador particulado através das fitas elásticas evitando tocar a parte interna da máscara;</li>
<li>a cada EPI removido, descartá-lo em conformidade com o gerenciamento de resíduos.</li>
</ul>

</ul>
     
	 <br><br>
	
</div>
</div>
</div><p style="margin-left: 276px;clear:both;"><b>Fonte:</b> (ANVISA, 2009, adaptado).</p>
<br><br>
 <p style="clear:both;">Para que o procedimento de coleta seja realizado satisfatoriamente, é preciso (SÃO PAULO, 2006):</p>
	<ul id="lista">
		<li>que o ambiente esteja bem iluminado;</li>
		<li>conferir o nome do usuário com a requisição do exame;</li>
		<li>indagar sobre o preparo seguido pelo usuário (jejum, dieta e medicação);</li>
		<li>separar o material para a coleta conforme solicitação, quanto ao tipo de tubo e volume necessário;</li>
		<li>os insumos para coleta deverão estar disponibilizados de forma organizada;</li>
		<li>preencher as etiquetas de identificação do material com nome e número do registro;</li>
		<li>os tubos com aditivos tipo gel ou anticoagulantes devem ser homogeneizados por inversão de 5 a 8 vezes;</li>
		<li>o profissional responsável pela coleta deve assinar o pedido e colocar a data da coleta.</li>
	</ul>

<div class="box">
        <img src="../images/img_atencao_ad.png"  alt="Atenção">
        <span class="titulo_box">Atenção!</span>
        <br>
        <hr/>
		<ul>
        <li>A integridade de cada material deve ser observada, e, em caso de alteração, o material deve ser substituído;</li>
		<li>Em caso de acidente com material biológico, envolvendo face, olhos e mucosas, deve-se lavar imediatamente todas as parte atingidas com água corrente; </li> 
		<li>Logo após a lavagem, o profissional deverá realizar a notificação à chefia imediata, a qual, por sua 
		vez, notificará o setor responsável para avaliar o profissional acidentado e determinar a conduta, 
		o mais precocemente possível, idealmente nas primeiras duas horas, e, no máximo, até 72 horas após o acidente.
		O departamento pessoal deve emitir a Comunicação de Acidente de Trabalho (CAT), cujo verso será preenchido 
		 pelo médico do trabalho que atender o acidentado, a fim de documentar o acidente para efeitos legais.</li></ul>
		 <br>
		<p><strong>Fonte: </strong>(RAPPARINI; VITÓRIA; LARA, [20--?]).</p>
</div>
<br>
	<h5>Dispositivos e acessórios necessários</h5>
	A maleta da equipe de saúde deve conter garrote, algodão hidrófilo, álcool etílico a 70%, agulha descartável, seringa descartável, sistema a vácuo (suporte, tubo e agulha descartável), tubos com e sem anticoagulante, etiquetas para identificação de amostras, EPIs, estantes para os tubos e, preferencialmente, recipiente rígido próprio para desprezar material perfuro cortante.<br><br>
	
    <style type="text/css">
    		  .table td {
                border-top: 1px solid #DDDDDD;
                line-height: 20px;
                padding: 8px;
                text-align: left;
                vertical-align: middle;
            }
           .um_descricao , .dois_descricao{
           
           }
           .um ,.dois{
            cursor: pointer;
           }
           .um{
           	width: 300px;
           }
           .quadro1, .quadro2{
           	width: 319px;
           
           }
		   
		   .quadro1{
				float: left;
				margin-left: 60px;
		   
		   }
		   
		   .quadro2{
				float: right;
				margin-right: 9px;
		   
		   }
		   
		     .quadro2 img{
				margin-left: 16px;
			 }
		   
           .descricao1,.descricao2{
           	display: none;
           }
           </style>
           <script type="text/javascript">
            $(".um").mouseover(function(){
                $(".descricao1").slideDown('slow', function() {
  
				  });
				});
            
            $(".dois").mouseover(function(){
              $(".descricao2").slideDown('slow', function() {
  
				  });
				});
          

           </script>
           
