		//Funcão usada para comparar dois arrays
		function compare(x, y) {
			if (x.length != y.length) {
				return false;
			}
			for (key in x) {
				if (x[key] !== y[key]) {
					return false;
				} 
			}
			return true;
		}

		function gerarinputmultipla(alternativas, name, gabarito, id_exercicio) {
			var alt = new Array();
				alt[0] = "a";
				alt[1] = "b";
				alt[2] = "c";
				alt[3] = "d";
				alt[4] = "e";   
			
			for (var i = 0; i < alternativas.length; i++) {
				document.write('<input class="radio_me" type="radio" name="' + name + '"  value="' + alt + '"><div class="boolean"> ' + alt[i] + ') ' + alternativas[i] + '</div><br>');
			}

			document.write('<input class="btn" type="button" style="float: right;" value="Corrigir" onClick="multiplaEscolha(\''+ name +'\',' +gabarito+ ',' + alternativas.length + ','+id_exercicio+');">');
		}

		function gerarInputVF(entrada, name, gabarito, container,id_exercicio){

			var tamanho = entrada.length;

			var buffer = " ";

			for (var i = 0; i < tamanho; i++) {
				
				buffer = buffer + "<tr><td>" + entrada[i] + "</td><td style='text-align:center;'><input type='radio' name='" + name + i + "' value='V'></td><td style='text-align:center;'><input type='radio' name='" + name + i + "' value='F'></td></tr> ";
			};

			$('#' + container).html("<table class='table table-striped table-bordered' id='table_ex' border=1><tr class='linha_table'><td>Sentenças</td><td>V</td><td>F</td></tr>" + buffer + "</table>");

			$('#' + container).append('<input class="btn" style="float: right;" type="button" value="Corrigir" onClick="verdadeiroFalso(\'' + name + '\',' + tamanho  + ', new Array' + gabarito + ');" >');

			
		}

		function gerarInputVFSISAB(entrada, name, gabarito, container,id_exercicio){

			var tamanho = entrada.length;

			var buffer = " ";

			for (var i = 0; i < tamanho; i++) {
				
				buffer = buffer + "<tr><td style='font-size: 13px;'>" + entrada[i] + "</td><td style='text-align:center; font-size: 13px;'><input style='margin-right: 5px;' type='radio' name='" + name + i + "' value='V'>SIAB</td><td style='text-align:center;font-size: 13px;'><input style='margin-right: 5px;' type='radio' name='" + name + i + "' value='F'>SISAB</td></tr> ";
			};

			$('#' + container).html("<table class='table table-striped table-bordered' id='table_ex' border=1><tr class='linha_table'><td style='background-color: #d1bbd9 !important; line-height: 38px;'><center><b>Sentenças</b></center></td><td colspan='2' style='background-color: #d1bbd9 !important;'><center><b>Sistema de informação</b></center></td></tr>" + buffer + "</table>");

			$('#' + container).append('<input class="btn" style="float: right;" type="button" value="Corrigir" onClick="verdadeiroFalso(\'' + name + '\',' + tamanho  + ', new Array' + gabarito + ','+id_exercicio+');" >');

			
		}

		
		//Função que recebe a resposta de questão verdadeiro/falso do usuário e verifica se o mesmo acertou ou não
		function verdadeiroFalso(name, alternativas, gabarito, id_exercicio){

			var resp = new Array(alternativas);
				

				for (var i = 0; i < alternativas; i++) {
					
					resp[i] = document.getElementsByName(name + i);
					
				}

				var completo = true;
				var contador = alternativas + 1;

				for(var x = 0; x < (contador + 1); x++){
					

		            if(resp[x] != null){
		                 if(resp[x][0].checked == true){
		                           resp[x] = 'V';
		                }else if(resp[x][1].checked == true){
		                            resp[x] = 'F';
		                }else{
		                	
		                    completo = false;
		                }
		            }
	        	}

	        	//marcou todas
	        	if(completo){

	        		 if(compare(resp,gabarito) == true){
	        		 	alert(id_exercicio);
	        		 	//post de marcacao correta
						$.post( "../functions/chamaExCerto.php", 
						{ exercicio: id_exercicio},
						 function(){ }
						 );
	        		 	$('#box' + name).removeClass();
	        		 	$('#box' + name).html("Parabéns! Você acertou!");
	        		 	$('#box' + name).toggleClass("box alert alert-success");
	        		 	$('#box' + name).slideDown();

	        		 }else{
	        		 	$.post( "../functions/chamaExErrado.php", 
						{ exercicio: id_exercicio},
						 function(){ }
						 );
	        		 	$('#box' + name).removeClass();	
	        		 	$('#box' + name).html("Esta não é a resposta correta.");
	        		 	$('#box' + name).toggleClass("box alert alert-error");
	        		 	$('#box' + name).slideDown();

	        		 }

	        		 escolhido = 100;
	        		 mostrarBotoes(name, escolhido);

	        	//não marcou todas as alternativas
	        	}else{
					//post
				
	        		$('#box' + name).removeClass();
	        		$('#box' + name).html("Marque todas as alternativas.");
	        		$('#box' + name).toggleClass("box alert alert-block");
	        		$('#box' + name).slideDown();
	        		//$('.modal-footer').html('');
	        		//$('#modalEx').show();
					//$(".feedback" + name).slideDown();
				

	        	}

		}

		//Função que recebe a resposta da questão de múltipla escolha do usuário e verifica se a resposta está correta
	function multiplaEscolha(name, resposta, alternativas,id_exercicio) {

			var retorno;
			var escolhido;
			var p1 = document.getElementsByName(name);
			
			for (var x = 0; x < alternativas; x++) {
				
				if (p1[x].checked == true) {
						escolhido = x;

					if (x == resposta) {
						retorno = true;
						
						//marcou a resposta correta
						//post de marcacao correta
						$.post( "../functions/chamaExCerto.php", 
						{ exercicio: id_exercicio},
						 function(){ }
						 );
						
						$('#box' + name).removeClass();
	        		 	$('#box' + name).html("Parabéns! Esta é a alternativa correta.");
	        		 	$('#box' + name).toggleClass("box alert alert-success");
	        		 	$('#box' + name).slideDown();
	        		 	
						mostrarBotoes(name, escolhido);
						
					} else {
						retorno = false;
					}
				}
			}

			//não marcou nenhuma
			if(retorno == null){
					$('#box' + name).removeClass();
					$('#box' + name).html("Marque uma das alternativas.");
	        		$('#box' + name).toggleClass("box alert alert-block");
	        		$('#box' + name).slideDown();

			//marcou errado	
			}else if(retorno == false){
				//post
				$.post( "../functions/chamaExErrado.php", 
						{ exercicio: id_exercicio},
						 function(){ }
						 );
				$('#box' + name).removeClass();
				$('#box' + name).html("Esta não é a alternativa correta.");
	        	$('#box' + name).toggleClass("box alert alert-error");
	        	mostrarBotoes(name, escolhido);
	        	$('#box' + name).slideDown();
				
			}

		}
function mostrarFeedback(name, escolhido){
			
			//$('.modal-body').hide();
			if(escolhido == 100){
				$('#boxfeedback' + name).html( $(".feedback" + name).html());
				$("#boxfeedback" + name ).slideDown();

			}else{
				$('#boxfeedback' + name).html( $(".feedback" + name + " .feed:eq(" + escolhido + ")").html());
				$("#boxfeedback" + name ).slideDown();

			}

			
			


		}

		function mostrarBotoes(name, escolhido){
				
				$('#box' + name).append('<br><br><button type="button" class="btn" onclick="mostrarFeedback(\''+ name + '\','+ escolhido +');">Ver respostas</button>'+
	        		 		'<button type="button" onclick="document.location.reload();" class="btn">Tentar novamente</button><br><br><div id="boxfeedback' + name + '"></div>');

		}

		