<?php

class arouca_ws {
		//controle de falha de websevice
		var $WS_AROUCA_FAILURE=FALSE;
		var $WS_AROUCA_FAILURE_MSG='';
		
		
         function cadastrarIngressante($cpf,$ofertaid,$data_ingresso){
		
    		$this->WS_AROUCA_FAILURE=FALSE;
    		$this->WS_AROUCA_FAILURE_MSG='';
    		global $AROUCA;
       		
       		
   		 try {
                     
        	$client = new SoapClient($AROUCA->web_service_url_wsdl,    
                array('trace'=>true,
                      'cache_wsdl'=>WSDL_CACHE_NONE,
                      'exceptions'=>true,
                      'local_cert' =>$AROUCA->path_file_cert,
                      'passphrase' =>$AROUCA->cert_pwd));
         		try {
          			$res=$client->cadastrarIngressante(array('cpf' =>$cpf,'idOferta'=>$ofertaid,'dataIngresso'=>$data_ingresso));
          			
           			//print_r($res);
           		
             	}
        		catch (Exception $e) {
            		$this->WS_AROUCA_FAILURE=TRUE;
            		$this->WS_AROUCA_FAILURE_MSG=$e->getMessage();
            		//echo "<br>Falha na opera��o1 faultstring : $e->faultstring";
            		//echo "<br>Falha na opera��o1 codigoErro : $e->getFaultcode() ";
            		
            		//echo "<br>DETALHE DO ERRO: ";
            		//print_r($e);
   				 }
   
    	} catch (Exception $e) {
              $this->WS_AROUCA_FAILURE=TRUE;
            $this->WS_AROUCA_FAILURE_MSG=$e->getMessage();
           
            
        }
    
  }
	
	function cadastrarConcluinte($cpf,$coursoid,$data_conclusao,$nome){
		    $this->WS_AROUCA_FAILURE=FALSE;
    		$this->WS_AROUCA_FAILURE_MSG='';
    		global $AROUCA;
       		
   			 try {
        			
        	$client = new SoapClient($AROUCA->web_service_url_wsdl,    
                array('trace'=>true,
                      'cache_wsdl'=>WSDL_CACHE_NONE,
                      'exceptions'=>true,
                      'local_cert' =>$AROUCA->path_file_cert,
                      'passphrase' =>$AROUCA->cert_pwd));
        		 try {
          			$res=$client->cadastrarConcluinte(array('cpf' =>$cpf,'idCurso'=>$coursoid,'dataConclusao'=>$data_conclusao,'nomeConcluinteParaCertificado'=>$nome));
          			
           			//print_r($res);
           		
            	 }
        	catch (Exception $e) {
           	 $this->WS_AROUCA_FAILURE=TRUE;
            	$this->WS_AROUCA_FAILURE_MSG=$e->getMessage();
            	//echo "<br>Falha na opera��o1: $e->getMessage()";
   		 		}
   
    	} catch (Exception $e) {
              $this->WS_AROUCA_FAILURE=TRUE;
            $this->WS_AROUCA_FAILURE_MSG=$e->getMessage();
            
        }
    
  }
		
	
  function cadastrarConcluinteOferta($cpf,$ofertaid,$data_conclusao,$nome){
		    $this->WS_AROUCA_FAILURE=FALSE;
    		$this->WS_AROUCA_FAILURE_MSG='';
    		global $AROUCA;
       		
   			 try {
        			
        	$client = new SoapClient($AROUCA->web_service_url_wsdl,    
                array('trace'=>true,
                      'cache_wsdl'=>WSDL_CACHE_NONE,
                      'exceptions'=>true,
                      'local_cert' =>$AROUCA->path_file_cert,
                      'passphrase' =>$AROUCA->cert_pwd));
        		 try {
          			$res=$client->cadastrarConcluinte(array('cpf' =>$cpf,'idCurso'=>null,'idOferta'=>$ofertaid, 'dataConclusao'=>$data_conclusao,'nomeConcluinteParaCertificado'=>$nome));


          			
           			//print_r($res);
           		
            	 }
        	catch (Exception $e) {
           	 $this->WS_AROUCA_FAILURE=TRUE;
            	$this->WS_AROUCA_FAILURE_MSG=$e->getMessage();
            	//echo "<br>Falha na opera��o1: $e->getMessage()";
   		 		}
   
    	} catch (Exception $e) {
              $this->WS_AROUCA_FAILURE=TRUE;
            $this->WS_AROUCA_FAILURE_MSG=$e->getMessage();
            
        }
    
  }
	
	function recuperarCertificadoConcluinte($cpf,$coursoid,$data_conclusao){
		    $this->WS_AROUCA_FAILURE=FALSE;
    		$this->WS_AROUCA_FAILURE_MSG='';
    		global $AROUCA;
    		$url='';
       		 try {
        			 
        	$client = new SoapClient($AROUCA->web_service_url_wsdl,    
                array('trace'=>true,
                      'cache_wsdl'=>WSDL_CACHE_NONE,
                      'exceptions'=>true,
                      'local_cert' =>$AROUCA->path_file_cert,
                      'passphrase' =>$AROUCA->cert_pwd));
         
        		 try {
          			$res=$client->recuperarCertificadoConcluinte(array('cpf' =>$cpf,'idCurso'=>$coursoid,'dataConclusao'=>$data_conclusao));
          			$url= $res->return;
          			//echo "Resposta<br>:";
           			//print_r($res);
           		
            	 }
        	catch (Exception $e) {
           	 $this->WS_AROUCA_FAILURE=TRUE;
            	$this->WS_AROUCA_FAILURE_MSG=$e->getMessage();
            	//echo "<br>Falha na opera��o1: $e->getMessage()";
   		 		}
   
    	} catch (Exception $e) {
              $this->WS_AROUCA_FAILURE=TRUE;
            $this->WS_AROUCA_FAILURE_MSG=$e->getMessage();
            echo "<br>Falha na conex�o: $e->getMessage()";
        }
    return $url;
  }
  
  function recuperarCertificadoConcluinteOferta($cpf,$ofertaid,$data_conclusao){
		    $this->WS_AROUCA_FAILURE=FALSE;
    		$this->WS_AROUCA_FAILURE_MSG='';
    		global $AROUCA;
    		$url='';
       		 try {
        			 
        	$client = new SoapClient($AROUCA->web_service_url_wsdl,    
                array('trace'=>true,
                      'cache_wsdl'=>WSDL_CACHE_NONE,
                      'exceptions'=>true,
                      'local_cert' =>$AROUCA->path_file_cert,
                      'passphrase' =>$AROUCA->cert_pwd));
         
        		 try {
          			 $res=$client->recuperarCertificadoConcluinte (array('cpf' =>$cpf,'idCurso'=>null, 'idOferta'=>$ofertaid, 'dataConclusao'=>$data_conclusao));

          			$url= $res->return;
          			//echo "Resposta<br>:";
           			//print_r($res);
           		
            	 }
        	catch (Exception $e) {
           	 $this->WS_AROUCA_FAILURE=TRUE;
            	$this->WS_AROUCA_FAILURE_MSG=$e->getMessage();
            	//echo "<br>Falha na opera��o1: $e->getMessage()";
   		 		}
   
    	} catch (Exception $e) {
              $this->WS_AROUCA_FAILURE=TRUE;
            $this->WS_AROUCA_FAILURE_MSG=$e->getMessage();
            echo "<br>Falha na conex�o: $e->getMessage()";
        }
    return $url;
  }
  
  function cadastrarPessoa($cpf, $nome,$nomePai, $nomeMae,$sexo, $dataNasc, $email, $municipio){
		    $this->WS_AROUCA_FAILURE=FALSE;
    		$this->WS_AROUCA_FAILURE_MSG='';
    		global $AROUCA;
       		
   			 try {
        			
        	$client = new SoapClient($AROUCA->web_service_url_wsdl,    
                array('trace'=>true,
                      'cache_wsdl'=>WSDL_CACHE_NONE,
                      'exceptions'=>true,
                      'local_cert' =>$AROUCA->path_file_cert,
                      'passphrase' =>$AROUCA->cert_pwd));
         
        		 try {
          			$res=$client->cadastrarPessoa(array('cpf' =>$cpf,'nome'=>$nome,'nomeMae'=>$nomeMae,'nomePai'=>$nomePai,'sexo'=>$sexo,'dataNasc'=>$dataNasc,'email'=>$email,'municipio'=>$municipio));
          			
           			//print_r($res);
           		
            	 }
        	catch (Exception $e) {
           	 $this->WS_AROUCA_FAILURE=TRUE;
            	$this->WS_AROUCA_FAILURE_MSG=$e->getMessage();
            	//echo "<br>Falha na opera��o1: $e->getMessage()";
   		 		}
   
    	} catch (Exception $e) {
              $this->WS_AROUCA_FAILURE=TRUE;
            $this->WS_AROUCA_FAILURE_MSG=$e->getMessage();
            
        }
    
  }
	
	

}
?>
