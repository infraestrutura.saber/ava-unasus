<?php
/*
 * Created on 05/04/2012
 *
 *Essa class tem as bibliotecas de consulta da base de dados sobre o contexto do site, categoria de curso e curso 
 */
 class context  {
 	
 	 function get_courses() {
    	global $CFG;
		$sql ="SELECT c.id,c.category, e.id AS contextid, c.fullname FROM {$CFG->prefix}context e INNER  JOIN {$CFG->prefix}course c ON c.id=e.instanceid WHERE  e.contextlevel=50 AND c.format != 'site' ORDER BY c.fullname ";
		return get_records_sql($sql); 
		
 	}
 	
 	 function get_courses_category() {
    	global $CFG;
		$sql ="SELECT c.id, e.id AS contextid, c.name FROM {$CFG->prefix}context e INNER  JOIN {$CFG->prefix}course_categories c ON c.id=e.instanceid WHERE  e.contextlevel=40 ORDER BY c.name ";
		return get_records_sql($sql); 
		
 	}
 	  function get_contextlevel($id) {
    	global $CFG;
		$sql ="SELECT contextlevel FROM {$CFG->prefix}context  WHERE  id=$id";
		$r=get_record_sql($sql);
	   return $r->contextlevel; 
	}
	function get_instanceid($id) {
    	global $CFG;
		$sql ="SELECT instanceid FROM {$CFG->prefix}context  WHERE  id=$id";
		$r=get_record_sql($sql);
	   return $r->instanceid; 
	}
 	 function get_course_name($instanceid) {
    	global $CFG;
		$sql ="SELECT fullname FROM {$CFG->prefix}course  WHERE  id= $instanceid";
		$r=get_record_sql($sql);
	   return $r->fullname; 
		
 	}
 	function get_course_category_name($instanceid) {
    	global $CFG;
		$sql ="SELECT name FROM {$CFG->prefix}course_categories  WHERE  id= $instanceid";
		$r=get_record_sql($sql);
	   return $r->name; 
		
 	}
 	function get_system_name() {
    	global $CFG;
    		$contextlevel=$this->get_contextlevel($id);
    	if($contextlevel==CONTEXT_SYSTEM) return get_string('context_system','enrol_unasus');
    	else return null;
	}
 	 function get_name($id) {
    	global $CFG;
    	$contextlevel=$this->get_contextlevel($id);
    	if($contextlevel==CONTEXT_SYSTEM) return $this->get_system_name();
    	else{
    		$instanceid=$this->get_instanceid($id);
    		 if($contextlevel==CONTEXT_COURSECAT) return $this->get_course_category_name($instanceid);
    		 else if($contextlevel==CONTEXT_COURSE) return  $this->get_course_name($instanceid);
    	}
    }
 }