<!DOCTYPE html!>
<html lang="pt-br">
<meta charset="utf-8">
<head>
<link rel="stylesheet" type="text/css" href="apresentacao/css/style.css">
<script type="text/javascript" src="lib/jquery.js"></script>
<script type="text/javascript" src="lib/jquery.cycle.all.js"></script>
<script type="text/javascript" src="apresentacao/script/script.js"></script>



</head>
<body>

<?php
//Já estamos dentro da pasta apresentacao, logo nao será necessário escapar o nivel 'apresentacao'
require_once("functions/conectar.php");

conectar();
require_once("../../../config.php");

//Já estamos dentro da pasta apresentacao, logo nao será necessário escapar o nivel 'apresentacao'
require_once("functions/functions.php");

session_start();

if ($_SESSION['logado'] == 1) {
	//echo "<script> alert('vc esta logado ainda'); </script>";
}else{
	//echo "<script> alert('NAO logado'); </script>";
}


$id=$USER->id;
$name_user=$USER->firstname;
$cpf=$USER->username;

//salva a sessão do aluno
// if (empty($_SESSION['id'])) {
// 	//echo "usuario vazio"."<br>";
// }else{

// 	//echo "sesao ini"."<br>";
// }

?> 


<div id="ceu"></div>
<div id="grama"></div>

	<div id="barra_topo">
<?php
		include('menu.php'); 
?>

</div>
<div id="footer"></div>



<div id="pagina">
	<div id="texto_inicial">
		<span id="titulo">Princípios para o cuidado domiciliar 2<br></span>
		<br>
		A assistência domiciliar é uma modalidade do cuidar que exige muito da 
		equipe de saúde, uma vez que não estará atuando em seu ambiente ideal de 
		trabalho. Por outro lado, expõe estas equipes à real condição social do 
		paciente, às rotinas nas quais as famílias estão inseridas e aos seus 
		valores, fazendo com que o cuidado não fique restrito aos aspectos 
		biológicos da doença. Este módulo procura contribuir com os profissionais 
		de enfermagem que prestam cuidados no domicílio, facilitando sua prática 
		cotidiana, sempre na perspectiva de trabalho que envolve o paciente, o 
		cuidador, os familiares e os profissionais de saúde.
		<?php  validar_usuario($name_user,$cpf); ?>
<br>
<p></p>	
		

	</div>





	<span id="disciplinas">Unidades Didáticas</span>
	<!-- <div id="sair"> Bem-vindo(a), <?php //echo ($USER->firstname ."! | "); ?>Sair</div> -->
	<div id="sair_botao">
	<a href="http://ufpe.unasus.gov.br/moodle_unasus">

	<img src="apresentacao/imagens/sair.png" alt="Sair" title"Sair" border="0"></a>
	</div>
	<div id="conteudo">
		<div id="casa"><img src="apresentacao/imagens/casa.png"></div>
		<nav>

			
			
			<div id="seta_botton"><img class="unidades_hover" src="apresentacao/imagens/unidades_texto.png"></div>
			<div id="descricao_seta_botton">Acesse o Curso</div>
			<div id="descricao_tour_acesso">Conheça o Curso</div>
			<div id="seta_up"><img src="apresentacao/imagens/up.png"></div>
			<div id="tour_acesso"><a class="group1" href="apresentacao/imagens/tur/1.jpg" title="Bem-vindo(a)!"><img class="tour_hover" src="apresentacao/imagens/tour_texto.png" border="0"></a></div>

			<div id="tour">
		<!--<p><a class="group1" href="ad/modulo1/apresentacao/imagens/tur/1.jpg" title="Bem-vindo(a)!"></p>-->
		<p><a class="group1" href="apresentacao/imagens/tur/2.jpg" title="Acesso às unidades"></a></p>
		<p><a class="group1" href="apresentacao/imagens/tur/3.jpg" title="Página de conteúdo"></a></p>
		<p><a class="group1" href="apresentacao/imagens/tur/4.jpg" title="Página de conteúdo - barra superior"></a></p>
		<p><a class="group1" href="apresentacao/imagens/tur/5.jpg" title="Página de conteúdo - barra superior"></a></p>
		<p><a class="group1" href="apresentacao/imagens/tur/6.jpg" title="Informações sobre os conteúdos acessados"></a></p>
		<p><a class="group1" href="apresentacao/imagens/tur/7.jpg" title="Informações sobre o conteúdo"></a></p>
		<p><a class="group1" href="apresentacao/imagens/tur/8.jpg" title="Informações sobre o conteúdo"></a></p>
		<p><a class="group1" href="apresentacao/imagens/tur/9.jpg" title="Fórum de discussões"></a></p>
		<p><a class="group1" href="apresentacao/imagens/tur/10.jpg" title="Fórum de discussões"></a></p>
		<p><a class="group1" href="apresentacao/imagens/tur/11.jpg" title="Fórum de discussões"></a></p>
		<p><a class="group1" href="apresentacao/imagens/tur/12.jpg" title="Fórum de discussões"></a></p>
		<p><a class="group1" href="apresentacao/imagens/tur/13.jpg" title=""></a></p>
		
</div>

	
			
			<div id="menu_acesso"><img src="apresentacao/imagens/menu_acesso.png"></div>
		</nav>
		<script type="text/javascript">
		$("#tour_acesso").mouseover(function(){
			$(".tour_hover").attr("src", "apresentacao/imagens/tour_texto_hover.png");
			$("#descricao_tour_acesso").fadeIn(200).animate({left:314});
		});
		$("#tour_acesso").mouseleave(function(){
			$(".tour_hover").attr("src", "apresentacao/imagens/tour_texto.png");
			$("#descricao_tour_acesso").fadeOut(100).animate({left:314});;
		});

			$("#seta_botton").mouseover(function(){
			$(".unidades_hover").attr("src", "apresentacao/imagens/unidades_texto_hover.png");
			$("#descricao_seta_botton").fadeIn(200);
		});
		$("#seta_botton").mouseleave(function(){
			$(".unidades_hover").attr("src", "apresentacao/imagens/unidades_texto.png");
			$("#descricao_seta_botton").fadeOut(100);
		});


			$("#seta_botton").click(function(){
        $("html,body").animate({scrollTop: $("#disciplinas").offset().top}, 2500);
    
                              });
				$("#seta_up").click(function(){
        $("html,body").animate({scrollTop: $("#pagina").offset().top}, 2500);
    
                              });
		</script>
		
	<div id="descricao">
		<ul id="rotulos">
			<li class="texto_unidades">O conteúdo está dividido em sete unidades 
				que abordam cuidados e procedimentos necessários, comuns e possíveis 
				de serem realizados no domicílio. <br><br>

				Para conhecer os conteúdos que serão trabalhados neste módulo, 
				passe o mouse sobre as casinhas ao lado. Para acessar as unidades 
				didáticas, clique sobre as casinhas.<br><br>

				Começaremos estudando os cuidados na avaliação do ambiente domiciliar, necessários para execução dos procedimentos que serão abordados neste módulo; em seguida, veremos procedimentos para auxílio da eliminação, como o enteroclisma e a sondagem vesical; na sequência, abordaremos a assistência ao paciente na alimentação, por meio da sondagem e nutrição enteral; os cuidados com estomias diversas e a oxigenoterapia no ambiente domiciliar; e, por fim, a coleta, o acondicionamento e o transporte de material biológico a partir do domicílio. </li>
			<li > <span class="casa1_cor">Para um cuidado compartilhado e ético</span><br><br>
						Para a realização de procedimentos de enfermagem, é necessário fazer o reconhecimento do ambiente domiciliar, 
						identificando os fatores facilitadores, dificultadores ou inviabilizadores do cuidado domiciliar. Nesta unidade, 
						identificaremos os aspectos cruciais a serem observados na residência dos pacientes para a execução de 
						procedimentos de forma segura.<br><br>
						Bons estudos!

			</li>
			<li >
				<span class="casa2_cor">Cuidados na Avaliação do Ambiente Domiciliar</span>
				<br/><br/>

				Para a realização de procedimentos de enfermagem, é necessário fazer o reconhecimento do ambiente domiciliar, 
				identificando os fatores facilitadores, dificultadores ou inviabilizadores do cuidado domiciliar. Nesta unidade, 
				identificaremos os aspectos cruciais a serem observados na residência dos pacientes para a execução de 
				procedimentos de forma segura.<br><br>
				Bons estudos!
			</li>
			<li>

				<span class="casa3_cor">Enteroclisma</span>
				<br/><br/>
				A equipe de enfermagem é responsável pelo monitoramento do padrão 	
				eliminatório do paciente; afinal, muitos fatores podem influenciar 
				na eliminação intestinal do paciente que está sob cuidado domiciliar. 
				<br/><br/>
				A idade, o tipo de dieta, a quantidade de ingestão de líquido, a 
				frequência de atividade física, os fatores psicológicos, os hábitos 
				pessoais, os processos cirúrgicos, o uso de medicações, a gravidez 
				e o puerpério, e até mesmo os exames diagnósticos podem interferir
				 nessa necessidade fisiológica normal do paciente. <br/><br/>
				Nesta unidade, você estudará não somente o procedimento de 
				enteroclisma, como também identificará as competências da equipe 
				de enfermagem durante a execução da técnica e os cuidados 
				necessários para uma boa execução desse procedimento. <br><br>
				Bons estudos!
			</li>
			<li>
				<span  class="casa4_cor"> Sondagem Vesical</span>
				<br/><br/>
				A eliminação de resíduos pela urina é uma necessidade básica que 
				depende do correto funcionamento do sistema urinário, que acontece 
				de forma sincronizada. <br/><br/>Por vezes, este sincronismo encontra-se
				 prejudicado, fazendo com que o paciente domiciliado necessite 
				 do procedimento de sondagem vesical. O acompanhamento do débito
				  urinário é de responsabilidade da equipe de enfermagem. <br/><br/>
				Nesta unidade, conheceremos as indicações e as finalidades da 
				sondagem vesical, os tipos de sonda, os cuidados necessários no 
				manuseio dos equipamentos utilizados na execução da sondagem 
				vesical e as responsabilidades da equipe de enfermagem na execução
				 do procedimento.<br/><br/>
				Bons estudos!

			</li>
			<li> <span class="casa5_cor">Sondagem e Nutrição Enteral</span>
				<br/><br/>
				 A alimentação, além de ser uma necessidade fisiológica, é 
				 uma atividade importante na prevenção de doenças e na recuperação 
				 da saúde do paciente que necessita de cuidados domiciliares.<br><br>
				  Em algumas situações, devido às enfermidades, aos processos 
				  cirúrgicos ou até mesmo ao processo de envelhecimento, ela 
				  pode se apresentar prejudicada. <br><br>
				Nesta unidade, veremos as indicações e os tipos de sondagem para 
				fins nutricionais, os cuidados no manuseio dos materiais, as 
				responsabilidades da equipe de enfermagem na execução do 
				procedimento.<br><br> Também estudaremos as orientações para o 
				seguimento nutricional no domicílio e as possíveis intercorrências 
				que poderão ocorrer durante a sondagem e a nutrição enteral.<br><br>
				Bons estudos!
			</li>
			<li><span  class="casa6_cor"> Estomas</span>
				<br/><br/>
				Como você já sabe, a pele é uma barreira mecânica contra a invasão 
				dos microrganismos causadores de doença. Quando lesionada, torna-se 
				uma porta de entrada para patógenos, pondo em risco a saúde do indivíduo. 
				Portanto, especial atenção deve ser dada ao paciente estomizado.<br><br>
				Nesta unidade, não somente estudaremos os tipos de estomias, como também identificaremos 
				as responsabilidades da equipe de enfermagem na realização do procedimento e os 
				dispositivos e acessórios necessários para realização de procedimentos no paciente 
				estomizado. Por fim, refletiremos sobre as condições facilitadoras e dificultadoras 
				do cuidado ao paciente estomizado no domicílio.<br><br>
				Bons estudos!
			</li>
			<li><span  class="casa7_cor">Oxigenoterapia Domiciliar</span>
				<br/><br/>
				Com o objetivo de prevenir a hipóxia tecidual, a oxigenoterapia 
				domiciliar é uma terapêutica que trabalha uma das necessidades mais 
				vitais do ser humano, que é o padrão respiratório. No domicílio, a 
				oxigenoterapia tem a intenção de proporcionar qualidade de vida ao 
				paciente com dificuldades respiratórias<br><br>				
				Nesta unidade, reconheceremos as indicações e finalidades do 
				procedimento, as responsabilidades da equipe de enfermagem na 
				realização de oxigenoterapia domiciliar, os cuidados necessários 
				durante a execução da oxigenoterapia, quais equipamentos são 
				necessários, as intercorrências que podem surgir durante o 
				procedimento e as contraindicações para a administração do 
				 domiciliar.<br><br>
				Bons estudos!

			</li>
			<li><span  class="casa8_cor">Material biológico: coleta, acondicionamento e transporte</span>
				<br/><br/>
				Durante o cuidado domiciliar, o paciente pode necessitar realizar 
				alguns exames para monitoramento da sua situação de saúde sem precisar 
				ser removido ou encaminhado para uma unidade de saúde. <br/><br/>
				Nesta unidade, identificaremos os acessórios necessários para a coleta de material 
				biológico no domicílio, além dos cuidados na execução do procedimento, acondicionamento, 
				transporte do material biológico e descarte do material utilizado na coleta.<br><br>
				Bons estudos!
			</li>
		
		</ul>

	</div>	
	
	<div id="box_casas">
			<!--<div class="casa1"><a href="unidade_1/index.php"><img width="65%"  width="65%" src="apresentacao/imagens/casa1.png" title="Princípios Éticos e de Biossegurança no Cuidado Comiciliar"></a></div>
				<div class="evolucao_unid1"><?php porcentagem_unid(1,$id); ?></div>-->
			
			<div class="casa2"><a href="unidade_2/index.php"><img width="65%"  src="apresentacao/imagens/casa2.png" title="Cuidados na Avaliação do Ambiente Domiciliar"></a></div>
				<div class="evolucao_unid2"><?php porcentagem_unid(2,$cpf); ?></div>
			
			<div class="casa3"><a href="unidade_3/index.php"><img width="65%"  src="apresentacao/imagens/casa3.png" title="Enteroclisma"></a></div>
				<div class="evolucao_unid3"><?php porcentagem_unid(3,$cpf); ?></div>
			
			<div class="casa4"><a href="unidade_4/index.php"><img width="65%"  src="apresentacao/imagens/casa4.png" title="Sondagem Vesical"></a></div>
				<div class="evolucao_unid4"><?php porcentagem_unid(4,$cpf); ?></div>
			
			<div class="casa5"><a href="unidade_5/index.php"><img width="65%"  src="apresentacao/imagens/casa5.png" title="Sondagem e Nutrição Enteral"></a></div>
				<div class="evolucao_unid5"><?php porcentagem_unid(5,$cpf); ?></div>
			
			<div class="casa6"><a href="unidade_6/index.php"><img width="65%"  src="apresentacao/imagens/casa6.png" title="Estomas"></a></div>
				<div class="evolucao_unid6"><?php porcentagem_unid(6,$cpf); ?></div>
			
			<div class="casa7"><a href="unidade_7/index.php"><img width="65%"  src="apresentacao/imagens/casa7.png" title=
			"Oxigenoterapia Domiciliar"></a></div>
				<div class="evolucao_unid7"><?php  porcentagem_unid(7,$cpf); ?></div>
			
			<div class="casa8"><a href="unidade_8/index.php"><img width="65%"  src="apresentacao/imagens/casa8.png" title="Material biológico: coleta, acondicionamento e transporte"></a></div>
				<div class="evolucao_unid8"><?php  porcentagem_unid(8,$cpf); ?></div>
	</div>	
		<script type="text/javascript">
		jQuery(function($){
			//passagem das unidades
			$('#rotulos').cycle({ 
				fx:      'fade',
				timeout: 0, 
				speed:   1000, 
				startingSlide: 0 
			});
					
			$('.casa1').hover(function() { 
				$('#rotulos').cycle(1); 
				return false; 
			});

			$('.casa1').mouseleave(function() { 
				$('#rotulos').cycle(0); 
				return false; 
			}); 			
			 
			$('.casa2').hover(function() { 
				$('#rotulos').cycle(2); 
				return false; 
			}); 
			
			$('.casa2').mouseleave(function() { 
				$('#rotulos').cycle(0); 
				return false; 
			}); 

			$('.casa3').hover(function() { 
				$('#rotulos').cycle(3); 
				return false; 
			}); 
			
			$('.casa3').mouseleave(function() { 
				$('#rotulos').cycle(0); 
				return false; 
			}); 
				
			$('.casa4').hover(function() { 
				$('#rotulos').cycle(4); 
				return false; 
			}); 
			
			$('.casa4').mouseleave(function() { 
				$('#rotulos').cycle(0); 
				return false; 
			}); 
			
			$('.casa5').hover(function() { 
				$('#rotulos').cycle(5); 
				return false; 
			}); 
			
			$('.casa5').mouseleave(function() { 
				$('#rotulos').cycle(0); 
				return false; 
			}); 
			
			$('.casa6').hover(function() { 
				$('#rotulos').cycle(6); 
				return false; 
			}); 
			
			$('.casa6').mouseleave(function() { 
				$('#rotulos').cycle(0); 
				return false; 
			}); 
			
			$('.casa7').hover(function() { 
				$('#rotulos').cycle(7); 
				return false; 
			}); 
			
			$('.casa7').mouseleave(function() { 
				$('#rotulos').cycle(0); 
				return false; 
			}); 
			
			$('.casa8').hover(function() { 
				$('#rotulos').cycle(8); 
				return false; 
			}); 
			
			$('.casa8').mouseleave(function() { 
				$('#rotulos').cycle(0); 
				return false; 
			}); 
			

		});


	</script>	
	
	<div id="marcas">

				<!--<img style="width: 80%;" src="apresentacao/imagens/Assinaturas-negativo-AD(base).jpg">-->
<br><br>
<img  src="apresentacao/imagens/assinaturas.jpg">
		
	</div>
	</div>
</div>
<script type="text/javascript" src="lib/colorbox/jquery.colorbox.js"></script>
<link rel="stylesheet" type="text/css" href="lib/colorbox/colorbox.css">
			<script type="text/javascript">
				
				$("#cboxClose").click(function(){
					jQuery.colorbox.close();
					alert("oi");
				});
				$("#cboxClose").hide();
				$(".group1").colorbox({rel:'group1'});
				
			</script>

			<script type="text/javascript">
$(document).ready(function(){
			 $("#menu li a").mouseover(function(){
			 var index = $("#menu li a").index(this);
			 $("#menu li").eq(index).children("ul").slideDown(100);
			
			 if($(this).siblings('ul').size() > 0){
			 return false;
			 }
			 });
			 $("#menu li").mouseleave(function(){
			 var index = $("#menu li").index(this);
			 $("#menu li").eq(index).children("ul").slideUp(100);
			 });
			});
</script>

</body>
</html>
