<?php
/*
 * Created on 23/05/2012
 *
 *Essa class tem as bibliotecas de gerenciamento
 * dos campos do perfil do usu�rio. 
 */
 class profilefield_field {
 	
 	//inserir regristro na base de dados. 
 	function save($dto) {
 		global $CFG;
 		$dto->sortorder=$this->get_next_order($dto->categoryid);
 		return insert_record('enrol_unasus_info_field', $dto);
 	}
 	
 
 //alteriar regristro na base de dados. 
 	function edit($dto) {
 		global $CFG;
 		return update_record('enrol_unasus_info_field', $dto);
 	}

  //excluir regristro na base de dados. 
 function delete_by_id($id) {
 		global $CFG;
 		return delete_records_select('enrol_unasus_info_field', "id=$id");
 	}
  //Retora a quantidade de registro cadastrada na base de dados
 function count() {
 		global $CFG;
		$sql ="SELECT COUNT(id)  AS qreg FROM {$CFG->prefix}enrol_unasus_info_field ";
		$r=get_record_sql($sql);
		return $r->qreg;
 	}
 	function get_by_id($id) {
 		global $CFG;
		$sql ="SELECT id,contextid,shortname,name,datatype,description,categoryid,sortorder,required,visible,forceunique,defaultdata,param1,param2,param3,param4,param5  FROM {$CFG->prefix}enrol_unasus_info_field WHERE id=$id";
		$r=get_record_sql($sql);
		return $r;
 	}
 	function get_by_contextid($contextid,$visible=NULL) {
 		global $CFG;
 		$sql_visible="";
 		if($visible!=NULL){
 			$sql_visible=" AND visible=$visible ";
 		}
		$sql ="SELECT id,contextid,shortname,name,datatype,description,categoryid,sortorder,required,visible,forceunique,defaultdata,param1,param2,param3,param4,param5  FROM {$CFG->prefix}enrol_unasus_info_field WHERE contextid=$contextid $sql_visible ";
		$r=get_records_sql($sql);
		return $r;
 	}
 	function get_all() {
 		global $CFG;
		$sql ="SELECT id,contextid,shortname,name,datatype,description,categoryid,sortorder,required,visible,forceunique,defaultdata,param1,param2,param3,param4,param5   FROM {$CFG->prefix}enrol_unasus_info_field ";
		$r=get_records_sql($sql);
		return $r;
 	}
 	function get_all_by_category($categoryid) {
 		global $CFG;
		$sql ="SELECT id,contextid,shortname,name,datatype,description,categoryid,sortorder,required,visible,forceunique,defaultdata,param1,param2,param3,param4,param5   FROM {$CFG->prefix}enrol_unasus_info_field WHERE categoryid=$categoryid ORDER BY sortorder";
		$r=get_records_sql($sql);
		return $r;
 	}
 	function exist_create_shortname($contextid,$shortname) {
 		global $CFG;
		$sql ="SELECT COUNT(id)  AS qreg   FROM {$CFG->prefix}enrol_unasus_info_field WHERE contextid=$contextid AND shortname='".$shortname."'";
		$r=get_record_sql($sql);
		if( $r->qreg==0) return FALSE;
		if( $r->qreg==1) return TRUE;
		return TRUE;
 	}
 	function exist_edit_shortname($id, $contextid,$shortname) {
 		global $CFG;
		$sql ="SELECT COUNT(id)  AS qreg   FROM {$CFG->prefix}enrol_unasus_info_field WHERE id!=$id AND contextid=$contextid AND shortname='".$shortname."'";
		$r=get_record_sql($sql);
		if( $r->qreg==0) return FALSE;
		if( $r->qreg==1) return TRUE;
		return TRUE;
 	}
 	function get_last_order($categoryid) {
 		global $CFG;
		$sql ="SELECT MAX(sortorder) AS maxorder   FROM {$CFG->prefix}enrol_unasus_info_field WHERE categoryid=$categoryid ";
		
		$r=get_record_sql($sql);
		return $r->maxorder;
 	}
 	  function get_next_order($categoryid) {
 		$last=$this->get_last_order($categoryid);
 		if(empty($last)) return 1;
 		else return $last+1;
 	}
 	function get_order($contextid,$sortorder) {
 		global $CFG;
		$sql ="SELECT id,sortorder  FROM {$CFG->prefix}enrol_unasus_info_field WHERE contextid=$contextid AND sortorder=$sortorder";
		$r=get_record_sql($sql);
		return $r;
 	}
 	
 	function count_field_by_contextid() {
 		global $CFG;
		$sql ="SELECT COUNT(id) AS value,contextid  FROM {$CFG->prefix}enrol_unasus_info_field GROUP BY contextid ";
		$r=get_records_sql($sql);
		return $r;
 	}
 	function get_contextid_registred(){
 		$context_value = array();
 		$rows=$this->count_field_by_contextid();
 		if(is_array($rows)){
 			 foreach ($rows as $row){
 		  	$context_value[$row->contextid]=$row->value;
 		  }
 		}
 		 
 	return 	$context_value ;
	}
 	function get_general_html_option_yes_not(){
 		$options = array();
 		$options['']='----';
		$options[1]=get_string('yes','enrol_unasus');
		$options[0]=get_string('not','enrol_unasus');
		return 	$options ;
	}
	function get_general_html_option_yes_not_filter(){
 		$options = array();
 		$options['']='----';
		$options[1]=get_string('yes','enrol_unasus');
		$options[2]=get_string('not','enrol_unasus');
		return 	$options ;
	}
	function create_form_text($field,$mform){
	   if(!empty($field)){
	   	   if($field->visible){
	   	   	
	   		   $mform->addElement('text', $field->id, $field->name);
       			$mform->setType($field->id, PARAM_TEXT);
      			$mform->setDefault($field->id,$field->defaultdata);
      			
      			 if($field->required){
      			 	$mform->addRule($field->id, get_string('requiredfield','enrol_unasus'), 'required', null, 'cliente');
      			 }
	   	   }
	   }
	 
	}
	function create_form_textarea($field,$mform){
	   if(!empty($field)){
	   	   if($field->visible){
	   		   $mform->addElement('textarea', $field->id, $field->name,'wrap="virtual" rows="7" cols="40"');
       			$mform->setType($field->id, PARAM_TEXT);
      			$mform->setDefault($field->id,$field->defaultdata);
      			
      			 if($field->required){
      			 	$mform->addRule($field->id, get_string('requiredfield','enrol_unasus'), 'required', null, 'cliente');
      			 }
	   	   }
	   }
	 
	}
	
	function create_form_menu($field,$mform){
	   if(!empty($field)){
	   	   if($field->visible){
	   		   $mform->addElement('select', $field->id, $field->name,$this->convert_menu_feild_data_to_array_for_form($field->param1));
       			$mform->setType($field->id, PARAM_TEXT);
      			$mform->setDefault($field->id,$field->defaultdata);
      			
      			 if($field->required){
      			 	$mform->addRule($field->id, get_string('requiredfield','enrol_unasus'), 'required', null, 'cliente');
      			 }
	   	   }
	   }
	 
	}
	function convert_menu_feild_data_to_array_for_form($param){
    	$paramql = preg_replace("/(\\r)?\\n/i","|_#_|",$param);
    	$p = explode("|_#_|",$paramql); 
    	$a=array();
    	$a['']="---";
			foreach ($p as $v){ 
				$a[$v]=$v;
			}
			 return $a;
    }
	function create_form_checkbox($field,$mform){
	   if(!empty($field)){
	   	   if($field->visible){
	   		   $mform->addElement('checkbox', $field->id, $field->name);
       		  $mform->setDefault($field->id,$field->defaultdata);
      	   }
	   }
	 
	}
	function create_form($categoryid,$mform){
    	 $rows = $this->get_all_by_category($categoryid);
   			if(is_array($rows)){
   				 foreach ($rows as $row){
   				 	 if($row->datatype=='text')$this->create_form_text($row,$mform);
   				 	 else if($row->datatype=='textarea')$this->create_form_textarea($row,$mform);
   				 	 else if($row->datatype=='menu')$this->create_form_menu($row,$mform);
   				 	 else if($row->datatype=='checkbox')$this->create_form_checkbox($row,$mform);
   				 }
   			
   			}
    }
    

  function get_all_in_sort_by_order($categoryid) {
 		global $CFG;
		$sql ="SELECT id,sortorder   FROM {$CFG->prefix}enrol_unasus_info_field  WHERE categoryid=$categoryid ORDER BY sortorder ";
		$rows=get_records_sql($sql);
		$result=array();
		if(empty($rows) )return $result;
		$cont=0;
		foreach ($rows as $row) {
			$cont++;
			$result[$cont]=$row;
		}
		return $result;
 	}
 	function update_order($categoryid,$seq,$operation) {
 		global $CFG;
 		$list_cat=$this->get_all_in_sort_by_order($categoryid);
 		
 		$selected_cat=$list_cat[$seq];
 		$selected_cat_ref=null;
 		print_r($list_cat);
 		if($operation=='up'){
 			$selected_cat->sortorder--;
 			$selected_cat_ref=$list_cat[$seq-1];
 			$selected_cat_ref->sortorder++;
 			update_record('enrol_unasus_info_field', $selected_cat);
 			update_record('enrol_unasus_info_field', $selected_cat_ref);
 			
 		}
		else if($operation=='down'){
			$selected_cat->sortorder++;
 			$selected_cat_ref=$list_cat[$seq+1];
 			$selected_cat_ref->sortorder--;
 			update_record('enrol_unasus_info_field', $selected_cat);
 			update_record('enrol_unasus_info_field', $selected_cat_ref);
		}
	
 	}
 }