<?php

//include '../forum_discussoes/funcoes/funcoes.php';  referencia certa, mas quebra o select das postagens

//include 'funcaoAlert.php';
$user_id=$USER->id;
?>
<div id="menu">
								 <ul>
									 <li>
								 		<a href="http://ufpe.unasus.gov.br/moodle_unasus/cursos/pcd2_semcertificacao/"><img src="../images/home.png"> Início</a>
								 		<!-- http://ufpe.unasus.gov.br/adteste/index.php?curso=ad1 ENDERECO ANTIGO-->
									 </li>
									 <li>
										 <a href="">Unidades didáticas <b class="caret"></b></a>
										 <ul>

											<li><a href="../unidade_2/index.php">Cuidados na Avaliação do Ambiente Domiciliar</a></li>
									      	<li><a href="../unidade_3/index.php">Enteroclisma</a></li>
									      	<li><a href="../unidade_4/index.php">Sondagem Vesical</a></li>
									      	<li><a href="../unidade_5/index.php">Sondagem e Nutrição Enteral</a></li>
									      	<li><a href="../unidade_6/index.php">Estomas</a></li>
									      	<li><a href="../unidade_7/index.php">Oxigenoterapia Domiciliar</a></li>
									      	<li><a href="../unidade_8/index.php">Material biológico: coleta, acondicionamento e transporte</a></li>
										 </ul>
									 </li>
								 <li><a href="../forum_discussoes/">Fórum de discussões<?php //alert_postage($user_id); ?></a></li>
						      <li><a class="inline" href="#modal_certificacao" onclick="abreModal();">Certificação</a></li>
						      <li><a class="inline" href="#modal_perguntas" onclick="abreModal();">Perguntas frequentes</a></li>
						      <li><a class="inline" href="#modal_perfil" onclick="abreModal();">Perfil e opinião</a></li>
						      <li><a class="inline" href="#modal_creditos" onclick="abreModal();">Créditos</a></li>
								 </ul>
								<div id="sair">Sair</div>
								<div id="fechar">
									<p>
									<?php
										session_start();


										if ($_SESSION['logado'] == 1) {
											
										}else{
											header('Location: http://ufpe.unasus.gov.br/moodle_unasus/login/index.php');
										}

										include('sair.php');
									?>
									</p>
								</div>

							  </div>

							  <script type="text/javascript">

							  		function abreModal () {
							  			
							  			 $(".inline").colorbox({inline:true, width:"60%"});
							  		}

							  </script>

			<!-- Certificacao-->
			<div style='display:none'>
				<div id="modal_certificacao" style='padding:10px; background:#fff;'>
					<h4>Certificação</h4>

					<br>

					<!--Para que você receba a declaração de conclusão deste módulo, é necessário fazer a avaliação  final. Recomendamos que esta seja a última etapa, depois que você tiver concluído seus estudos.  <br><br>
					A avaliação vale de zero (0,0) a dez (10,0). Para receber a declaração de conclusão do módulo é necessário tirar, pelo menos, 7,0 (sete). São permitidas 3 (três tentativas). Caso você  não atinja nota mínima 7,0 (sete), será necessário fazer o módulo novamente, se novas turmas forem lançadas.   Após atingir nota igual ou superior a 7,0 (sete), você receberá, por e-mail, um link que  permitirá a impressão do certificado. O link será encaminhado em até 72 horas após a conclusão da avaliação.   <br><br>
					Clique <a href="http://ufpe.unasus.gov.br/moodle_unasus/mod/quiz/view.php?id=21" target="_blank">aqui</a> para iniciar a avaliação final.-->
					
					Esta versão do curso está disponível para todos os interessados no assunto, porém não dá direito à certificação. <br><br>

Ou seja, você pode fazer a avaliação final para testar seus conhecimentos, clicando <a href="http://ufpe.unasus.gov.br/moodle_unasus/mod/quiz/view.php?id=33" target="_blank">aqui</a>, mas não receberá declaração de conclusão de curso. <br><br>

Se você deseja receber a declaração de conclusão do curso, participe da versão com certificação. Vale lembrar que, neste caso, o curso é exclusivo para enfermeiros, técnicos de enfermagem e auxiliares de enfermagem. <br><br>

Bons estudos! 
<br><br>
Equipe UNA-SUS UFPE

			
				<br>
				
				</div>
			</div><!-- fim -->

			<!-- Perguntas-->
			<div style='display:none'>
				<div id="modal_perguntas" style='padding:10px; background:#fff;'>
					<h4>Perguntas frequentes</h4>

					<br>

					<b>1. Tenho interesse de fazer este módulo. Onde encontro informações?</b><br>
					As informações sobre o módulo <b><i>Princípios para o cuidado domiciliar 2</i></b>, 
					bem como o link para o sistema de matrícula estão disponíveis em: <a href="http://www.unasus.gov.br/cursoAD" target="_blank">http://www.unasus.gov.br/cursoAD</a>

					<br>
					<br />
					<b>2. Não estou conseguindo receber a declaração de conclusão de curso. O que está havendo?</b><br>
					Reforçamos que o conteúdo didático do curso tem acesso público e irrestrito. No entanto, a declaração de conclusão de curso é disponibilizada apenas para enfermeiros e técnicos de enfermagem que atenderam aos requisitos para matrícula na versão do módulo com certificação.
					
					<br>
					<br />
					<b>3. Fiz a avaliação final da capacitação, mas a declaração de conclusão de curso ainda não chegou. O que aconteceu?</b><br>
					O link para acesso à declaração é enviado por e-mail no prazo de 72 horas após o término da avaliação final. Se já passou este período, recomendamos que você verifique qual foi o e-mail que cadastrou no Cadastro Nacional de Profissionais de Saúde (<a href="https://ufpe.unasus.gov.br/moodle_unasus" target="_blank">https://ufpe.unasus.gov.br/moodle_unasus</a>) e verifique, também, as caixas SPAM, lixo eletrônico ou quarentena do seu e-mail.					
					<br><br>
					Você também pode consultar a declaração na Plataforma Arouca de acordo com as orientações abaixo:<br> 
					1) Acesse o endereço: <a href="https://cnps.unasus.gov.br/cnps/Home.app" target="_blank">https://cnps.unasus.gov.br/cnps/Home.app</a> <br>
					2) Clique em "Entrar" <br>
					3) Digite seu CPF e sua senha e clique em "Autenticar" <br>
					4) Clique em "Minha Página > Meus Certificados" ou "Minha Página > Meu Histórico".  Atenciosamente,  Equipe de Suporte UFPE / UNA-SUS

					<br>
					<br />
					<b>4. É possível enviar minha declaração de conclusão de curso por e-mail?</b><br>
					Não. O link para ter acesso à declaração de conclusão da capacitação é enviado exclusivamente por e-mail. Verifique o seu e-mail, lembrando que você deve acessar o e-mail que cadastrou no Cadastro Nacional de Profissionais de Saúde. Recomendamos, ainda, que você verifique as caixas SPAM, lixo eletrônico ou quarentena do seu e-mail.
					<br><br>
					Você também pode consultar sua declaração na Plataforma Arouca de acordo com as orientações abaixo:<br>
					1) Acesse o endereço: <a href="http://arouca.unasus.gov.br" target="_blank">http://arouca.unasus.gov.br</a><br>
					2) Clique em "Entrar" <br>
					3) Digite seu CPF e sua senha e clique em "Autenticar" <br>
					4) Clique em "Minha Página > Meus Certificados" ou "Minha Página > Meu Histórico".  Atenciosamente,  Equipe de Suporte UFPE / UNA-SUS

					<br>
					<br />
					<b>5. Gostaria de mais informações sobre o Programa Multicêntrico de Qualificação Profissional em Atenção Domiciliar a Distância. Onde encontro?</b><br>
					Você pode consultar as informações no link: <a href="http://www.unasus.gov.br/cursoAD" target="_blank">http://www.unasus.gov.br/cursoAD</a>

					<br>
					<br />
					<b>6. Como posso ter acesso aos módulos Princípios para o cuidado domiciliar 1 e Princípios para o cuidado domiciliar 3?</b><br>
					Esses módulos estão sob responsabilidade de outras universidades. Assim que estiverem disponíveis, serão divulgados no link: <a href="http://www.unasus.gov.br/cursoAD" target="_blank">http://www.unasus.gov.br/cursoAD</a>

			
				<br>
				
				</div>
			</div><!-- fim -->

			<!-- Opiniao-->
			<div style='display:none'>
				<div id="modal_perfil" style='padding:10px; background:#fff;'>
					<h4>Perfil e opinião</h4>

					<br>
				Gostaríamos de conhecer o seu perfil tecnológico e a sua opinião sobre o curso.  <br>
				Clique <a href=" http://ufpe.unasus.gov.br/moodle_unasus/mod/feedback/view.php?id=25" target="_blank">aqui</a> para responder ao questionário.


			
				<br>
				
				</div>
			</div><!-- fim -->

			<!-- Creditos-->
			<div style='display:none'>
				<div id="modal_creditos" style='padding:10px; background:#fff;' style="font-size: 14">
					<h4>Créditos</h4>
						<br>
						<b>© 2014. Ministério da Saúde. Secretaria de Atenção à Saúde.  Departamento de Atenção Básica. 
						<br>Coordenação Geral de Atenção Domiciliar.</b><br><br>
Todos os direitos reservados. É permitida a reprodução parcial ou total desta obra, desde que citada a  fonte e que não seja para venda ou qualquer fim comercial. A responsabilidade pelos direitos autorais  de textos e imagens desta obra é de responsabilidade da área técnica.<br>
						<br>

				<div id="left">
				
					<b>Curso online Princípios para o cuidado domiciliar 2</b><br>
					Primeira oferta: junho de 2014 <br><br>

					<b>Elaboração, difusão e informações</b><br>
					Universidade Aberta do Sistema Único De Saúde (UNA-SUS)<br>
					Secretaria de Gestão do Trabalho e da Educação Na Saúde (SGTES) <br>
					Secretaria de Atenção à Saúde – Departamento de Atenção Básica – Coordenação Geral de Atenção Domiciliar<br><br>

					<b>UNA-SUS | Universidade Federal de Pernambuco </b><br>
					Coordenação Geral: Cristine Martins Gomes de Gusmão <br>
					Coordenação Técnica: Josiane Lemos Machiavelli <br>
					Coordenação de Educação a Distância: Sandra de Albuquerque Siebra <br>
					Gerente de Projetos: Júlio Venâncio de Menezes Júnior<br>
					<br>

					<i><b>Equipe de produção do curso</b></i><br><br>

					<b>Editora </b><br>
					Josiane Lemos Machiavelli (UNA-SUS UFPE)  <br><br>

					<b>Conteudistas</b><br>Geruza Pereira Gomes Almeida<br>
					Luzia Maria dos Santos <br>
					Vanuza Regina Lommez De Oliveira<br>
					<br>
					<!-- <b>Autora colaboradora</b><br>
					Patrícia Pereira da Silva (UNA-SUS UFPE)<br>
					<br> -->
					<b>Organizadora e Designer instrucional </b><br>
					Patrícia Pereira da Silva (UNA-SUS UFPE)  <br>
					<br>
					<b>Designers gráficos </b><br>
					Juliana Leal (UNA-SUS UFPE)  <br>
					Rodrigo Cavalcanti Lins (UNA-SUS UFPE)<br>
					<br />

					<b>Ilustrações</b><br />
					Juliana Leal (UNA-SUS UFPE)<br />
					Vinícius Haniere Saraiva Milfont (UNA-SUS UFPE)<br />
					<!-- <br /> -->



				</div>
				

				<div id="right">
					<!-- <b>Ilustrações</b><br />
Juliana Leal (UNA-SUS UFPE)<br />
Vinícius Haniere Saraiva Milfont (UNA-SUS UFPE)<br />
<br /> -->
<b>Produção audiovisual</b><br />
Amanda Rosineide da Silva (UFPE)<br>
Caio Lira (UFPE)<br>
Caroline Barbosa Rangel (UNA-SUS UFPE) <br>
Geraldo Luiz Monteiro do Nascimento (UNA-SUS UFPE) <br>
Jamilly Muniz de Oliveira (UFPE)<br>
Luiz Miguel Picelli Sanches (UNA-SUS UFPE)<br>
Patrícia Pereira da Silva (UNA-SUS UFPE)<br>
Rodrigo Luis da Silveira Silva (UFPE)<br>





<b><br />
Revisão linguística</b><br />
Emanuel Cordeiro da Silva (UNA-SUS UFPE)<br />
<br />
<b>Equipe de tecnologia da informação</b><br />
Lucy do Nascimento Cavalcante (UNA-SUS UFPE)<br />
Mirela Natali Vieira de Souza (UNA-SUS UFPE)

					<br />
Rodrigo Cavalcanti Lins (UNA-SUS UFPE)

<br />
Wellton Thiago Machado Ferreira (UNA-SUS UFPE)



<br />
<br />
<b>Equipe de ciência da informação </b><br />
Vildeane da Rocha Borba (UNA-SUS UFPE)

<br />
Jacilene Adriana da Silva Correia (UNA-SUS UFPE)



<br />
<br />
<b>Equipe de supervisão acadêmica </b><br />
Fabiana de Barros Lima (UNA-SUS UFPE)

<br />
Geisa Ferreira da Silva (UNA-SUS UFPE)

<br />
Isabella Maria Lorêto da Silva (UNA-SUS UFPE)

<br />
Rosilândia Maria da Silva (UNA-SUS UFPE)


<br />
<br />
<b>Colaboradores na validação externa do curso </b><br />
Leonardo Savassi (Secretaria Executiva da UNA-SUS)

<br>

Lina Sandra Barreto Brasil (Secretaria Executiva da UNA-SUS) 

<br />
Mariana Borges Dias (Coordenação Geral de Atenção Domiciliar)

<br />
Mara Lúcia Renostro Zachi (Secretaria Executiva da UNA-SUS)


				</div>


			
				<br>
				
				</div>
			</div><!-- fim -->


			<style type="text/css">

				#right{
					width: 48%;
					float: right;
					font-size: 12px !important;
					margin-left: 10px; 

				}

				#left{
					width: 46%;
					float: left;
					font-size: 12px !important;

				}

				#modal_creditos b{
					font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif; !important;
				}

			</style>
<?php //alert_postage($user_id) ?>