<?php
               
    $pg_atual=8;
    registro($id,unid5_pg,$pg_atual,unid5_ev);
?>
     <style type="text/css">
			#c1,#c5{
			
				background-color: rgb(192, 247, 176);
			
			}
			
			#c2, #c3, #c4, #c6, #c7, #c8, #c9, #c10{
				background-color: #8BB47F;
				
			}
			
			#num_tab1 ul{
				width: 186px;
				display: block;
				height: 20px;
				margin-left: 21px;
			
			}
			
			
			#num_tab1 ul li{

				float: left;
				width: 40px;
				border: 1px solid #fff;
				border-radius:  0px 0px 10px 10px;
				text-align: center;
				border-top: none;
				cursor: pointer;
			
			
			}
			
			
			.table{
				margin-bottom: 0px;
			}
			
			.box_table.complicacoes{
				width: 200px;
			
			}
			
			.box_table.complicacoes2 {
				width: 280px !important;
			
			}
      #lista li{
      list-style-type: disc;
       margin-left: 30px;
    }
  
	</style>
   
	<script src = "http://www.youtube.com/player_api"></script>
   <h4 class="titulo">Intercorrências </h4>
   <br>
	Para uma abordagem holística 
    e humana, é essencial, no cuidado de pessoas, que na prática assistencial se 
    considere a segurança do paciente. Esta deve ser influenciada por diversos 
    fatores, que incluem o conhecimento da técnica e da legislação que a rege, a 
    estrutura, a organização e as condições de trabalho. 
	Sendo assim, é importante a 
    avaliação sistemática de fatores que interferem na prevenção de riscos 
    relacionados às ações executadas durante o cuidado, evitando complicações 
    durante a sondagem e a permanência da mesma no paciente. Veja mais detalhes na figura a seguir
    (DREYER et al, 2011; BRASIL, 2013).
	<br/><br/>
	<b>Complicações  da sonda e como evitar</b><i class=" icon-hand-up" title="Esse quadro possui interação."></i>
  <br>


<div class="tabContainer" >
  <ul class="digiTabs" id="sidebarTabs">
    <li  id="tab1" class="selected"  onclick="tabs(this);">Complicação 1</li>
    <li id="tab2" onclick="tabs(this);">Complicação 2</li>
    <li id="tab3"  onclick="tabs(this);">Complicação 3</li>
    <li id="tab4"  onclick="tabs(this);">Complicação 4</li>
  </ul>
  <div id="tabContent">
    <b>Risco de aspiração traqueal ou nasotraqueal por mal posicionamento da sonda.</b>
<br> <br>
O que fazer?
<br><br> 
Para evitar a complicação, é altamente recomendado 
        a realização de testes de posicionamento da sonda. Assim, você saberá o 
        real posicionamento dela. A sonda deve ser fixada corretamente a fim de 
        evitar mudança de trajeto devido ao peristaltismo. 
<br><br><b>Fonte:</b> (POTTER; PERRY, 2009; NETTINA, 2003).
  </div>

</div>
 
<div id="tab1Content" style="display:none;">
<b>Risco de aspiração traqueal ou nasotraqueal por mal posicionamento da sonda</b>
<br> <br>
O que fazer?
<br><br> 
Para evitar a complicação, é altamente recomendado 
        a realização de testes de posicionamento da sonda. Assim, você saberá o 
        real posicionamento dela. A sonda deve ser fixada corretamente a fim de 
        evitar mudança de trajeto devido ao peristaltismo. 
        <br><br><b>Fonte:</b> (POTTER; PERRY, 2009; NETTINA, 2003).
</div>

<div id="tab2Content" style="display:none;">
<b>Obstrução</b>
<br> <br>
O que fazer?
<br><br> 
A obstrução da sonda interfere diretamente no fluxo adequado da administração da dieta enteral. Para evitar a obstrução, a sonda deve ser higienizada após a administração da dieta e até mesmo de medicamentos. Para a higienização da sonda, injete, com o auxílio de uma seringa, água filtrada ou fervida à temperatura ambiente pela sonda, até que a desobstrua. A água deve ser administrada lentamente para evitar que a sonda fissure devido à pressão da água. 
<br><br><b>Fonte:</b> (POTTER; PERRY, 2009; NETTINA, 2003).
</div>
<div id="tab3Content" style="display:none;">
  <b>Irritação e lesão das mucosas</b>
<br> <br>
O que fazer?
<br><br> 
A correta fixação da sonda evita tracionamento e compressão da aba da narina. A falta de oxigenação nesta região pode levar à necrose. Portanto, deve-se realizar higiene diariamente, mantendo as mucosas nasais sempre limpas. 
<br><br><b>Fonte:</b> (POTTER; PERRY, 2009; NETTINA, 2003).
</div>
<div id="tab4Content" style="display:none;">
  <b>Saída acidental</b>
<br> <br>
O que fazer?
<br><br> 
Neste caso, a repassagem da sonda deve ser feita por uma pessoa capacitada. Sendo assim, deve-se entrar em contato o médico ou enfermeiro da equipe multiprofissional. Oriente o cuidador a nunca tentar realizar este procedimento. 
<br><br><b>Fonte:</b> (POTTER; PERRY, 2009; NETTINA, 2003).
</div>


<style>
  .tabContainer{margin:10px 0;width:760px;font-size: 14px;}
  .tabContainer .digiTabs{font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;list-style:none;display:block;overflow:hidden;margin:0;padding:0px;position:relative;top:1px;}
  .tabContainer .digiTabs li{font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;float:left;background-color:#b5b5b5;border:1px solid #e1e1e1;padding:5px!important;cursor:pointer;border-bottom:none;margin-right:10px;font-family:verdana;font-size:.8em;font-weight:bold;color:#fff;border-top-left-radius: 6px;border-top-right-radius: 6px;}
  .tabContainer .digiTabs .selected{font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;background-color:#fff;color:#393939;}
  #tabContent{font-size:14px; padding:10px;background-color:#F6F6F6;overflow:hidden;float:left;margin-bottom:10px;border-top: 1px solid #E1E1E1;border-bottom: 1px solid #E1E1E1;border-left: 1px solid #E1E1E1;border-right: 1px solid #E1E1E1;width:93%;background-color: #fff;border-top-right-radius: 6px;border-bottom-left-radius: 6px;border-bottom-right-radius: 6px;}
</style>
<script type="text/javascript">
  function tabs(x)
  {
    var lis=document.getElementById("sidebarTabs").childNodes; //gets all the LI from the UL
 
    for(i=0;i<lis.length;i++)
    {
      lis[i].className=""; //removes the classname from all the LI
    }
    x.className="selected"; //the clicked tab gets the classname selected
    var res=document.getElementById("tabContent");  //the resource for the main tabContent
    var tab=x.id;
    switch(tab) //this switch case replaces the tabContent
    {
      case "tab1":
        res.innerHTML=document.getElementById("tab1Content").innerHTML;
        break;
 
      case "tab2":
        res.innerHTML=document.getElementById("tab2Content").innerHTML;
        break;
      case "tab3":
        res.innerHTML=document.getElementById("tab3Content").innerHTML;
        break;
        case "tab4":
        res.innerHTML=document.getElementById("tab4Content").innerHTML;
        break;
      default:
        res.innerHTML=document.getElementById("tab1Content").innerHTML;
        break;
 
    }
  }
 
</script>

  <br/>  
  <br/>
  <br/>  


  <!-- Tabs 2 -->
<b>Complicações  da dieta e o que fazer para evitar</b><i class=" icon-hand-up" title="Esse quadro possui interação."></i>
<div class="tabContainer2" >
  <ul class="digiTabs2" id="sidebarTabs2">
    <li  id="tab5" class="selected"  onclick="tabs2(this);">Complicação 1</li>
    <li id="tab6" onclick="tabs2(this);">Complicação 2</li>
    <li id="tab7"  onclick="tabs2(this);">Complicação 3</li>
    <li id="tab8"  onclick="tabs2(this);">Complicação 4</li>
     <li id="tab9"  onclick="tabs2(this);">Complicação 5</li>
     <li id="tab10"  onclick="tabs2(this);">Complicação 6</li>
  </ul>
  <div id="tabContent_">
    <b>Desconforto e distensão abdominal</b>
<br> <br>
O que fazer?
<br><br> 
Para evitar, deve-se realizar a infusão da dieta com o paciente em posição de 
Fowler ou sentado, quando permitido. Respeitar a velocidade de infusão de 20ml 
por minuto.
<br><br><b>Fonte:</b> (POTTER; PERRY, 2009; NETTINA, 2003).
  </div>
</div>

<div id="tab5Content" style="display:none;">

 <b>Desconforto e distensão abdominal</b>
<br> <br>
O que fazer?
<br><br> 
Para evitar, deve-se realizar a infusão da dieta com o paciente em posição de 
Fowler ou sentado, quando permitido. Respeitar a velocidade de infusão de 20ml 
por minuto.

<br><br><b>Fonte:</b> (POTTER; PERRY, 2009; NETTINA, 2003).
</div> 

<div id="tab6Content" style="display:none;">

<b>Nauseas e vômitos</b>
<br> <br>
O que fazer?
<br><br> 
Deve-se tomar cuidado para que o paciente não aspire as secreções eliminadas e 
tenha complicações respiratórias. Além disso:
<ul id="lista">
  <li>verifique se o paciente está bem posicionado, se a sonda está na localização 
    correta e não se esqueça de manter o paciente em posição de Fowler durante a 
    administração da dieta e por 20 a 30 minutos após o término da dieta; </li>
  <li>
    diminua a velocidade de gotejamento;
  </li>
  <li>
verifique a temperatura da dieta antes de sua administração. Dietas quentes 
ou geladas podem levar ao desconforto e ocasionar vômitos. 
  </li>
  </ul>
<br><br><b>Fonte:</b> (POTTER; PERRY, 2009; NETTINA, 2003).
</div>

<div id="tab7Content" style="display:none;">

 <b>Cólica abdominal</b>
<br> <br>
O que fazer?
<br><br> 
Avaliar as causas, como dietas laxativas, infusão de dieta com grande volume, ou temperatura gelada.
<br><br><b>Fonte:</b> (POTTER; PERRY, 2009; NETTINA, 2003).
</div>
<div id="tab8Content" style="display:none;">
  <b> Distúrbio hidroeletrolítico</b>
<br> <br>
O que fazer?
<br><br> 
Avaliar quadros de diarreia ou de vômitos frequentes.
<br><br><b>Fonte:</b> (POTTER; PERRY, 2009; NETTINA, 2003).
</div>

<div id="tab9Content" style="display:none;">
  <b> Diarreia</b>
<br> <br>
O que fazer?
<br><br> 
<ul id="lista">
  A diarreia caracteriza-se pela ocorrência de fezes líquidas em grande quantidade, três vezes ao dia ou mais, gerando desconforto, perda de nutrientes e estado de má nutrição. Nestes casos: 
  <li>converse com o nutricionista sobre a indicação de alimentos mais obstipantes;</li>
  <li>diminua a velocidade de gotejamento;</li>
  <li>fique atento(a) ao tempo de preparo dos alimentos e recomendações de higiene.</li> 
</ul>
<br><br><b>Fonte:</b> (POTTER; PERRY, 2009; NETTINA, 2003).
</div>
<div id="tab10Content" style="display:none;">
 <b>Constipação intestinal</b>
<br> <br>
O que fazer?
<br><br> 
<!--<ul id="lista">
  Neste caso é importante avaliar as causas, como dietas laxativas, infusão de dieta com grande volume, ou temperatura gelada. 

  <li>converse com o nutricionista sobre a indicação de alimentos menos obstipantes;</li>
  <li>diminua a velocidade de gotejamento;</li>
  <li>fique atento(a) ao tempo de preparo dos alimentos e recomendações de higiene.</li> 
</ul>

<br><br><b>Fonte:</b> (POTTER; PERRY, 2009; NETTINA, 2003).-->

Converse com o nutricionista sobre a indicação de alimentos mais laxativos e aumente a oferta de água.
</div>



<style>
  .tabContainer2{margin:10px 0;width:760px;font-size: 12px;font-size: 12px;font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;}
  .tabContainer2 .digiTabs2{font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;list-style:none;display:block;overflow:hidden;margin:0;padding:0px;position:relative;top:1px;}
  .tabContainer2 .digiTabs2 li{font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;float:left;background-color:#b5b5b5;border:1px solid #e1e1e1;padding:5px!important;cursor:pointer;border-bottom:none;margin-right:10px;font-family:verdana;font-size:.8em;font-weight:bold;color:#fff;border-top-left-radius: 6px;border-top-right-radius: 6px;}
  .tabContainer2 .digiTabs2 .selected{font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;background-color:#fff;color:#393939;}
  #tabContent_{font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;padding:10px;background-color:#F6F6F6;overflow:hidden;float:left;margin-bottom:10px;border-top: 1px solid #E1E1E1;border-bottom: 1px solid #E1E1E1;border-left: 1px solid #E1E1E1;border-right: 1px solid #E1E1E1;width:93%;background-color: #fff;border-top-right-radius: 6px;border-bottom-left-radius: 6px;border-bottom-right-radius: 6px;}
</style>
<script type="text/javascript">
  function tabs2(x)
  {
    var lis=document.getElementById("sidebarTabs2").childNodes; //gets all the LI from the UL
 
    for(i=0;i<lis.length;i++)
    {
      lis[i].className=""; //removes the classname from all the LI
    }
    x.className="selected"; //the clicked tab gets the classname selected
    var res=document.getElementById("tabContent_");  //the resource for the main tabContent
    var tab=x.id;
    switch(tab) //this switch case replaces the tabContent
    {
      case "tab5":
        res.innerHTML=document.getElementById("tab5Content").innerHTML;
        break;
 
      case "tab6":
        res.innerHTML=document.getElementById("tab6Content").innerHTML;
        break;
      case "tab7":
        res.innerHTML=document.getElementById("tab7Content").innerHTML;
        break;
        case "tab8":
        res.innerHTML=document.getElementById("tab8Content").innerHTML;
        break;
         case "tab9":
        res.innerHTML=document.getElementById("tab9Content").innerHTML;
        break;
          case "tab10":
        res.innerHTML=document.getElementById("tab10Content").innerHTML;
        break;
      default:
        res.innerHTML=document.getElementById("tabContent_").innerHTML;
        break;
 
    }
  }
 
</script>

  <!-- fim Tabs2 -->

<b class="espaco_tab">.</b>
<br>
<br>



<style type="text/css">
.espaco_tab{
  color: #f6f6f6;
}
.um {
  width: 200px;

 
  
  background-color: #C6E6BD;
  padding: 10px;


}
.dois {
  width: 66px;

  background-color: #C6E6BD;
  padding: 10px;

  display: none;

}
.tres{
   width: 200px;

  
  display: none;
  background-color: #C6E6BD;
  padding: 10px;

}
.quatro{
   width: 200px;

  
  display: none;
  background-color: #C6E6BD;
  padding: 10px;

}

.cinco{


  background-color: #C6E6BD;
  padding: 10px;


}


.seis{

  
  display: none;
  background-color: #C6E6BD;
  padding: 10px;


}


.sete{

  
  display: none;
  background-color: #C6E6BD;
  padding: 10px;



}

.oito{

  
  display: none;
  background-color: #C6E6BD;
  padding: 10px;


}

.nove{

  
  display: none;
  background-color: #C6E6BD;
  padding: 10px;


}

.dez{

  
  display: none;
  background-color: #C6E6BD;
  padding: 10px;



}



.um_descricao{
  background-color: #E0F5DA;
  padding: 10px;

}
.dois_descricao{
  display: none;
  background-color: #E0F5DA;
  padding: 10px;

}
.tres_descricao{
  display: none;
  background-color: #E0F5DA;
  padding: 10px;

}
.quatro_descricao{
  display: none;
  background-color: #E0F5DA;
  padding: 10px;

}
.cinco_descricao{
  background-color: #E0F5DA;
  padding: 10px;

}
.seis_descricao{
  display: none;
  background-color: #E0F5DA;
  padding: 10px;

}
.sete_descricao{
  display: none;
  background-color: #E0F5DA;
  padding: 10px;

}
.oito_descricao{
  display: none;
  background-color: #E0F5DA;
  padding: 10px;

}
.nove_descricao{
  display: none;
  background-color: #E0F5DA;
  padding: 10px;

}
.dez_descricao{
  display: none;
  background-color: #E0F5DA;
  padding: 10px;

}


</style>
<script type="text/javascript">

function change(id){

			var nome ;

			if(id == 1){
				nome = "um";
			}else if(id == 2){
				nome = "dois";
			}else if(id == 3){
				nome = "tres";
			}else if(id == 4){
				nome = "quatro";
			}

		
			
			
			for(var i = 0; i<5; i++){	
				$("#c"+ i).css("background-color", "#8BB47F");
			}
			$("#c" + id).css("background-color", "#C0F7B0");
			

			
			if ($("." + nome).is(":hidden")) {
				
					
				

				$('.dois').fadeOut("1");
				$('.dois_descricao').fadeOut("1");
				$('.um').fadeOut("1");
				$('.um_descricao').fadeOut("1");
				$('.tres').fadeOut("1");
				$('.tres_descricao').fadeOut("1");
				$('.quatro').fadeOut("100");
				$('.quatro_descricao').fadeOut("1",function() {
				$("." + nome).fadeIn("3000");
				$("." + nome + "_descricao").fadeIn("3000");
  
  
          });
		  
			
				
			}
			
			
}

function change2(id){

			var nome ;

			if(id == 5){
				nome = "cinco";
			}else if(id == 6){
				nome = "seis";
			}else if(id == 7){
				nome = "sete";
			}else if(id == 8){
				nome = "oito";
			}else if(id == 9){
				nome = "nove";
			}else if(id == 10){
				nome = "dez";
			}
			
			for(var i = 5; i<11; i++){	
				$("#c"+ i).css("background-color", "#8BB47F");
			}
			$("#c" + id).css("background-color", "#C0F7B0");


	
		
			if ($("." + nome).is(":hidden")) {
				$('.cinco').fadeOut("1");
				$('.cinco_descricao').fadeOut("1");
				$('.seis').fadeOut("1");
				$('.seis_descricao').fadeOut("1");
				$('.sete').fadeOut("1");
				$('.sete_descricao').fadeOut("1");
				$('.oito').fadeOut("1");
				$('.oito_descricao').fadeOut("1");
				$('.nove').fadeOut("1");
				$('.nove_descricao').fadeOut("1");
				$('.dez').fadeOut("1");
				$('.dez_descricao').fadeOut("",function() {
				$("." + nome).fadeIn("3000");
				$("." + nome + "_descricao").fadeIn("3000");
  
  
          });
		  
			
				
			}
			
			
}




</script>

	
