/*Alternativas do verdadeiro/falso*/
	var opcao1 = 'I) Caso sejam utilizadas as fichas de papel, as mesmas serão digitadas no sistema com instalação CDS, e, da própria UBS, os dados podem ser enviados para outra máquina com o sistema PEC instalado, e esse sim irá enviar os dados para o Centralizador ou diretamente para o Ministério da Saúde.';
	var opcao2 = 'II) Se a UBS não tiver outra máquina com o PEC instalado, os dados das fichas de papel digitados no CDS serão enviados para o Centralizador e, posteriormente, para o SISAB.';
	var opcao3 = 'III) Sem internet na Unidade Básica de Saúde, não é possível utilizar o PEC, mesmo que os dados sejam descritos em fichas e, posteriormente, sejam digitados em uma máquina que possua PEC instalado e acesso à internet.';

	/*array de variaveis que vai para a funcao geradora*/
	var entrada = [opcao1, opcao2, opcao3];
	/*variavel com o name do formulario. Deve ser único na mesma página*/
	var name = 'unid3_ex2';
	/*gabarito da questao.*/
	var gabarito = '(\'V\', \'V\', \'F\')';
	/*funcao que gera o html*/
	gerarInputVF(entrada, name, gabarito, 'unid3_ex2',12); 
