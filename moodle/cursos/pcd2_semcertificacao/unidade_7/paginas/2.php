<?php
               
    $pg_atual=2;
     registro($id,unid7_pg,$pg_atual,unid7_ev);
?>

<style type="text/css">

#lista li{
            list-style-type: disc;   
       margin-left: 30px;

        }


</style> 

   <h4 class="titulo">Responsabilidades da equipe na execução do procedimento</h4>

 A assistência domiciliar oferece à equipe multiprofissional uma oportunidade de 
 demonstrar a importância de seu papel como profissionais de competência que 
 desempenham suas funções com base no conhecimento científico. 
<br><br>
    As intervenções no contexto do cuidado domiciliar devem seguir as práticas 
    de um modelo de assistência à saúde que engloba a técnica, a ciência e a 
    orientação, no intuito de promover o desenvolvimento físico, mental, social 
    e cultural do paciente. Portanto, devemos sempre orientar o paciente, o 
    cuidador e a família quanto aos riscos e cuidados na oxigenoterapia domiciliar. 
    <br><br>
    Na oxigenoterapia familiar é de responsabilidade da equipe de atenção domiciliar e/ou da empresa fornecedora de oxigênio (BRASIL, 2012):
    <ul id="lista">
    	<li>instalar, no domicílio do paciente, os equipamentos necessários para a oxigenoterapia;</li>
    	<li>orientar e treinar o paciente, cuidador ou familiar quanto ao uso do equipamento;</li>
    	<li>realizar a manutenção preventiva mensal do equipamento;</li>
    	<li>garantir a manutenção corretiva ou substituição dos equipamentos 24 horas/dia, todos os dias da semana;</li>
    	<li>atender ao chamado para avaliação de problemas no equipamento sempre que necessário, em no máximo 2 horas.</li>	
    </ul>
    <br><br>
        
    <div class="box">
       <img src="../images/img_atencao_ad.png" alt="Atenção" >
        <span class="titulo_box">Atenção!</span>
        <br>
        <hr/>
 Vale recordar que é de responsabilidade do médico determinar a forma de administração do O<sub>2</sub>, mediante avaliação e reavaliações por meio de uma prescrição detalhada que inclua (SANTOS, 2010; SOCIEDADE BRASILEIRA DE PNEUMOLOGIA E TISIOLOGIA, 2000):
<ul id="lista">
<li>o fluxo de oxigênio a ser usado em repouso, durante o sono e em atividades;</li>
<li>a fonte de oxigênio (se portátil, concentrador ou cliindros);</li>
<li>o tipo de cateter a ser usado.</li>
</ul>
</div>
