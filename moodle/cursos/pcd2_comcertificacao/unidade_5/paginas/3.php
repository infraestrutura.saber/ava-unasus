﻿<?php
               
    $pg_atual=3;
    registro($id,unid5_pg,$pg_atual,unid5_ev);
?>
     <style type="text/css">
		#lista li{
			list-style-type: disc;
            margin-left: 30px;
		}
	
	</style>
   <h4 class="titulo" style="margin-top: -15px;">Cuidados no manuseio dos equipamentos</h4>
   <br>
   É importante ressaltar que o ambiente domiciliar difere das condições hospitalares. 
   Logo, o conhecimento das rotinas domiciliares é fundamental para que a equipe 
   multiprofissional realize os cuidados propostos na terapia nutricional.<br><br> Isso 
   contribuirá para a melhor efetividade do atendimento dentro da realidade familiar 
   que o paciente se encontra inserido. Sendo assim, para o manuseio dos utensílios 
   e equipamentos, é necessário seguir as práticas de higiene desde a sua manipulação 
   até a sua administração, tais como (DOVERA, 2007; POTTER; PERRY, 2009):  <br><br>
	<ul id="lista">
		<li>a dieta deverá ser conservada e acondicionada entre 2º a 8ºC, tanto as industrializadas quanto as artesanais; </li>
		<li>retirar a dieta da geladeira uma hora antes da sua administração, para que esteja na temperatura ambiente;</li>
		<li>certificar-se do correto posicionamento da sonda enteral através da ausculta gástrica, antes de administrar a dieta;</li>
		<li>lavar as mãos antes e após o procedimento; friccionar durante 30 segundos com álcool a 70%;</li>
		<li>observar as características dos frascos e das dietas (higiene, conservação);</li>
        <li>fornecer materiais para que o cuidador faça a troca dos frascos e equipos conforme padronização do SAD;</li>
		<li>trocar a fixação das sondas sempre que necessário;</li>
		<li>realizar limpeza da pele para retirada de sujidades e excesso de oleosidade para melhor fixação;</li>
		<li>promover higiene oral e das narinas 4 vezes ao dia;</li>
		<li>monitorar a pele próxima à inserção das sondas e da fixação;</li>
		<li>lavar a sonda utilizando <b style="font-family: 'Verdana ; font-size: 12px;">seringa de 20 ml</b> água filtrada após as medicações e dietas;</li>
        <li>a troca de sondas e equipos devem ser realizadas em caso de obstrução ou desgaste mediante avaliação da equipe do SAD e/ou de acordo com o protocolo municipal.</li>
		
	</ul>
	<br/>
	<div class="box">
        <img src="../images/img_vocesabiaque_ad.png" alt="Você sabia" >
        <span class="titulo_box">Você sabia que...</span>
        <br>
        <hr/>
        O enfermeiro, ao introduzir a Sonda Nasogástrica (SNG) no domicílio, deve iniciar a
         dieta somente após realizar os testes de ausculta e da aspiração gástrica, pois 
         estes testes permitem certificar-se do posicionamento da SNG. No caso da Sonda 
         Nasoenteral (SNE), o único meio de se certificar quanto à localização desta 
         é através da realização do exame de RX de abdome, para visualizar a 
         localização da ponta radiopaca desta sonda, para então iniciar ou não a 
         dieta com segurança para o paciente (BRASÍLIA, 2011).<br><br>
         <b>Tipos de sondagem</b><i class=" icon-hand-up" title="Esse quadro possui interação."></i>
         <div id="base">
         <table class="table table-bordered" >
            <tr  style="background-color:#D9E4D4">
            <td align="center" >

                <img class="base" src="images/Fig01.2_sondanasograstrica.jpg" alt="Tipo de sondagem 1" class="um_descricao">
            </td>
            <td ><img src="images/Fig01.1_sondanasograstrica.jpg" alt="Tipo de sondagem 2" class="dois_descricao"></td>
            </tr>
             <tr style="background-color:#D9E4D4">
                <td class="um"><b>Sondagem nasoenteral</b>:<br> é passada pelo nariz e vai até o intestino delgado.</td>
                <td class="dois"><b>Sondagem nasogástrica</b>:<br> é passada pelo nariz e vai até o estômago.</td>
            </tr>
            
           
    	</table>
		 <div class="um_action"></div>
        <div class="dois_action"></div>
        </div>
       
    	
        </div>


        <style type="text/css">
        #base{position: relative;}
        .um{
           
            color: #D9E4D4;
        }
        .dois{
          
          color: #D9E4D4;
        }
        .um_action{
            background-color: #0F8A44;
            position: absolute;
            height: 7px;
           top: 227px;
			left: 152px;
            width: 7px;
            z-index: 3;
            border: 2px solid #fff;
            border-radius: 30px 30px 30px 30px;
            cursor: pointer;
        }
         .dois_action{
            background-color: #0F8A44;
            position: absolute;
            height: 7px;
            top: 200px;
			left: 523px;
            width: 7px;
            z-index: 3;
            border: 2px solid #fff;
            border-radius: 30px 30px 30px 30px;
            cursor: pointer;
        }
        .dois_descricao {
            
        }
        
        </style>
        <script type="text/javascript">
        $('.um_action').click(function(){
            $('.um').css('color','#000');
        });
          $('.dois_action').click(function(){
            $('.dois').css('color','#000');
        });

        </script>


   
