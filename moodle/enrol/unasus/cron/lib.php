<?php
/*
 * Created on 17/04/2012
 *
 *Essa class tem as bibliotecas de gerenciamento
 * do do cron. 
 */
 class cron  {
	public static $ERRO_AROUCA_CONNECTION =3;
	public static $CBO_AROUCA_NOT_EXIST =4;
 	public static $ROLE_NOT_CREATED =5;
 	public static $CBO_NOT_IN_ROLE =6;
 	
 	
 	function cast_param_to_string_value($param){
    	$value="";
    	if(isset($param->erro_arouca_conn)) {$value=self::$ERRO_AROUCA_CONNECTION;}
    	
    	if(isset($param->no_cbo_arouca)) {
    		if(empty($value)){$value=self::$CBO_AROUCA_NOT_EXIST;}
    		else{$value.=",".self::$CBO_AROUCA_NOT_EXIST;}
    	}
    	if(isset($param->no_role_created)) {
    		if(empty($value)){$value=self::$ROLE_NOT_CREATED;}
    		else{$value.=",".self::$ROLE_NOT_CREATED;}
    	}
    	
    	if(isset($param->no_cbo_in_role)) {
    		if(empty($value)){$value=self::$CBO_NOT_IN_ROLE;}
    		else{$value.=",".self::$CBO_NOT_IN_ROLE;}
    	}
    	return $value;
 	}
 	function get_exist_param($param,$value){
 		
    	$pos = strstr($param, (string)$value);
    	return $pos;
    	
 	}
 	
 	function get_status_text($param,$value){
 		$actived=$this->get_exist_param($param,$value);
 		if($actived) return get_string('cron_activated','enrol_unasus');
    	else  return get_string('cron_inactive','enrol_unasus');
    	
 	}
 }