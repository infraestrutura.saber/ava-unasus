<?php

require_once $CFG->libdir.'/formslib.php';

class form_edit extends moodleform {

    function definition() {
    	global $param;
    	global $profilefield_category;
    	global $profilefield_field;
    	$options_yes_not=$profilefield_field->get_general_html_option_yes_not();
    	$options_categories=$profilefield_category->get_html_option($param->contextid);
    	$dto=$profilefield_field->get_by_id($param->id);
    	
        $mform    =& $this->_form;
        $renderer =& $mform->defaultRenderer();

        $mform->addElement('header', '', get_string('profilefield_commun_config','enrol_unasus'), '');
        $mform->addElement('hidden', 'id', $dto->id);
        $mform->setType('id', PARAM_INT);
        
       
        $mform->addElement('hidden', 'contextid', $dto->contextid);
        $mform->setType('contextid', PARAM_INT);
        
        $mform->addElement('text', 'shortname', get_string('profilefield_short_name','enrol_unasus'));
        $mform->setType('shortname', PARAM_TEXT);
       $mform->addRule('shortname', get_string('requiredfield','enrol_unasus'), 'required', null, 'cliente');
       $mform->setDefault('shortname', $dto->shortname);
       
       $mform->addElement('text', 'name', get_string('profilefieldcategory_name','enrol_unasus'));
        $mform->setType('name', PARAM_TEXT);
       $mform->addRule('name', get_string('requiredfield','enrol_unasus'), 'required', null, 'cliente');
       $mform->setDefault('name', $dto->name);
       
        $mform->addElement('textarea', 'description', get_string('profilefield_description','enrol_unasus'),'wrap="virtual" rows="7" cols="40"');
        $mform->setType('description', PARAM_TEXT);
     $mform->setDefault('description', $dto->description);
     
       $mform->addElement('select',  'required',  get_string('profilefield_required','enrol_unasus'),$options_yes_not);
       $mform->setType('required', PARAM_INT);
       $mform->setDefault('required', $dto->required);
      
        	
       $mform->addElement('select',  'visible',  get_string('profilefield_show','enrol_unasus'),$options_yes_not);
       $mform->setType('visible', PARAM_INT);
       $mform->setDefault('visible', $dto->visible);
       
         $mform->addElement('select',  'forceunique',  get_string('profilefield_unique','enrol_unasus'),$options_yes_not);
       $mform->setType('forceunique', PARAM_INT);
       $mform->setDefault('forceunique', $dto->forceunique);
       
        $mform->addElement('select',  'categoryid',  get_string('profilefield_category','enrol_unasus'),$options_categories);
       $mform->setType('categoryid', PARAM_INT);
      $mform->setDefault('categoryid', $dto->categoryid);
       
        $mform->addElement('header', '', get_string('profilefield_espcific_config','enrol_unasus'), '');
        
       $mform->addElement('text', 'defaultdata', get_string('profilefield_default_value','enrol_unasus'));
       $mform->setType('defaultdata', PARAM_TEXT);
       $mform->setDefault('defaultdata', $dto->defaultdata);
       
       $mform->addElement('text', 'param1', get_string('profilefield_size_show','enrol_unasus'));
       $mform->setType('param1', PARAM_TEXT);
       $mform->setDefault('param1', $dto->param1);
       
       $mform->addElement('text', 'param2', get_string('profilefield_size_max','enrol_unasus'));
       $mform->setType('param2', PARAM_TEXT);
        $mform->setDefault('param2', $dto->param2);
        
       $mform->addElement('select',  'param3',  get_string('profilefield_password','enrol_unasus'),$options_yes_not);
      $mform->setType('param3', PARAM_INT);
       $mform->setDefault('param3', $dto->param3);
       
        $this->add_action_buttons(true,get_string('edit','enrol_unasus'));
    }

    function validation($data, $files) {
    	global $profilefield_field;
       $errors = parent::validation($data, $files);
       if($profilefield_field->exist_edit_shortname($data['id'],$data['contextid'],$data['shortname'])){
       		$errors['shortname'] = get_string('duplication_record_db','enrol_unasus');
       }
        
     return $errors;
    }

}

?>