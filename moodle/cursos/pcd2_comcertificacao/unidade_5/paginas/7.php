<?php
               
    $pg_atual=7;
    registro($id,unid5_pg,$pg_atual,unid5_ev);
?>
     <style type="text/css">
		/*ul li{
			list-style-type: disc;
		}
*/
	
	</style>
   

   <h4 class="titulo">Métodos de administração nutricional</h4>
   <br>
	A preparação do paciente que 
	necessita de nutrição enteral deve ser iniciada precocemente. O método de 
	administração e o intervalo entre as dietas dependerão das necessidades e 
	condições clínicas do paciente. A dieta pode ser administrada através de 
	sistema fechado ou aberto, de forma contínua ou intermitente, por meio 
	<b>gravitacional</b> ou de <b>bolos</b>, conforme apresentado a seguir.
	<br/><br/>
	<b>Métodos de administração de dieta enteral</b><i class=" icon-hand-up" title="Esse quadro possui interação."></i>
	<br>
	<br>

	<table>
		<tr>
			<td><div class="um" style="diplay:none"><b>Intermitente</b></div><br></td>
			<td><div class="dois" style="diplay:none"><b>Contínua</b></div><br></td>
		</tr>
		<tr align="center">
			<td width="370px">
				
<div class="um_descricao">
	<b>Técnica de Administração</b><br>
	<b>Bolos:</b> Infusão da dieta enteral com o auxílio de uma seringa de 100 
	ml a 350 ml, a cada 2 a 6 horas. Evitar a administração muito rápida, 
	que poderá causar transtornos digestivos.<br>
	<b>Gravitacional: </b>Administração de 100 a 350 ml por gotejamento de 60ml/min, a cada 
	2 a 6h.<br><br>
	<b>Indicações</b><br>
	Indicada para pacientes com esvaziamento gástrico normal e com nutrição 
	enteral domiciliar. <br>*Mais desejável ao paciente domiciliar. <br>Permite deambulação.
	<br><br>
	<b>Custo</b><br>
	Baixo devido ao uso de materiais como equipo, seringa e/ou frasco.

</div>
			</td>
			<td>
<div class="dois_descricao">
	<b>Técnica de Administração</b><br>
	Gotejamento gravitacional ou por bomba de infusão (BI) no volume de 50 a
	 125ml/h, em um período de 12 ou 24h, no jejuno, duodeno ou estômago.<br><br>
	<b>Indicações</b><br>
	Indicada para pacientes com retardo no esvaziamento gástrico. Em desnutridos
	 graves, a alimentação deve iniciar com 0,5 cal/ml a 1 cal/ml até 2 cal/ml. 
	 <br>*Maior segurança e confiabilidade devido ao uso da bomba de infusão.
	<br><br>
	<b>Custo</b><br>
	Alto, devido ao uso da bomba de infusão e à restrição do paciente ao leito.
</div></td>
		</tr>

	</table>

<br>



<br><br>
<b>Fonte:</b> (CINTRA; NISHIDE; NUNES, 2000; DREYER et al, 2011, adaptado).

<br>
	<br/><br/>
<div class="box">
        <img src="../images/img_vocesabiaque_ad.png" alt="Você sabia">
        <span class="titulo_box">Você sabia que...</span>
        <br>
        <hr/>
      As dietas administradas de forma intermitente são consideradas mais 
      adequadas porque permitem o esvaziamento gástrico, evitando distúrbios 
      gastrointestinais e aspiração. Além disso, não exigem tecnologias complexas.
       A administração da dieta não deve ultrapassar 40 minutos (SMELTIZER; BARE 2005).
</div>
<br/>
<br>
<div class="box">
        <img src="../images/img_saibamais_ad.png" alt="Saiba mais" >
        <span class="titulo_box">Saiba mais...</span>
        <br>
        <hr/>
      Para recordar como deve ser a passagem da sonda nasoentérica no domicílio, recomendamos que você assista ao vídeo a seguir.
       <br><br>
       <table width="100%" ><td align="center"><div id="player"></div><!--<iframe id="player" width="400" height="250"   src="http://www.youtube.com/embed/VuyBL-L-A1A" onload="floaded('VuyBL-L-A1A')" frameborder="0" allowfullscreen="">
        </iframe>--></td></table>
       
<b>Fonte:</b> (COELHO, 2007).
<br/><br/>




Se você quiser mais informações para orientar os usuários e familiares em relação à nutrição enteral,
clique <a href="http://www.hc.unicamp.br/servicos/emtn/Manual_paciente.pdf" target="_blank">aqui</a>.




</div><br>


<table border="0" width="100%" cellpadding="4" cellspacing="4" align="center">
	<tr >
		<td width="450px"><h5>Como administrar a dieta</h5>Coloque o paciente sentado ou deitado, com a cabeceira da cama elevada 
			em 30 ou 45 graus. Mantenha o paciente nesta posição 20 a 30 minutos 
			após o término da injesta da dieta.<br><br>

			Inicie a administração em bolus ou gravitacional (conforme citado anteriormente).<br><br>

			Imediatamente após o término da dieta, retire o equipo e limpe a sonda com água, 
			filtrada ou fervida, em temperatura ambiente.</td>
		<td width="400px"><img src="images/Fig3-unidade5.jpg" alt="Posição do paciente">
			</td><br>
			<tr>
				
				<td></td>
			<td><b>Fonte:</b> (UNA-SUS UFPE, 2014).</td>
			</tr>
			
		
	</tr>
	
</table>
<br>

<style type="text/css">
.um_descricao,.dois_descricao , .tres_descricao {
	
}
.um {
	width: 100px;
	cursor: pointer;
	position: relative;
	text-align: center;
	background-color: #C6E6BD;
	padding: 10px;
	border-radius: 5px;
	left: 130px;

}
.dois {
	width: 75px;
	cursor: pointer;
	background-color: #AEC4A7;
	padding: 10px;
	border-radius: 5px;
	position: relative;
	left: 127px;
	text-align: center;

}
.um_descricao{
	display: none;
	background-color: #E0F5DA;
	padding: 10px;
	border-radius: 10px;
	width: 330px;
	text-align: left;
}
.dois_descricao{
	display: none;
	background-color: #C4D1C1;
	padding: 10px;
	border-radius: 10px;
	width: 330px;
	text-align: left;
}
</style>

<script type="text/javascript">

$('.um').click(function(){
	 $(".um_descricao").fadeToggle();('slow', function() {
  
				  });
				});
$('.dois').click(function(){
	 $(".dois_descricao").fadeToggle();('slow', function() {
  
				  });
				});

//onYouTubePlayerAPIReady

// 2. This code loads the IFrame Player API code asynchronously.
      var tag = document.createElement('script');

      tag.src = "https://www.youtube.com/iframe_api";
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

      // 3. This function creates an <iframe> (and YouTube player)
      //    after the API code downloads.
      var player;
      function onYouTubeIframeAPIReady() {
        player = new YT.Player('player', {
          height: '250',
          width: '400',
          videoId: 'VuyBL-L-A1A',
          events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
          }
       
        });
      }
      // 4. The API will call this function when the video player is ready.
      function onPlayerReady(event) {
        //event.target.playVideo();
      }

      // 5. The API calls this function when the player's state changes.
      //    The function indicates that when playing a video (state=1),
      //    the player should play for six seconds and then stop.
      var done = false;
      function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.PLAYING && !done) {
    $.post( "../apresentacao/functions/chamaCadVideo.php", { video: 8}, function() {
						
						});
          done = true;
        }
      }

</script>

<?php

	if ($_GET['video']==1){ 

		echo '<script type="text/javascript">$("html,body").animate({scrollTop: $("#player").offset().top}, 1500);</script>';
	}
	
?>




   
