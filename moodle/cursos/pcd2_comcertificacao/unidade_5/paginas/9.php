<?php
               
    $pg_atual=9;
    registro($id,unid5_pg,$pg_atual,unid5_ev);
?>

<!-- colorbox exercicio1 -->
<div style='display:none'>
			<div id="content_color1" style='padding:10px; background:#fff;'>

			<div id="texto_color1"></div>
			<br><br>
			<a class="btn" href="index.php?pagina=9">Tentar novamente</a>
			<a class="btn" href="index.php?pagina=0">Estudar mais</a>
			<a class="inline btn" href="#feedback1">Mostrar respostas</a>
		
			<br><br>
			
			</div>
		</div><!-- fim colorbox exercicio1-->
	 
	<div style='display:none'>
		<div id="feedback1">
			
				<h4>Respostas</h4>
				<br>
				<b>1. Verdadeiro</b><br>
				<b>2. Verdadeiro</b><br>
				<b>3. Falso</b> - A prescrição da sondagem é de competência do médico, bem como o acompanhamento do paciente, a prescrição dietética, a composição e a preparação da nutrição enteral. Ao enfermeiro cabe a passagem da sonda e a administração da dieta, podendo o técnico de enfermagem realizar tais procedimentos, desde que esteja sob a supervisão do enfermeiro.<br>
				<b>4. Falso</b> – As dietas devem ser preparadas em pias e/ou bancadas. O local deve apresentar boa iluminação e condição de higiene.<br>
				<br>
		
			<a class="inline btn" href="#content_color1">Voltar</a>
		</div>
	</div>
<!-- colorbox exercicio1 final -->
<!-- colorbox exercicio2 -->
<div style='display:none'>
			<div id="content_color2" style='padding:10px; background:#fff;'>

			<div id="texto_color2"></div>
			<br><br>
			<a class="btn" href="index.php?pagina=9">Tentar novamente</a>
			<a class="btn" href="index.php?pagina=0">Estudar mais</a>
			<a class="inline2 btn" href="#feedback2">Mostrar respostas</a>
		
			<br><br>
			
			</div>
		</div>
		<div style='display:none'>
		<div id="feedback2">
			
			
				<h4>Respostas</h4>
				<br>
				<b>a)	Correto</b><br>
				<b>b)	Errado</b> – Para a fixação da sonda, é necessário tracionar levemente a sonda nasoentérica em sentido à região temporal sem dobrá-la ou puxar a narina.<br>
				<b>c)	Errado</b> - É necessário realizar a troca da fixação da sonda enteral sempre que necessário.<br>
				<b>d)	Errado</b> - O uso de sondas por longos períodos pode ocasionar lesões e feridas no local de fixação, por isso, a avaliação da pele e da mucosa deve ser realizada diariamente.<br>
				<br><br>
		
				<a class="inline2 btn" href="#content_color2">Voltar</a>
		</div>
	</div>
<!-- fim colorbox exercicio2-->

	
   <h4 class="titulo">Exercícios</h4>
   
 <h5>Atividade 1</h5>
	  
	  <div id="ex1">
	  	<table width="100%" ><td align="center"><img src="images/maria dolores-01.jpg" alt="Exercício 1 Maria Dolores" /></td></table><br><br>
			
			<p style="text-align: justify; margin-top: 10px;">
			Maria Dolores tem 55 anos de idade, é casada, religiosa, tem três filhos e reside na periferia de Florianópolis - Santa Catarina. É aposentada, trabalhava como professora em um colégio particular. Realizou uma cirurgia para extirpação de tumor de reto há seis meses e, após alta hospitalar, foi inserida no Programa de Assistência Domiciliar. Desde então, a cada 21 dias faz quimioterapia, evoluindo com fraqueza, astenia e desnutrição acentuada. Durante a visita da equipe multiprofissional no domicílio, foi prescrita pelo médico a passagem de uma sonda nasoenteral para infusão de alimentação e indicada pela nutricionista a dieta artesanal.  </p>
			
			<p>O procedimento foi realizado pelo enfermeiro. Mediante o exposto, marque verdadeiro (V) ou falso (F) nas sentenças a seguir:</p>
			
	<table  style="width: 640px;margin-left: 45px;margin-top: 33px;margin-bottom: 20px;">
	
	
	<tr bgcolor="#118944">	
	<td><center><div class="d1"><h5 style="color:#fff;">Sentenças</h5></div> </center><center> <div class="d3"><h5 style="color:#fff;">Verdadeiro | Falso</h5></div> </center></td>
	</tr>
	
	<tr bgcolor="#CEDBCA"  >
		<td><div class="d1"><p>1. As possíveis complicações para infusão de alimentação por sonda nasoenteral podem ser: diarreia, náuseas, vômitos, cólicas e distenção abdominal.</p></div>
		<div class="d2"><p><input type="radio" name="1" value="V"></p><span class="boolean">V</span></div>
		<div class="d3"><p><input type="radio" name="1" value="F"></p><span class="boolean">F</span> </div></td>
	
	</tr>
	
	<tr bgcolor="#A5C1A1" >
		<td><div class="d1"><p>2. Promover suporte calórico, adequar as demandas metabólicas às necessidades clínicas do paciente, administrar medicamentos, líquidos e dieta são as principais finalidades da sondagem enteral.</p></div>
		<div class="d2"><p><input type="radio" name="2" value="V"></p><span class="boolean">V</span></div>
		<div class="d3"><p><input type="radio" name="2" value="F"></p><span class="boolean">F</span></div></td>
	
	</tr>
	
	<tr bgcolor="#CEDBCA" >
		<td><div class="d1"><p>3. Os seguintes procedimentos são de competência do enfermeiro: prescrição da sondagem, acompanhamento do paciente, prescrição dietética, composição e preparação da nutrição enteral. </p></div>
		<div class="d2"><p><input type="radio" name="3" value="V"></p><span class="boolean">V</span></div>
		<div class="d3"><p><input type="radio" name="3" value="F"></p><span class="boolean">F</span></div></td>
	
	</tr>
	
	<tr bgcolor="#A5C1A1">
		<td><div class="d1"><p>4. A dieta artesanal pode ser preparada em qualquer local no domicílio.</p></div>
		<div class="d2"><p><input type="radio" name="4" value="V"></p><span class="boolean">V</span></div>
		<div class="d3"><p><input type="radio" name="4" value="F"></p><span class="boolean">F</span></div></td>
	
	</tr>
	
	
		
</table>

		 <a class='inline btn' style="float: right;margin-right: 45px;" type="button" href="#content_color1" onClick="respostas(1);" >Corrigir</a>

		
</div>

<hr style="margin-top: 70px;">
<div id="ex2" style="margin-top: 10px;">
	
	<h5>Atividade 2</h5>
	
	<p style="text-align: justify;">
	Os cuidados propostos na terapia nutricional no ambiente domiciliar requerem conhecimento por parte da equipe de enfermagem quanto à realização do procedimento e manuseio adequado dos equipamentos necessários a essa terapia. Sendo assim, para uma fixação correta da sonda enteral, é correto afirmar que: 
	
	</p>
	
	<div id="op1">
	<input type="radio" name="ex2" value="a">
	<p>
	a) A fixação deve sempre ser realizada sobre a pele do paciente, utilizando-se fita adesiva hipoalergênica ou esparadrapo.
	</p>
	</div>
	
	<div id="op2">
	<input type="radio" name="ex2" value="b">
	
	<p>
	b) Para a fixação da sonda, é necessário tracionar fortemente a sonda nasoentérica em sentido à região temporal e dobrá-la puxando a narina.
</p>
</div>


<div id="op3">
<input type="radio" name="ex2" value="c">
<p>
c) É desnecessária a troca da fixação da sonda enteral.
</p>
</div>


<div id="op4">
<input type="radio" name="ex2" value="d">
<p>
d)	O uso de sondas por longos períodos pode ocasionar lesões e feridas no local de fixação, por isso, a avaliação da pele e da mucosa deve ser realizada a cada 15 dias.

</p>
</div>
	

	<a class='inline2 btn' style="float: right;margin-top: 20px;margin-right: 45px;" type="button" href="#content_color2" onClick="respostas2(2);" >Corrigir</a>
	
	

</div>



<script type="text/javascript">
   
	
    function respostas(id){

		
        var p1 = document.getElementsByName('1');
        var p2 = document.getElementsByName('2');
        var p3 = document.getElementsByName('3');
        var p4 = document.getElementsByName('4');
        var gabarito = new Array('V','V','F','F');


        var resp = new Array(p1,p2,p3,p4);
        var completo = true;


        for(var x = 0; x <5; x++){

            if(resp[x] != null){
                 if(resp[x][0].checked == true){
                           resp[x] = p1[0].value;
                }else if(resp[x][1].checked == true){
                            resp[x] = p1[1].value;

                }else{
                    completo = false;
                }
            }
        }

        if(completo){

			if(compare(resp,gabarito) == true){
				$("#texto_color1").html("<h4>Resultado</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, você acertou todas as alternativas.");
            	$(".inline").colorbox({inline:true, width:"50%"});
				$.post( "../apresentacao/functions/chamaExCerto.php", { exercicio: 5}, function() {
						
				});
            	
              
            }else{
				$("#texto_color1").html("<h4>Resultado</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>,você não acertou todas as alternativas.");
               	$(".inline").colorbox({inline:true, width:"50%",onClosed:function(){  }});
                $.post( "../apresentacao/functions/chamaExErrado.php", { exercicio: 5}, function() {
						
				});
               
            }
			
			mostrarFeedback(id);

         }else{
            $("#content_color1").html("<h4>Atenção!</h4> <br>Preencha todas as sentenças antes de clicar em corrigir.");
         	
         }
		 $(".inline").colorbox({inline:true, width:"50%",onClosed:function(){ location.reload();}});

           
}

 function compare(x, y) {
            if (x.length != y.length) {
                return false;
            }
            for (key in x) {
                if (x[key] !== y[key]) {
                    return false;
                }
            }
            return true;
        }
		
	function mostrarFeedback(id){
		document.getElementById("feedback" + id).style.display="block";
	}
	
	//-----EXERCÍCIO 2 --------//
	
	function respostas2(id){
		
		 var p1 = document.getElementsByName('ex2');
		 
		 if(p1[0].checked == true){
			
			$(".inline2").colorbox({inline:true, width:"50%",onClosed:function(){ document.getElementById("feedback" + id).style.display="block"; }});
			$("#texto_color2").html("<h4>Resultado</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, você selecionou a alternativa correta.");
			$.post( "../apresentacao/functions/chamaExCerto.php", { exercicio: 6}, function() {
						
				});
			mostrarFeedback(2);
		}else if(p1[1].checked == true){
			
			$(".inline2").colorbox({inline:true, width:"50%",onClosed:function(){ document.getElementById("feedback" + id).style.display="block"; }});
			$("#texto_color2").html("<h4>Resultado</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, você selecionou a alternativa incorreta.");
			$.post( "../apresentacao/functions/chamaExErrado.php", { exercicio: 6}, function() {
						
				});
			mostrarFeedback(2);
		}else if(p1[2].checked == true){
			
			$(".inline2").colorbox({inline:true, width:"50%",onClosed:function(){ document.getElementById("feedback" + id).style.display="block"; }});
			$("#texto_color2").html("<h4>Resultado</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, você selecionou a alternativa incorreta.");
			$.post( "../apresentacao/functions/chamaExErrado.php", { exercicio: 6}, function() {
						
				});
			mostrarFeedback(2);
		}else if(p1[3].checked == true){
			
			$(".inline2").colorbox({inline:true, width:"50%",onClosed:function(){ document.getElementById("feedback" + id).style.display="block"; }});
			$("#texto_color2").html("<h4>Resultado</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, você selecionou a alternativa incorreta.");
			$.post( "../apresentacao/functions/chamaExErrado.php", { exercicio: 6}, function() {
						
				});
			mostrarFeedback(2);
		}else{
			$("#content_color2").html("<h4>Atenção!</h4> <br><?php echo $USER->firstname; ?></b>, marque uma das alternativas!");
         	
			
		}
		$(".inline2").colorbox({inline:true, width:"50%",onClosed:function(){ location.reload();}});
	}
	
</script>

<style type="text/css">
	
		
		table tr td{
			font-size: 15px;
			border: 1px solid #f6f6f6;
		}
		.d1{

			width: 469px;
			float: left;
			margin-right: 16px;
			border-right: 1px solid #fff;
			
		}
		
		.d1 p{
			margin-top: 8px;
			margin-left: 10px;
		}
		
		.d2{
			float: left;
			padding-right: 15px;
			padding-left: 15px;
			
		
		}
		
		.d2 p{
			margin-top: 5px;
			margin-left: 10px;
		
		}
		
		.d3{
			float: left;
			padding-right: 15px;
			
			
		}
		
		.d3 p{
		
			margin-top: 5px;
			margin-left: 10px;
		}
		
		.boolean{
			margin-top: -24px;
			margin-left: 29px;
			float: right;
		}
		
				
		#feedback1, #feedback2{
			
			
		}
		
		#op1 input , #op2 input, #op3 input, #op4 input{
			float: left;
			margin-right: 10px;
			margin-left: 10px;
		}
		
</style>		

	<?php
	if ($_GET['ex']==1){ 

	}
	if ($_GET['ex']==2){ 

		echo '<script type="text/javascript">$("html,body").animate({scrollTop: $("#ex2").offset().top}, 1500);</script>';
	}
?>
