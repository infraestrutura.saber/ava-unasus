<?php

   require("../../../config.php");
      require("$CFG->dirroot/enrol/unasus/lib/context.php");
   require("$CFG->dirroot/enrol/unasus/processmode/lib.php");
   require("$CFG->dirroot/enrol/unasus/lib/configcontext.php");
   require("$CFG->dirroot/enrol/unasus/processmode/form_edit.php");

	require_login();

   //permiss�o
    require_capability('enrol/unasus:processmodeconfig', get_context_instance(CONTEXT_SYSTEM), NULL, false);
    add_to_log(1, 'enrol_unasus', 'processmodeconfig', "enrol/unasus/processmode/edit.php?contextid=$contextid", getremoteaddr());    
     
    
    $configcontext =new configcontext();
    $processmode =new processmode();
    $param=  new object();
    $param->contextid= required_param('contextid',PARAM_INT);
    $dto=$configcontext->get_by_contextid($param->contextid,configcontext::$PROCESSING_MODE);
   
    $context =new context();
    $context_name=$context->get_name($param->contextid);  
    $form= new form_edit();

        
    if ($form->is_cancelled()) {
        redirect($CFG->httpswwwroot.'/enrol/unasus/processmode/index.php') ;

    } else if ($formdata = $form->get_data()) {
    	$configcontext->edit($formdata);
    	redirect($CFG->httpswwwroot.'/enrol/unasus/processmode/index.php') ;
    }
 
  
     //Navega��o
   $navigation = build_navigation(array(array('name' => get_string('enrolname','enrol_unasus'), 'link' => "$CFG->wwwroot/enrol/unasus/index.php", 'type' => 'misc'),
                                     array('name' => get_string('processmode_config','enrol_unasus'), 'link' => "$CFG->wwwroot/enrol/unasus/processmode/index.php", 'type' => 'misc'), array('name' => $context_name, 'link' => null, 'type' => 'misc'), array('name' => get_string('processmode_config_edit','enrol_unasus'), 'link' => null, 'type' => 'misc')));

     print_header(get_string('enrolname','enrol_unasus'),get_string('applicationsenrolment','enrol_unasusem'), $navigation);
   
 	
    $form->display();
  
    print_footer();
    
?>