<?php

require_once $CFG->libdir.'/formslib.php';

class form_add extends moodleform {

    function definition() {
    	global $param;
    	global $profilefield_category;
    	global $profilefield_field;
    	$options_yes_not=$profilefield_field->get_general_html_option_yes_not();
    	$options_categories=$profilefield_category->get_html_option($param->contextid);
        $mform    =& $this->_form;
        $renderer =& $mform->defaultRenderer();

        $mform->addElement('header', '', get_string('profilefield_commun_config','enrol_unasus'), '');
        $mform->addElement('hidden', 'contextid', $param->contextid);
        $mform->setType('contextid', PARAM_INT);
        
        $mform->addElement('text', 'shortname', get_string('profilefield_short_name','enrol_unasus'));
        $mform->setType('shortname', PARAM_TEXT);
       $mform->addRule('shortname', get_string('requiredfield','enrol_unasus'), 'required', null, 'cliente');
       
       $mform->addElement('text', 'name', get_string('profilefieldcategory_name','enrol_unasus'));
        $mform->setType('name', PARAM_TEXT);
       $mform->addRule('name', get_string('requiredfield','enrol_unasus'), 'required', null, 'cliente');
       
       $mform->addElement('hidden', 'datatype', 'textarea');
       $mform->setType('datatype',  PARAM_TEXT);
       
        
       $mform->addElement('textarea', 'description', get_string('profilefield_description','enrol_unasus'),'wrap="virtual" rows="7" cols="40"');
        $mform->setType('description', PARAM_TEXT);
     
       $mform->addElement('select',  'required',  get_string('profilefield_required','enrol_unasus'),$options_yes_not);
       $mform->setType('required', PARAM_INT);
       $mform->setDefault('required', 0);
      
        	
       $mform->addElement('select',  'visible',  get_string('profilefield_show','enrol_unasus'),$options_yes_not);
       $mform->setType('visible', PARAM_INT);
       $mform->setDefault('visible', 1);
       
         $mform->addElement('select',  'forceunique',  get_string('profilefield_unique','enrol_unasus'),$options_yes_not);
       $mform->setType('forceunique', PARAM_INT);
       $mform->setDefault('forceunique', 0);
       
        $mform->addElement('select',  'categoryid',  get_string('profilefield_category','enrol_unasus'),$options_categories);
       $mform->setType('categoryid', PARAM_INT);
      
       
        $mform->addElement('header', '', get_string('profilefield_espcific_config','enrol_unasus'), '');
        
       $mform->addElement('textarea', 'defaultdata', get_string('profilefield_default_value','enrol_unasus'),'wrap="virtual" rows="7" cols="40"');
       $mform->setType('defaultdata', PARAM_TEXT);
       
       
       
        $this->add_action_buttons(true,get_string('save','enrol_unasus'));
    }

    function validation($data, $files) {
    	global $profilefield_field;
       $errors = parent::validation($data, $files);
       if($profilefield_field->exist_create_shortname($data['contextid'],$data['shortname'])){
       		$errors['shortname'] = get_string('duplication_record_db','enrol_unasus');
       }
        
        
         
     return $errors;
    }

}

?>