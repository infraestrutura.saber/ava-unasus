<?php
               
    $pg_atual=7;
     registro($id,unid6_pg,$pg_atual,unid6_ev);
?>
    
	<style type="text/css">
		#tab_1{
			width: 440px;
			height: 273px;
			margin-left: 141px;
			clear: both;
			margin-top: 280px;
		}
		
		.bg{
			background-color: #F68C23;
			padding: 7px;
			color: #fff;
			padding-left: 88px;
		}
		
		#p1{
			width: 207px;
			height: 186px;
			background-color: #FFF0E2;
			padding: 13px;
			float: left;
			font-size: 15px;
			border: 1px solid #F68C23;
		
		}
		
		#p1 h4{
			margin-left: 70px;
			
		
		}
		
		#p2{
			width: 160px;
			height: 186px;
			background-color: #FFF0E2;
			padding: 13px;
			float: right;
			font-size: 15px;
			border: 1px solid #F68C23;			
		}
		
		#p2 h4{
			margin-left: 50px;
			
		}
		
		#lista{
            list-style-type: disc;  
            margin-left: 30px;
        }
        .tabela{
        	width: 400px;
        	margin-left: 154px;
        }
        .fontes{
        	margin-left: 154px;	
        }
        .center{
      	margin-left: 87px;
      }
	
	</style>
   
  
   
   <h4 class="titulo">Estoma de Eliminação</h4>
   <h5>Ileostomia</h5><br/>
		<span style="margin-left: 400px;"><b>Estoma realizado no intestino delgado</b>
</span>
		<ul style="float: left;width: 355px;">
		É uma intervenção cirúrgica realizada no intestino delgado que consiste na exteriorização de um 
		segmento intestinal através da parede abdominal, criando, assim, uma abertura artificial para a 
		saída do conteúdo fecal inicialmente líquido, de alta drenagem, e posteriormente evoluindo para 
		fezes pastosas. É indicada em (ROCHA, 2011):
		<li id="lista">carcinoma de cólon e reto;</li>
		<li id="lista">fístulas enterocutâneas;</li>
		<li id="lista">proteção de anastomoses de alto risco: íleoanais.</li>
		</ul>
	<img style="float:right" src="images/Fig08_iliestomia.jpg" alt="Ileostomia">
	<span style="position: absolute;top: 559px;right: 438px;"><b>Fonte:</b> (UNA-SUS UFPE, 2014) <!--(UFPE, 2014)-->.</span>
  <br>
   
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

   <table class="table table-bordered tabela " width="306px" style="background-color:#ffe4ca">
   	<tr>
   		<td style="background-color:#ccc"  colspan="2" ><b class="center">Complicações da Ileostomia</b></td>
   		
   	</tr>	
   	<tr>
   		<td><b>Precoce</b></td>
   		<td><b>Tardia</b></td>
   	</tr>	
   		<tr>
   		<td style="width: 178px;">Sangramento<br>
			Infecção da ferida<br>
			Edema<br>
			Dermatite peri-estomal<br>
			Isquemia ou necrose da
			alça exteriorizada<br>
			Disfagia<br></td>
   		<td >Estenose e obstrução<br>
			Prolapso<br>
			Hérnia para-estomal<br>
			Fístulas<br></td>
   	</tr>	
   </table>
   <span class="fontes"><b>Fonte:</b> (ROCHA, 2011, adaptado).</span>
<br><br>
<h5 style="clear:both">Cuidados com a ileostomia</h5>
  
  Para a maioria das pessoas, a perda da continência urinária ou fecal pode causar vários desequilíbrios, levando, assim, além dos problemas de ordem cirúrgica e física, a problemas de ordem psicológica e social. Dentro desse contexto, a equipe multiprofissional deve sempre orientar o paciente e o cuidador sobre a necessidade de (BRASIL, 2003):<br/>
  <ul>
<li id="lista">a bolsa estar adequada ao tamanho do estoma intestinal;</li>
<li id="lista">acondicionar as bolsas reservas em lugar arejado, limpo, seco e fora do alcance da luz solar;</li>
<li id="lista">esvaziar a bolsa de ileostomia e urostomia quando alcançar 1/3 de seu espaço preenchido;</li>
<li id="lista">proteger a bolsa durante o banho com um plástico e fitas adesivas;</li>
<li id="lista">trocar a bolsa, quando esta se apresentar saturada (placa protetora se torna quase completamente branca);</li>
<li id="lista">observar sempre a cor (deve ser vermelho vivo), o brilho, a umidade, a presença de muco, o tamanho e a forma do estoma;</li>
<li id="lista">realizar a limpeza do estoma delicadamente, não sendo necessário nem aconselhável esfregá-lo;</li>
<li id="lista">realizar a limpeza da pele ao redor do estoma com água e sabonete neutro, sem esfregar com força, nem usar esponjas ásperas;</li>
<li id="lista">aparar os pelos ao redor do estoma com tesoura;</li>
<li id="lista">expor a pele ao redor do estoma (sempre que possível) ao sol da manhã, de 15 a 20 minutos por dia, tendo cuidado de proteger o estoma com gaze umedecida;</li>
<li id="lista">não utilizar substâncias agressivas à pele, como álcool, benzina, colônias, tintura de benjoim, mercúrio, digluconato de clorexidina, pomadas e cremes, pois estes produtos podem ressecar a pele, ferindo-a e causando reações alérgicas.</li>
</ul>
<br>


<!--<a href="javascript:history.go(-1)" class="btn">Voltar</a>-->
