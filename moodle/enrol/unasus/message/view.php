<?php

   require("../../../config.php");
   require("$CFG->dirroot/enrol/unasus/lib/context.php");
   require("$CFG->dirroot/enrol/unasus/message/lib.php");
   require_login();
   //permiss�o
    //require_capability('enrol/unasus_wskey:create', get_context_instance(CONTEXT_SYSTEM), NULL, false);
    
    //Navega��o
   $navigation = build_navigation(array(array('name' => get_string('enrolname','enrol_unasus'), 'link' => "$CFG->wwwroot/enrol/unasus/index.php", 'type' => 'misc'),
                                     array('name' => get_string('message_maneger','enrol_unasus'), 'link' => "$CFG->wwwroot/enrol/unasus/message/index.php", 'type' => 'misc'), array('name' => get_string('message_view','enrol_unasus'), 'link' => null, 'type' => 'misc')));
   
    
    
    //form
    $message =new message();
    $param=  new object();
    $param->contextid= required_param('contextid',PARAM_INT);
    $context =new context();
     $context_name=$context->get_name($param->contextid);   
     //permiss�o
    require_capability('enrol/unasus:messageview', get_context_instance(CONTEXT_SYSTEM), NULL, false);
    add_to_log(1, 'enrol_unasus', 'messageview', "enrol/unasus/message/view.php?contextid=$param->contextid", getremoteaddr());   
    
   
    $dto=$message->get_by_contextid($param->contextid);
   
    
   //Navega��o
   $navigation = build_navigation(array(array('name' => get_string('enrolname','enrol_unasus'), 'link' => "$CFG->wwwroot/enrol/unasus/index.php", 'type' => 'misc'),
                                     array('name' => get_string('message_maneger','enrol_unasus'), 'link' => "$CFG->wwwroot/enrol/unasus/message/index.php", 'type' => 'misc'), array('name' => $context_name, 'link' => null, 'type' => 'misc'), array('name' => get_string('message_view','enrol_unasus'), 'link' => null, 'type' => 'misc')));
   
    
     print_header(get_string('enrolname','enrol_unasus'),get_string('applicationsenrolment','enrol_unasusem'), $navigation);
   
 	
    get_table_view();
  
    print_footer();
    
    function get_table_view(){
   		global $dto;
   		
   	 	$output = "<center><table class=\"generaltable\"   border=\"1\"   width=\"95%\" cellspacing=\"1\" cellpadding=\"5\">";
    	$output .= "<tr>";
    	$output .= "<th align=\"center\" COLSPAN=2><h3><b>".get_string('message_view','enrol_unasus')."</h3></th>";
     	$output .= "</tr>";
     	
     	$output .= "<tr>";
     	$output .= "<td align=\"left\" width=\"25%\"><b>".get_string('message_presentation','enrol_unasus')."</b></td>";
     	$output .=  "<td>".$dto->presentation."</td>"; 
     	$output .= "</tr>";
     	
     	$output .= "<tr>";
		$output .= "<td align=\"left\" width=\"25%\"><b>".get_string('message_approval','enrol_unasus')."</b></td>";
		$output .=  "<td>".$dto->approval."</td>"; 
		$output .= "</tr>";
		
		$output .= "<tr>";
		$output .= "<td align=\"left\" width=\"25%\"><b>".get_string('message_disapproval','enrol_unasus')."</b></td>";
		$output .=  "<td>".$dto->disapproval."</td>";
		$output .= "</tr>";
		
		$output .= "<tr>";
		$output .= "<td align=\"left\" width=\"25%\"><b>".get_string('message_autreview','enrol_unasus')."</b></td>";
		$output .=  "<td>".$dto->manualreviewapproval."</td>";
		$output .= "</tr>";
				
		/*$output .= "<tr>";
		$output .= "<td align=\"left\" width=\"25%\"><b>".get_string('message_pendingapprovalmanager','enrol_unasus')."</b></td>";
		$output .=  "<td>".$dto->pendingapprovalmanager."</td>";
		$output .= "</tr>";
		*/	
		
		$output .= "<tr>";
		$output .= "<td align=\"center\"  COLSPAN=2>  <input type=button onClick=\"location.href='index.php'\" value='".get_string('back','enrol_unasus')."'>  </td>";
		
		$output .= "</tr>";		
		
    	$output .= "</table></center>";
    	echo $output;
   }
?>
