<?php
/*
 * Created on 0/04/2012
 *
 *Essa class tem as bibliotecas de gerenciamento da tabela mdl_enrol_unasus_config
 */
 class configcontext {
 	
 	
 	public static $PROCESSING_MODE ="processing_mode";
 	public static $CRON_PROCESS_ITEM ="cron_process_item";
 	public static $CRON_PROCESS_FREQUENCY ="cron_process_frequency";
 
 	public static $USER_PROFILE_ENROL_FILTER ="enrol_unasus_profile_enrol_filter";
 	public static $USER_PROFILE_ENROL_REPORTER ="enrol_unasus_profile_enrol_reporter";
 		
 	
 	//inserir regristro na base de dados. 
 	function save($dto) {
 		global $CFG;
 		return insert_record('enrol_unasus_conf', $dto);
 	}
 
 //alteriar regristro na base de dados. 
 	function edit($dto) {
 		global $CFG;
 		return update_record('enrol_unasus_conf', $dto);
 	}
 //faz altualiza��o ou edi��o
 function update($dto) {
 		global $CFG;
 		$exist=$this->exist_by_contextid($dto->contextid,$dto->name);
 		
 		if($exist){
 			if($exist>1){
 				$this->delete_by_contextid($dto->contextid,$dto->name);
 				$this->save($dto);
 			}else{
 				$dto->id=$this->get_id_by_contextid($dto->contextid,$dto->name);
 				$this->edit($dto);
 				}
 		}else{$this->save($dto);}
 		
 	}
  //excluir regristro na base de dados. 
 function delete_by_id($id) {
 		global $CFG;
 		return delete_records_select('enrol_unasus_conf', "id=$id");
 	}
 	 //excluir regristro na base de dados pelo id do contexto. 
 function delete_by_contextid($contextid,$name) {
 		global $CFG;
 		return delete_records_select('enrol_unasus_conf', "contextid=$contextid AND name='".$name."'");
 	}
  //Retora a quantidade de registro cadastrada na base de dados
 function count() {
 		global $CFG;
		$sql ="SELECT COUNT(id)  AS qreg FROM {$CFG->prefix}enrol_unasus_conf ";
		$r=get_record_sql($sql);
		return $r->qreg;
 	}
 	function get_by_id($id) {
 		global $CFG;
		$sql ="SELECT id,contextid,name,value FROM {$CFG->prefix}enrol_unasus_conf WHERE id=$id";
		$r=get_record_sql($sql);
		return $r;
 	}
 	
 	function get_all() {
 		global $CFG;
		$sql ="SELECT id,contextid,name,value  FROM {$CFG->prefix}enrol_unasus_conf ";
		$r=get_records_sql($sql);
		return $r;
 	}
 	function get_all_by_name($name) {
 		global $CFG;
		$sql ="SELECT id,contextid,name,value  FROM {$CFG->prefix}enrol_unasus_conf  WHERE name='".$name."'";
		$r=get_records_sql($sql);
		return $r;
 	}
 	function get_all_by_names($names) {
 		global $CFG;
		$sql ="SELECT id,contextid,name,value  FROM {$CFG->prefix}enrol_unasus_conf  WHERE name IN ($names) ";
		
		$r=get_records_sql($sql);
		return $r;
 	}
 	 function exist_by_contextid($contextid,$name) {
 		global $CFG;
		$sql ="SELECT COUNT(id)  AS qreg FROM {$CFG->prefix}enrol_unasus_conf WHERE contextid=$contextid AND name='".$name."'";
		$r=get_record_sql($sql);
		return $r->qreg;
 	}
 	function get_id_by_contextid($contextid,$name) {
 		global $CFG;
		$sql ="SELECT id  FROM {$CFG->prefix}enrol_unasus_conf WHERE contextid=$contextid AND name='".$name."'";
		$r=get_record_sql($sql);
		if(!empty($r) && isset($r->id))return $r->id;
		else return NULL;
		
 	}
 	function get_by_contextid($contextid,$name) {
 		global $CFG;
		$sql ="SELECT id,contextid,name,value  FROM {$CFG->prefix}enrol_unasus_conf WHERE contextid=$contextid AND name='".$name."'";
		$r=get_record_sql($sql);
		return $r;
 	}
 	function get_value_by_contextid($contextid,$name) {
 		global $CFG;
		$sql ="SELECT value  FROM {$CFG->prefix}enrol_unasus_conf WHERE contextid=$contextid AND name='".$name."'";
		$r=get_record_sql($sql);
		if(!empty($r) && isset($r->value))return $r->value;
		else return NULL;
 	}
 	function get_contextid_value($name){
 		$context_value = array();
 		$rows=$this->get_all_by_name($name);
 		if(is_array($rows)){
 			 foreach ($rows as $row){
 		  	$context_value[$row->contextid]=$row->value;
 		  }
 		}
 		 
 	return 	$context_value ;
	}
	
		function get_contextid_values($names){
 		$context_value = array();
 		$rows=$this->get_all_by_names($names);
 		if(is_array($rows)){
 			 foreach ($rows as $row){
 		  	$context_value[$row->contextid]=$row->value;
 		  }
 		}
 		 
 	return 	$context_value ;
	}
 }