<div style='display:none'>
			<div id="content_color1" style='padding:10px; background:#fff;'>

			<div id="texto_color"></div>
			<br><br>
			<a class="btn inline" href="index.php?pagina=0" onClick="apagar_confirmar();" >Sim, desejo recomeçar os estudos.</a>
			<a class="btn" href="index.php?pagina=0">Não, quero continuar.</a>
				
			<br><br>
			
			</div>
</div>

<style type="text/css">
        #lista{
            list-style-type: disc; 
             margin-left: 30px;
        }
    </style>
        <p>
            <h4 class='titulo'>Apresentação da Unidade</h4>
        
        </p>
		Com o objetivo de prevenir a hipóxia tecidual, a oxigenoterapia domiciliar 
        é uma terapêutica que trabalha uma das necessidades mais vitais do ser 
        humano, que é o padrão respiratório. No domicílio, a oxigenoterapia tem 
        a intenção de proporcionar qualidade de vida ao paciente com dificuldades 
        respiratórias.<br><br>
        Nesta unidade, reconheceremos as indicações e finalidades do procedimento, as 
        responsabilidades da equipe de enfermagem na realização de oxigenoterapia 
        domiciliar, os cuidados necessários durante a execução da oxigenoterapia, 
        quais equipamentos são necessários, as intercorrências que podem surgir 
        durante o procedimento e as contraindicações para a administração do oxigênio 
        domiciliar.<br><br>
        <b>Objetivos educacionais da unidade: </b>
        <ul id="lista">
           <li id="lista">Reconhecer as indicações e finalidades do procedimento; responsabilidades da equipe na realização de oxigenoterapia domiciliar;</li>
           <li id="lista">  Identificar os cuidados durante a execução do procedimento; equipamentos necessários e intercorrências que podem advir durante a oxigenoterapia e contraindicações para a administração do oxigênio domiciliar.</li>
        </ul>
           
           <br><br>
		
        <br>
        <img src="images/abertura.png" alt="Imagem de abertura Unidade 7">
		<script type="text/javascript">
			
		function apagar(){
			$("#texto_color").html("<h4>Confirmação</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, você tem certeza que deseja recomeçar os estudos desta unidade? Todos os seus registros serão zerados.");
            $(".inline").colorbox({inline:true, width:"50%",onClosed:function(){}});
		}
		
		function apagar_confirmar(){
			
			$.post( "../apresentacao/functions/apagar_tudo.php", { unidade: 7 }, function() {
						location.reload();
				});
			
		
		}
		
		</script>