<?php 
require_once $CFG->libdir.'/formslib.php';

class form_edit extends moodleform {

    function definition() {
    	global $dto;
    	
        $mform    =& $this->_form;
        $renderer =& $mform->defaultRenderer();

        $mform->addElement('header', '', get_string('role_edit','enrol_unasus'), '');
        
        
         $mform->addElement('hidden', 'id',  $dto->id);
        $mform->setType('id', PARAM_INT);
        
         $mform->addElement('hidden', 'contextid', $dto->contextid);
        $mform->setType('contextid', PARAM_INT);
        
         $mform->addElement('hidden', 'type',$dto->type);
        $mform->setType('type', PARAM_TEXT);
        
         $mform->addElement('hidden', 'name', $dto->name);
        $mform->setType('name', PARAM_TEXT);
        
        
       $mform->addElement('text', 'title', get_string('role_title','enrol_unasus'));
        $mform->setType('title', PARAM_TEXT);
      	$mform->addRule('title', get_string('requiredfield','enrol_unasus'), 'required', null, 'cliente');
         $mform->setDefault('title', $dto->title);
        $mform->setHelpButton('title', array('role_title', get_string('role_title', 'enrol_unasus'), 'enrol_unasus'));
        
        $mform->addElement('textarea', 'value', get_string('cbo','enrol_unasus'),'wrap="virtual" rows="7" cols="40"');
        $mform->setType('value', PARAM_TEXT);
      	$mform->addRule('value', get_string('requiredfield','enrol_unasus'), 'required', null, 'cliente');
         $mform->setDefault('value', $dto->value);
       $mform->setHelpButton('value', array('role_cbo', get_string('cbo', 'enrol_unasus'), 'enrol_unasus'));
       
        $mform->addElement('textarea', 'description', get_string('descriptiont','enrol_unasus'),'wrap="virtual" rows="7" cols="40"');
        $mform->setType('description', PARAM_TEXT);
        $mform->setDefault('description', $dto->description);
         $mform->setHelpButton('description', array('role_description', get_string('descriptiont', 'enrol_unasus'), 'enrol_unasus'));
        $this->add_action_buttons(true,get_string('edit','enrol_unasus'));
    }

    function validation($data, $files) {
        global $CFG;

         $errors = parent::validation($data, $files);
        
      
     return $errors;
    }

}

?>