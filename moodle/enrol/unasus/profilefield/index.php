<?php

   require("../../../config.php");
    require("$CFG->dirroot/enrol/unasus/lib/context.php");
    require("$CFG->dirroot/enrol/unasus/lib/configcontext.php");
    require("$CFG->dirroot/enrol/unasus/profilefield/lib.php");
   require("$CFG->dirroot/enrol/unasus/tab.php");

 require_login();
 
  // Acesso permitido apenas ao usu�rio admin
    require_capability('enrol/unasus:profilefieldview', get_context_instance(CONTEXT_SYSTEM), NULL, false);
     add_to_log(1, 'enrol_unasus', 'profilefieldview', "$CFG->wwwroot/enrol/unasus/profilefield/index.php", getremoteaddr());    
     
    $navigation = build_navigation(array(array('name' => get_string('enrolname','enrol_unasus'), 'link' => "$CFG->wwwroot/enrol/unasus/index.php", 'type' => 'misc'),
                                     array('name' => get_string('profilefield','enrol_unasus'), 'link' => null, 'type' => 'misc')));
      //tab menu
     $currenttab = 'config';
     $secondrow = array();
      $secondrow[] = new tabobject('message',"$CFG->wwwroot/enrol/unasus/message/index.php",get_string('message','enrol_unasus'));
  	 $secondrow[] = new tabobject('processmode',"$CFG->wwwroot/enrol/unasus/processmode/index.php",get_string('processmode','enrol_unasus'));
  	 $secondrow[] = new tabobject( 'cron',"$CFG->wwwroot/enrol/unasus/cron/index.php",get_string('cron','enrol_unasus'));
  	 $secondrow[] = new tabobject( $currenttab,"$CFG->wwwroot/enrol/unasus/profilefield/index.php",get_string('profilefield','enrol_unasus'));
  	  $secondrow[] = new tabobject('reporter_profile',"$CFG->wwwroot/enrol/unasus/reporter/conf/profile.php",get_string('config_reporter_profile','enrol_unasus'));
  	  $secondrow[] = new tabobject('reporter_profile_enrol',"$CFG->wwwroot/enrol/unasus/reporter/conf/enrol/index.php",get_string('config_reporter_profile_enrol','enrol_unasus'));
  
  	 $tabs = array($firstrow, $secondrow);
      $configcontext =new configcontext();
    $context =new context();
    $profilefield_field = new profilefield_field(); 
    $config=$profilefield_field-> get_contextid_registred();
    
     $displaylist = array();
    $parentlist = array();
    make_categories_list($displaylist, $parentlist);
    
    print_header(get_string('enrolname','enrol_unasus'),get_string('applicationsenrolment','enrol_unasusem'), $navigation);
    print_tabs($tabs, $currenttab, $inactive, $activated);
 	get_sistemacontext_view();
   get_course_category_view();
     get_course_view();
     print_footer();
    
     function get_course_view(){
   		global $context,$config,$displaylist;
   		 $rows = $context->get_courses();
   				 
   		$output = "<center><table   border=\"1\"   width=\"95%\" cellspacing=\"1\" cellpadding=\"5\">";
    	$output .= "<tr>";
                $output .= "<th align=\"left\">".get_string('id','enrol_unasus')."</th>";
				$output .= "<th align=\"left\">".get_string('course','enrol_unasus')."</th>";
				$output .= "<th align=\"left\">".get_string('category','enrol_unasus')."</th>";
				$output .= "<th align=\"left\">".get_string('cron_config','enrol_unasus')."</th>";
				$output .= "</tr>";
    
    	if(!empty($rows)){
	    	foreach ($rows as $row){
	    		$exist_conf=FALSE;
	    		if(isset($config[$row->contextid]))$exist_conf=$config[$row->contextid];
	    		$link="<a href='manage.php?contextid=".$row->contextid."'>".get_string('add','enrol_unasus')."</a>";
	    		if($exist_conf){
	    			$link="<a href='manage.php?contextid=".$row->contextid."'>".get_string('edit','enrol_unasus')."</a>";
	    			$link .=" | <a href='view.php?contextid=".$row->contextid."'>".get_string('view','enrol_unasus')."</a>";
	    			
	    		}
	    		$output .= "<tr>";
			 	$output .=  "<td>".$row->id."</td>"; 
            	$output .=  "<td>".$row->fullname."</td>";
            	$output .=  "<td>".$displaylist[$row->category]."</td>";
            	$output .=  "<td>$link</td>";
            	$output .= "</tr>";    
    		}
    	}    
    	$output .= "</table></center>";
    	echo $output;
   }
   
   function get_course_category_view(){
   		global $context,$config,$displaylist;
   		 $rows = $context->get_courses_category();
   				 
   		$output = "<center><table   border=\"1\"   width=\"95%\" cellspacing=\"1\" cellpadding=\"5\">";
    	$output .= "<tr>";
                $output .= "<th align=\"left\">".get_string('id','enrol_unasus')."</th>";
				$output .= "<th align=\"left\">".get_string('course_category','enrol_unasus')."</th>";
				$output .= "<th align=\"left\">".get_string('cron_config','enrol_unasus')."</th>";
				$output .= "</tr>";
    
    	if(!empty($rows)){
	    	foreach ($rows as $row){
	    		$exist_conf=FALSE;
	    		if(isset($config[$row->contextid]))$exist_conf=$config[$row->contextid];
	    		$link="<a href='manage.php?contextid=".$row->contextid."'>".get_string('add','enrol_unasus')."</a>";
	    		if($exist_conf){
	    			$link="<a href='manage.php?contextid=".$row->contextid."'>".get_string('edit','enrol_unasus')."</a>";
	    			$link .=" | <a href='view.php?contextid=".$row->contextid."'>".get_string('view','enrol_unasus')."</a>";
	    		}
	    		$output .= "<tr>";
			 	$output .=  "<td>".$row->id."</td>"; 
            	$output .=  "<td>".$displaylist[$row->id]."</td>";
            	$output .=  "<td>$link</td>";
            	$output .= "</tr>";    
    		}
    	}    
    	$output .= "</table></center>";
    	$output .= "<br><br>";
    	echo $output;
   }
   
   function get_sistemacontext_view(){
   		global $config;
   		$output = "<center><table   border=\"1\"   width=\"95%\" cellspacing=\"1\" cellpadding=\"5\">";
    	$output .= "<tr>";
               
				$output .= "<th align=\"left\">".get_string('context_system','enrol_unasus')."</th>";
				$output .= "<th align=\"left\">".get_string('cron_config','enrol_unasus')."</th>";
				$output .= "</tr>";
    			$exist_conf=FALSE;
    			if(isset($config[1]))$exist_conf=$config[1];
	    		
	    		$link="<a href='manage.php?contextid=1'>".get_string('add','enrol_unasus')."</a>";
	    		if($exist_conf){
	    			$link="<a href='manage.php?contextid=1'>".get_string('edit','enrol_unasus')."</a>";
	    			$link .=" | <a href='view.php?contextid=1'>".get_string('view','enrol_unasus')."</a>";
	    		}
	    		$output .= "<tr>";
		
            	$output .=  "<td>".get_string('config_global','enrol_unasus')."</td>";
            	$output .=  "<td>$link</td>";
            	$output .= "</tr>";    
    		
    	$output .= "</table></center>";
    	$output .= "<br><br>";
    	echo $output;
   }
?>