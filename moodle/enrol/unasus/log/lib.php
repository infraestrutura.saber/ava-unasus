<?php
/*
 * Created on 31/07/2012
 *
 gerenciar log de controle de inscri��o
 */
 class enrol_unasus_log {
 	
 	public static $STATUS_ENROL_COMPLETED_WITH_SUCCESS =1;
 	public static $STATUS_CPF_NOT_VALID =2;
 	public static $STATUS_CONNECTION_AROUCA_FAILED =3;
 	public static $STATUS_NO_COBO_IN_AROUCA =4;
 	public static $STATUS_NO_ROLE_ENROL_IN_DATABASE =5;
 	public static $STATUS_NO_CBO_IN_ROLE =6;
 	public static $STATUS_FAILED_ON_ENROL =7;
 	
 	public static $TYPE_EXEC_CRON =1;
 	public static $TYPE_EXEC_MANUAL =2;

 	//inserir regristro na base de dados. 
 	function save($dto) {
                global $CFG;
 		$this->delete_by_user($dto->userid,$dto->courseid);
 		return insert_record('enrol_unasus_log', $dto);
 	}
 
 //alteriar regristro na base de dados. 
 	function edit($dto) {
 		global $CFG;
 		return update_record('enrol_unasus_log', $dto);
 	}

 	
  //excluir regristro na base de dados. 
 function delete_by_id($id) {
 		global $CFG;
 		return delete_records_select('enrol_unasus_log', "id=$id");
 	}
 	 function delete_by_user($userid,$courseid) {
 		global $CFG;
 		return delete_records_select('enrol_unasus_log', "userid=$userid AND courseid=$courseid");
 	}
  //Retorna a quantidade de registro cadastrada na base de dados
 function count() {
 		global $CFG;
		$sql ="SELECT COUNT(id)  AS qreg FROM {$CFG->prefix}enrol_unasus_log ";
		$r=get_record_sql($sql);
		return $r->qreg;
 	}
 	function get_by_id($id) {
 		global $CFG;
		$sql ="SELECT id,userid,courseid,rolecontextid,time,status,error,type FROM {$CFG->prefix}enrol_unasus_log WHERE id=$id ";
		$r=get_record_sql($sql);
		return $r;
 	}
 	
 	function get_rolecontextid() {
 		global $CFG;
		$sql ="SELECT  DISTINCT rolecontextid  FROM {$CFG->prefix}enrol_unasus_log ";
		$r=get_records_sql($sql);
		return $r;
 	}
 	
 	function get_by_status($rolecontextid,$status_list) {
 		if(empty($status_list)) return null;
 		global $CFG;
 		
		$sql ="SELECT  id,userid,courseid,rolecontextid,time,status,error,type   FROM {$CFG->prefix}enrol_unasus_log WHERE rolecontextid= $rolecontextid AND status IN ($status_list) ";
		$r=get_records_sql($sql);
		return $r;
 	}
 	function get_all() {
 		global $CFG;
		$sql ="SELECT id,userid,courseid,rolecontextid,time,status,error,type FROM {$CFG->prefix}enrol_unasus_log ";
		$r=get_records_sql($sql);
		return $r;
 	}
 	
 	
 
 }