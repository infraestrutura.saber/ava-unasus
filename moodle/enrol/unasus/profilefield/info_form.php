<?php

require_once $CFG->libdir.'/formslib.php';

class infofield_form extends moodleform {

    function definition() {
    	$param=  new object();
    	$courseid= optional_param('id', NULL,PARAM_INT);
    	if($courseid!=NULL){
    		$contex=get_context_instance(CONTEXT_COURSE,$courseid);
    		$contextid= $contex->id;
    	}else{$contextid= required_param('contextid',PARAM_INT);}
    	
    	 $profilefield_category =new profilefield_category();
		$profilefield_field = new profilefield_field(); 
    	$this->_form =& new MoodleQuickForm($this->_formname, 'get', '', '', '');
    	$mform    =& $this->_form;
        $renderer =& $mform->defaultRenderer();
 	    $mform->addElement('hidden', 'id', $courseid);
 	     $mform->addElement('hidden', 'confirm',1);
		$rows = $profilefield_category->get_by_contextid($contextid);
		if(is_array($rows)){
 			 foreach ($rows as $row){
 			 	$mform->addElement('header', '', $row->name, '');
 		  		$profilefield_field->create_form($row->id,$mform);
 			 }
 		  }
        
         $this->add_action_buttons(true,  get_string('goon','enrol_unasus'));
    }

    function validation($data, $files) {
    	$profilefield_field = new profilefield_field(); 
       $errors = parent::validation($data, $files);
       if($profilefield_field->exist_create_shortname($data['contextid'],$data['shortname'])){
       		$errors['shortname'] = get_string('duplication_record_db','enrol_unasus');
       }
        
        
         
     return $errors;
    }

	
}

?>