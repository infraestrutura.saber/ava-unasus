<?php

   require("../../../../config.php");
   require("$CFG->dirroot/enrol/unasus/profilefield/category/lib.php");
   require("$CFG->dirroot/enrol/unasus/profilefield/lib.php");
     
   require_login();
   
    require_capability('enrol/unasus:profilefielddelete', get_context_instance(CONTEXT_SYSTEM), NULL, false);
    add_to_log(1, 'enrol_unasus', 'profilefielddelete', "enrol/unasus/profilefield/delete_confirm.php?id=$id", getremoteaddr());
    
    //Navega��o
   $navigation = build_navigation(array(array('name' => get_string('enrolname','enrol_unasus'), 'link' => "$CFG->wwwroot/enrol/unasus/index.php", 'type' => 'misc'),
                                     array('name' => get_string('cron_config','enrol_unasus'), 'link' => "$CFG->wwwroot/enrol/unasus/cron/index.php", 'type' => 'misc'), array('name' => get_string('cron_config_add','enrol_unasus'), 'link' => null, 'type' => 'misc')));
 
     $id= required_param('id',PARAM_INT);
    
     print_header(get_string('enrolname','enrol_unasus'),get_string('applicationsenrolment','enrol_unasusem'), $navigation);
   
 	
    get_table_view();
  
    print_footer();
    
    function get_table_view(){
   		global $id;
   		global  $CFG;
   		 $profilefield_category =new profilefield_category();
    	 $profilefield_field = new profilefield_field();
    	 $dto=$profilefield_field->get_by_id($id);
    	 $category_name=$profilefield_category->get_name_by_id($dto->categoryid);
   	 	$output = "<center><table class=\"generaltable\"   border=\"1\"   width=\"95%\" cellspacing=\"1\" cellpadding=\"5\">";
    	$output .= "<tr>";
    	$output .= "<th align=\"center\" COLSPAN=2><h3><b>".get_string('profilefield_delete','enrol_unasus')."</h3></th>";
     	$output .= "</tr>";
     	
     	$output .= "<tr>";
     	$output .= "<td align=\"left\" width=\"25%\"><b>".get_string('id','enrol_unasus')."</b></td>";
     	$output .=  "<td>".$dto->id."</td>"; 
     	$output .= "</tr>";
     	
     	$output .= "<tr>";
		$output .= "<td align=\"left\" width=\"25%\"><b>".get_string('profilefield_short_name','enrol_unasus')."</b></td>";
		$output .=  "<td>".$dto->shortname."</td>"; 
		$output .= "</tr>";
		
		$output .= "<tr>";
		$output .= "<td align=\"left\" width=\"25%\"><b>".get_string('profilefieldcategory_name','enrol_unasus')."</b></td>";
		$output .=  "<td>".$dto->name."</td>"; 
		$output .= "</tr>";
		
		$output .= "<tr>";
		$output .= "<td align=\"left\" width=\"25%\"><b>".get_string('profilefield_category','enrol_unasus')."</b></td>";
		$output .=  "<td>".$category_name."</td>";
		$output .= "</tr>";
		
		
		$output .= "<tr>";
		$output .= "<td align=\"center\"  COLSPAN=2>  <input type=button onClick=\"location.href='delete.php?id=".$dto->id."&contextid=".$dto->contextid."'\" value='".get_string('confirm','enrol_unasus')."'> &nbsp;&nbsp;<input type=button onClick=\"location.href='".$CFG->wwwroot."/enrol/unasus/profilefield/manage.php?contextid=".$dto->contextid."'\" value='".get_string('cancel','enrol_unasus')."'> </td>";
		
		$output .= "</tr>";		
		
    	$output .= "</table></center>";
    	echo $output;
   }
?>
?>