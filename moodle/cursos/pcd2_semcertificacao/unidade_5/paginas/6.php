<?php
               
    $pg_atual=6;
    registro($id,unid5_pg,$pg_atual,unid5_ev);
?>
     <style type="text/css">
		#lista li{
			list-style-type: disc;
			 margin-left: 30px;
		}
	
	
	</style>
   

   <h4 class="titulo">Seguimento nutricional</h4>
   <br>
	É o acompanhamento nutricional sistematizado, realizado pela equipe multiprofissional, que tem como objetivo avaliar as metas estabelecidas para a nutrição do paciente, bem como a avaliação das necessidades da família. As principais metas estabelecidas para o paciente são (SMELTIZER; BARE, 2005):
	<ul id="lista">
		<li>restauração do equilíbrio nutricional;</li>
		<li>manutenção do padrão intestinal normal;</li>
		<li>redução do risco de aspiração;</li>
		<li>redução de quadros de infecções;</li>
		<li>hidratação adequada;</li>
		<li>enfrentamento individual, conhecimento e habilidade no autocuidado e prevenção das complicações. </li>
	</ul>
	
	<br/><br/>
<div class="box">
        <img src="../images/img_vocesabiaque_ad.png" alt="Você sabia" >
        <span class="titulo_box">Você sabia que...</span>
        <br>
        <hr/>
       A equipe de enfermagem possui um papel importante no monitoramento nutricional do paciente. Durante as visitas domiciliares, deve ser verificado o estado físico do paciente (peso, sinais vitais, nível de atividade). <br/><br/>
	   O paciente, o cuidador e a família devem receber educação em saúde continuada, e o cuidado compartilhado deve ser considerado como uma das principais ferramentas para o restabelecimento do paciente que necessita de suporte nutricional (SOCIEDADE BRASILEIRA DE NUTRIÇÃO PARENTERAL E ENTERAL, 2012).
</div>
   