<?php
   require("../../../../config.php");
   require("$CFG->dirroot/enrol/unasus/manage.php");
    require("$CFG->dirroot/enrol/unasus/reporter/lib.php");
   require("$CFG->dirroot/enrol/unasus/role/data/lib.php");
   require("$CFG->dirroot/enrol/unasus/role/lib.php");
   require("$CFG->dirroot/enrol/unasus/role/cbo/lib.php");
   require("$CFG->dirroot/enrol/unasus/tab.php");
   require("$CFG->dirroot/enrol/unasus/message/lib.php");
   require("$CFG->dirroot/enrol/unasus/manual_accept_form.php");
   require("$CFG->dirroot/enrol/unasus/profilefield/data/lib.php");
   require("$CFG->dirroot/enrol/unasus/lib/configcontext.php");

 require_login();
   // Acesso permitido apenas ao usu�rio admin
    require_capability('enrol/unasus:viewreporterenrol', get_context_instance(CONTEXT_SYSTEM), NULL, false);
     //require_capability('enrol/unasus:wskeyview', get_context_instance(CONTEXT_SYSTEM), NULL, false);
     add_to_log(1, 'enrol_unasus', 'view', 'reporter/general/detail.php?enrolid='.required_param('enrolid', PARAM_INT), getremoteaddr());    
    //Navega��o
     $navigation = build_navigation(array(array('name' => get_string('enrolname','enrol_unasus'), 'link' => "$CFG->wwwroot/enrol/unasus/index.php", 'type' => 'misc'),
                                     array('name' => get_string('search','enrol_unasus'), 'link' => null, 'type' => 'misc')));
     //tab menu
     $currenttab = 'search';
 
    $manage=new enrol_unasus_manage();
    $role_data=new unasus_role_data();
   
     $param=  new object();
     $enrolid=required_param('enrolid', PARAM_INT);
     $row=$manage->get_detail_by_id($enrolid);
     $cbo_enrol=$role_data->get_cbo_by_enrolid($row->id);
 	 $manualform= new enrol_unasus_manual_accept_form();
 	 $data_infofield=new data_infofield();
 	 $configcontext=new configcontext();
 	 
 	 if ($manualform->is_cancelled()) {
        redirect($CFG->httpswwwroot.'/enrol/unasus/reporter/general/detail.php?enrolid='.$enrolid) ;
     } else if ($formdata = $manualform->get_data()) {
     	manual_enrol($formdata);
    	redirect($CFG->httpswwwroot.'/enrol/unasus/reporter/general/detail.php?enrolid='.$enrolid) ;
     }
     
  //print_r($row);
  function print_cbo($contextid,$cbo_enrol){
  	$txthtml="";
  	$cbo=new unasus_cbo();
  	$role=new role();
  	$role_cbo=$role->get_by_contextid($contextid);
   
  	$cbo_nome=$cbo->get_cbo_name($role_cbo->value,$role_cbo->description);

 
  	foreach ($cbo_enrol as $cbo){ 
  		$cbon=$cbo->value;
  		if(array_key_exists($cbon,$cbo_nome)){
  			$cbon.= " - ".$cbo_nome[$cbon];
  		}
  		$txthtml .=$cbon."<br>";
  	}
  	return $txthtml;
  }
 function manual_enrol($data){
 	  
 	    global $CFG,$row,$manage,$SITE,$configcontext;
 	    
 	    if(!has_capability('enrol/unasus:manualenrol', get_context_instance(CONTEXT_SYSTEM))) return null;
 		 $process_mode=$configcontext->get_by_contextid($row->rolecontextid,configcontext::$PROCESSING_MODE);
 		if(empty($process_mode)) return FALSE;
    	 else if($process_mode->value=='automatic' || empty($process_mode->value) ){return FALSE;}
 
 
     	$manage->update_enrol_manual_status($row->id,enrol_unasus_manage::$STATUS_ENROL_ACCEPT,$data->justify);
      	$manage->save_enrol_course_manual($row->courseid,$row->userid);
       
       // enviar e-mail de notifica��o aluno
       $USERENROL=get_record('user','id',$row->userid);
       $a->site = $SITE->shortname;
       $a->course = $row->shortname;        
       $a->course_fullname =$row->fullname;
       //$a->justify =$data->justify;
       $a->course_url ="$CFG->wwwroot/course/view.php?id=$row->courseid";
       $subject = get_string('message_enrol_aproved_subject','enrol_unasus',$a);
       $message= new message();
       $msg=$message->get_by_contextid($row->rolecontextid);
       //$body = $msg->manualreviewapproval;
       $body = $message->fill_tag_justify($msg->manualreviewapproval,$data->justify);
    
       email_to_user($USERENROL, $SITE->shortname, $subject, $body);
     
     
  
 
 }
 function show_form_manual_accept(){
 	 global $row,$manage, $manualform,$configcontext;
 	if(!has_capability('enrol/unasus:manualenrol', get_context_instance(CONTEXT_SYSTEM))) return null;
 
 	 $process_mode=$configcontext->get_by_contextid($row->rolecontextid,configcontext::$PROCESSING_MODE);
 
 	if(empty($process_mode)) return FALSE;
 		
    else if($process_mode->value=='automatic' || empty($process_mode->value) ){return FALSE;}
     		 
 	if($row->status==enrol_unasus_manage::$STATUS_ENROL_NOT_ACCEPT){
   		//apresentar form se  n�o nenhuma inscri��o aceite
   		$exist=$manage->exist_enrol_by_status($row->userid,$row->courseid,enrol_unasus_manage::$STATUS_ENROL_ACCEPT);
   		if(!$exist){$manualform->display();}
   }
 }
 function get_table_view(){
    	global $row,$cbo_enrol;
   		$util = new enrol_unasus_report_util();
   		if($row->timeupdate){$update=date('d/m/Y',$row->timeupdate);}
    	else $update="";
    
    	if($row->statusinfo==2){
    		$author_manual_enrol=get_record("user", "id", $row->author);
    	}
      $output ='<center><FORM><INPUT TYPE="BUTTON" VALUE="Voltar" ONCLICK="window.location.href='."'".'search.php'."'".'"></FORM>';
    $output .= '<table class="generaltable boxaligncenter"  cellspacing="1" cellpadding="5">
   				<tr>
                <td align="left"><b>'.get_string('user','enrol_unasus').'</b></td>
				<td align="left">'.$row->firstname.' '.$row->lastname.'</td>
				</tr>
				<tr>
                <td align="left"><b>'.get_string('email').'</b></td>
				<td align="left">'.$row->email.'</td>
				</tr>';
    			
			 $output .= '<tr>
                <td align="left"><b>'.get_string('course','enrol_unasus').'</b></td>
				<td align="left">'.$row->fullname.'</td>
				</tr>
				
				<tr>
                <td align="left"><b>'.get_string('enroldate','enrol_unasus').'</b></td>
				<td align="left">'.date('d/m/Y H:i:s',$row->time).'</td>
				</tr> .
				<tr>
                <td align="left"><b>'.get_string('profilefield_data_profile','enrol_unasus').'</b></td>
				<td align="left">'.enrol_profile_date($row->rolecontextid,$row->userid).'</td>
				</tr>';
				
				if(!empty($cbo_enrol)){
					$output .= '
					<tr>
               		 <td align="left"><b>'.get_string('role_cbo_enrol','enrol_unasus').'</b></td>
					<td align="left">'.print_cbo($row->rolecontextid,$cbo_enrol).'</td>
					</tr>';
				}
				if($row->statusinfo==2){
					$output .= '
					<tr>
               		 <td align="left"><b>'.get_string('enroldateupdate','enrol_unasus').'</b></td>
					<td align="left">'. $update.'</td>
					</tr>';
				}
				
				
				$output .= '<tr>
                <td align="left"><b>'.get_string('status','enrol_unasus').'</b></td>
				<td align="left">'.$util->get_status_domain_table_name($row->status).'</td>
				</tr>';
				
				if($row->statusinfo==2){
					$output .= '
					<tr>
               		 	<td align="left"><b>'.get_string('justify','enrol_unasusem').'</b></td>
						<td align="left">'.$row->justify.'</td>
					</tr>
					<tr>
               		 	<td align="left"><b>'.get_string('manual_aprove_by','enrol_unasusem').'</b></td>
						<td align="left">'.$author_manual_enrol->firstname .$author_manual_enrol->lastname .'</td>
					</tr>';
				}	
				$output .= '
				<tr>
                <td align="left"><b>'.get_string('type','enrol_unasusem').'</b></td>
				<td align="left">'.$util->get_status_type_domain_table_name($row->statusinfo).'</td>
				</tr>';
				
			
               $output .= '	</table>';
               print_box($output);
    }
    
    function enrol_profile_date($contextid,$userid){
    	global $data_infofield;
    	$table="";
    	$rows = $data_infofield->get_all_by_user($contextid,$userid);
    	  if(!empty($rows)){
    	  	$table="<table>";
    	  	foreach ($rows as $row) {
    	  		$table.="<tr>";
    	  		$table.="<td>$row->name</td>";
    	  		$table.="<td>$row->data</td>";
    	  		$table.="</tr>";
    	  	}
    	  	$table.="</table>";
    	  }
    	  return $table;
    }
      
    print_header(get_string('applicationsenrolment','enrol_unasusem'),get_string('applicationsenrolment','enrol_unasusem'), $navigation);
    print_tabs($tabs, $currenttab, $inactive, $activated);
   // manual_enrol();
    get_table_view();
    show_form_manual_accept();
    print_footer();
    
?>