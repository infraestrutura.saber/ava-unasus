<?php
               
    $pg_atual=5;
     registro($id,unid3_pg,$pg_atual,unid3_ev);
?>

<style type="text/css">
#conteudo{
	word-wrap: break-word !important;
}


</style>

  <h4 class="titulo">Referências</h4>
BRASIL. Ministério da Saúde. Secretaria de Atenção à Saúde. Departamento de Atenção Básica.
<b>Caderno de Atenção Domiciliar.</b> Brasília: Ed. Ministério da Saúde, 2013.
<br><br>

    BRUNI, D. S. et al. Aspectos fisiopatológicos e assistenciais de enfermagem na reabilitação da pessoa com lesão medular.<b> Rev Esc Enferm USP </b>, v. 38, n. 1, p. 71-79, 2004. Disponível em:<a href="http://www.scielo.br/pdf/reeusp/v38n1/09.pdf"  target="_blank"> http://www.scielo.br/pdf/reeusp/v38n1/09.pdf</a>.  Acesso em: 1 maio 2013.
<br><br>

    DUARTE, Y. A. de O.<b> Atendimento domiciliar:</b> um enfoque gerontológico. São Paulo: Atheneu, 2000.
<br><br>

    LOPES, M. H. B. de M. et al. O uso do enteroclisma no preparo para o parto: análise de suas vantagens e desvantagens.<b> Rev. Latino Americana de Enfermagem</b>, Ribeirão Preto, v. 9, n. 6, p.49-55, nov. 2001. Disponível em: <a href="http://www.scielo.br/scielo.php?pid=S0104-11692001000600009&script=sci_arttext"  target="_blank">www.scielo.br/scielo.php?script=sci_arttext&pid=S0104-11692001000600009&Lang=pt</a>. Acesso em: 11 abr. 2013.
<br><br>
NETTINA, S. M. <b>Prática de enfermagem.</b> 7. ed. Rio de Janeiro: Guanabara Koogan, 2003. 
<br><br>
POTTER, P. A. PERRY, A. G. <b>Fundamentos da enfermagem.</b> 7. ed. Rio de Janeiro: Elsevier, 2009.
<br><br>
    RIO GRANDE DO NORTE (Estado). Governo do Estado. Secretaria de Saúde Pública. Hospital
     Monsenhor Walfredo Gurgel. Departamento de Enfermagem. <b>Lavagem Intestinal 
     (Enteroclisma).</b> n. 28., 2010. Disponível em: <a href="http://www.scielo.br/scielo.php?pid=S0104-11692001000600009&script=sci_arttext"  target="_blank">http://www.walfredogurgel.rn.gov.br/content/aplicacao/sesap_hwg/servico/28%20lavagem%20intestinal%20(enteroclisma).pdf</a>. Acesso em: 11 abr. 2013.
<br><br>

    SÃO PAULO (Estado). Conselho Regional de Enfermagem.<b> Parecer COREN-SP CAT. Nº 032/2010:</b> lavagem intestinal. 2010. Disponível em: 
    <a href="http://portal.coren-sp.gov.br/sites/default/files/parecer_coren_sp_2010_32.pdf"  target="_blank">"http://coren-sp.gov.br/sites/default/files/032_2010_lavagem_intestinal.pdf</a>. Acesso em: 10 abr. 2013
<br><br>

    SÃO PAULO (Estado). Conselho Regional de Medicina do Estado de São Paulo (CREMESP). Pareceres. <b>Parecer Nº 87507: </b>Retirada de fecaloma. 2006. Disponível em: <a href="http://www.cremesp.org.br/?siteAcao=Pareceres&dif=s&ficha=1&id=6566&tipo=PARECER&orgao=Conselho%20Regional%20de%20Medicina%20do%20Estado%20de%20S%E3o%20Paulo&numero=87507&situacao=&data=02-08-2006"  target="_blank">http://www.cremesp.org.br/?siteAcao=Pareceres&dif=s&ficha=1&id=6566&tipo=PARECER&orgao=Conselho%20Regional%20de%20Medicina%20do%20Estado%20de%20S%E3o%20Paulo&numero=87507&situacao=&data=02-08-2006</a>. Acesso em: 11 abr. 2013.
<br><br>
SCRAMIN, A. P.; MACHADO, W. C. A. Cuidar de pessoas com tetraplegia no ambiente domiciliário: intervenções de enfermagem na dependência de longo prazo. <b>Esc. Anna Nery</b>,  Rio de Janeiro,  v. 10, n. 3, dez. 2006. Disponível em: <a target="_blank" href="http://www.scielo.br/scielo.php?script=sci_arttext&pid=S1414-81452006000300020&lng=en&nrm=iso">http://www.scielo.br/scielo.php?script=sci_arttext&pid=S1414-81452006000300020&lng=en&nrm=iso</a>. Acesso em: 12 mar. 2014.   
<br><br>




       


   