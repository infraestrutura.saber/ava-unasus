
<?php
               
    $pg_atual=5;
   registro($id,unid8_pg,$pg_atual,unid8_ev);
?>
    
   

   <h4 class="titulo">Referências</h4>
ANVISA (AGÊNCIA NACIONAL DE VIGILÂNCIA SANITÁRIA).  <b>Gerenciamento dos resíduos 
de serviços de saúde</b>. Brasília : Ministério da Saúde, 2006. Disponível em: &#60 <a href="https://www.anvisa.gov.br/servicosaude/manuais/manual_gerenciamento_residuos.pdf" target="_blank">https://www.anvisa.gov.br/servicosaude/manuais/manual_gerenciamento_residuos.pdf</a>&#62.
Acesso em: 3 maio 2013.<br><br>


ANVISA (AGÊNCIA NACIONAL DE VIGILÂNCIA SANITÁRIA). <b>Protocolo de uso de EPI:</b> orientações sobre a necessidade do uso de Equipamentos de Proteção Individual (EPI’s) para os serviços de Portos, Aeroportos, Fronteiras e Recintos Alfandegados. Brasília: ANVISA, 2009. 7 p. Disponível em: &#60 <a href="http://www.anvisa.gov.br/hotsite/influenza/arquivos/Protocolo_uso_EPI.pdf" target="_blank">http://www.anvisa.gov.br/hotsite/influenza/arquivos/Protocolo_uso_EPI.pdf</a>&#62.Acesso em: 3 maio 2013.<br><br>


ABNT (ASSOCIAÇÃO BRASILEIRA DE NORMAS TÉCNICAS). <b>NBR 12809</b>: Manejo de Resíduos de Serviços de Saúde. Rio de Janeiro: ABNT, 1993.<br><br>


ABNT (ASSOCIAÇÃO BRASILEIRA DE NORMAS TÉCNICAS). <b>NBR 13852</b>: Coletores para Resíduos de Serviços de Saúde perfurantes ou cortantes. Rio de Janeiro: ABNT, 1997.
<br><br>

COFEN (CONSELHO FEDERAL DE ENFERMAGEM).<b>Resolução COFEN Nº 390/2011.</b> Normatiza a execução pelo Enfermeiro da punção arterial, tanto para fins de gasometria como para monitorização de pressão arterial invasiva. Brasília: COFEN, 2011. Disponível em: &#60 <a target="_blank" href="http://www.novo.portalcofen.gov.br/resoluo-cofen-n-3902011_8037.html">http://www.novo.portalcofen.gov.br/resoluo-cofen-n-3902011_8037.html</a>&#62. Acesso em: 2 maio 2013.
<br><br>
PARANÁ (Estado). Secretaria de Estado da Saúde. Laboratório Central de Saúde Pública - LACEN-PR. <b>Manual de coleta e envio de amostras biológicas ao Lacen/PR.</b> Curitiba: LACEN-PR, 2012. Disponível em:&#60 <a href="http://www.lacen.saude.pr.gov.br/arquivos/File/Manuais/Manual_de_Coleta_e_Envio_de_Amostras.pdf" target="_blank">http://www.lacen.saude.pr.gov.br/arquivos/File/Manuais/Manual_de_Coleta_e_Envio_de_Amostras.pdf</a>&#62. Acesso em: 5 abr. 2013.
<br><br>
RAPPARINI, C.; VITÓRIA, M. A. de Á.; LARA, L. T. de R. <b>Recomendações para atendimento e acompanhamento de exposição ocupacional a material biológico:</b> HIV e hepatites B e C. [20--?]. Disponível em:&#60 <a  href="http://bvsms.saude.gov.br/bvs/publicacoes/04manual_acidentes.pdf" target="_blank">http://bvsms.saude.gov.br/bvs/publicacoes/04manual_acidentes.pdf</a>&#62. Acesso em: 29 abr. 2013.
<br><br>
SÃO PAULO (Cidade). Secretaria da Saúde. Coordenação de Desenvolvimento de Programas e Políticas de Saúde. Assistência Laboratorial. <b>Caderno de coleta de exames laboratoriais.</b> n. 1. São Paulo, 2006. Disponível em: &#60 <a href="http://www.prefeitura.sp.gov.br/cidade/secretarias/upload/saude/arquivos/assistencialaboratorial/Coleta_Laboratorial.pdf" target="_blank">http://www.prefeitura.sp.gov.br/cidade/secretarias/upload/saude/arquivos/assistencialaboratorial/Coleta_Laboratorial.pdf</a>&#62. Acesso em: 28 abr. 2013.