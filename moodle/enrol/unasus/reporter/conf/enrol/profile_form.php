<?php //$Id: forgot_password_form.php,v 1.7.2.1 2007/11/23 22:12:35 skodak Exp $

require_once $CFG->libdir.'/formslib.php';

class enrol_unasus_profile_enrol_form extends moodleform {

    function definition() {
    	global $profile,$profilefield_field,$param,$configcontext;
    	$fields= $profilefield_field->get_by_contextid($param->contextid,1);
        $value_filter=$configcontext->get_value_by_contextid($param->contextid,configcontext::$USER_PROFILE_ENROL_FILTER);
       
        $value_reporter=$configcontext->get_value_by_contextid($param->contextid,configcontext::$USER_PROFILE_ENROL_REPORTER);
         
        $seleted_fields_filter=$profile->get_profile_config_in_array('',$value_filter);
      
        //print_r( $seleted_fields_filter);
    	$seleted_fields_reporter=$profile->get_profile_config_in_array('',$value_reporter);
    	
    		$mform    =& $this->_form;
        $renderer =& $mform->defaultRenderer();
         
        $mform->addElement('header', '', get_string('config_enable_field_filter','enrol_unasus'), '');
        $mform->addElement('hidden', 'contextid',  $param->contextid);
        $mform->setType('contextid', PARAM_INT);
       if(!empty($fields)){
       	 foreach ($fields as $field){
         	 $mform->addElement('checkbox', 'F_'.$field->id, $field->name);
         	if(isset($seleted_fields_filter[$field->id])) {$mform->setDefault('F_'.$field->id, 1);}
         	 
         }
       }
       
         
         $mform->addElement('header', '', get_string('config_enable_field_reporter','enrol_unasus'), '');
         if(!empty($fields)){
         	 foreach ($fields as $field){
         	 $mform->addElement('checkbox', 'R_'.$field->id, $field->name);
         	if(isset($seleted_fields_reporter[$field->id])) {$mform->setDefault('R_'.$field->id, 1);}
         	 
         }
         }
       
       if(!empty($fields)) $this->add_action_buttons(true, get_string('save','enrol_unasus'));
    }

    function validation($data, $files) {
        global $CFG;

         $errors = parent::validation($data, $files);
         
      
     return $errors;
    }

}

?>