<?php
/*
 * Created on 20/08/2012
 *
 */
 class enrol_unasus_userprofile  {
 		public static $USER_PROFILE_FILTER ="enrol_unasus_profile_filter";
 		public static $USER_PROFILE_REPORTER ="enrol_unasus_profile_reporter";
 		
 		
 	 /*
    	 * retorna lista de todos os campos do perfil
    	 */
    	function get_fields_user_profile(){
				global $CFG;
			   $sql ="SELECT id,shortname,name,datatype,sortorder,defaultdata,param1 FROM {$CFG->prefix}user_info_field";
			
			return get_records_sql($sql);
    	}	
    	
    	function get_profile_config_in_array($key,$param=''){
    		
    		if(empty($param)) $param=$this->get_profile_config($key);
    		
    		  $p = explode(",", $param); 
    		  $a=array();
    		  foreach ($p as $v){ 
				 $a[$v]=$v;
			   }
			 return $a;
 				
    	}
    	
    	function get_profile_config($key){
    		global $CFG;
    			$sql="SELECT value FROM {$CFG->prefix}config WHERE name='".$key."'";
 			  $r=get_record_sql($sql);
 			  if(empty($r)) return null;
 				return $r->value;
    	}
    	
    	/*
    	 * retorna lista com nome dos campos selecionados separado por vírgula
    	 */
    	function get_selected_profile_field($fields,$prefix){
    		
    		$cont=0;
    		foreach ($fields as $field){
    			$param=$prefix.$field->id;
    	    	if(optional_param($param,false, PARAM_TEXT)){
    	  			$cont++;
    	  			if($cont==1){$selected_fields=$field->id;}
    	  		else{$selected_fields.=",".$field->id;}
    	  	
    	  	}
   		  }
   		  return $selected_fields;
    	}
    	
    	/*
    	 * retorna um array com dados dos campos selecionados
    	 */
    	function get_data_profile_field($fields,$prefix){
    		
    		 $data_fields = array();
    		foreach ($fields as $field){
    			$param=$prefix.$field->id;
    	    	if(optional_param($param,false, PARAM_TEXT)){
    	  			$data_fields[$field->id]= optional_param($param,'', PARAM_TEXT);
    	       }
   		   }
   		  return $data_fields;
    	}
    	function updadte_profile_config($key,$value){
    		global $CFG;
    		$result="";
    		//ver se j� existe
    		 $exist=record_exists('config', 'name', $key);
    		//cadastrar se n�o existe
    		if(!$exist){
    			$conf=  new object();
    			$conf->name=$key;
    			$conf->value=$value;
    			$result = insert_record('config', $conf);
    		}
    		//atualize se j� existe registro
    		else{
    			$conf=  new object();
    			//recupere o id
    			$sql="SELECT id FROM {$CFG->prefix}config WHERE name='".$key."'";
 				$r=get_record_sql($sql);
 				$conf->id=$r->id;
 				$conf->name=$key;
    			$conf->value=$value;
    			$result= update_record('config', $conf); 
    		}
    		
    		return $result;
    	}
    	
   function convert_menu_feild_data_to_array_for_form($param){
    	$paramql = preg_replace("/(\\r)?\\n/i","|_#_|",$param);
    	$p = explode("|_#_|",$paramql); 
    	$a=array();
    	$a['']="---";
			foreach ($p as $v){ 
				$a[$v]=$v;
			}
			 return $a;
    }
    
    function get_sql_filter_user_data($fields,$param_fields,$sql_field='er.userid'){
    	$sql_filer="";
    	if(empty($param_fields)) return null;
    	 //faz loop nos em todos os fields
		foreach ($fields as $field){
			//verifique se tem par�metro 
			$param=null;
			if(array_key_exists ($field->id,$param_fields)){
				$param=$param_fields[$field->id];
			}
			
			if(!empty($param)){
				$sql_filer.=$this->get_usersid_by_data($field->datatype,$field->id,$param,$sql_field);
			}
		}
		return $sql_filer;		 	
    }
    
    /*
    	 * retorna lista de iduser com base  na pesquisa no no perfil
    	 */
    	function get_usersid_by_data($datatype,$fieldid,$data,$sql_field){
				global $CFG;
			 $wsql=" AND data LIKE '%".$data."%'";
			  if($datatype=='menu') $wsql=" AND data='".$data."'";
			  else if($datatype=='checkbox') {
			  	if($data==2){$data=0;}
			  		$wsql=" AND data= ".$data." ";
			  	}
			  $sql =" SELECT  userid FROM  {$CFG->prefix}user_info_data  WHERE fieldid=$fieldid $wsql";
			
			 $rows= get_records_sql($sql);
			 $sql_user=  $this->get_sql_in_userid($rows,$sql_field);
			
			  return $sql_user;
    	}	
    	
    	public function get_sql_in_userid($list,$sql_field){
    		$cont=0;
    		$result="";
    	
    		
    		if(empty($list)) return " AND $sql_field IN (-1) ";
    		
    		  foreach ($list as $v){ 
    		 	
    		 	$cont++;
    		    if($cont==1){$result=$v->userid;}
    		 	else{$result.=",".$v->userid."";}
    		 	
    		 }
    		 
    		 return " AND $sql_field IN ($result) ";
    	}
    	
    	/*
    	 * converte array para string sql
    	 */
    	function conver_array_to_sql_in($fields_report){
    		$cont=0;
    		
    		$result="";
    		 foreach ($fields_report as $v){ 
    		 	$cont++;
    		 	if($cont==1){$result=$v;}
    		 	else{$result.=",".$v;}
    		 	
    		 }
    		 return $result;
    	}
    	
    	
    	/*
    	 * retorna a lista com os valores do perfil que devem ser exibidos no relat�rio
    	 */
    	function get_data_fields_user_profile($fields_report){
    			global $CFG;
    			$fields_report_sql=$this->conver_array_to_sql_in($fields_report);
    			if(!empty($fields_report_sql)){
    				$sql =" SELECT  id,fieldid,userid,data FROM {$CFG->prefix}user_info_data  WHERE fieldid IN ($fields_report_sql) ";
    				
    				return get_records_sql($sql);
    			}
    			
				
			    return NULL;
				
    	}	
    	/*
    	 * retorna a lista com os valores do perfil que devem ser exibidos no relat�rio em array
    	 */
    	function get_data_fields_user_profile_in_array($fields_report){
				$rows=$this->get_data_fields_user_profile($fields_report);
			 	$a=array();
    		  	foreach ($rows as $row){ 
    		  		$key=$row->fieldid."/".$row->userid;
    		  		$a[$key]=$row->data;
			  }
				return $a;
				
    	}	
 }