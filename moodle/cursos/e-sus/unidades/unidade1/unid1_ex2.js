// var alternativas = new Array();alternativas[0] = " (  ) SIAB ou ( x ) SISAB.";
// alternativas[1] = " ( x ) SIAB ou (  ) SISAB.";alternativas[2] = " (  ) SIAB ou ( x ) SISAB.";
// alternativas[3] = " (  ) SIAB ou ( x ) SISAB.";
// var name = "unid1_ex2";gerarinputmultipla(alternativas, name, 3);

/*Alternativas do verdadeiro/falso*/
	var opcao1 = '1) Por meio deste sistema o tipo de registro é individualizado, e não consolidado.';
	var opcao2 = '2) Por meio deste sistema, apenas os profissionais da Estratégia de Saúde da Família (ESF) e Equipes de Atenção Básica (participantes do PMAQ) podem realizar a alimentação dos dados. Outros profissionais de programas como Consultório na Rua, Atenção Domiciliar, NASF e Academia da Saúde não têm permissão para uso do sistema.';
	var opcao3 = '3) O acompanhamento do território, neste sistema, é realizado por domicílio, núcleos familiares e indivíduos.';
	var opcao4 = '4) Por meio deste sistema, os indicadores são fornecidos a partir da situação de saúde do território, atendimentos e acompanhamento dos indivíduos do território.';
	var opcao5 = '5) Por meio deste sistema, os relatórios gerenciais são limitados aos dados consolidados, e não ocorrem de forma dinâmica.';

	/*array de variaveis que vai para a funcao geradora*/
	var entrada = [opcao1, opcao2, opcao3, opcao4, opcao5];
	/*variavel com o name do formulario. Deve ser único na mesma página*/
	var name = 'unid1_ex2';
	/*gabarito da questao.*/
	var gabarito = '(\'F\', \'V\', \'F\', \'F\', \'V\')';
	/*funcao que gera o html*/
	gerarInputVFSISAB(entrada, name, gabarito, 'unid1_ex2',2); 
