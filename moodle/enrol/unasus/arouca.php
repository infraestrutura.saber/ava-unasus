<?php

class arouca {
		//controle de falha de websevice
		var $WS_AROUCA_FAILURE=FALSE;
		var $WS_AROUCA_FAILURE_MSG='';
		var $wsdto=NULL;	
		
		
	function get_cbo_arouca($cpf){
         global $CFG;
      
    	$this->WS_AROUCA_FAILURE=FALSE;
    	$this->WS_AROUCA_FAILURE_MSG='';
    	
     	$cbos = array();
     	
    	try {
       		  require("$CFG->dirroot/enrol/unasus/config.php");
        	$client = new SoapClient($AROUCA->web_service_url_wsdl,    
                array('trace'=>true,
                      'cache_wsdl'=>WSDL_CACHE_NONE,
                      'exceptions'=>true,
                      'local_cert' =>$AROUCA->path_file_cert,
                      'passphrase' =>$AROUCA->cert_pwd));
         
         	try {
          		$res=$client->recuperarPessoa(array('cpf' =>$cpf));
           		$x= NULL;
           		if(!empty($res->return)){$x=$res->return;}
           		if (!empty($x) && is_array($x->cbos)) {
           			 foreach($x->cbos as $chave){
               		array_push($cbos,$chave->codigo);
               		}
            	} else {
              		if (!empty($x))array_push($cbos,$x->cbos->codigo);
            	}
                
             }
        catch (Exception $e) {
            $this->WS_AROUCA_FAILURE=TRUE;
            $this->WS_AROUCA_FAILURE_MSG=$e->getMessage();
   	 }
   
    } catch (Exception $e) {
              $this->WS_AROUCA_FAILURE=TRUE;
            $this->WS_AROUCA_FAILURE_MSG=$e->getMessage();
        }
      return $cbos;
  }
	
	

}
?>
