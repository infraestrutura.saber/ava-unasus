<?php

    require("../../../../config.php");
    
	 require("$CFG->dirroot/admin/report/synciccarouca/course/homologar_concluinte_form.php");
	 require("$CFG->dirroot/admin/report/synciccarouca/course/lib.php");
 	require("$CFG->dirroot/admin/report/synciccarouca/tab.php");
 	  require("$CFG->dirroot/admin/report/synciccarouca/cpf.php");


  // Acesso permitido apenas ao usu�rio admin
    require_capability('moodle/legacy:admin', get_context_instance(CONTEXT_SYSTEM), NULL, false);
    
    
    //Navega��o
    $navlinks = array();
    $navlinks[] = array('name' => get_string('synciccarouca','report_synciccarouca'), 'link' => ".", 'type' => 'misc');
    $navigation = build_navigation($navlinks);
    
   //tab menu
     $currenttab = 'homologar_concluintes';

    
   
       //regras da sinc
    $syc_ing_conc=new ingr_conc_sync_couse();
    $cpf=new cpf();
	 
	 $form_curso= new selecionar_curso_form();
    
    //par�metros
   $param=  new object();
   $param->courseid=optional_param('courseid', NULL,PARAM_INT);
   $param->status= $syc_ing_conc::$STATUS_CONCLUINTE_HOMOLOGADO_MOODLE;
   $param->homologar= optional_param('homologar', 0,PARAM_INT); //par�metro de controle para processar o formul�rio de homologa��o
   
   
   
  
  //retornar ao formul�rio de sele��o do curso clicar no bot�o cancelar
   if (!empty($_POST['cancel'])) {
                    redirect("$CFG->wwwroot/admin/report/synciccarouca/course/homologar_concluintes.php");
   }
  
  //extrair rela��o dos conlcuintes para homologar se o par�metro curso for passado
  if($param->courseid){
  		$rows = $syc_ing_conc->recuperarConcluinteParaHomologarcaoManual($param->courseid,$param->status);
    	$rows_count = $syc_ing_conc->contarConcluinteParaHomologarcaoManual($param->courseid,$param->status);
  }
  //processar formul�rio de homologa��o
   if($param->courseid  && $param->homologar){
   			  foreach ($rows as $row){
	       	//echo "<br>param id: ".optional_param($row->id,false, PARAM_TEXT);
      			if(optional_param($row->id,false, PARAM_TEXT)){
      					$dto=new object();
      					$dto->id=$row->id;
      					$dto->mdluseridaut=$USER->id;
      					$dto->status=$syc_ing_conc::$STATUS_CONCLUINTE_HOMOLOGADO_GESTOR;
      					echo "<br>ID SELECIONADO1: ". $row->id;
      					echo "editado: ".$syc_ing_conc->editar($dto);
      					//atualizar ap�s o cadastro
      					$rows = $syc_ing_conc->recuperarConcluinteParaHomologarcaoManual($param->courseid,$param->status);
    					$rows_count = $syc_ing_conc->contarConcluinteParaHomologarcaoManual($param->courseid,$param->status);
      				}
      		}
      	
    	
   } 
 
    function info(){
    	
    	global $param,$rows_count,$syc_ing_conc;
    	$curso=$syc_ing_conc->get_course($param->courseid);
    	$output="<br>";
    	$output.="<center>";
    	$output.= "<a href='homologar_concluintes.php'>".get_string('voltar_selecionar_outro_curso','report_synciccarouca')."</a>";
    	$output.="</center>";
    	$output.="<br>";
    	$output.="<h4>";
    	$output.= get_string('course','report_synciccarouca').": ";
    	$output.=$curso->fullname;
    	$output.="<br>";
    	$output.=get_string('num_concluintes_nhomologados','report_synciccarouca');
    	$output.=$rows_count;
    	$output.="</h4>";
    	$output.="<br>";
    	echo $output;
    }  
    
    function form(){
    	global $rows,$rows_count,$syc_ing_conc,$cpf,$param,$uf,$cbo;
    	
    	
    		$output = ' <FORM ACTION="homologar_concluintes.php"  method="POST">
    				<INPUT TYPE=HIDDEN NAME="homologar" value="1"> .
    				<INPUT TYPE=HIDDEN NAME="courseid" value=\"'.$param->courseid.'"\">
    				<table class="generaltable   border="1"  boxaligncenter" width="95%" cellspacing="1" cellpadding="5">';
    $output .= '<tr>
                <th align="left">'.get_string('name').'</th>
				<th align="left">'.get_string('cpf','report_synciccarouca').'</th>
				<th align="left">'.get_string('email','report_synciccarouca').'</th>
				<th align="left">'.get_string('metodo_matricula','report_synciccarouca').'</th> 		
				<th align="left">'.get_string('data_ingresso','report_synciccarouca').'</th>
				<th align="left">'.get_string('data_conclusao','report_synciccarouca').'</th>
				<th align="left">'.get_string('nota_final','report_synciccarouca').'</th>
				<th align="left">'.get_string('homologar','report_synciccarouca').'</th>
                
                </tr>';
               
  		
    if(!empty($rows)){
    	 foreach ($rows as $row){
        	
        
        	
    	    
        	 $output .= '<tr>';
			$output .= '<td>'.$row->firstname.' '.$row->lastname.'</td>';
			$output .= '<td>'.$cpf->format($row->cpf).'</td>';
            $output .= "<td>$row->email</td>";
             
              $output .= "<td>$row->mdlenrol</td>";
           $output .= "<td>".date('d/m/Y',$row->dataingresso)."</td>";
            $output .= "<td>".date('d/m/Y',$row->dataconclusao)."</td>";  
            $output .= "<td>".number_format($row->finalgrade,2,",",".")."</td>";            
             $output .= "<td><INPUT TYPE=CHECKBOX NAME=\"".$row->id."\" checked></td>";
            $output .= '</tr>';   
    	}
    	
    }  
    
    $output .= '</table>
    	
    		<center><input type="submit" name="Operation" value="Homologar"></center>	
    				</form>
    		';
    print_box($output);
    }
    print_header(get_string('synciccarouca','report_synciccarouca'),get_string('synciccarouca','report_synciccarouca'), $navigation);
   
 	print_tabs($tabs, $currenttab, $inactive, $activated);
    
     if($param->courseid){
     	 info();
     	if($rows_count>0)form();
     }
   	 else $form_curso->display();
    
   
    print_footer();
?>