<?php
               
    $pg_atual=7;
     registro($id,unid4_pg,$pg_atual,unid4_ev);
?>
<h4 class="titulo">Referências </h4>

BARROS, A. L. B. L. de et al. <b>Anamnese e exame físico</b>: avaliação 
diagnóstica de enfermagem no adulto. Porto Alegre: Artmed, 2002. 272 p.
<br><br>

    BRASIL. Ministério da Saúde. Secretaria de Atenção à Saúde. Departamento de 

    Atenção Básica. <b>Caderno de Atenção Domiciliar. </b> Brasília: Ed. Ministério da Saúde, 2013.
<br><br>

    
    BRASIL. Lei Nº 7.498 de 25 de junho de 1986. Dispõe sobre a regulamentação 
    do exercício da enfermagem, e dá outras providências. <b> . Diário Oficial 
    [da] República Federativa do Brasil, </b>Poder Legislativo, Brasília, DF, 26 
    jun. 1986. Seção 1, p. 9.273 a 9.275. Disponível em: 
    &#60<a href="http://www.planalto.gov.br/ccivil_03/leis/L7498.htm"  target="_blank">http://www.planalto.gov.br/ccivil_03/leis/L7498.htm</a>&#62.Acesso em: 8 abr. 2013.
    <br><br>

   CONSELHO FEDERAL DE ENFERMAGEM. Resolução Nº 0450/2013. <b>Normatiza o procedimento de Sondagem Vesical  no âmbito do Sistema 
   COFEN / Conselhos Regionais de Enfermagem.</b> Disponível em:<br> 
   &#60<a href="http://www.cofen.gov.br/resolucao-cofen-no-04502013-4_23266.html"  target="_blank"> http://www.cofen.gov.br/resolucao-cofen-no-04502013-4_23266.html.</a>&#62
    Acesso em: 22 nov. 2013.
	<br><br>
   CONSELHO FEDERAL DE ENFERMAGEM. <b>Parecer normativo para atuação da equipe de enfermagem em sondagem vesical.</b>
    Disponível em:
   &#60<a href="http://www.cofen.gov.br/wp-content/uploads/2014/01/ANEXO-PARECER-NORMATIVO-PARA-ATUACAO-DA-EQUIPE-DE-ENFERMAGEM-EM-SONDAGEM-VESICAL1.pdf"  target="_blank"> 
   	http://www.cofen.gov.br/wp-content/uploads/2014/01/ANEXO-PARECER-NORMATIVO-PARA-ATUACAO-DA-EQUIPE-DE-ENFERMAGEM-EM-SONDAGEM-VESICAL1.pdf.</a>&#62
  Acesso em: 22 nov. 2014.
	<br><br>
    IZZO, H. Papel da fisioterapia. In: YAMAGUCHI, A. M. et al. <b>Assistência 
    domiciliar:</b> uma proposta interdisciplinar. Barueri, SP: Manole, 2010. p. 44-47.<br><br>


    LENZ, L. L. Cateterismo vesical: cuidados, complicações e medidas preventivas. <b>Arquivos Catarinenses de Medicina,</b> Florianópolis, 
    v. 35, n. 1, p.82-91, 2006. Disponível em: &#60<a href="http://www.acm.org.br/revista/scripts/pdf.php?CD_ARTIGO=361"  target="_blank"> http://www.acm.org.br/revista/scripts/pdf.php?CD_ARTIGO=361</a>&#62. Acesso em: 7 abr. 2013.

	<br><br>
    SMELTZER, S. C.; BARE, B. G.<b> Tratado de enfermagem médico-cirúrgica.</b> 10. ed. v. 3. Rio de Janeiro: Guanabara Koogan, 2005.
    
	<br><br>
    SOUZA, E. do C. de; MOROOKA, M.; GONÇALVES, S. R. <b>Atendimento ao usuário com necessidade de cateterismo vesical intermitente</b>. 
    Londrina: Secretaria Municipal de Saúde, 2011. Disponível em: &#60<a href="http://www.londrina.pr.gov.br/dados/images/stories/Storage/sec_saude/protocolos_clinicos_saude/instrucao_de_trabalho_acvi_tl_2011.pdf" target="_blank"> 
    	http://www.londrina.pr.gov.br/dados/images/stories/Storage/sec_saude/protocolos_clinicos_saude/instrucao_de_trabalho_acvi_tl_2011.pdf</a>&#62. Acesso em: 12 mar. 2014.

	<br><br>
    TIMBY, B. K.<b> Conceitos e habilidades fundamentais no atendimento de enfermagem.</b> 6. ed. Porto Alegre: Artmed, 2001. 836p.

       


   