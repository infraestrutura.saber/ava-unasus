<?php
               
    $pg_atual=1;
     registro($id,unid4_pg,$pg_atual,unid4_ev);
?>

<style type="text/css">

#lista li{
 list-style-type: disc;   
  margin-left: 30px;

}
</style> 

   <h4 class="titulo">O que é e para que serve a sondagem vesical?</h4>

Durante a assistência prestada no 
domicílio, os profissionais de saúde se deparam com pacientes que, por um longo 
período de tempo, foram subjugados a uma internação em decorrência de patologias 
agudas ou crônicas. Ao retornarem para o ambiente domiciliar, podem necessitar de 
uma variedade de sondas, cateteres e outros dispositivos necessários ao seu cuidado. 
<br><br>
Dentro dessa perspectiva, a equipe de enfermagem deve estar preparada para auxiliar 
na escolha desses dispositivos e acessórios, orientar o manuseio e os cuidados 
necessários, na tentativa de sanar dúvidas em busca da melhor adaptação do 
indivíduo ao seu contexto diário (IZZO, 2010). 
<br><br>
A sondagem vesical é caracterizada 
pela introdução de um cateter estéril através da uretra até a bexiga, quando o 
paciente está impossibilitado de urinar, necessita realizar exames ou quando há 
a necessidade de uma melhor avaliação clínica. O uso da técnica asséptica/estéril
 evita uma possível infecção urinária no paciente (TIMBY, 2001).
    <br><br>
No atendimento domiciliar, este 
procedimento tem por finalidade (BARROS, 2002):
 
  <ul id="lista">
<li>colher urina asséptica para exames;</li>
<li>controlar o débito urinário;</li>
<li>proporcionar conforto em indivíduos com incontinência urinária;
<li>aliviar a retenção urinária aguda ou crônica, quando as medidas para estimular a micção forem ineficazes.</li>
  </ul>


<br><br>

<div class="box">
        <img src="../images/img_atencao_ad.png" alt="Atenção" >
        <span class="titulo_box">Atenção!</span>
        <br>
        <hr/>
            É importante lembrar 
            que esta é uma via que pode ser usada para administrar medicações,<b> 
            somente quando prescritas pelo médico responsável. </b>
<br>
</div>