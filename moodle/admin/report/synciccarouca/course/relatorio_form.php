<?php //$Id: forgot_password_form.php,v 1.7.2.1 2007/11/23 22:12:35 skodak Exp $

require_once $CFG->libdir.'/formslib.php';

class relatorio_form extends moodleform {

    function definition() {
    	global $param;
    	
    	$syc_ing_conc=new ingr_conc_sync_couse();
 
        $mform    =& $this->_form;
        $renderer =& $mform->defaultRenderer();

        $mform->addElement('header', '',get_string('relatorio_sinc_arouca','report_synciccarouca'), '');
        
      	
        $mform->addElement('select', 'courseid',get_string('course','report_synciccarouca'),$syc_ing_conc->get_courses_html_select());
        $mform->setType('courseid', PARAM_INT);
         $mform->setDefault('courseid', $param->courseid);
         
         $mform->addElement('select', 'userid', get_string('user','report_synciccarouca'),$syc_ing_conc->get_user_html_select());
        $mform->setType('userid', PARAM_INT);
         $mform->setDefault('userid', $param->userid);
         
        $mform->addElement('select', 'status', get_string('status','report_synciccarouca'),$syc_ing_conc->get_status_tabela_dominio());
        $mform->setType('status', PARAM_INT);
		 $mform->setDefault('status', $param->status);
        //data
        $this->add_action_buttons(true,get_string('research','report_synciccarouca'));
    }

    function validation($data, $files) {
        global $CFG;

         $errors = parent::validation($data, $files);
         
      
     return $errors;
    }

}

?>