<!DOCTYPE html>
<!-- <html lang="pt-br" manifest="unasus_ufpe.manifest"> -->
<head>
	<meta charset="utf-8">
	<!--<html lang="pt-br" manifest="unasus_ufpe.manifest">-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>HPV</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1"/>
	 <link rel="stylesheet" type="text/css" href="css/style.css">
	 <link rel="stylesheet" type="text/css" href="libs/bootstrap/css/bootstrap.css">
	 <link rel="stylesheet" type="text/css" href="libs/bootstrap/css/bootstrap-responsive.css">
	 <link type="text/css" href="libs/aTooltip/css/atooltip.css" rel="stylesheet"  media="screen" />
	 <script type="text/javascript" src="libs/jquery.js"></script>
	 <script type="text/javascript" src="scripts/script.js"></script>
	 <script type="text/javascript" src="libs/aTooltip/js/jquery.atooltip.min.js"></script> 
	 <script type="text/javascript" src="libs/bootstrap/js/bootstrap.min.js"></script> 
	 <script src = "https://www.youtube.com/player_api"></script>
</head>
<body >
<!--plugin google analitics-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-48444233-1', 'unasus.gov.br');
  ga('send', 'pageview');

</script>

<?php 

       require_once('../../config.php');
      

      require_login();
      $USUARIO = &$_SESSION['USER'];

?>
<div class="modal tutorial hide fade">
	 <div class="modal-header">
	   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	   <h3>Guia de uso</h3>
	 </div>
	 <div class="modal-body">
	 	<!--Em breve o guia de uso estará disponível.-->

	 		<!-- div responsiva -->
	          <div class="videoresponsivo">

				
	         	<div id="player"></div> 
				<script type="text/javascript">

					 var player;
					 function onYouTubeIframeAPIReady() {
					 		player = new YT.Player('player', {
					 		height: '390',
					 		width: '640',
					 		videoId: '_sIjIbW-MC8',
					 		events: {
					
					 		}
					 	});
					 }

					 function stopVideo() {
					 	player.stopVideo();
					 }
			</script>
		
</div>
<input style="position:absolute;" type="checkbox" name="check_modal" id="check_modal" value="">
<p style="margin-left: 18px;">Não exibir na próxima vez</p>

<script type="text/javascript">

				function videoTutorial(){
					$('.tutorial').modal('show');
				}

			
				if (localStorage.getItem('modal_block') == 'on') {
					if (document.getElementById('check_modal') != null)
						document.getElementById('check_modal').checked = true;
				} else {
					if (document.getElementById('check_modal') != null){
						document.getElementById('check_modal').checked = false;
						videoTutorial();					
					}

				}
			 

			$( "#check_modal" ).change(function() {

			 	if($("#check_modal").is(":checked") == true){
			 		 localStorage.setItem('modal_block', 'on');
			 	}else{
			 		 localStorage.setItem('modal_block', 'off');
			 	}



			});

</script>

	  </div>
 <div class="modal-footer">
<p align="left">Caso você não esteja visualizando o vídeo, verifique se o plug-in Adobe Flash Player está instalado e habilitado no seu navegador.<br> Para mais informações, clique <a target="_blank" href="http://get.adobe.com/br/flashplayer/?no_redirect">aqui.</a></p>
</div> 
 </div>
 <script>

 	
 	$(".modal" ).on('hidden.bs.modal', function () {
  		 stopVideo();
	 });

	
 </script>
	<!-- modal certificado  -->
		<div class="modal certificado hide fade">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3>Certificação</h3>
  </div>
  <div class="modal-body">
    <p>Esta versão do curso está disponível para todos os interessados no assunto, porém não dá direito à certificação.<br>
  <br>
Se você deseja receber certificado de conclusão do curso, participe da versão com certificação. Lembrando que, neste caso, <b>o curso é exclusivo para enfermeiros, médicos, técnicos de enfermagem e auxiliares de enfermagem.</b>
 <br> <br>
Bons estudos!
 <br> <br>
Equipe UNA-SUS UFPE</p>
  </div>
  
</div>
	<!-- fim modal certificado  -->
	<div id="container">
		<div id="topo" class="hidden-desktop">
			<nav class="menu">	
		    <input type="checkbox" id="button" onclick="toogle_menu();">
		    <!-- voltar para nivel modulo -->
				<label for="button"><img class="marca" src="imagens/logo.svg"></label>
				<a onclick="voltarModulo('index.php');"><img class="botaoVoltarModulo" src="imagens/arrow.svg" alt="Botão Voltar"></a>
			<!-- voltar para nivel unidade -->
				<a onclick="voltarUnidade();"><img class="botaoVoltarUnidade" src="imagens/arrow.svg" alt="Botão Voltar"></a>
		    
				
			<ul class="menu_ul">
				<li>
					<a onclick="fechar_menu();" class="unidade1">Unidade 1</a>
				</li>
				<li>
					<a onclick="fechar_menu();" class="unidade2">Unidade 2</a>
				</li>
				<li>
					<a onclick="fechar_menu();" class="unidade3">Unidade 3</a>
				</li>
				<li>
					<a onclick="load_menu('certificacao.html');">Certificação</a>
				</li>
				<li>
					<a onclick="fechar_menu();">Guia</a>
				</li>
				<li>
					<a onclick="load_menu('perfil_opiniao.html');">Sua opinião</a>
				</li>
				<li>
				<a href="https://sistemas.unasus.gov.br/suporte/"  target="_blank">
					Suporte
				</a>
				</li>
				<li>
					<a onclick="load_menu('creditos.html');">Créditos</a>
				</li>
				<li>
					<a onclick="fechar_menu();">Sair</a>
				</li>
				
			</ul>
		</nav>

	</div>

		<div id="topo" class="visible-desktop">
			<!-- voltar para nivel modulo -->
				<label for="button" onclick></label>
				<a onclick="voltarModulo('index.php');"><img style="cursor:pointer;" class="botaoVoltarModulo" src="imagens/arrow.svg" alt="Botão Voltar"></a>
				<div class="nome_voltar"></div>
			<!-- voltar para nivel unidade -->
				<a onclick="voltarUnidade();"><img style="cursor:pointer;" class="botaoVoltarUnidade" src="imagens/arrow.svg" alt="Botão Voltar"></a>
			<img class="marca"  src="imagens/logo.svg" alt="Marca Unasus">
			<div id="sair"><a href="http://ufpe.unasus.gov.br/moodle_unasus/">Sair <img src="imagens/sair.png"></a></div>
		</div>


			<!--<img class="marca"  src="imagens/logo.svg" alt="Marca Unasus">-->
			
		<!--</div>-->
		<div id="imagemCurso" class="centered">
                       <div id="relativo">
                       		   <div id="rosa"></div>
                               <div id="roxo"></div>
                               <div id="laranja"></div> 

                               <div class="centered hidden-phone margem_topo absoluto">
                                       <img src="imagens/titulo_HPV_web.svg">
                               </div>
                               <div id="imagemCurso" class="centered visible-phone margem_topo absoluto-m">
                                       <img src="imagens/titulo_HPV_mobile.svg">
                               </div>
                       </div>

               </div>

		<script type="text/javascript">
		

		if (localStorage.getItem('entrou_na_unidade') == null) {
			//retirado por conta da campanha eleitoral
           // $('.tutorial').modal('show');
            } 

            var ver_tutorial = localStorage.setItem('entrou_na_unidade',1);


		//Atualizar css
		function removejscssfile(filename, filetype){
               var targetelement=(filetype=="js")? "script" : (filetype=="css")? "link" : "none" //determine element type to create nodelist from
               var targetattr=(filetype=="js")? "src" : (filetype=="css")? "href" : "none" //determine corresponding attribute to test for
               var allsuspects=document.getElementsByTagName(targetelement)
               for (var i=allsuspects.length; i>=0; i--){ //search backwards within nodelist for matching elements to remove
                if (allsuspects[i] && allsuspects[i].getAttribute(targetattr)!=null && allsuspects[i].getAttribute(targetattr).indexOf(filename)!=-1)
                 allsuspects[i].parentNode.removeChild(allsuspects[i]) //remove element by calling parentNode.removeChild()
           }
          }


          removejscssfile("css/unidade1.css", "css") //remove all occurences "somestyle.css" on page
          removejscssfile("css/unidade2.css", "css") //remove all occurences "somestyle.css" on page
          removejscssfile("css/unidade3.css", "css") //remove all occurences "somestyle.css" on page



              function naoDeixaSair(){
              	//$('').modal('show');
                return "Se você clicar em OK, estará saindo do curso. Tem certeza que deseja sair?";

              }
              onbeforeunload = naoDeixaSair; 
          

				//teste de conexao
				function verificaStatus() {  

					$('.defaultTheme').remove();
					if(window.navigator.onLine){  
				 	

						$('.off').attr('style', 'display: block !important');
					 	$( ".on" ).html( "" );
					 	//alert('online user');
					 	

					
					} else {  
						

					 	$('.off').attr('style', 'display: none !important');
						$( ".on" ).append( "<img src='imagens/off.png'>" );
						//alert('OFF');
						
					} 
					
				}

				function unidades(){
						
							$(".unidade1").click(function(){
								sessionStorage.setItem('uni',1);
								var x = sessionStorage.getItem('uni');
								$('#imagemCurso').css('background-image', 'url(imagens/textura_rosa.jpg)');
								$( "#principal" ).load( "conteudo/unidade_1/index.html" , function(){
						 		//load_topico();
						 		// unidades();
						 		verificaStatus();

						 		verificaTopico('topico',14);
						 		
						 });
								$(".botaoVoltarModulo").show();
								$('.nome_voltar').html('<b>Unidade 1</b>');
							});

							$(".unidade2").click(function(){
								sessionStorage.setItem('uni',2);
								var x2 = sessionStorage.getItem('uni');
								$('#imagemCurso').css('background-image', 'url(imagens/textura_roxo.jpg)');
								$( "#principal" ).load( "conteudo/unidade_2/index.html" , function(){
						 		//load_topico();
						 		// unidades();
						 		verificaStatus();
						 		
						 		verificaTopico('uni2_top',11);
						 });
								$(".botaoVoltarModulo").show();
								$('.nome_voltar').html('<b>Unidade 2</b>');
							});

							$(".unidade3").click(function(){
								sessionStorage.setItem('uni',3);
								var x3 = sessionStorage.getItem('uni');
								$('#imagemCurso').css('background-image', 'url(imagens/textura_laranja.jpg)');
								$( "#principal" ).load( "conteudo/unidade_3/index.html" , function(){
						 		//load_topico();
						 		// unidades();
						 		verificaStatus();

						 		verificaTopico('uni3_top',7);
						 });
								$(".botaoVoltarModulo").show();
								$('.nome_voltar').html('<b>Unidade 3</b>');
								
								//$(".botaoVoltarModulo").show();
					});
						}

				unidades();
				
				// funcao que volta para o nivel modulo
				function voltarModulo(local){
						$("#principal").load(local+' #principal',function(){
								unidades();
								verificaStatus();
						});	
						$(".botaoVoltarModulo").hide();
						$('.nome_voltar').html('Início');
						$('#imagemCurso').css('background-image', 'url(imagens/textura_roxo.jpg)');
						
					}
					voltarModulo();
				// funcao que volta para o novel unidade
				function voltarUnidade(){
							var unidade = sessionStorage.getItem('uni');
						$( "#principal" ).load( "conteudo/unidade_"+unidade+"/index.html",function(){
						 		//load_topico();
						 		// unidades();
						 		verificaStatus();

						 		if (unidade == 1) {
						 			verificaTopico('topico',14);
						 			$('.nome_voltar').html('<b>Unidade 1</b>');
						 		} else if(unidade == 2){
						 			verificaTopico('uni2_top',11);
						 			$('.nome_voltar').html('<b>Unidade 2</b>');
						 		}else if(unidade == 3){
						 			verificaTopico('uni3_top',7);
						 			$('.nome_voltar').html('<b>Unidade 3</b>');
						 		}
						 });
						
						
						$(".botaoVoltarUnidade").hide();
						$(".botaoVoltarModulo").show();
						
						
					}



					//função verifica se o aluno já concluiu algum tópico.
					function verificaTopico(chave,quantidade){
						 			for (var i = 1; i< (quantidade + 1); i++) {
						 				var x = localStorage.getItem(chave+i);
						 				
											if(x != null){

												
						 							$('.topico:eq('+(x-1)+')').addClass('marcar_topico'); 
						 						}
													
						 						
						 					}
						 			}
						 		

				//pega posicionamento do aluno no topico. paginação.
					function posicaoTopico(){
						 			
						 				var x = sessionStorage.getItem('posicao_topico');
						 				
											
						 						$('.pagination li:eq('+(x-1)+')').addClass('disabled'); 
						 		}



				//funcao que carrega os modulos
				function load_topico(topico, unidade){
							if(unidade == '1'){
								$(".conteudo").load('conteudo/unidade_1/topicos/'+topico, function(){
						 		//load_topico();
						 		// unidades();

						 		$('.nome_voltar').html('<b>Unidade 1</b>');

						 		verificaTopico('topico',14);
						 		
						 		
						 		var top = topico;
						 		if (top == "unid1_top1.html") {
							 		localStorage.setItem('topico1',1);	
							 		sessionStorage.setItem('posicao_topico',1);
						 		}

						 		else if(top == "unid1_top2.html"){
						 			localStorage.setItem('topico2',2);
						 			sessionStorage.setItem('posicao_topico',2);						 			
						 		}
						 		else if(top == "unid1_top3.html"){
						 			localStorage.setItem('topico3',3);						 			
						 			sessionStorage.setItem('posicao_topico',3);
						 		}
						 		else if(top == "unid1_top4.html"){
						 			localStorage.setItem('topico4',4);
							 		sessionStorage.setItem('posicao_topico',4);
						 		}
						 		else if(top == "unid1_top5.html"){
						 			localStorage.setItem('topico5',5);
						 			sessionStorage.setItem('posicao_topico',5);
						 		}
						 		else if(top == "unid1_top6.html"){
						 			localStorage.setItem('topico6',6);
						 			sessionStorage.setItem('posicao_topico',6);
						 		}
						 		else if(top == "unid1_top7.html"){
						 			localStorage.setItem('topico7',7);
						 			sessionStorage.setItem('posicao_topico',7);
						 		}
						 		else if(top == "unid1_top8.html"){
						 			localStorage.setItem('topico8',8);
						 			sessionStorage.setItem('posicao_topico',8);
						 		}
						 		else if(top == "unid1_top9.html"){
						 			localStorage.setItem('topico9',9);
						 			sessionStorage.setItem('posicao_topico',9);
						 		}
						 		else if(top == "unid1_top10.html"){
						 			localStorage.setItem('topico10',10);
						 			sessionStorage.setItem('posicao_topico',10);
						 		}
						 		else if(top == "unid1_top11.html"){
						 			localStorage.setItem('topico11',11);
						 			sessionStorage.setItem('posicao_topico',11);
						 		}
						 		else if(top == "unid1_top12.html"){
						 			localStorage.setItem('topico12',12);
						 			sessionStorage.setItem('posicao_topico',12);
						 		}
						 		else if(top == "unid1_top13.html"){
						 			localStorage.setItem('topico13',13);
						 			sessionStorage.setItem('posicao_topico',13);
						 		}
						 		else if(top == "unid1_top14.html"){
						 			localStorage.setItem('topico14',14);
						 			sessionStorage.setItem('posicao_topico',14);
						 		}

						 		else {
						 			alert('nada');
						 		}

						 		posicaoTopico();
						 		verificaStatus();
						 });		
								$(".botaoVoltarUnidade").show();		

							}if(unidade == '2'){
								$(".conteudo").load('conteudo/unidade_2/topicos/'+topico, function(){
						 		//load_topico();
						 		// unidades();

						 		$('.nome_voltar').html('<b>Unidade 2</b>');

						 		verificaTopico('uni2_top',11);

						 		var top = topico;
						 		if (top == "unid2_top1.html") {
						 			localStorage.setItem('uni2_top1',1);
						 			sessionStorage.setItem('posicao_topico',1);
						 		}
						 		else if(top == "unid2_top2.html"){
						 			localStorage.setItem('uni2_top2',2);
						 			sessionStorage.setItem('posicao_topico',2);						 			
						 		}
						 		else if(top == "unid2_top3.html"){
						 			localStorage.setItem('uni2_top3',3);
						 			sessionStorage.setItem('posicao_topico',3);						 			
						 		}
						 		else if(top == "unid2_top4.html"){
						 			localStorage.setItem('uni2_top4',4);
						 			sessionStorage.setItem('posicao_topico',4);
						 		}
						 		else if(top == "unid2_top5.html"){
						 			localStorage.setItem('uni2_top5',5);
						 			sessionStorage.setItem('posicao_topico',5);
						 		}
						 		else if(top == "unid2_top6.html"){
						 			localStorage.setItem('uni2_top6',6);
						 			sessionStorage.setItem('posicao_topico',6);
						 		}
						 		else if(top == "unid2_top7.html"){
						 			localStorage.setItem('uni2_top7',7);
						 			sessionStorage.setItem('posicao_topico',7);
						 		}
						 		//topico resumo
						 		else if(top == "unid2_top8.html"){
						 			localStorage.setItem('uni2_top8',8);
						 			sessionStorage.setItem('posicao_topico',8);
						 		}
						 		//teste seus conhecimentos
						 		else if(top == "unid2_top9.html"){
						 			localStorage.setItem('uni2_top9',9);
						 			sessionStorage.setItem('posicao_topico',9);
						 		}
						 		//referencia
						 		else if(top == "unid2_top10.html"){
						 			localStorage.setItem('uni2_top10',10);
						 			sessionStorage.setItem('posicao_topico',10);
						 		}
						 		else if(top == "unid2_top11.html"){
						 			localStorage.setItem('uni2_top11',11);
						 			sessionStorage.setItem('posicao_topico',11);
						 		}
						 		else {

						 		}

						 		verificaStatus();
						 		posicaoTopico();
						 		
						 });		

							}if(unidade == '3'){
								$(".conteudo").load('conteudo/unidade_3/topicos/'+topico ,function(){
						 		//load_topico();
						 		// unidades();

						 		$('.nome_voltar').html('<b>Unidade 3</b>');


						 		verificaTopico('uni3_top',7);

						 		var top = topico;
						 		if (top == "unid3_top1.html") {
						 			localStorage.setItem('uni3_top1',1);
						 			sessionStorage.setItem('posicao_topico',1);
						 		}
						 		else if(top == "unid3_top2.html"){
						 			localStorage.setItem('uni3_top2',2);
						 			sessionStorage.setItem('posicao_topico',2);
						 		}
						 		else if(top == "unid3_top3.html"){
						 			localStorage.setItem('uni3_top3',3);
						 			sessionStorage.setItem('posicao_topico',3);
						 		}
						 		else if(top == "unid3_top4.html"){
						 			localStorage.setItem('uni3_top4',4);
						 			sessionStorage.setItem('posicao_topico',4);
						 		}
						 		else if(top == "unid3_top5.html"){
						 			localStorage.setItem('uni3_top5',5);
						 			sessionStorage.setItem('posicao_topico',5);
						 		}
						 		else if(top == "unid3_top6.html"){
						 			localStorage.setItem('uni3_top6',6);
						 			sessionStorage.setItem('posicao_topico',6);
						 		}
						 		else if(top == "unid3_top7.html"){
						 			localStorage.setItem('uni3_top7',7);
						 			sessionStorage.setItem('posicao_topico',7);
						 		}
						 		else{
						 			
						 		}

						 		verificaStatus();
						 		posicaoTopico();

						 		
						 });
							}
							$(".botaoVoltarModulo").hide();
							$(".botaoVoltarUnidade").show();
				}
				//funcao que carrega itens menu
				function load_menu(item){
							
								$(".conteudo").load('menu/'+item, function(){
						 		//load_topico();
						 		// unidades();
						 		verificaStatus();
						 });		
					
							$(".botaoVoltarModulo").hide();
							$(".botaoVoltarUnidade").show();
							fechar_menu();
				}

				function load_menu_principal(item){
							if(item == 'principal.html'){
								$('.menu_principal').show();

							}else {
								$('.menu_principal').hide();
							}
								$(".conteudo").load('menu/'+item, function(){
						 		//load_topico();
						 		// unidades();
						 		verificaStatus();
						 });		
				}
				
				function fechar_menu(){

					$(".menu_ul").css("display","none");

				}

				function toogle_menu(){
					if($(".menu_ul").is(":visible") == false){
						$(".menu_ul").css("display","block");
						
					}else{
						$(".menu_ul").css("display","none");
					
					}
					
					
				}

				
				
				function modalCertificacao(){
					$('.certificado').modal('show');
				}
				
		
		</script>
		<div id="principal">
		<!--<div class="span12 hidden-desktop no_left"><div class="videoresponsivo"><iframe width="217" height="200" src="//www.youtube.com/embed/wLPGqqL2L58??wmode=transparent" frameborder="0" allowfullscreen></iframe></div></div>-->
		<div class="row-fluid">
	  		<div class="conteudo span8">
	  			<br>
	  			<h4>Bem-vindo(a) à capacitação Vacinação contra o Papilomavírus Humano!</h4>
	  			
<br>
A vacinação de adolescentes contra o Papilomavírus Humano (HPV) é uma importante estratégia de saúde pública para a prevenção do câncer do colo do útero, terceiro câncer de maior incidência entre as mulheres no Brasil.<br><br> 

Nesse sentido, caro(a) trabalhador(a) da saúde, a sua capacitação é muito importante para o êxito dessa estratégia e para alcançarmos a meta de vacinação preconizada pelo Ministério da Saúde. <br><br>

Neste curso, você terá a oportunidade de entender os aspectos relacionados ao vírus HPV, bem como sua relação com o câncer do colo do útero. Verá, ainda, as ações para operacionalizar a  campanha de vacinação contra o HPV e os aspectos relacionados ao registro nominal da vacinação e à cobertura vacinal. 
<br><br>
<b>Aproveite e bons estudos!</b><br><br>
</div>

	  			<script type="text/javascript">
	  	  removejscssfile("css/unidade1.css", "css") //remove all occurences "somestyle.css" on page
          removejscssfile("css/unidade2.css", "css") //remove all occurences "somestyle.css" on page
          removejscssfile("css/unidade3.css", "css") //remove all occurences "somestyle.css" on page
	  			</script>


 
<div class="span4" >

                  <div class="span12 no_left visible-desktop">
                    <div class="span12 no_left">
                    <a onclick="videoTutorial();"><div class="span6 box2 centered no_left boxMenuPrincipal"> Guia de uso</div></a> 
                    <a onclick="load_menu_principal('principal.html');"><div class="span6 box2 centered no_left boxMenuPrincipal">Página inicial</div></a>  
                    <a href="https://sistemas.unasus.gov.br/suporte/"  target="_blank"><div class="span6 box2 centered no_left boxMenuPrincipal" >Suporte</div></a>                  
                    <a onclick="load_menu_principal('perfil_opiniao.html');"><div class="span6 box2 centered no_left boxMenuPrincipal" >Perfil e opinião</div></a>
                    <a onclick="modalCertificacao();"><div class="span6 box2 centered no_left boxMenuPrincipal">Certificação</div></a>
                    <a onclick="load_menu_principal('creditos.html');"><div class="span6 box2 centered no_left boxMenuPrincipal">Créditos</div></a>

                   </div>
                   
                   </div>

                  <div class="on visible-desktop "></div>
                  <div class="off ">
                 <!--  <div class="span12 visible-desktop no_left menu_top">
                  <div class="videoresponsivo">
	  			<iframe style="z-index: 1" width="410" height="325" src="//www.youtube.com/embed/wLPGqqL2L58??wmode=transparent" frameborder="0" allowfullscreen></iframe></div>
			</div> -->
			 <div class="on visible-phone"></div>
                   <div class="off">
                  </div> 

		
	</div></div>
	<br><br>
	<div class="span12 no_left row-fluid menu_principal">
		<div class="span4 unidade1 box"><div class="borda_unid1"><img src="imagens/icones_unidade1.svg"><h4><b>Unidade 1</b> - Papilomavírus Humano (HPV) e o câncer do colo do útero</h4></div></div>
		<div class="span4 unidade2 box"><div class="borda_unid2"><img src="imagens/icones_unidade2.svg"><h4>Unidade 2 - Vacinação contra o HPV<br><br></h4></div></div>
		<div class="span4 unidade3 box"><div class="borda_unid3"><img src="imagens/icones_unidade3.svg"><h4>Unidade 3 - Registro nominal de vacina<br><br></h4></div></div>
	</div>
	</div>

</div>
<!--fim div tudo -->
<div id="linkExterno">
	<div class="row-fluid">
<div class="span12 no_left">

		<div class="span8" style="margin-left: 22px;">
		<div class="span12 no_left"></div>
		<h5><i class="icon-globe icon-white"></i> Links Externos</h5>
		
	<!--Aqui-->	
	<!--Aqui-->	
		<div id="link" class="span4 no_left"><a  href="http://www.saude.gov.br/" target="_blank">Portal Saúde</a><br>
			<a href="http://portalsaude.saude.gov.br/index.php/cidadao/principal/campanhas-publicitarias" target="_blank"> Portal Saúde - Material das Campanhas</a> <br>
			<a href="http://www.sbim.org.br/" target="_blank">Sociedade Brasileira de Imunizações</a></div>
		<div id="link" class="span4"><a href="http://www.aids.gov.br/pagina/dst-1" target="_blank">Portal Saúde - DST </a><br><a href="http://dab.saude.gov.br/portaldab/index2.php" target="blank">Portal Saúde - Atenção Básica</a></div>
		<div id="link" class="span4"><a href="http://pni.datasus.gov.br/" target="_blank">Sistema de informações do PNI </a><br><a href="http://www1.inca.gov.br/conteudo_view.asp?id=2687" target="_blank">INCA-HPV e câncer</a></div>
		</div>
		
		
		<div id="link" class="span3" style="margin-left: 22px;">
			<div class="span12 no_left"></div>
			<div class="span12 no_left" style="margin-top: -10px;"><h5><i class="icon-thumbs-up icon-white"></i> Redes sociais</h5></div>
		<div id="link" class="span4"><a href="https://pt-br.facebook.com/ProgramaNacionaldeImunizacoes" target="_blank">Facebook PNI</a><br>
			<a href="http://www.blog.saude.gov.br/" target="_blank">Blog da Saúde</a></div>
		</div>
<!--Até aqui-->
	
		<br><br>
		
	</div>
</div>
</div>

<div id="rodape">
		<img src="imagens/assinatura_ufpe2.svg" alt="Marca UFPE">
		<img src="imagens/assinatura_saber1.svg" alt="Marca Saber">
		<img src="imagens/assinatura_unasus3.svg" alt="Marca Unasus">
<!--		<img src="imagens/assinatura_secretaria4.svg" alt="Marca Secretaria">
		<img src="imagens/assinatura_sus5.svg" alt="Marca SUS">-->
		<img src="imagens/assinatura_ministerio6.svg" alt="Marca Ministério">
		<!--<img src="imagens/assinatura_governo7.svg" alt="Marca Governo">-->


</div>
</body>
<script type="text/javascript">
verificaStatus();
</script>
</html>
