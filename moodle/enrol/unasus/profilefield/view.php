<?php

   require("../../../config.php");
   require("$CFG->dirroot/enrol/unasus/lib/context.php");
      require("$CFG->dirroot/enrol/unasus/profilefield/category/lib.php");
   require("$CFG->dirroot/enrol/unasus/profilefield/lib.php");
   require("$CFG->dirroot/enrol/unasus/profilefield/form_view.php");
  
 require_login();
   
    require_capability('enrol/unasus:profilefieldview', get_context_instance(CONTEXT_SYSTEM), NULL, false);
  
     add_to_log(1, 'enrol_unasus', 'profilefieldview', "enrol/unasus/profilefield/view.php?contextid=$contextid", getremoteaddr());    
   
  
   
   $param=  new object();
    $param->contextid= required_param('contextid',PARAM_INT);
   //  $profilefield_category =new profilefield_category();
	//$profilefield_field = new profilefield_field(); 
	$context =new context();
	$context_name=$context->get_name($param->contextid);
    $form= new form_view();
    
   //Navega��o
   $navigation = build_navigation(array(array('name' => get_string('enrolname','enrol_unasus'), 'link' => "$CFG->wwwroot/enrol/unasus/index.php", 'type' => 'misc'),
                                     array('name' => get_string('profilefield','enrol_unasus'), 'link' => "$CFG->wwwroot/enrol/unasus/profilefield/index.php", 'type' => 'misc'), array('name' => $context_name, 'link' => null, 'type' => 'misc'), array('name' => get_string('profilefield_view_form','enrol_unasus'), 'link' => null, 'type' => 'misc')));
    
    print_header(get_string('enrolname','enrol_unasus'),get_string('applicationsenrolment','enrol_unasusem'), $navigation);
   $form->display();
   print_footer();
?>