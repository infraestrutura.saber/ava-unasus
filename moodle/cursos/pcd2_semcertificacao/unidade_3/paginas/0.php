<div style='display:none'>
			<div id="content_color1" style='padding:10px; background:#fff;'>

			<div id="texto_color"></div>
			<br><br>
			<a class="btn inline" href="index.php?pagina=0" onClick="apagar_confirmar();" >Sim, desejo recomeçar os estudos.</a>
			<a class="btn" href="index.php?pagina=0">Não, quero continuar.</a>
				
			<br><br>
			
			</div>
</div>

<style type="text/css">
        #lista{
            list-style-type: disc; 
             margin-left: 30px;
        }
    </style>
        <p>
            <h4 class='titulo'>Apresentação da Unidade</h4>
        
        </p>
		A equipe de enfermagem é responsável pelo monitoramento do padrão eliminatório
     do paciente; afinal, muitos fatores podem influenciar na eliminação intestinal
      do paciente que está sob cuidado domiciliar. A idade, o tipo de dieta, a 
      quantidade de ingestão de líquido, a frequência de atividade física, os 
      fatores psicológicos, os hábitos pessoais, os processos cirúrgicos, o uso 
      de medicações, a gravidez e o puerpério, e até mesmo os exames diagnósticos 
      podem interferir nessa necessidade fisiológica normal do paciente. <br><br>
Nesta unidade, você estudará não somente o procedimento de enteroclisma, como 
também identificará as competências da equipe de enfermagem durante a execução 
da técnica e os cuidados necessários para uma boa execução desse procedimento. <br>


        <br>
       <b>Objetivos educacionais da unidade </b>
           <li id="lista">Definir enteroclisma;</li>
           <li id="lista">Identificar as competências da equipe durante a execução do enteroclisma;</li>
           <li id="lista">Apresentar os cuidados necessários na execução do procedimento.</li>
           <br><br>
          

		
        <br>
        <img src="images/abertura.png" alt="Imagem de abertura Unidade 3">
		
		<script type="text/javascript">
			
		function apagar(){
			$("#texto_color").html("<h4>Confirmação</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, você tem certeza que deseja recomeçar os estudos desta unidade? Todos os seus registros serão zerados.");
            $(".inline").colorbox({inline:true, width:"50%",onClosed:function(){}});
		}
		
		function apagar_confirmar(){
			
			$.post( "../apresentacao/functions/apagar_tudo.php", { unidade: 3 }, function() {
						location.reload();
				});
			
		
		}
		
		</script>