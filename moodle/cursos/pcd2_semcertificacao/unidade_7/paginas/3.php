<?php
               
    $pg_atual=3;
     registro($id,unid7_pg,$pg_atual,unid7_ev);
?>

<style type="text/css">

#lista li{
            list-style-type: disc;   
       margin-left: 30px;

        }
</style> 

   <h4 class="titulo">Utilização  e cuidados no manuseio dos equipamentos de oxigenoterapia</h4>

   Os gases medicinais devem ser manipulados conforme legislação específica (MINAS GERAIS, 2007). 
   A equipe de enfermagem é responsável pela administração do oxigênio e pelo uso correto dos equipamentos 
   utilizados, orientando o paciente e o cuidador quanto aos cuidados apresentados no quadro a seguir 
   (RIBEIRÃO PRETO, 2007; SMELTIZER; BARE, 2005; ZEFERINO; SILVA, 2004; ANVISA, 2009).
<br><br>
    
    
    <!-- Ajustar tabela a primeira linha deve ter fonte maior -->
    
    
      <table class="table table-bordered"  style="background-color:#d5c9c1" >
        <tr style="background-color:#ccc">
          <td colspan="2"><b><span style="margin-left: 261px;">Como usar o oxigênio com segurança</span></b></td>
        </tr>
    <tr>
        <!-- inserir imagens na primeira coluna -->
    <td class="imagem"><img src="images/fig1.1_evitar-explosao.jpg" alt="Evitar explosão" > </td>
        <td><b>Evitar explosão/incêndio</b><br>
O oxigênio é um gás altamente inflamável, por isso os equipamentos devem estar distantes de chamas como cigarros e isqueiros. Também devem estar afastados de fogões, micro-ondas, velas, entre outros.<br>
Sempre manusear os equipamentos de oxigênio com as mãos livres de sujidades como óleo, graxa ou similares, para evitar que sofram combustão.
</td> 
        </tr>
        <tr> 
        <td class="imagem"><img src="images/fig1.2_prescricaomedica.jpg"  alt="Prescricao Médica" > </td>
            <td><b>Seguir a prescrição médica</b><br>
                O tratamento com oxigenoterapia deve ser sempre prescrito pelo médico. Sendo assim, <b>nunca</b> altere por conta própria a dosagem de oxigênio a ser utilizada, pois a quantidade prescrita é a ideal para o caso em questão.
Anote sempre as intercorrências no prontuário do paciente.
</td> <tr>
           <td class="imagem"><img src="images/fig1.3_nao-tomar-bebida.jpg"  alt="Não tomar bebida"> </td>
            <td><b>Não tomar bebida alcoólica</b><br>
                O paciente e familiares devem ser orientados a evitar bebidas alcoólicas e sedativos, pois podem afetar a respiração.
</td>      
   <tr>
           <td class="imagem"><img src="images/fig1.4_cuidado_pele.jpg"  alt="Cuidado da pele"> </td>
            <td><b>Cuidar da pele</b><br>
               A pele que está em contato direto com o cateter de oxigênio deve ser avaliada diariamente (inclusive atrás das orelhas) como medida preventiva contra lesões.
Fazer uso de protetores atrás das orelhas (rolinho de algodão ou gaze) ou creme à base de água em caso de irritação da pele.
Nunca fazer uso de lubrificantes oleosos, cremes comuns ou pomadas, pois essas substâncias oferecem perigo de combustão com o oxigênio.
</td>       
        <tr>
           <td class="imagem"><img src="images/fig1.5_cuidar_equipamentos.jpg"  alt="Cuidar dos equipamentos" >  </td>
            <td><b>Cuidar dos equipamentos</b><br>
               Leia e esteja sempre atento(a) ao manual dos equipamentos antes do seu manuseio.
Os equipamentos usados na oxigenoterapia (cilindros, máscaras, suportes) não podem ser destruídos, emprestados ou vendidos.
O paciente e familiares devem ser orientados a sempre comunicar à equipe de saúde problemas com equipamentos e acessórios utilizados na oxigenoterapia.
Utilizar sempre água estéril na umidificação dos gases medicinais, evite outros líquidos.
</td>              
  <tr>
           <td class="imagem"><img src="images/fig1.6_cuidar_cilindro.jpg"  alt="Manutenção dos equipamentos" > </td>
      <td><b>Fazer a manutenção dos equipamentos</b><br>
O sistema (fluxômetro e cilindros) e dispositivos de administração (cateter nasal, máscaras) devem ser testados diariamente para detectar possíveis falhas no fluxo do gás.<br>
Conferir a quantidade do fluxo de O<sub>2</sub> com a prescrição médica.<br>
Solicitar a manutenção dos equipamentos, na presença de falhas, para a equipe técnica responsável pelo fornecimento dos gases.
Solicitar troca do cilindro sempre que necessário.<br>
Trocar os dispositivos (cateteres e máscaras) conforme protocolo do município, protegendo-os quando não estiverem sendo utilizados.
    </tr>
  </table>
     <span class="credito"> 
         <b>Fonte: </b>(ZEFERINO; SILVA, 2004, adaptado). </span>

      </span>
    <style type="text/css">
    .imagem{
      width: 170px;
    }
    </style>
  
