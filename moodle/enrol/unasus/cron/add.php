<?php

   require("../../../config.php");
      require("$CFG->dirroot/enrol/unasus/lib/context.php");
   require("$CFG->dirroot/enrol/unasus/cron/lib.php");
   require("$CFG->dirroot/enrol/unasus/lib/configcontext.php");
   require("$CFG->dirroot/enrol/unasus/cron/form_add.php");

require_login();

   //permiss�o
    require_capability('enrol/unasus:cronconfig', get_context_instance(CONTEXT_SYSTEM), NULL, false);
     add_to_log(1, 'enrol_unasus', 'cronconfig', "enrol/unasus/cron/add.php?contextid=$contextid", getremoteaddr());    
    $configcontext =new configcontext();
   $cron =new cron();
     $param=  new object();
     
     $param->contextid= required_param('contextid',PARAM_INT);
     $context =new context();
     $context_name=$context->get_name($param->contextid);   
    $form= new form_add();

    $param->name=configcontext::$CRON_PROCESS_ITEM;
    $param->name=configcontext::$CRON_PROCESS_FREQUENCY;
    
    if ($form->is_cancelled()) {
        redirect($CFG->httpswwwroot.'/enrol/unasus/cron/index.php') ;

    } else if ($formdata = $form->get_data()) {
    	 $param->name=configcontext::$CRON_PROCESS_ITEM;
    	$param->value=$cron->cast_param_to_string_value($formdata);
    	
    	$configcontext->update($param);
    	
    	$param->id=NULL;
    	$param->name=configcontext::$CRON_PROCESS_FREQUENCY;
    	$param->value=$formdata->interval_teme_process;
    	$configcontext->update($param);
    	redirect($CFG->httpswwwroot.'/enrol/unasus/cron/index.php') ;
    	  
    	
    }
 
     //Navega��o
   $navigation = build_navigation(array(array('name' => get_string('enrolname','enrol_unasus'), 'link' => "$CFG->wwwroot/enrol/unasus/index.php", 'type' => 'misc'),
                                     array('name' => get_string('cron_config','enrol_unasus'), 'link' => "$CFG->wwwroot/enrol/unasus/cron/index.php", 'type' => 'misc'), array('name' => $context_name, 'link' => null, 'type' => 'misc'), array('name' => get_string('cron_config_add','enrol_unasus'), 'link' => null, 'type' => 'misc')));

   print_header(get_string('enrolname','enrol_unasus'),get_string('applicationsenrolment','enrol_unasusem'), $navigation);
 	
    $form->display();
  
    print_footer();
    
?>