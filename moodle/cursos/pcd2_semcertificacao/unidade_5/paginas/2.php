<?php
               
    $pg_atual=2;
    registro($id,unid5_pg,$pg_atual,unid5_ev);
?>
 
   <style type="text/css">
		#lista li{
			list-style-type: disc;
       margin-left: 30px;
		}

	
	</style>

   <h4 class="titulo">Responsabilidades  da equipe na execução do procedimento</h4><br>
  A equipe multiprofissional 
  envolvida na terapia nutricional enteral deve regularmente desenvolver, rever 
  e atualizar os procedimentos relativos ao cuidado com o paciente, além de conhecer os princípios que regem esta terapia, tendo cada profissional suas 
  atribuições dispostas em legislações específicas, entre as quais podemos destacar:
   <br><br>
   
<ul id="lista">
<li>a prescrição da sondagem é de competência médica, assim como o acompanhamento 
  do paciente. Ao nutricionista cabe realizar todas as operações inerentes à 
  prescrição dietética, composição e preparação da nutrição enteral (ANVISA, 2000);</li>
<li>a passagem da sonda e a administração da dieta é de competência do enfermeiro, 
  podendo o técnico de enfermagem realizar tais procedimentos sob supervisão do
   enfermeiro (BRASIL, 1986);</li>
<li>a prescrição da dieta pode ser discutida entre a equipe multiprofissional, 
  que avaliará as condições clínicas do paciente e do ambiente domiciliar (ANVISA, 2000).</li>

</ul>
 <br><br>
 


   
