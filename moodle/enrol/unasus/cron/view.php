<?php

   require("../../../config.php");
      require("$CFG->dirroot/enrol/unasus/lib/context.php");
   require("$CFG->dirroot/enrol/unasus/cron/lib.php");
   require("$CFG->dirroot/enrol/unasus/lib/configcontext.php");
   

	require_login();

   //permiss�o
    require_capability('enrol/unasus:cronview', get_context_instance(CONTEXT_SYSTEM), NULL, false);
    add_to_log(1, 'enrol_unasus', 'cronview', "enrol/unasus/cron/view.php?contextid=$contextid", getremoteaddr());    
     
    
    $configcontext =new configcontext();
    $cron =new cron();
     $param=  new object();
     $param->contextid= required_param('contextid',PARAM_INT);
      $config_data=$configcontext->get_by_contextid($param->contextid,configcontext::$CRON_PROCESS_ITEM);
     $config_process_item= $config_data->value;
      $config_process_frequency=$configcontext->get_by_contextid($param->contextid,configcontext::$CRON_PROCESS_FREQUENCY);
     $context =new context();
     $context_name=$context->get_name($param->contextid);  
   

        
   
  
     //Navega��o
   $navigation = build_navigation(array(array('name' => get_string('enrolname','enrol_unasus'), 'link' => "$CFG->wwwroot/enrol/unasus/index.php", 'type' => 'misc'),
                                     array('name' => get_string('cron_config','enrol_unasus'), 'link' => "$CFG->wwwroot/enrol/unasus/cron/index.php", 'type' => 'misc'), array('name' => $context_name, 'link' => null, 'type' => 'misc'), array('name' => get_string('cron_config_view','enrol_unasus'), 'link' => null, 'type' => 'misc')));

     print_header(get_string('enrolname','enrol_unasus'),get_string('applicationsenrolment','enrol_unasusem'), $navigation);
   
 	
   get_table_view();
  
    print_footer();
    
    function get_table_view(){
   			global $cron,$param,$config_process_item, $config_process_frequency;
   		
   	 	$output = "<center><table class=\"generaltable\"   border=\"1\"   width=\"95%\" cellspacing=\"1\" cellpadding=\"5\">";
    	$output .= "<tr>";
    	$output .= "<th align=\"center\" COLSPAN=2><h3><b>".get_string('cron_config_view','enrol_unasus')."</h3></th>";
     	$output .= "</tr>";
     	
     	$output .= "<tr>";
     	$output .= "<td align=\"left\" width=\"25%\"><b>".get_string('msg_erro_arouca_conn','enrol_unasus')."</b></td>";
     	$output .=  "<td>".$cron->get_status_text($config_process_item,cron::$ERRO_AROUCA_CONNECTION)."</td>"; 
     	$output .= "</tr>";
     	
     	$output .= "<tr>";
		$output .= "<td align=\"left\" width=\"25%\"><b>".get_string('msg_no_cbo_arouca','enrol_unasus')."</b></td>";
		$output .=  "<td>".$cron->get_status_text($config_process_item,cron::$CBO_AROUCA_NOT_EXIST)."</td>"; 
		$output .= "</tr>";
		
		$output .= "<tr>";
		$output .= "<td align=\"left\" width=\"25%\"><b>".get_string('msg_no_role_created','enrol_unasus')."</b></td>";
		$output .=  "<td>".$cron->get_status_text($config_process_item,cron::$ROLE_NOT_CREATED)."</td>";
		$output .= "</tr>";
		
						
		$output .= "<tr>";
		$output .= "<td align=\"left\" width=\"25%\"><b>".get_string('msg_no_cbo_in_role','enrol_unasus')."</b></td>";
		$output .=  "<td>".$cron->get_status_text($config_process_item,cron::$CBO_NOT_IN_ROLE)."</td>";
		$output .= "</tr>";
		
		$output .= "<tr>";
		$output .= "<td align=\"left\" width=\"25%\"><b>".get_string('cron_interval_teme_process','enrol_unasus')."</b></td>";
		$output .=  "<td>".$config_process_frequency->value."</td>";
		$output .= "</tr>";	
		
		$output .= "<tr>";
		$output .= "<td align=\"center\"  COLSPAN=2>    </td>";
		
		$output .= "</tr>";		
		
    	$output .= "</table></center>";
    	echo $output;
   }
?>