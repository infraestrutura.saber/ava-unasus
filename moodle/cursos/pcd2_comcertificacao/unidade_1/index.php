﻿<!DOCTYPE html!>
<meta charset="utf-8">
<html lang="pt-br">
<head>

		
		<link rel="stylesheet" type="text/css" href="../css/style.css">
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<link rel="stylesheet" type="text/css" href="../lib/bootstrap/css/bootstrap-responsive.css">
		<link rel="stylesheet" type="text/css" href="../lib/bootstrap/css/bootstrap-responsive.min.css">
		<link rel="stylesheet" type="text/css" href="../lib/bootstrap/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="../lib/bootstrap/css/bootstrap.min.css">
		<script type="text/javascript" src="../lib/bootstrap/js/bootstrap.js"></script>
		<script type="text/javascript" src="../lib/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="../lib/bootstrap/js/bootstrap-dropdown.js"></script>
		<script type="text/javascript" src="../lib/bootstrap/js/jquery-1.10.1.min.js"></script>
		<title>Unidade 1</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>




	
			<body>
						<script type="text/javascript">
			$(document).ready(function(){
			 $("#menu li a").mouseover(function(){
			 var index = $("#menu li a").index(this);
			 $("#menu li").eq(index).children("ul").slideDown(100);
			 if($(this).siblings('ul').size() > 0){
			 return false;
			 }
			 });
			 $("#menu li").mouseleave(function(){
			 var index = $("#menu li").index(this);
			 $("#menu li").eq(index).children("ul").slideUp(100);
			 });
			});


		</script>
				
				<?php 
				
				require_once('../../../config.php');
				require_once('../apresentacao/functions/conectar.php');	require_once('../apresentacao/functions/functions.php');conectar();
				$id=$USER->id; 

				session_start();

				if ($_SESSION['logado'] == 1) {
					echo "<script> alert('vc esta logado na unidade'); </script>";
				}else{
					echo "<script> alert('NAO logado'); </script>";
				}
				
				
				function b($id){
					echo "ID: " .$id;
				}
				
				//unidade1 nao foi liberada
				header('Location: http://ufpe.unasus.gov.br/moodle_unasus/login/index.php');

				//redirecionar caso nao tenha permissao
				if($id==""){
					header('Location: http://ufpe.unasus.gov.br/moodle_unasus/login/index.php');
					//header('Location: http://ufpe.unasus.gov.br/adteste/login/index.php'); header antigo					
				}		

				if(empty($_GET['pagina'])){
					//se variavél pagina não existir 
					$temp = '0';
					

				}

				if ($_GET['pagina']==1){ 
					$temp = 1;
					
				}
				
				?>
				<div id="geral">
					<div id="fundo">
							<div id="barra_topo">
								<?php
										include('../includes/menu.php'); 

								?>
							  
							</div>

					<div id="disciplina">
						<p>Princípios Éticos e Biossegurança no Cuidado Domiciliar</p>
						<div class="info"><img src="../images/info.png" title="Título da unidade didática." alt="Título da unidade didática."></div>
					</div>
					
					<div id="usuario">
							  		<p>Olá, <b><span style="margin-right: 8px;"><?php echo ($USER->firstname ."</span></b>"."   |  "); ?></p>
					</div>

			<div id="barra_evolucao">
				<div id="evolucao">
					<p class="descricao">Evolução<img src="../images/info.png" title="Contabiliza a quantidade de páginas visualizadas. A proposta é lhe dar 
a visão do percentual de conteúdos já acessados." alt="Contabiliza a quantidade de páginas visualizadas. A proposta é lhe dar 
a visão do percentual de conteúdos já acessados."></p>
					
					  <div class="navbar">
			          
			            <div class="progress evolucao">
			                 <div class="bar" style="width: <?php porcentagem_unid(1,$cpf); ?> "></div>
			            </div>
			          

			      </div>
				</div>
					
	
					
				</div>


			</div> <!--daf-->


			<div id="central">
				
				<div id="conteudo">
					<?php
						if($temp==0){
							include('paginas/0.php'); 
						}

						if($temp == 1){
							include('paginas/1.php');
						}
					?>
				</div>

				<div id="indice">
					<div id="titulo">
						<h3>Índice</h3>
						<div class="info"><img src="../images/info.png" title="Acesso ao(s) conteúdos(s) disponível(is) nesta unidade didática." alt="Acesso ao(s) conteúdos(s) disponível(is) nesta unidade didática."></div>
					</div>

					<ul>
					      	<li><a href="index.php?pagina=0">Página inicial</a><?php verificarTopico($cpf,0, 1); ?></li>
							
							<?php
					      			//require_once('../apresentacao/functions/functions.php');
					      			if($_GET['pagina'] == 0) {
					      				//$id = ID do usuario , $id_unidade = ID da unidade
					      				cadastrarTopico($cpf, 1);
					      			}
					      		?>
							
							
					      	<li><a href="index.php?pagina=1">Para um cuidado compartilhado e ético</a><?php verificarTopico($cpf,1, 1); ?></li>
							
							<?php
							    	require_once('../apresentacao/functions/functions.php');							
									if($_GET['pagina'] == 1) {
										// $id - id do usuario e id_unidade
										cadastrarTopico($cpf, 1);
									}
					      		?>
								
					</ul>
				</div>

			</div>


		</div>


	</body>

	</html>