<?php 

require_once $CFG->libdir.'/formslib.php';

class form_edit extends moodleform {

    function definition() {
    
    	global $syc_ing_conc,$dto, $course_sync_config;
    	 $courses_options=$syc_ing_conc->get_courses_html_select();
           $conclinteoferta_options= $course_sync_config->option_yes_not();
        $mform    =& $this->_form;
        $renderer =& $mform->defaultRenderer();

        $mform->addElement('header', '', get_string('sync','report_synciccarouca'), '');
        
       
        $mform->addElement('hidden', 'id',  $dto->id);
        $mform->setType('id', PARAM_INT);
        
        
         $mform->addElement('select', 'mdlcourseid', get_string('mdlcoursesync','report_synciccarouca'), $courses_options);
        $mform->setType('mdlcourseid', PARAM_INT);
        $mform->addRule('mdlcourseid', get_string('requiredfield','report_synciccarouca'), 'required', null, 'server');
        $mform->setDefault('mdlcourseid', $dto->mdlcourseid);
        
        
        $mform->addElement('text', 'arcidcurso', get_string('aroucacoursesync','report_synciccarouca'),'size="8"');
        $mform->setType('arcidcurso', PARAM_INT);
      	//$mform->addRule('arcidcurso', get_string('requiredfield','report_synciccarouca'), 'required', null, 'server');
     
       if($dto->arcidcurso==0){$dto->arcidcurso="";}
        $mform->setDefault('arcidcurso', $dto->arcidcurso);
        
         $mform->addElement('text', 'arcidoferta', get_string('aroucaofertaync','report_synciccarouca'),'size="8"');
        $mform->setType('arcidoferta', PARAM_INT);
      	$mform->addRule('arcidoferta', get_string('requiredfield','report_synciccarouca'), 'required', null, 'server');
        $mform->setDefault('arcidoferta', $dto->arcidoferta);
          
          $mform->addElement('select', 'concluinteferta', get_string('coursesync_aroucaconcluinteoferta','report_synciccarouca'), $conclinteoferta_options);
        $mform->setType('concluinteferta', PARAM_INT);
        $mform->addRule('concluinteferta', get_string('requiredfield','report_synciccarouca'), 'required', null, 'server');
         $mform->setDefault('concluinteferta', $dto->concluinteferta);
        
         $mform->addElement('text', 'gradeapproved', get_string('approvedgrade','report_synciccarouca'),'size="8"');
        $mform->setType('gradeapproved', PARAM_TEXT);
       	$mform->addRule('gradeapproved', get_string('requiredfield','report_synciccarouca'), 'required', null, 'server');
       $mform->setDefault('gradeapproved', $dto->gradeapproved);
        
         $mform->addElement('header', '', get_string('sendmailcoursesync','report_synciccarouca'), '');
        
          $mform->addElement('text', 'subjectmailcert', get_string('sujectcoursesync','report_synciccarouca'),'size="30"');
        $mform->setType('subjectmailcert', PARAM_TEXT);
        $mform->setDefault('subjectmailcert', $dto->subjectmailcert);
      	//$mform->addRule('subjectmailcert', get_string('requiredfield','report_synciccarouca'), 'required', null, 'server');
       
         
           
         $mform->addElement('htmleditor', 'messagemailcert', get_string('messagemailcoursesync','report_synciccarouca'),'wrap="virtual" rows="7" cols="40"');
        $mform->setType('messagemailcert', PARAM_TEXT);
        $mform->setDefault('messagemailcert', $dto->messagemailcert);
         
        
         
        //data
        $this->add_action_buttons(true,get_string('edit','report_synciccarouca'));
    }

    function validation($data) {
        global $CFG;
         global  $course_sync_config;
	$errors = parent::validation($data, $files);
        
       //  $data['gradeapproved']=str_replace(".", "", $data['gradeapproved']) ;
         $data['gradeapproved']=str_replace(",", ".", $data['gradeapproved']) ;
           if(!is_numeric($data['gradeapproved'])){
         	$errors['gradeapproved'] = get_string('require_number','report_synciccarouca');
         }
         
         
      if( $course_sync_config->exist_edit($data['id'],$data['mdlcourseid'])){
         	$errors['mdlcourseid'] = get_string('coursesync_duplication','report_synciccarouca');
         }
         
          if(empty($data['arcidcurso']) && $data['concluinteferta']==0)   {
            $errors['arcidcurso'] = get_string('aroucacoursesyncrequiredcf','report_synciccarouca');
          } 
     return $errors;
    }

}

?>