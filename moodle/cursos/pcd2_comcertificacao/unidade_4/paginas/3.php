<?php $pg_atual = 3;
registro($id, unid4_pg, $pg_atual, unid4_ev);
?>

<style type="text/css">
	#lista li {
		list-style-type: disc;
		margin-left: 30px;
	}
</style>

<h4 class="titulo">Responsabilidades da equipe na execução do procedimento</h4>

O cuidar engloba a capacidade do
saber e do fazer, visando à promoção da autonomia do paciente para o cuidado,
<br>
ou preparação do cuidador para o exercício de suas funções quando o paciente
apresentar-se incapacitado. Dessa forma, a equipe de atenção domiciliar, frente
aos pacientes em uso de sonda vesical, precisa atentar-se para (SMELTZER; BARE,
2005; TIMBY, 2001):
<br>
<br>
<ul id="lista">
	<li>
		a coloração, viscosidade e o odor são sinais de alerta quanto à presença de sedimentos. Pus
		ou sangue na urina, além do volume podem indicar desequilíbrios hídricos do paciente. O cuidador
		deve ser orientado a informar qualquer alteração para a equipe do SAD;
	</li>
	<li>
		o registro exato da ingesta de líquidos e débito urinário, que nos fornece
		informações sobre a adequação da função renal e drenagem urinária;
	</li>
	<li>
		a orientação ao paciente e ao cuidador sobre o esvaziamento da bolsa coletora
		quando a drenagem alcançar o limite permitido;
	</li>
	<li>
		a orientação ao paciente e ao cuidador sobre a ingesta adequada de líquido,
		respeitando as condições cardíacas e renais do paciente;
	</li>
	<li>
		a orientação sobre o autocateterismo para o paciente e, na impossibilidade de o paciente realizar, orientar o cuidador.
	</li>
</ul>

<br>
<div class="box">
	<br>
	<img src="../images/img_saibamais_ad.png" alt="Saiba mais" >
	<span class="titulo_box">Saiba mais</span>
	<hr/>
	De acordo com a Resolução <a href="http://www.cofen.gov.br/resolucao-cofen-no-04502013-4_23266.html" target="_blank"> Nº 450/2013</a>, publicada pelo Conselho Federal de Enfermagem (COFEN), 
	que estabelece as competências da equipe de Enfermagem em relação ao procedimento de sondagem,
	 e segundo o <a href="http://www.cofen.gov.br/wp-content/uploads/2014/01/ANEXO-PARECER-NORMATIVO-PARA-ATUACAO-DA-EQUIPE-DE-ENFERMAGEM-EM-SONDAGEM-VESICAL1.pdf" target="_blank">Parecer Normativo</a>, aprovado pela Resolução supracitada, <b>a inserção de cateter 
	 vesical é função privativa do enfermeiro</b>, em função dos seus conhecimentos científicos e do caráter 
	 invasivo do procedimento, que envolve riscos ao paciente, como infecções do trato urinário e trauma uretral ou vesical.<br><br>
	De acordo com o mesmo parecer, <b>ao técnico de enfermagem compete a realização das atividades prescritas 
	pelo enfermeiro no planejamento da assistência</b>, a exemplo de monitoração e registro das queixas do 
	paciente e condições do sistema de drenagem, do débito urinário; manutenção de técnica limpa durante o 
	manuseio do sistema de drenagem e coleta de urina para exames; monitoração do balanço hídrico – ingestão 
	e eliminação de líquidos, sempre sob supervisão e orientação do enfermeiro.
	<br>
	<br>
	

</div>

