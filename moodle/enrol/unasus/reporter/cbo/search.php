<?php
   require("../../../../config.php");
   require("$CFG->dirroot/enrol/unasus/manage.php");
    require("$CFG->dirroot/enrol/unasus/reporter/lib.php");
   require("$CFG->dirroot/enrol/unasus/reporter/cbo/search_form.php");
   require("$CFG->dirroot/enrol/unasus/tab.php");
   require("$CFG->dirroot/enrol/unasus/lib/userprofile.php");
   require("$CFG->dirroot/enrol/unasus/profilefield/lib.php");
    require("$CFG->dirroot/enrol/unasus/profilefield/data/lib.php");
    require("$CFG->dirroot/enrol/unasus/lib/configcontext.php");

 require_login();
   // Acesso permitido apenas ao usu�rio admin
   
     require_capability('enrol/unasus:viewreporterenrol', get_context_instance(CONTEXT_SYSTEM), NULL, false);
     add_to_log(1, 'enrol_unasus', 'view', 'reporter/cbo/search.php', getremoteaddr());    
    //Navega��o
     $navigation = build_navigation(array(array('name' => get_string('enrolname','enrol_unasus'), 'link' => "$CFG->wwwroot/enrol/unasus/index.php", 'type' => 'misc'),
                                     array('name' => get_string('reporter_consolidate_by_cbo','enrol_unasus'), 'link' => null, 'type' => 'misc')));
      //tab menu
     $currenttab = 'reporter';
    $secondrow = array();
     $secondrow[] = new tabobject('course',"$CFG->wwwroot/enrol/unasus/reporter/course/search.php",get_string('course','enrol_unasus'));
     $secondrow[] = new tabobject($currenttab,"$CFG->wwwroot/enrol/unasus/reporter/cbo/search.php",get_string('cbo','enrol_unasus'));
   //  $secondrow[] = new tabobject('enrol_data',"$CFG->wwwroot/enrol/unasus/reporter/enrolprofile/index.php",get_string('enrol_data','enrol_unasus'));
  	 $tabs = array($firstrow, $secondrow);
  
        
    $manage=new enrol_unasus_manage();
    $profile =new enrol_unasus_userprofile();
    $profilefield_field = new profilefield_field();
    $profilefield_data = new data_infofield();  
    
     $configcontext =new configcontext();
    
    //perfil do usu�rio
    $fields= $profile->get_fields_user_profile();
    $seleted_fields_filter=$profile->get_profile_config_in_array(enrol_unasus_userprofile::$USER_PROFILE_FILTER);
    $seleted_fields_reporter=$profile->get_profile_config_in_array(enrol_unasus_userprofile::$USER_PROFILE_REPORTER);
    $profile_data= null; 
  
 
    //parâmetros
    $param=  new object();
    $param->courseid= optional_param('courseid', NULL,PARAM_INT);
    $param->userid= optional_param('userid', NULL,PARAM_INT);
     $param->username= optional_param('username', NULL,PARAM_TEXT);
    $param->status= optional_param('status', NULL,PARAM_INT);
    $param->statusinfo= optional_param('statusinfo', NULL,PARAM_INT);
   // $param->role= optional_param('role', NULL,PARAM_INT);
    $param->userid_sql_profile="";


 if($param->courseid){ 
 	$rolecontextid=  $manage->get_rolecontextid_by_course($param->courseid);
 	
  	if(!empty($rolecontextid)) {
  		
  		$fields_enrol= $profilefield_field->get_by_contextid($rolecontextid,1);
  		$data_fields_enrol=$profile->get_data_profile_field($fields_enrol,'FE_');
  	 	$param->userid_sql_profile.=$profilefield_data->get_sql_filter_user_data($fields_enrol,$data_fields_enrol);
  	 	 $value_filter_enrol=$configcontext->get_value_by_contextid($rolecontextid,configcontext::$USER_PROFILE_ENROL_FILTER);
  		$value_filter_enrol=$profile->get_profile_config_in_array('',$value_filter_enrol);
  	}
  }
  
   $data_fields=$profile->get_data_profile_field($fields,'F_');
   		$param->userid_sql_profile.=$profile->get_sql_filter_user_data($fields,$data_fields);
   
   //form
    $form= new cbo_search_form();
    
    function get_table_view(){
   	global $manage,$param;
    $rows = $manage->search_by_cbo($param);
     $rows_count = $manage->count_search_by_cbo($param);
     $util = new enrol_unasus_report_util();
      echo "<center>".get_string('rowcount','enrol_unasus') ." " . $rows_count."</centar>";
      
   	  if(!empty($rows)){
   			  $table = new object();
    	  		$table->head  = array(get_string('cbo','enrol_unasus'),get_string('userount','enrol_unasus'));
    	  		$table->align = array('left', 'left');
    	  		
    	  		$table->width = '95%';
         		 $table->class = 'generaltable';
          		$table->data = array();
				foreach ($rows as $row) {
					$cbo=$row->value;
					$table->data[] = array($cbo,$row->qvalue);
           			}
        		
   				 print_table($table);
   		}
   		
   }
     
     
    print_header(get_string('applicationsenrolment','enrol_unasusem'),get_string('applicationsenrolment','enrol_unasusem'), $navigation);
   
 	print_tabs($tabs, $currenttab, $inactive, $activated);
 
    $form->display();
  get_table_view();
   print_footer();
    
  
?>