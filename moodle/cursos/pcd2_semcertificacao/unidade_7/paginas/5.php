<?php
               
    $pg_atual=5;
     registro($id,unid7_pg,$pg_atual,unid7_ev);
?>

<style type="text/css">


#lista li{
            list-style-type: disc;   
       margin-left: 30px;


}

#esp tr td{

	text-align: center;

}

</style> 

   <h4 class="titulo">Equipamentos  necessários para a realização do procedimento</h4>

   A seleção do equipamento dependerá das condições clínicas do paciente, do nível das 
   informações prestadas a ele e à família e, principalmente, das condições gerais do 
   domicílio, considerando que equipamentos muito complexos demandam mais tecnologia, 
   além de trazer muita ansiedade para o ambiente domiciliar. <br><br>
     A discriminação dos materiais necessários dependerá da avaliação pela equipe 
   multiprofissional. Vale lembrar que a oxigenoterapia pode ser dividida em dois 
   sistemas para administrar o O<sub>2</sub>, sendo de baixo e de alto fluxo, conforme 
   apresentado no quadro a seguir (BARROS et al, 2002). 
<br><br>
    
    <div class="box">
    <img src="../images/img_atencao_ad.png"  alt="Atenção">
        <span class="titulo_box">Atenção!</span>
        <br>

        <hr/>
        <table>
            <tr>
            <td class="um">    <img src="images/pag23atencao_umidificador.jpg"  alt="Umidificador"> </td>
            <td><ul id="lista">
    <li>Para o uso do oxigênio, torna-se necessário fazer uso do umidificador, a fim de evitar ressecamento da mucosa nasal (dentro do nariz);</li>
    <li>É importante realizar a higienização do umidificador com solução sanitizante diariamente;</li>
    <li>A água do umidificador deve ser trocada diariamente;</li>
    <li>O uso indevido do umidificador pode ocasionar entupimento no sistema de oxigênio, bloqueando-o. No caso de uso do concentrador, em situações de obstrução/bloqueio, o alarme (luz vermelha) é acionado.</li>  </ul></td>
            </tr>
        </table>
                        <br>
                <span class="credito"><b>Fonte:</b> (ZEFERINO; SILVA, 2004, adaptado).
                
</div>
<br>
 
    <br><br>
       
    <b>Tipos de sistemas para administração de oxigênio no domicílio</b><i class=" icon-hand-up" title="Esse quadro possui interação."></i>
<div class="tabContainer" >
  <ul class="digiTabs" id="sidebarTabs">
    <li  id="tab1" class="selected"  onclick="tabs(this);">De baixo fluxo ou fluxo variável</li>
    <li id="tab2" onclick="tabs(this);">De alto fluxo ou fluxo fixo</li>
    
  </ul>
  <div id="tabContent">

<table class="table table-bordered"   width="700px">
     <tr >
      <td><b>Características</b></td>  
      <td colspan="2" ><b>Dispositivos</b></td>
      
    </tr> 
    <tr>
       
        <td rowspan="3">Administram O<sub>2</sub> em velocidades de fluxo que suplementam o ar ambiente.</td> 
        <td class="imagens">Cânula ou prolongador nasal<br><img src="images/cateter.jpg"  alt="Cateter nasal"><br></td>
        <td>     Método simples, barato, e o mais utilizado. 
            Excelente nos casos de acentuação da dispneia, durante a alimentação 
            e tosse, pois não é necessária sua remoção. Pode ocorrer ressecamento ou 
            sangramento da mucosa nasal após a utilização de fluxos altos.
    </tr>
     <tr> 
     
     <td class="imagens">Cateteres nasais<br><img src="images/Cateter_nasal_pag23_cap6a8_(1).jpg"  alt="Cateter nasal"><br></td>
      <td>Também considerado um método simples, barato e confortável para 
        administração de O<sub>2</sub>. O cateter pode ser um tubo de plástico macio com 
        pequenos orifícios na ponta. Oferece fluxo ¼ a 5 l/min e concentrações 
        de O<sub>2</sub> entre 22% a 45%. A umidificação do ar, com ABD (Água Bidestliada) 
        é importante para prevenção de lesões. 
		</td>     
    </tr> 
     <tr> 
     
         <td class="imagens">Máscara simples<br><img src="images/Mascara_simples-pag23_Cap6a8_(3).jpg"  alt="Máscara simples"></td>
      <td>Tem seu uso limitado por não existirem modelos e tamanhos variáveis que 
        se adaptem a todos os tipos de face, e o fluxo mínimo necessário é de l l/min. 
        A máscara facilita a administração de níveis elevados de oxigênio. 
        Os principais problemas resultantes da sua utilização são: úlceras de 
        pressão na face e nas orelhas, risco de aspiração, desconforto pelo calor 
        e sensação claustrofóbica. Interfere na alimentação e na expectoração.
</td>     
    </tr> 
    

</table>
</div>
  
  
  
</div>
  
<div id="tab1Content" style="display:none;">

<table class="table table-bordered"   width="700px">
     <tr >
      <td><b>Características</b></td>  
      <td colspan="2" ><b>Dispositivos</b></td>
      
    </tr> 
<tr>
       
        <td rowspan="3">Administram O<sub>2</sub> em velocidades de fluxo que suplementam o ar ambiente.</td> 
        <td class="imagens">Cânula ou prolongador nasal<br><img src="images/cateter.jpg"  alt="Cateter nasal"><br></td>
        <td>     Método simples, barato, e o mais utilizado. 
            Excelente nos casos deacentuação da dispneia, durante a alimentação 
            etosse, pois não é necessária sua remoção. Pode ocorrer ressecamento ou 
            sangramento da mucosa nasal após a utliização de fluxos altos.
    </tr>
     <tr> 
     
     <td class="imagens">Cateteres nasais<br><img src="images/Cateter_nasal_pag23_cap6a8_(1).jpg"  alt="Cateter nasal"><br></td>
      <td>Também considerado um método simples, barato e confortável para 
        administração de O2. O cateter pode ser um tubo de plástico macio com 
        pequenos orifícios na ponta. Oferece fluxo ¼ a 5 l/min e concentrações 
        de O2 entre 22% a 45%. A umidificação do ar, com ABD (Água Bidestliada) 
        é importante para prevenção de lesões. 
		</td>     
    </tr>  
     <tr> 
     
         <td class="imagens">Máscara simples<br><img src="images/Mascara_simples-pag23_Cap6a8_(3).jpg"  alt="Máscara simples"></td>
      <td>Tem seu uso limitado pornão existirem modelos e tamanhos variáveis que 
        se adaptem a todos os tiposde face, e ofluxo mínimo necessário é de l l/min. 
        A máscara facliita a administração de níveis elevados de oxigênio. 
        Os principais problemas resultantes da sua utliizaçãosão: úlceras de 
        pressão na face e nas orelhas, risco de aspiração, desconforto pelo calor 
        e sensação claustrofóbica. Interfere na alimentação e na expectoração.
</td>     
    </tr> 
    

</table>
</div>

<div id="tab2Content" style="display:none;">
<table class="table table-bordered"   width="700px">
	 <tr>
      <td><b>Características</b></td>  
      <td colspan="2" ><b>Dispositivos</b></td>
      
    </tr> 
	 <tr> 
      
      <td>Satisfazem à velocidade do fluxo inspiratório do paciente ou de 
        maneira a excedê-lo (permite administração exata de O2 a ser inspirado).</td>       
        <td class="imagens">Máscaras de Venturi<br><img src="images/Mascara_venturi_cap6a8_(4).jpg"  alt="Máscara venturi"></td>
      <td>É o método mais seguro e exato para liberar a concentração necessária de 
        oxigênio. Porém, devem ser utliizadas junto a um sistema de umidificação.</td>     
    </tr> 

</table>
<b>Fonte:</b> (SURHONE; TENNOE; HENSSONOW, 2010). </span>
<br><br>
<span class="subtitulo"><b>Você sabe o que significa cada uma das cores encontradas nos kits da máscara de Venturi?</b></span>
<span class="credito"> 
         
        <br><br>
<table id="esp" class="table table-bordered"  style="background-color:#e2e0de" width="700px">
            <tr style="background-color:#ccc">
                 <td><b>Cores</b></td>
                 <td><b>Concentração de oxigênio</b></td>
                 <td><b>Fluxo de oxigênio sugerido</b></td>
                 <td><b>Total de fluxo de gás</b></td>    
            </tr>
                
            <tr>       
                <td>Azul</td> 
                <td>24%</td>
                <td>3 l/min</td>
                <td>78 l/min.</td>
            </tr>
                <td>Amarelo</td>
                <td>28%</td>
                <td>6    l/min</td>
                <td>66  l/min.</td>
            </tr>
            <tr>
                <td>Branco</td>
                <td>31%</td>
                <td>8    l/min</td> 
                <td>72  l/min.</td>
             </tr>
             <tr>
                <td>Verde</td>  
                <td>35%</td>    
                <td>12    l/min</td>    
                <td>72  l/min.</td>
            </tr>
                <td>Rosa</td>   
                <td>40%</td>    
                <td>15    l/min</td>    
                <td>60  l/min.</td>
            <tr>
                <td>Laranja</td>    
                <td>50%</td>    
                <td>15    l/min</td>    
                <td>40  l/min.</td>
            </tr>
            </table>
            <span class="credito"> 
         <b>Fonte: </b>(SCANLAN; WILKINS; STOLLER, 2009; SMELTIZER; BARE, 2005, adaptado). </span>
            



<style>
  .tabContainer{margin:10px 0;width:760px;font-size: 12px;font-size: 12px;font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;}
  .tabContainer .digiTabs{font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;list-style:none;display:block;overflow:hidden;margin:0;padding:0px;position:relative;top:1px;}
  .tabContainer .digiTabs li{font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;float:left;background-color:#b5b5b5;border:1px solid #e1e1e1;padding:5px!important;cursor:pointer;border-bottom:none;margin-right:10px;font-family:verdana;font-size:.8em;font-weight:bold;color:#fff;border-top-left-radius: 6px;border-top-right-radius: 6px;}
  .tabContainer .digiTabs .selected{font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;background-color:#fff;color:#393939;}
  #tabContent{font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;padding:10px;background-color:#F6F6F6;overflow:hidden;float:left;margin-bottom:10px;border-top: 1px solid #E1E1E1;border-bottom: 1px solid #E1E1E1;border-left: 1px solid #E1E1E1;border-right: 1px solid #E1E1E1;width:93%;background-color: #fff;border-top-right-radius: 6px;border-bottom-left-radius: 6px;border-bottom-right-radius: 6px;}
</style>
<script type="text/javascript">
  function tabs(x)
  {
    var lis=document.getElementById("sidebarTabs").childNodes; //gets all the LI from the UL
 
    for(i=0;i<lis.length;i++)
    {
      lis[i].className=""; //removes the classname from all the LI
    }
    x.className="selected"; //the clicked tab gets the classname selected
    var res=document.getElementById("tabContent");  //the resource for the main tabContent
    var tab=x.id;
    switch(tab) //this switch case replaces the tabContent
    {
      case "tab1":
        res.innerHTML=document.getElementById("tab1Content").innerHTML;
        break;
 
      case "tab2":
        res.innerHTML=document.getElementById("tab2Content").innerHTML;
        break;
      case "tab3":
        res.innerHTML=document.getElementById("tab3Content").innerHTML;
        break;
        case "tab4":
        res.innerHTML=document.getElementById("tab4Content").innerHTML;
        break;
      default:
        res.innerHTML=document.getElementById("tab1Content").innerHTML;
        break;
 
    }
  }
 
</script>

     
           </div>
<br><br>
    
A oxigenoterapia domiciliar, para ser instalada, precisa de estrutura e equipamentos essenciais. Equipamentos 
estes que necessitam de adequação do ambiente domiciliar e suporte técnico e assistencial rigoroso
 (LIMA; VARGAS, 2004). Então, o método mais adequado de administração do oxigênio deve ser não apenas 
 o que for mais eficaz para o tratamento da hipoxemia e manutenção do equipamento, mas aquele que também 

 propicie condições para o desempenho das Atividades de Vida Diária – AVDs (SCANLAN; WILKINS; STOLLER, 2009). 
 Para entender melhor, veja o quadro abaixo.    
   <br><br>
    
 <span class="credito"> 
         <b>Fontes de oxigênio, vantagens e desvantagens</b><i class=" icon-hand-up" title="Esse quadro possui interação."></i></span>
         <br>
         <div class="tabContainer2" >
  <ul class="digiTabs2" id="sidebarTabs2">
    <li  id="tab1.2" class="selected2"  onclick="tabs2(this);">Cilindros de gás sob pressão</li>
    <li id="tab2.2" onclick="tabs2(this);">Oxigênio líquido</li>
    <li id="tab3.2"  onclick="tabs2(this);">Concentrador</li>
   
  </ul>
  <div id="tabContent2">
   
   <table class="table"><tr>       
                
            <td class="um_1"><img src="images/Fig4.1_cilindro.jpg"  alt="Cilindro"><br></td>  
            <td class="um_descricao"> <ul id="lista">
              <b>Vantagens:</b><br><br>
                <li>Boa acessibliidade;</li></br>
                <li>Armazenamento por longo períodos sem perdas;</li><br>
                <li>Dispostos em pequenos cilindros para deambulação.</li></ul>
</td>   
            <td class="um_descricao">
                <ul id="lista">
                  <b>Desvantagens:</b><br><br>
                    <li>Custo elevado;</li><br>
                    <li>São pesados e grandes;</li><br>
                    <li>São perigosos às quedas;</li><br>
                    <li>Necessitam recargas frequentes.</li>
</td>   
                   <tr>
                    </table>
                    <b>Fonte:</b> (SOCIEDADE BRASILEIRA DE PNEUMOLOGIA E TISIOLOGIA, 2000, adaptado). </span>
  </div>
</div>
 
<div id="tab1Content2" style="display:none;">
   <table class="table" ><tr>       
                
            <td class="um_1"><img src="images/Fig4.1_cilindro.jpg"  alt="Cilindro"><br></td>  
            <td class="um_descricao"> <ul id="lista">
              <b>Vantagens:</b><br><br>
                <li>Boa acessibliidade;</li></br>
                <li>Armazenamento por longo períodos sem perdas;</li><br>
                <li>Dispostos em pequenos cilindros para deambulação.</li></ul>
</td>   
            <td class="um_descricao">
                <ul id="lista">
                  <b>Desvantagens:</b><br><br>
                    <li>Custo elevado;</li><br>
                    <li>São pesados e grandes;</li><br>
                    <li>São perigosos às quedas;</li><br>
                    <li>Necessitam recargas frequentes.</li>
</td>   
                   <tr>
                    </table>
                     <b>Fonte:</b> (SOCIEDADE BRASILEIRA DE PNEUMOLOGIA E TISIOLOGIA, 2000, adaptado). </span>
</div>

<div id="tab2Content2" style="display:none;">
 <table class="table"><tr>       
                
            <td class="um_1"><img src="images/Fig4.2_oxigenio.jpg"  alt="Oxigênio Líquido"><br></td>  
            <td class="um_descricao"> <ul id="lista">
              <b>Vantagens:</b><br><br>
               <li>Permite a deambulação;</li></br>
                <li>Fornece fluxo de até 6 l/min;</li><br>
                <li>Não consome energia elétrica.</li></ul>
</td>   
            <td class="um_descricao">
                <ul id="lista">
                  <b>Desvantagens:</b><br><br>
                    <li>Alto custo;</li><br>
                    <li>Risco de queimaduras durante a recarga (armazenado abaixo de zero ºC).</li>
</td>   
                   <tr>
                    </table>
                    <b>Fonte:</b> (SOCIEDADE BRASILEIRA DE PNEUMOLOGIA E TISIOLOGIA, 2000, adaptado). </span>
</div>
<div id="tab3Content2" style="display:none;">
 <table class="table"><tr>       
                
            <td class="um_1"><img src="images/Fig4.3_concentrador.jpg"  alt="Concentrador"><br></td>  
            <td class="um_descricao"> <ul id="lista">
              <b>Vantagens:</b><br><br>
               <li>Volume de gás ilimitado;</li></br>
                <li>Não ocupam espaço;</li><br>
                <li>Custo de manutenção baixo;</li><br>
                <li>Facilidade de uso.</li></ul>
</td>   
            <td class="um_descricao">
                <ul id="lista">
                  <b>Desvantagens:</b><br><br>
                   <li>Fluxo máximo limitado a 10 l/min;</li><br>
           <li>Funcionamento a base de energia elétrica;</li><br>
           <li>Não são portáteis;</li><br>
           <li>Necessidade de um cilindro extra na falta de energia elétrica.</li>
</td>   
                   <tr>
                    </table>
 <b>Fonte:</b> (SOCIEDADE BRASILEIRA DE PNEUMOLOGIA E TISIOLOGIA, 2000, adaptado). </span>
</div>


<style>
  .tabContainer2{margin:10px 0;width:760px;font-size: 12px;font-size: 12px;font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;}
  .tabContainer2 .digiTabs2{font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;list-style:none;display:block;overflow:hidden;margin:0;padding:0px;position:relative;top:1px;}
  .tabContainer2 .digiTabs2 li{font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;float:left;background-color:#b5b5b5;border:1px solid #e1e1e1;padding:5px!important;cursor:pointer;border-bottom:none;margin-right:10px;font-family:verdana;font-size:.8em;font-weight:bold;color:#fff;border-top-left-radius: 6px;border-top-right-radius: 6px;}
  .tabContainer2 .digiTabs2 .selected2{font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;background-color:#fff;color:#393939;}
  #tabContent2{font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;padding:10px;background-color:#F6F6F6;overflow:hidden;float:left;margin-bottom:10px;border-top: 1px solid #E1E1E1;border-bottom: 1px solid #E1E1E1;border-left: 1px solid #E1E1E1;border-right: 1px solid #E1E1E1;width:93%;background-color: #fff;border-top-right-radius: 6px;border-bottom-left-radius: 6px;border-bottom-right-radius: 6px;}
</style>
<script type="text/javascript">
  function tabs2(x)
  {
    var lis=document.getElementById("sidebarTabs2").childNodes; //gets all the LI from the UL
 
    for(i=0;i<lis.length;i++)
    {
      lis[i].className=""; //removes the classname from all the LI
    }
    x.className="selected2"; //the clicked tab gets the classname selected
    var res=document.getElementById("tabContent2");  //the resource for the main tabContent
    var tab=x.id;
    switch(tab) //this switch case replaces the tabContent
    {
      case "tab1.2":
        res.innerHTML=document.getElementById("tab1Content2").innerHTML;
        break;
 
      case "tab2.2":
        res.innerHTML=document.getElementById("tab2Content2").innerHTML;
        break;
      case "tab3.2":
        res.innerHTML=document.getElementById("tab3Content2").innerHTML;
        break;
        case "tab4.2":
        res.innerHTML=document.getElementById("tab4Content2").innerHTML;
        break;
      default:
        res.innerHTML=document.getElementById("tab1Content2").innerHTML;
        break;
 
    }
  }
 
</script>

        <br><span class="credito"> 
        
                      
           <style type="text/css">
            .um{
                width: 188px;
            }

            .imagens{
                width: 190px;
            }
            .table td {
                border-top: 1px solid #DDDDDD;
                line-height: 20px;
                padding: 8px;
                text-align: left;
                vertical-align: top;
				word-break: normal;
            }
            #tabela1 .table td {
                border-top: 1px solid #000;
                line-height: 20px;
                padding: 8px;
                text-align: left;
                vertical-align: top;
            }
            .um_1,.dois, .tres{
                cursor: pointer;
            }
           </style>
           <script type="text/javascript">
            $('.um_1').mouseover(function(){
                $('.um_descricao').css('color','#000');
            });
            $('.dois').mouseover(function(){
                $('.dois_descricao').css('color','#000');
            });
            $('.tres').mouseover(function(){
                $('.tres_descricao').css('color','#000');
            });
           </script>
           
    
  
