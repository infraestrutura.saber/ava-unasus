<?php
               
    $pg_atual=8;
     registro($id,unid6_pg,$pg_atual,unid6_ev);
?>

	<style type="text/css">
		#lista{
            list-style-type: disc;  
            margin-left: 30px;
        }
		
		table tr td{
			text-align: center !important;
			padding-left: 10px;
		}
		.txt_localizacao{
            	left: 460px;
				position: absolute;
				top: 56px;
				z-index: 20;
				width: 250px;
				text-align: center;
            }
	
	</style>
    
   
	<h4 class="titulo">Estoma de Eliminação</h4>
   <h5 >Urostomia</h5><br/>
 <span style="margin-left: 475px;">
</span>
<ul style="float:left; width: 424px">
 É uma intervenção cirúrgica que desvia o curso natural da urina de forma permanente ou temporária. Ou seja, é uma derivação urinária na qual a drenagem da urina se dá fora dos condutos que envolvem a pelve renal, ureteres, bexiga e uretra. É indicada para (SILVA; HIGA-TANIGUCHI, 2010):
<br><br>
<li id="lista">neoplasia de bexiga e uretra;</li>
<li id="lista">fístulas;</li>
<li id="lista">disfunção neurológica;</li>
<li id="lista">lesões pós-tratamento radioterápico;</li>
<li id="lista">infecção crônica da bexiga;</li>
<li id="lista">defeitos congênitos.  </li>
</ul> 

<div class="txt_localizacao"><b>Derivação urinária realizada<br> nos ureteres </b><br>
<img  src="images/Fig09_urostomia.jpg" alt="Urostomia"><br>
<b>Fonte:</b> (UNA-SUS UFPE, 2014).
</div>
	<br/>
	
	<div style="width: 384px; margin-left: 223px; margin-top: 270px;">

	<div id="hide2" style="width: 300px; ">
<table style="background-color:#FFE4CA" class="table table-bordered " style="width: 314px;">

		<tr bgcolor="#ccc">
		<td><b>Complicações da Urostomia</b></td>
	
	</tr>
	<tr >
		<td>Sepse</td>
	
	</tr>
	
	<tr >
		<td>Fístula urinária</td>
	
	</tr>
	
	<tr >
		<td>Abcesso</td>
	
	</tr>
	
	<tr >
		<td>Obstrução uretral</td>
	
	</tr>
	
	<tr >
		<td>Urolitíase</td>
	
	</tr>
	
	<tr>
		<td>Sangramento</td>
	
	</tr>
	
	<tr >
		<td>Deiscência na ferida</td>
	
	</tr>
	
	<tr > 
		<td>Acidose metabólica</td>
	
	</tr>
	
	<tr > 
		<td>Hérnia para-estomal</td>
	
	</tr>
	
	<tr> 
		<td>Íleo prolongado</td>
	
	</tr>

</table>	

	<span style="margin-left: 0px;"><center><b>Fonte:</b> (ROCHA, 2011, adaptado).</center></span></div></div>
	<br><br>
  <h5 style="clear:both">Cuidados com a urostomia</h5>
  
  Para a maioria das pessoas, a perda da continência urinária ou fecal pode causar vários desequilíbrios, levando, assim, além dos problemas de ordem cirúrgica e física, a problemas de ordem psicológica e social. Dentro desse contexto, a equipe multiprofissional deve sempre orientar o paciente e o cuidador sobre a necessidade de (BRASIL, 2003):<br/>
  <ul>
<li id="lista">a bolsa estar adequada ao tamanho do estoma intestinal;</li>
<li id="lista">acondicionar as bolsas reservas em lugar arejado, limpo, seco e fora do alcance da luz solar;</li>
<li id="lista">esvaziar a bolsa de ileostomia e urostomia quando alcançar 1/3 de seu espaço preenchido;</li>
<li id="lista">proteger a bolsa durante o banho com um plástico e fitas adesivas;</li>
<li id="lista">trocar a bolsa, quando esta se apresentar saturada (placa protetora se torna quase completamente branca);</li>
<li id="lista">observar sempre a cor (deve ser vermelho vivo), o brilho, a umidade, a presença de muco, o tamanho e a forma do estoma;</li>
<li id="lista">realizar a limpeza do estoma delicadamente, não sendo necessário nem aconselhável esfregá-lo;</li>
<li id="lista">realizar a limpeza da pele ao redor do estoma com água e sabonete neutro, sem esfregar com força, nem usar esponjas ásperas;</li>
<li id="lista">aparar os pelos ao redor do estoma com tesoura;</li>
<li id="lista">expor a pele ao redor do estoma (sempre que possível) ao sol da manhã, de 15 a 20 minutos por dia, tendo cuidado de proteger o estoma com gaze umedecida;</li>
<li id="lista">não utilizar substâncias agressivas à pele, como álcool, benzina, colônias, tintura de benjoim, mercúrio, digluconato de clorexidina, pomadas e cremes, pois estes produtos podem ressecar a pele, ferindo-a e causando reações alérgicas.</li>
</ul>
<br><br>
<!--<a href="javascript:history.go(-1)" class="btn">Voltar</a>-->
  <script>
	
		
		function esconder(id){
		
			if ($("#hide" + id).is(":hidden")) {
				$("#hide" + id).slideDown("");
				
			} else{
			$('#hide' + id).hide("");
			}
	
		}
 
	</script>
  
