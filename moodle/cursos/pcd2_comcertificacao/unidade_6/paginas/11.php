<?php
               
    $pg_atual=11;
     registro($id,unid6_pg,$pg_atual,unid6_ev);
?>

<!-- colorbox exercicio1 -->
<div style='display:none'>
			<div id="content_color1" style='padding:10px; background:#fff;'>

			<div id="texto_color1"></div>
			<br><br>
			<a class="btn" href="index.php?pagina=11">Tentar novamente</a>
			<a class="btn" href="index.php?pagina=0">Estudar mais</a>
			<a class="inline btn" href="#feedback1">Mostrar respostas</a>
		
			<br><br>
			
			</div>
		</div><!-- fim colorbox exercicio1-->
	 
	<div style='display:none'>
		<div id="feedback1">
			
				<h4>Respostas</h4>

A sequência correta é: <b>nutrir, hidratar, vias aéreas superiores, garantir uma boa ventilação, fecal, medicamentos, jejunostomia, laparotomia ou laparoscopia</b>
		<br><br>
			<a class="inline btn" href="#content_color1">Voltar</a>
		</div>
	</div>
<!-- colorbox exercicio1 final -->
<!-- colorbox exercicio2 -->
<div style='display:none'>
			<div id="content_color2" style='padding:10px; background:#fff;'>

			<div id="texto_color2"></div>
			<br><br>
			<a class="btn" href="index.php?pagina=11">Tentar novamente</a>
			<a class="btn" href="index.php?pagina=0">Estudar mais</a>
			<a class="inline2 btn" href="#feedback2">Mostrar respostas</a>
		
			<br><br>
			
			</div>
		</div>
		<div style='display:none'>
		<div id="feedback2">
			
			
				<h4>Respostas</h4>
		1. <b>Verdadeiro</b><br>
		2. <b>Falso</b> - A fixação da cânula deve ser inspecionada diariamente.<br>
		3. <b>Verdadeiro</b> <br>
		4. <b>Falso</b> - As cânulas devem ser trocadas após o banho ou na presença de sujidades.<br>
		5. <b>Falso</b> - Para a higienização do tubo traqueal, retire a subcânula e lave apenas com água e detergente líquido, escovando-a por dentro para retirar a secreção acumulada.<br><br>
		
				<a class="inline2 btn" href="#content_color2">Voltar</a>
		</div>
	</div>
<!-- fim colorbox exercicio2-->
<!-- colorbox exercicio3 -->
<div style='display:none'>
			<div id="content_color3" style='padding:10px; background:#fff;'>

			<div id="texto_color3"></div>
			<br><br>
			<a class="btn" href="index.php?pagina=11">Tentar novamente</a>
			<a class="btn" href="index.php?pagina=0">Estudar mais</a>
			<a class="inline3 btn" href="#feedback3">Mostrar respostas</a>
		
			<br><br>
			
			</div>
		</div>
		<div style='display:none'>
		<div id="feedback3">
			
			<h4>Respostas</h4>
A resposta correta é a letra <b>D</b>.<br><br>
a) A alternativa está incorreta, pois é sugerido um diâmetro de 10mm para o disco de fixação da bolsa, e o estoma da senhora Clotilde tem 20mm de diâmetro, ou seja, é inferior ao tamanho do estoma. Caso esta conduta seja tomada, a paciente poderá apresentar lesão seguida de dermatite. Para evitar danos, o tamanho da inserção da bolsa de colostomia deve ser adequado ao tamanho do estoma intestinal. <br><br>
b) As bolsas que coletam fezes (colostomia e ileostomia) devem ser esvaziadas sempre que for necessário, geralmente uma ou duas vezes ao dia, quando estiverem com 1/3 (um terço) do espaço ocupado com fezes.<br><br>
c) A limpeza da pele ao redor do estoma deve ser realizada cuidadosamente, utilizando água e sabonete neutro, sem esfregar com força, nem usar esponjas ásperas;<br><br>
e) Na higienização do estoma e da região periestomal, não se deve utilizar substâncias agressivas à pele, como álcool, benzina, colônias, tintura de benjoim, mercúrio, digluconato de clorexidina, pomadas e cremes, pois esses produtos podem ressecar a pele, ferindo-a e causando reações alérgicas. Quando a pele periestomal tiver pelos, deve-se cortá-los com uma tesoura rente à pele. O uso de barbeador apresenta o risco de cortar a pele e de causar inflamação no local de crescimento dos pelos.
<br><br>

<br><br>
		
				<a class="inline3 btn" href="#content_color3">Voltar</a>
		</div>
	</div>
<!-- fim colorbox exercicio3-->

	  
<h4 class="titulo">Exercícios</h4>
 

 
 <div id="ex1">
	 <h5>Atividade 1</h5>
 
 
Leia atentamente as frases abaixo. Em seguida, selecione as opções que completam as sentenças corretamente. 
<br><br>
a) Um estoma digestório tem por finalidade 
	<select id="s1">
		  <option value="0">-</option>
	  	  <option value="1">fecal ou urinário</option>
		  <option value="2">hidratar</option>
		  <option value="3">jejunostomia</option>
		  <option value="4">medicamentos</option>
		  <option value="5">nutrir</option>
		  <option value="6">garantir uma boa ventilação</option>
		  <option value="7">vias aéreas superiores</option>
		  <option value="8">laparotomia ou laparoscopia</option>
	  </select> e <select id="s2">
		  <option value="0">-</option>
	  	  <option value="1">fecal ou urinário</option>
		  <option value="2">hidratar</option>
		  <option value="3">jejunostomia</option>
		  <option value="4">medicamentos</option>
		  <option value="5">nutrir</option>
		  <option value="6">garantir uma boa ventilação</option>
		  <option value="7">vias aéreas superiores</option>
		  <option value="8">laparotomia ou laparoscopia</option>
	  </select> o paciente com distúrbios no sistema gastrointestinal.  <br>
b) Um estoma respiratório é capaz de desviar a obstrução aguda ou crônica das <select id="s3">
		  <option value="0">-</option>
	  	  <option value="1">fecal ou urinário</option>
		  <option value="2">hidratar</option>
		  <option value="3">jejunostomia</option>
		  <option value="4">medicamentos</option>
		  <option value="5">nutrir</option>
		  <option value="6">garantir uma boa ventilação</option>
		  <option value="7">vias aéreas superiores</option>
		  <option value="8">laparotomia ou laparoscopia</option>
	  </select> a fim de <select id="s4">
		  <option value="0">-</option>
	  	  <option value="1">fecal ou urinário</option>
		  <option value="2">hidratar</option>
		  <option value="3">jejunostomia</option>
		  <option value="4">medicamentos</option>
		  <option value="5">nutrir</option>
		  <option value="6">garantir uma boa ventilação</option>
		  <option value="7">vias aéreas superiores</option>
		  <option value="8">laparotomia ou laparoscopia</option>
	  </select>.<br>

c) Um estoma pode ter como finalidade drenar conteúdo <select id="s5">
		  <option value="0">-</option>
	  	  <option value="1">fecal ou urinário</option>
		  <option value="2">hidratar</option>
		  <option value="3">jejunostomia</option>
		  <option value="4">medicamentos</option>
		  <option value="5">nutrir</option>
		  <option value="6">garantir uma boa ventilação</option>
		  <option value="7">vias aéreas superiores</option>
		  <option value="8">laparotomia ou laparoscopia</option>
	  </select> e administrar <select id="s6">
		  <option value="0">-</option>
	  	  <option value="1">fecal ou urinário</option>
		  <option value="2">hidratar</option>
		  <option value="3">jejunostomia</option>
		  <option value="4">medicamentos</option>
		  <option value="5">nutrir</option>
		  <option value="6">garantir uma boa ventilação</option>
		  <option value="7">vias aéreas superiores</option>
		  <option value="8">laparotomia ou laparoscopia</option>
	  </select>.<br>
d) <select id="s7">
		  <option value="0">-</option>
	  	  <option value="1">fecal ou urinário</option>
		  <option value="2">hidratar</option>
		  <option value="3">jejunostomia</option>
		  <option value="4">medicamentos</option>
		  <option value="5">nutrir</option>
		  <option value="6">garantir uma boa ventilação</option>
		  <option value="7">vias aéreas superiores</option>
		  <option value="8">laparotomia ou laparoscopia</option>
	  </select> é um procedimento cirúrgico de caráter temporário ou definitivo, por meio de endoscopia, <select id="s8">
		  <option value="0">-</option>
	  	  <option value="1">fecal ou urinário</option>
		  <option value="2">hidratar</option>
		  <option value="3">jejunostomia</option>
		  <option value="4">medicamentos</option>
		  <option value="5">nutrir</option>
		  <option value="6">garantir uma boa ventilação</option>
		  <option value="7">vias aéreas superiores</option>
		  <option value="8">laparotomia ou laparoscopia</option>
	  </select> que estabelece o acesso ao jejuno (intestino) proximal através da parede abdominal.
<br>
<br>



 <a class='inline btn' style="float: right;margin-right: 45px;" type="button" href="#content_color1" onClick="respostas(1);" >Corrigir</a>
</div>



<div id="ex2">
 <h5>Atividade 2</h5>

Marque verdadeiro (V) ou falso (F) nas alternativas abaixo, em relação aos cuidados com a traqueostomia:<br><br>

			
			
	<table  style="width: 640px;margin-left: 45px;margin-top: 33px;margin-bottom: 20px;">
	
	
	<tr bgcolor="#F68C23">	
	<td><center><div class="d1"><h5 style="color:#fff;">Sentenças</h5></div> </center><center> <div class="d3"><h5 style="color:#fff;">Verdadeiro | Falso</h5></div> </center></td>
	</tr>
	
	<tr bgcolor="#FFF0E2"  >
		<td><div class="d1"><p>1. É importante observar o aspecto da traqueostomia, avaliando se há presença de secreção, sangramento, granuloma ou sinais de infecção</p></div>
		<div class="d2"><p><input type="radio" name="1" value="V"></p><span class="boolean">V</span></div>
		<div class="d3"><p><input type="radio" name="1" value="F"></p><span class="boolean">F</span> </div></td>
	
	</tr>
	
	<tr bgcolor="#FFE4C4" >
		<td><div class="d1"><p>2. A fixação da cânula deve ser inspecionada a cada 2 dias, sendo mantida limpa e com folga para que permita a movimentação do pescoço, evitando, assim, lesões na pele.</p></div>
		<div class="d2"><p><input type="radio" name="2" value="V"></p><span class="boolean">V</span></div>
		<div class="d3"><p><input type="radio" name="2" value="F"></p><span class="boolean">F</span></div></td>
	
	</tr>
	
	<tr bgcolor="#FFF0E2" >
		<td><div class="d1"><p>3. A umidificação do oxigênio deve ser feita com nebulização, sempre que necessário. </p></div>
		<div class="d2"><p><input type="radio" name="3" value="V"></p><span class="boolean">V</span></div>
		<div class="d3"><p><input type="radio" name="3" value="F"></p><span class="boolean">F</span></div></td>
	
	</tr>
	
	<tr bgcolor="#FFE4CA">
		<td><div class="d1"><p>4. Deve-se proteger a cânula com gazes. A troca da cânula após o  banho não é necessária.</p></div>
		<div class="d2"><p><input type="radio" name="4" value="V"></p><span class="boolean">V</span></div>
		<div class="d3"><p><input type="radio" name="4" value="F"></p><span class="boolean">F</span></div></td>
	
	</tr>
		
	<tr bgcolor="#FFF0E2">
		<td><div class="d1"><p>5. Para a higienização do tubo traqueal, retire a subcânula e lave com água e álcool a 70%, escovando-a por dentro para retirar a secreção acumulada.</p></div>
		<div class="d2"><p><input type="radio" name="5" value="V"></p><span class="boolean">V</span></div>
		<div class="d3"><p><input type="radio" name="5" value="F"></p><span class="boolean">F</span></div></td>
	
	</tr>
	
		
</table>


		
		 <a class='inline2 btn' style="float: right;margin-right: 45px;" type="button" href="#content_color2" onClick="respostas2(2);" >Corrigir</a>

</div>	


 
<div id="ex3">
 <h5>Atividade 3<b><i class=" icon-hand-up" title="Essa atividade possui interação."></i></h5>
 </b>

 Analise o caso apresentado a seguir.
 <br><br>
	<div style="margin-left:50px;margin-top: 17px;">
	<div class="um_action"></div>
	<div class="dois_action"></div>
    <div class="tres_action"></div>
	<div class="quatro_action"></div>
	<div class="cinco_action"></div>
	<div class="seis_action"></div>
	<div class="sete_action"></div>
	
	<div class="box_um"><img src="images/quadro1.png"/></div>
	<div class="box_dois"><img src="images/quadro2.png"/></div>
	<div class="box_tres"><img src="images/quadro3.png"/></div>
	<div class="box_quatro"><img src="images/quadro4.png"/></div>
	<div class="box_cinco"><img src="images/quadro5.png"/></div>
	<div class="box_seis"><img src="images/quadro6.png"/></div>

		
 <img id="img_din" src="images/img_base.jpg" alt="Exercício 3 Clotilde"/></div>
 <br><br>
Diante do quadro de Clotilde, durante a visita domiciliar, quais informações deverão ser fornecidas pela equipe multiprofissional ao cuidador e à paciente? Marque a alternativa que contém todas as informações corretas.
<br><br>
<div id="op1">
	<input type="radio" name="ex3" value="a">
	<p>
	<b>a)</b> A equipe multiprofissional deve recomendar ao cuidador que observe a perfusão do estoma, se há presença de muco e se há alteração do seu tamanho e da sua forma. Outro cuidado importante é a troca da bolsa de colostomia. Deve-se recomendar que o tamanho da abertura do disco de fixação da bolsa de colostomia de Clotilde seja de 10mm ou inferior para fixar melhor. 
	</p>
	</div>
	
	<div id="op2">
	<input type="radio" name="ex3" value="b">
	
	<p>
	<b>b)</b> Foi informado pelo cuidador que a bolsa de Clotilde está drenando muco intestinal, de consistência semissólida, em grande quantidade, ultrapassando o limite da indicação do fabricante da bolsa. O cuidador deve ser orientado a realizar o esvaziamento da bolsa a cada 24 horas.
</p>
</div>


<div id="op3">
<input type="radio" name="ex3" value="c">
<p>
<b>c)</b> Clotilde tem apresentado dermatite na área próxima ao estoma. Sobre a higiene corporal da paciente, o cuidador deve ser orientado a proteger a bolsa com um plástico e fita adesiva e, para auxiliar a limpeza da área periestomal, a fazer uso de uma esponja vegetal levemente hidratada e sabonete neutro. 
</p>
</div>


<div id="op4">
<input type="radio" name="ex3" value="d">
<p>
<b>d)</b>	A pele ao redor do estoma deve ser exposta ao sol da manhã, de 15 a 20 minutos por dia, tendo cuidado de sempre proteger o estoma com gaze umedecida.

</p>
</div>

<div id="op5">
<input type="radio" name="ex3" value="e">
<p>
<b>e)</b>	Na higienização da pele periestomal, o uso de substâncias como álcool, tintura de benjoim, mercúrio, digluconato de clorexidina, pomadas e cremes pode ser indicado ao cuidador. No caso de presença de pelos, cortá-los com uma tesoura ou barbeador.

</p>
</div>
	
	
	<a class='inline3 btn' style="float: right;margin-right: 45px;" type="button" href="#content_color3" onClick="respostas3(3);" >Corrigir</a>
	
	<div id="feedback3" style="margin-top: 80px;">
		

</div></div>

<style type="text/css">
		
		#ex1 select{
		
			width: 220px;
		
		}
		
		#ex1, #ex2, #ex3{
			margin-top: 60px;
		
		}
		
		#ex3{
			position: relative;
		
		}
		
		#feedback1, #feedback2, #feedback3{
		
			
		}
		
		table tr td{
			font-size: 15px;
			border: 1px solid #fff;
		}
		
		.d1{

			width: 469px;
			float: left;
			margin-right: 16px;
			border-right: 1px solid #fff;
			
		}
		
		.d1 p{
			margin-top: 8px;
			margin-left: 10px;
		}
		
		.d2{
			float: left;
			padding-right: 15px;
			padding-left: 15px;
			
		
		}
		
		.d2 p{
			margin-top: 5px;
			margin-left: 10px;
		
		}
		
		.d3{
			float: left;
			padding-right: 15px;
			
			
		}
		
		.d3 p{
		
			margin-top: 5px;
			margin-left: 10px;
		}
		
		.boolean{
			margin-top: -24px;
			margin-left: 29px;
			float: right;
		}
		
		#op1 , #op2, #op3, #op4 , #op5 {
			text-align: justify;
		}
		
		#op1 input , #op2 input, #op3 input, #op4 input, #op5 input{
			float: left;
			margin-right: 10px;
			margin-left: 10px;
		}
		
		.um_action{
			background-color: #FF6C0A;
			border: 2px solid #FFFFFF;
			border-radius: 30px 30px 30px 30px;
			cursor: pointer;
			height: 7px;
			left: 350px;
			position: absolute;
			top: 142px;
			width: 7px;
			z-index: 3;

		}
		
		.dois_action{
			background-color: #FF6C0A;
			border: 2px solid #FFFFFF;
			border-radius: 30px 30px 30px 30px;
			cursor: pointer;
			height: 7px;
			left: 335px;
			position: absolute;
			top: 248px;
			width: 7px;
			z-index: 3;
			
		}
		
		.tres_action{
			background-color: #FF6C0A;
			border: 2px solid #FFFFFF;
			border-radius: 30px 30px 30px 30px;
			cursor: pointer;
			height: 7px;
			left: 370px;
			position: absolute;
			top: 268px;
			width: 7px;
			z-index: 3;
			
		}
		
		.quatro_action{
			background-color: #FF6C0A;
			border: 2px solid #FFFFFF;
			border-radius: 30px 30px 30px 30px;
			cursor: pointer;
			height: 7px;
			left: 322px;
			position: absolute;
			top: 305px;
			width: 7px;
			z-index: 3;
			
		}
		
		.cinco_action{
			background-color: #FF6C0A;
			border: 2px solid #FFFFFF;
			border-radius: 30px 30px 30px 30px;
			cursor: pointer;
			height: 7px;
			left: 352px;
			position: absolute;
			top: 313px;
			width: 7px;
			z-index: 3;
			
		}
		
		.seis_action{
			background-color: #FF6C0A;
			border: 2px solid #FFFFFF;
			border-radius: 30px 30px 30px 30px;
			cursor: pointer;
			height: 7px;
			left: 354px;
			position: absolute;
			top: 338px;
			width: 7px;
			z-index: 3;
			
		}
		
		.sete_action{
			background-color: #FF6C0A;
			border: 2px solid #FFFFFF;
			border-radius: 30px 30px 30px 30px;
			cursor: pointer;
			height: 7px;
			left: 368px;
			position: absolute;
			top: 321px;
			width: 7px;
			z-index: 3;
			
		}
			
			
		.box_um{
			left: 55px;
			position: absolute;
			top: 127px;
			display: none;
		}
		
		.box_dois{
			left: 60px;
			position: absolute;
			top: 240px;
			display: none;

		}
		
		.box_tres{
			left: 53px;
			position: absolute;
			top: 322px;
			display: none;

		}
		
		.box_quatro{
			left: 53px;
			position: absolute;
			top: 348px;
			display: none;

		}
		
		.box_cinco{
			left: 375px;
			position: absolute;
			top: 132px;
			display: none;

		}

		
		.box_seis{
			left: 377px;
			position: absolute;
			top: 282px;
			display: none;

		}

		
		.box_sete{
			left: 153px;
			position: absolute;
			top: 50px;
			display: none;

		}


		
	
	
	</style>
	
	<script type="text/javascript">
	
    function respostas(id){

	
		
		var s1 = document.getElementById("s1").selectedIndex;
		var s2 = document.getElementById("s2").selectedIndex;
		var s3 = document.getElementById("s3").selectedIndex;
		var s4 = document.getElementById("s4").selectedIndex;
		var s5 = document.getElementById("s5").selectedIndex;
		var s6 = document.getElementById("s6").selectedIndex;
		var s7 = document.getElementById("s7").selectedIndex;
		var s8 = document.getElementById("s8").selectedIndex;
			
		
		$(".inline2").colorbox({inline:true, width:"50%",onClosed:function(){ document.getElementById("feedback" + id).style.display="block"; }});
		 
		 if(s1 == 5 && s2 == 2 && s3 == 7 && s4 ==6 && s5 == 1 && s6 == 4 && s7 == 3 && s8 == 8){
		 
		 	$(".inline").colorbox({inline:true, width:"50%",onClosed:function(){ document.getElementById("feedback" + id).style.display="block"; }});
			$("#texto_color1").html("<h4>Resultado</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, você completou as sentenças corretamente!");
		 	$.post( "../apresentacao/functions/chamaExCerto.php", { exercicio: 7}, function() {
						
				});
			mostrarFeedback(1);
		}else if(s1 == 0 || s2 == 0 || s3 == 0 || s4 == 0 || s5 == 0 || s6 == 0 || s7 == 0 || s8 == 0 ){
		
			$("#content_color1").html("<h4>Resultado</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, selecione uma opção válida em todos os campos.");
         	
		}else {
			$(".inline").colorbox({inline:true, width:"50%",onClosed:function(){ document.getElementById("feedback" + id).style.display="block"; }});
			$("#texto_color1").html("<h4>Resultado</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, você não completou as sentenças corretamente.");
			$.post( "../apresentacao/functions/chamaExErrado.php", { exercicio: 7}, function() {
						
				});
			mostrarFeedback(1);
		}
		$(".inline").colorbox({inline:true, width:"50%",onClosed:function(){ location.reload();}});
		
 
}

//Exercício 2 

    function respostas2(id){

		var p1 = document.getElementsByName('1');
        var p2 = document.getElementsByName('2');
        var p3 = document.getElementsByName('3');
        var p4 = document.getElementsByName('4');
		var p5 = document.getElementsByName('5');
        var gabarito = new Array('V','F','V','F','F');


        var resp = new Array(p1,p2,p3,p4,p5);
        var completo = true;


        for(var x = 0; x <6; x++){

            if(resp[x] != null){
                 if(resp[x][0].checked == true){
                           resp[x] = p1[0].value;
                }else if(resp[x][1].checked == true){
                            resp[x] = p1[1].value;

                }else{
                    completo = false;
                }
            }
        }

        if(completo){

			if(compare(resp,gabarito) == true){
            $("#texto_color2").html("<h4>Resultado</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, parabéns! Você acertou todas as alternativas.");
            	$(".inline2").colorbox({inline:true, width:"50%"});	
				$.post( "../apresentacao/functions/chamaExCerto.php", { exercicio: 8}, function() {
						
				});
              
            }else{
               
               $("#texto_color2").html("<h4>Resultado</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, você não acertou todas as alternativas.");
               	$(".inline2").colorbox({inline:true, width:"50%",onClosed:function(){  }});
				$.post( "../apresentacao/functions/chamaExErrado.php", { exercicio: 8}, function() {
						
				});
            }
			
			mostrarFeedback(id);

         }else{
           
		    $("#content_color2").html("<h4>Atenção!</h4> <br>Preencha todas as sentenças antes de clicar em corrigir.");
  
         }
		$(".inline2").colorbox({inline:true, width:"50%",onClosed:function(){ location.reload();}});
           
}

 function compare(x, y) {
            if (x.length != y.length) {
                return false;
            }
            for (key in x) {
                if (x[key] !== y[key]) {
                    return false;
                }
            }
            return true;
        }


		
	function mostrarFeedback(id){
		document.getElementById("feedback" + id).style.display="block";
	}
	
	//-----EXERCÍCIO 3 --------//
	
	function respostas3(id){
		
		 var p1 = document.getElementsByName('ex3');
		 
		 if(p1[3].checked == true){
			$("#texto_color3").html("<h4>Resultado</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, parabéns! Você selecionou a alternativa correta.");
            	$(".inline3").colorbox({inline:true, width:"50%"});
				$.post( "../apresentacao/functions/chamaExCerto.php", { exercicio: 13}, function() {
						
				});
			mostrarFeedback(3);
		}else if(p1[1].checked == true){
			$("#texto_color3").html("<h4>Resultado</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, você selecionou a alternativa incorreta.");
            	$(".inline3").colorbox({inline:true, width:"50%"});
				$.post( "../apresentacao/functions/chamaExErrado.php", { exercicio: 13}, function() {
						
				});
			mostrarFeedback(3);
		}else if(p1[2].checked == true){
			$("#texto_color3").html("<h4>Resultado</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, você selecionou a alternativa incorreta.");
            	$(".inline3").colorbox({inline:true, width:"50%"});
				$.post( "../apresentacao/functions/chamaExErrado.php", { exercicio: 13}, function() {
						
				});
			mostrarFeedback(3);
		}else if(p1[0].checked == true){
			$("#texto_color3").html("<h4>Resultado</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, você selecionou a alternativa incorreta.");
            	$(".inline3").colorbox({inline:true, width:"50%"});
				$.post( "../apresentacao/functions/chamaExErrado.php", { exercicio: 13}, function() {
						
				});
			mostrarFeedback(3);
		}else if(p1[4].checked == true){
			$("#texto_color3").html("<h4>Resultado</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, você selecionou a alternativa incorreta.");
            	$(".inline3").colorbox({inline:true, width:"50%"});
				$.post( "../apresentacao/functions/chamaExErrado.php", { exercicio: 13}, function() {
						
				});
			mostrarFeedback(3);
		}else{
			  $("#content_color3").html("<h4>Atenção!</h4> <br>Selecione uma opção antes de clicar em corrigir.");
         	
		}
		$(".inline3").colorbox({inline:true, width:"50%",onClosed:function(){ location.reload();}});
	}
	
	
	

	
</script>

<script type="text/javascript">
	$('.um_action').click(function(){
		$('.box_um').fadeIn(500);

	});
	
	$('.quatro_action').click(function(){
		$('.box_um').fadeIn(500);
	
	});
	
	$('.dois_action').click(function(){
		$('.box_dois').fadeIn(500);
		
		
	});
	$('.cinco_action').click(function(){
		$('.box_tres').fadeIn(500);
		
	});
	
	$('.tres_action').click(function(){
		$('.box_cinco').fadeIn(500);
		
	});
	
	$('.sete_action').click(function(){
		$('.box_seis').fadeIn(500);
		
	});
	
	$('.seis_action').click(function(){
		$('.box_quatro').fadeIn(500);
		
	});

</script>
	<?php
	if ($_GET['ex']==1){ 

	}
	if ($_GET['ex']==2){ 

		echo '<script type="text/javascript">$("html,body").animate({scrollTop: $("#ex2").offset().top}, 1500);</script>';
	}
	if ($_GET['ex']==3){ 

		echo '<script type="text/javascript">$("html,body").animate({scrollTop: $("#ex3").offset().top}, 1500);</script>';
	}
?>


