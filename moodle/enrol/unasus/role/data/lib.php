<?php
/*
 * Created on 30/07/2012
 *
 *Essa class tem as bibliotecas de gerenciamento
 * dos dados da homologação da inscrição. 
 */
 class unasus_role_data {
 	
 	public static $TYPE_CBO ="CBO";
 	
 	//inserir regristro na base de dados. 
 	function save($dto) {
 		global $CFG;
 		return insert_record('enrol_unasus_role_data', $dto);
 	}
 
 //alteriar regristro na base de dados. 
 	function edit($dto) {
 		global $CFG;
 		return update_record('enrol_unasus_role_data', $dto);
 	}

 	
  //excluir regristro na base de dados. 
 function delete_by_id($id) {
 		global $CFG;
 		return delete_records_select('enrol_unasus_role_data', "id=$id");
 	}
  //Retora a quantidade de registro cadastrada na base de dados
 function count() {
 		global $CFG;
		$sql ="SELECT COUNT(id)  AS qreg FROM {$CFG->prefix}enrol_unasus_role_data ";
		$r=get_record_sql($sql);
		return $r->qreg;
 	}
 	function get_by_enrolid($enrolid,$type) {
 		global $CFG;
		$sql ="SELECT id,enrolid,type,value FROM {$CFG->prefix}enrol_unasus_role_data WHERE enrolid=$enrolid AND type='".$type."'";
		$r=get_record_sql($sql);
		return $r;
 	}
 	
 	function get_cbo_by_enrolid($enrolid) {
 		global $CFG;
		$sql ="SELECT value FROM {$CFG->prefix}enrol_unasus_role_data WHERE enrolid=$enrolid AND type='".self::$TYPE_CBO."'";
		$r=get_records_sql($sql);
		return $r;
 	}
 	
 	function get_id_enrolid($enrolid,$type) {
 		global $CFG;
		$sql ="SELECT id FROM {$CFG->prefix}enrol_unasus_role_data WHERE enrolid=$enrolid AND type='".$type."'";
		$r=get_record_sql($sql);
		return $r->id;
 	}
 	function exist_by_enrolid($enrolid,$type) {
 		global $CFG;
		$sql ="SELECT COUNT(id) AS qreg   FROM {$CFG->prefix}enrol_unasus_role_data WHERE enrolid=$enrolid AND type='".$type."'";
		$r=get_record_sql($sql);
		return $r->qreg;
 	}
 	
 	function get_all() {
 		global $CFG;
		$sql ="SELECT id,enrolid,type,value  FROM {$CFG->prefix}enrol_unasus_role_data ";
		$r=get_records_sql($sql);
		return $r;
 	}
 	
 }