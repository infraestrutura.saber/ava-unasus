<?php
               
    $pg_atual=8;
     registro($id,unid7_pg,$pg_atual,unid7_ev);
?>
<h4 class="titulo">Referências</h4>


ANVISA (AGÊNCIA NACIONAL DE VIGILÂNCIA SANITÁRIA).<b> Infecções do trato respiratório: </b>orientações para prevenção de infecções relacionadas à assistência à saúde. Brasília: ANVISA, 2009.<br> Disponível em: <a href="http://www.anvisa.gov.br/servicosaude/controle/manual_%20trato_respirat%F3rio.pdf" target="_blank">&#60 http://www.anvisa.gov.br/servicosaude/controle/manual_%20trato_respirat%F3rio.pdf&#62</a>. Acesso em: 27 abr. 2013. 

<br><br>

    BARROS, A. L. B. L. et al. <b>Anamnese e exame físico: </b> avaliação diagnóstica de enfermagem no adulto. Porto Alegre: Artmed, 2002. 272p. 

<br><br>
        BRASIL. Ministério da Saúde. Departamento de Gestão e Incorporação de Tecnologias em Saúde. Secretaria de Ciência, Tecnologia e Insumos Estratégicos (DGITS/SCTIE). Comissão Nacional de Incorporação de Tecnologias no SUS (CONITEC). Relatório Nº 32.<b> Oxigenoterapia domiciliar para tratamento da doença pulmonar obstrutiva crônica (DPOC).</b> Brasília, 2012. Disponível em: <a href="http://portal.saude.gov.br/portal/arquivos/pdf/Relatorio_oxigenoterapiadomiciliar.pdf" target="_blank">&#60http://portal.saude.gov.br/portal/arquivos/pdf/Relatorio_oxigenoterapiadomiciliar.pdf&#62</a>. Acesso em: 28 abr. 2013.


    <br><br>

    BRASIL. Ministério da Saúde. Secretaria de Atenção à Saúde. Departamento de Atenção Básica. <b>Caderno de Atenção Domiciliar</b>. v . 2. Brasília: Ed. Ministério da Saúde, 2012. Disponível em: <a style="color:#5F9EA0;"href="http://189.28.128.100/dab/docs/geral/cap_7_vol_2_situacoes_especiais_final.pdf" target="_blank"> &#60http://189.28.128.100/dab/docs/geral/cap_7_vol_2_situacoes_especiais_final.pdf&#62</a>. Acesso em: 14 mar. 2013.



    <br><br>

    BRASIL. Ministério da Saúde. Secretaria de Atenção à Saúde. Departamento de Atenção Básica. Caderno de Atenção Domiciliar. v . 2. Brasília: Ed. Ministério da Saúde, 2012. Disponível em: <a href="http://189.28.128.100/dab/docs/geral/cap_7_vol_2_situacoes_especiais_final.pdf" target="_blank">&#60http://189.28.128.100/dab/docs/geral/cap_7_vol_2_situacoes_especiais_final.pdf&#62</a>. Acesso em: 14 mar. 2013. 

    <br><br>

        LIMA, T. da C.; VARGAS, M. A. de O. Cuidado domiciliar intensivo: uma possível realidade do Sistema Único de Saúde? <b>Rev. Bras. Enferm.</b>, Brasília, v. 57, n. 6, p.658-661, nov./dez.,  2004. Disponível em:<a href="http://dx.doi.org/10.1590/S0034-71672004000600004" target="_blank">&#60http://dx.doi.org/10.1590/S0034-71672004000600004&#62</a>. Acesso em: 28 abr. 2013.

<br><br>
    
        LORENTZ, M. N. et al. Mixoma de átrio direito associado a cor pulmonale agudo: relato de caso.<b> Rev. Bras. Anestesiologia</b>, Campinas, v. 58, n. 1, jan./feb., 2008. Disponível em:<a href="http://www.scielo.br/scielo.php?pid=S0034-70942008000100010&script=sci_arttext" target="_blank">&#60  http://www.scielo.br/scielo.php?pid=S0034-70942008000100010&script=sci_arttext&#62</a>. Acesso em: 27 abr. 2013.

    <br><br>

        MINAS GERAIS (Estado). Conselho Regional de Enfermagem de Minas Gerais (COREN-MG). <b>Norma regulamentadora 32:</b> segurança e saúde no trabalho em serviços de saúde. Belo Horizonte: COREN-MG, 2007. 52p.

<br><br>
    
    RIBEIRÃO PRETO (Estado). Secretaria Municipal de Saúde. Serviço de Assistência Domiciliar.
     <b>Protocolo de atendimento em oxigenoterapia domiciliar prolongada ODP.</b> 
     Ribeirão Preto: SMS, set., 2007. <br> Disponível em: <a href="http://www.ribeiraopreto.sp.gov.br/ssaude/programas/sad/protocolo-odp.pdf" target="_blank">&#60 www.ribeiraopreto.sp.gov.br/ssaude/programas/sad/protocolo-odp.pdf&#62</a>. Acesso em: 29 abr. 2013.

    <br><br>

        SANTOS, G. A. da S. O papel da enfermagem. In: YAMAGUCHI, A. M. et al. <b>Assistência domiciliar:</b> uma proposta interdisciplinar. Barueri, SP: Manole, 2010. 

    <br><br>

        SCANLAN, C. L.; WILKINS, R. L.; STOLLER, J. K.<b> Fundamentos da terapia respiratória de Egan.</b> São Paulo: Mosby, 2009. 

<br><br>
        
    SMELTIZER, S. C.; BARE, B. G.<b> Tratado de enfermagem médico-cirúrgica.</b> 10. ed. v. 2.  Rio de Janeiro: Guanabara Koogan, 2005.

<br><br>
SOCIEDADE BRASILEIRA DE PNEUMOLOGIA E TISIOLOGIA. Oxigenoterapia domiciliar prolongada (ODP). 
 <b>J. Bras. Pneumologia</b>, Brasília, v. 26, n. 6, nov./dez., 2000. Disponível em: 
 <a href="http://www.scielo.br/scielo.php?script=sci_arttext&pid=S0102-35862000000600011" target="_blank">&#62www.jornaldepneumologia.com.br/PDF/2000_26_6_9_portugues.pdf&#62</a>. Acesso em: 27 abr. 2013.

<br><br>
    
    SURHONE, L. M.; TENNOE, M. T.; HENSSONOW, S. F. <b>Venturi mask.</b> Saarbrücken: VDM Publishing, 2010.

    <br><br>

        ZEFERINO, M. T.; SILVA, A. S. da.<b> Guia do usuário de oxigenoterapia domiciliar.</b> Florianópolis: Secretaria do Estado, 2004. Disponível em: <a href="http://portalses.saude.sc.gov.br/index.php?option=com_docman&task=doc_download&gid=2495&Itemid=85" target="_blank">&#60http://portalses.saude.sc.gov.br/index.php?option=com_docman&task=doc_download&gid=2495&Itemid=85&#62</a>. Acesso em: 29 abr. 2013.
<br><br>
