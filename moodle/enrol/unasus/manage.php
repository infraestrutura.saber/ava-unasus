<?php
/*
 * Created on 10/07/2012
 *
 gerenciar isncri��o
 */
 class enrol_unasus_manage {
 	public static $STATUS_ENROL_ACCEPT =1;
 	public static $STATUS_ENROL_NOT_ACCEPT =2;
 	public static $STATUS_ENROL_MEET_RULES_WAIT_MANUAL_ACCEPT =3;
 	public static $STATUS_ENROL_NOT_MEET_RULES_WAIT_MANUAL_ACCEPT =4;
 	
 	public static $STATUS_ENROL_TYPE_AUTOMATIC =1;
 	public static $STATUS_ENROL_TYPE_MANUAL =2;
	
 	
 	
 	//inserir regristro na base de dados. 
 	function save($dto) {
 		global $CFG;
 		return insert_record('enrol_unasus', $dto);
 	}
 
 //alteriar regristro na base de dados. 
 	function edit($dto) {
 		global $CFG;
 		return update_record('enrol_unasus', $dto);
 	}

 	
  //excluir regristro na base de dados. 
 function delete_by_id($id) {
 		global $CFG;
 		return delete_records_select('enrol_unasus', "id=$id");
 	}
  //Retora a quantidade de registro cadastrada na base de dados
 function count() {
 		global $CFG;
		$sql ="SELECT COUNT(id)  AS qreg FROM {$CFG->prefix}enrol_unasus ";
		$r=get_record_sql($sql);
		return $r->qreg;
 	}
 	function get_by_id($id) {
 		global $CFG;
		$sql ="SELECT id,userid,courseid,rolecontextid,time,timeupdate,status,statusinfo,statuscause,author,justify,description FROM {$CFG->prefix}enrol_unasus WHERE id=$id ";
		$r=get_record_sql($sql);
		return $r;
 	}
 	
  	function get_detail_by_id($id) {
 		global $CFG;
 		$sql ="SELECT er.id,er.userid, u.firstname,u.lastname,u.email,u.idnumber,er.courseid,c.fullname,c.shortname,er.time,er.timeupdate,er.status,er.statusinfo,er.statuscause, er.justify,er.description,er.rolecontextid,er.author FROM {$CFG->prefix}enrol_unasus er INNER JOIN {$CFG->prefix}user u ON u.id=er.userid INNER JOIN {$CFG->prefix}course c ON er.courseid=c.id  WHERE  er.id=$id";
		$r=get_record_sql($sql);
		return $r;
 	}	
 	
 	function get_all() {
 		global $CFG;
		$sql ="SELECT id,enrolid,type,value  FROM {$CFG->prefix}enrol_unasus ";
		$r=get_records_sql($sql);
		return $r;
 	}
 	
 	function search_count($param){
				global $CFG;
				
				$wsql=$this->search_param($param);
			$sql ="SELECT COUNT(er.id) AS countrows FROM {$CFG->prefix}enrol_unasus er INNER JOIN {$CFG->prefix}user u ON u.id=er.userid INNER JOIN {$CFG->prefix}course c ON er.courseid=c.id  WHERE  er.id>0 $wsql ";
			$resp=get_record_sql($sql);
			return $resp->countrows;
    	}
 	function search($param){
				global $CFG;
				
				$wsql=$this->search_param($param);
			$sql ="SELECT er.id,er.userid, u.firstname,u.lastname,er.courseid,c.fullname,er.time,er.timeupdate,er.status,er.statusinfo,er.statuscause, er.justify,er.description FROM {$CFG->prefix}enrol_unasus er INNER JOIN {$CFG->prefix}user u ON u.id=er.userid INNER JOIN {$CFG->prefix}course c ON er.courseid=c.id  WHERE  er.id>0 $wsql ORDER BY er.time DESC";
			//echo $sql;
			if($param->paginar){$r= get_records_sql($sql,$param->page*$param->perpage, $param->perpage);}
			else{$r= get_records_sql($sql);}
			return $r;
    	}
 function search_param($param){
    		$wsql="";
    		if(!empty($param->courseid)){
    			$wsql.= "AND er.courseid= $param->courseid ";
    		}
    		
    	if(!empty($param->userid)){
    			$wsql.= "AND er.userid= $param->userid ";
    		}
    	if(!empty($param->userid_sql_profile)){
    			$wsql.= " $param->userid_sql_profile ";
    		}
    	if(!empty($param->username)){
    			$wsql.= "AND  (u.firstname LIKE '%".$param->username."%' OR u.lastname LIKE '%".$param->username."%') ";
    		}
    	
    	if(!empty($param->status)){
    			$wsql.= "AND er.status= $param->status ";
    		}
    	if(!empty($param->statusinfo)){
    			$wsql.= "AND er.statusinfo= $param->statusinfo ";
    		}
    	if(!empty($param->statuscause)){
    			$wsql.= "AND er.statuscause= $param->statuscause";
    		}
    	if(!empty($param->role)){
    			$wsql.= "AND er.role= $param->role ";
    	}
    	
    	return $wsql;
    	}
    	
    	function get_last_cron_exec($contextid){
    		global $CFG;
    		$action="cron_".$contextid;
    		$sql ="SELECT MAX(time) AS lasttime FROM {$CFG->prefix}log WHERE  module='unasus' AND action='".$action."' " ;
    		$r=get_record_sql($sql);
			return $r->lasttime;
    	}	
    	
    	function get_count_hour_last_exec_cron($contextid){
    		
    		$time_last_exec=$this->get_last_cron_exec($contextid);
    		
    		if(!empty($time_last_exec) && $time_last_exec>0){
    			$diff_time=time()-$time_last_exec;
    			$hour=$diff_time/60/60;
    			
    			
    		}else return -1;	
    		
    		return $hour;
    	}	
    	
    	function get_cpf($userid){
			global $CFG;
			global $USER;
			$sql ="SELECT idnumber	 FROM {$CFG->prefix}user WHERE id=$userid";
			 $r=get_record_sql($sql);
    		if(empty($r->idnumber)){
    			$sql ="SELECT username	 FROM {$CFG->prefix}user WHERE id=$userid";
    			 $r=get_record_sql($sql);
    			 return $r->username;
    		}else{return $r->idnumber;}
			
		}
		
		function exist_enrol_course($userid,$roleid,$contextid){
	global $CFG;
	$sql="SELECT COUNT(id) AS qreg FROM {$CFG->prefix}role_assignments WHERE contextid=$contextid AND userid=$userid AND roleid=$roleid ";
	$r=get_record_sql($sql);
	return $r->qreg;
}
function save_enrol_course($userid,$courseid){
	global $USER;
	global $CFG;
	
	$sql="SELECT id FROM {$CFG->prefix}context WHERE contextlevel=50 AND instanceid=$courseid";
	$context=get_record_sql($sql);
	
	$sql="SELECT defaultrole FROM {$CFG->prefix}course WHERE id=$courseid";
	$coursedata=get_record_sql($sql);
	if($coursedata->defaultrole==0){$coursedata->defaultrole=5;}
	
	$exist_enrol=$this->exist_enrol_course($userid,$coursedata->defaultrole,$context->id);
	if($exist_enrol)return 0;
	if(!$exist_enrol){
		$role_assignment=  new object();
		$role_assignment->roleid=$coursedata->defaultrole;
		$role_assignment->contextid=$context->id;
		$role_assignment->userid=$userid;
		$role_assignment->timestart=time();
		$role_assignment->enrol='unasus';
		$timevalidate=$this->get_validate_enrol_course($courseid);
		if(!empty($timevalidate)&& $timevalidate>0){
			$role_assignment->timeend =time()+$timevalidate;
		}
		
		return insert_record('role_assignments', $role_assignment);
	}
	
}
function get_validate_enrol_course($courseid){
			global $CFG;
			$sql ="SELECT enrolperiod FROM {$CFG->prefix}course   WHERE id=$courseid";
			$r=get_record_sql($sql);
			return $r->enrolperiod;
    	}  
function save_enrol_course_manual($courseid,$userid){
	
	global $CFG;
	
	$sql="SELECT id FROM {$CFG->prefix}context WHERE contextlevel=50 AND instanceid=$courseid";
	$context=get_record_sql($sql);
	
	$sql="SELECT defaultrole FROM {$CFG->prefix}course WHERE id=$courseid";
	$coursedata=get_record_sql($sql);
	if($coursedata->defaultrole==0){$coursedata->defaultrole=5;}
	
	$exist_enrol=$this->exist_enrol_course($userid,$coursedata->defaultrole,$context->id);
	if(!$exist_enrol){
		$role_assignment=  new object();
		$role_assignment->roleid=$coursedata->defaultrole;
		$role_assignment->contextid=$context->id;
		$role_assignment->userid=$userid;
		$role_assignment->timestart=time();
		$role_assignment->enrol='manual';
		
		$timevalidate=$this->get_validate_enrol_course($courseid);
		if(!empty($timevalidate)&& $timevalidate>0){
			$role_assignment->timeend =time()+$timevalidate;
		}
		return insert_record('role_assignments', $role_assignment);
	}else return 0;
	
	
}
function update_enrol_manual_status($id,$status,$justify){
	global $USER;

	$enrol=  new object();
	$enrol->id=$id;
	$enrol->timeupdate=time();
	$enrol->status=self::$STATUS_ENROL_ACCEPT;
	$enrol->statuscause=NULL;
	$enrol->statusinfo=self::$STATUS_ENROL_TYPE_MANUAL;
	$enrol->justify=$justify;
	$enrol->author=$USER->id;
	
	return update_record('enrol_unasus', $enrol);
}

function exist_enrol_by_status($userid,$courseid,$status){
	global $CFG;
	$sql="SELECT COUNT(id) AS qreg FROM  {$CFG->prefix}enrol_unasus WHERE courseid=$courseid AND userid=$userid AND status=$status";
	
	$r=get_record_sql($sql);
	return $r->qreg;
} 
function get_course_category($courseid){
			global $CFG;
			$sql ="SELECT category FROM {$CFG->prefix}course   WHERE id=$courseid";
			$r=get_record_sql($sql);
			return $r->category;
    	} 

function get_context_level_decreasing($courseid){
	 $context_decrease = array();
	 //contexto do curso
	 $contex=get_context_instance(CONTEXT_COURSE,$courseid);
	 array_push($context_decrease,$contex->id);
	 
	 //contexto da categoria do curso
	 $category_course=$this->get_course_category($courseid);
	 $contex= get_context_instance(CONTEXT_COURSECAT,$category_course);
	  array_push($context_decrease,$contex->id);
	 
	 //extrair contexto da categoria pai
	 $displaylist = array();
    $parentlist = array();
    make_categories_list($displaylist, $parentlist);
    $cats=$parentlist[$category_course];
	$total_elem=count($cats)-1;
	$category_father=null;
	
	for($i=$total_elem; $i >= 0; $i--){
		$category_father=null;
    	$category_father=$cats[$i];
    	 $contex= get_context_instance(CONTEXT_COURSECAT,$category_father);
    	array_push($context_decrease,$contex->id);
    	}
	  //extrair contexto do sistema
	  $contex= get_context_instance(CONTEXT_SYSTEM);
     array_push($context_decrease,$contex->id);
	 return $context_decrease;
	}
	
	function get_rolecontextid_by_course($courseid){
			global $CFG;
			$sql ="SELECT rolecontextid FROM {$CFG->prefix}enrol_unasus  WHERE courseid=$courseid";
			$rows=get_records_sql($sql,0,1);
			if(empty($rows)) return null;
			foreach ($rows as $row){
				if(!empty($row->rolecontextid)) return $row->rolecontextid;
			}
				
			
    	} 
    	 function count_search_by_course($param){
			global $CFG;
			$wsql=$this->search_by_course_param($param);
    		$sql ="SELECT COUNT(DISTINCT courseid) AS qreg FROM {$CFG->prefix}enrol_unasus  WHERE  id > 0  $wsql ";
			$r=get_record_sql($sql);
			return $r->qreg;
		} 
    function search_by_course_user($param){
			global $CFG;
			
			$wsql=$this->search_by_course_param($param);
    		
			$sql ="SELECT courseid, COUNT(userid) AS userid  FROM {$CFG->prefix}enrol_unasus  WHERE  id > 0  $wsql GROUP BY courseid ORDER BY COUNT(userid) DESC ";
			return get_records_sql($sql);
				
			
    	} 
    	 function search_by_course_status($param,$status){
			global $CFG;
			$param->status=$status;
			$wsql=$this->search_by_course_param($param);
    		
			$sql ="SELECT courseid, COUNT(status) AS status   FROM {$CFG->prefix}enrol_unasus  WHERE  id > 0  $wsql GROUP BY courseid";
			$rows =get_records_sql($sql);
			$result=array();
			if(empty($rows)) return $result;
			foreach ($rows as $row) {
				$result[$row->courseid]=$row->status;
			}
			 return $result;
    	} 
    	 function search_by_course_param($param){
			
			$wsql="";
    		if(!empty($param->userid_sql_profile)){
    			$wsql.= " $param->userid_sql_profile ";
    		}
    		if(!empty($param->status)){
    			$wsql.= "AND status= $param->status ";
    		}
    		if(!empty($param->statusinfo)){
    			$wsql.= "AND statusinfo= $param->statusinfo ";
    		}
    		if(!empty($param->statuscause)){
    			$wsql.= "AND statuscause= $param->statuscause";
    		}
    		
			return $wsql;
				
			
    	} 
    	//retorna um array com nome do curso
    	function get_sql_in_course_by_result($resul_rows){
    		$cont=0;
    		$result="";
    		if(empty($resul_rows)) return $result;
    		 foreach ($resul_rows as $v){ 
    		 	$cont++;
    		 	if($cont==1){$result=$v->courseid;}
    		 	else{$result.=",".$v->courseid;}
    		 	
    		 }
    		 return $result;
    	}
    	function get_course_name($resul_rows){
    		global $CFG;
    		$wsql="";
    		$sql_in=$this->get_sql_in_course_by_result($resul_rows);
    		$couses =array();
    		if(!empty($sql_in)){
    			$wsql=" id IN ($sql_in) ";
    			$sql ="SELECT id,fullname FROM {$CFG->prefix}course  WHERE  $wsql ";
				$rows= get_records_sql($sql);
				if(empty($rows)) return $couses;
				foreach ($rows as $row) {
					$couses[$row->id] =$row->fullname;
    			}
    		}
			return $couses;
    	}
    	
    	function count_search_by_cbo($param){
			global $CFG;
			
			$wsql=$this->search_by_cbo_param($param);
    		$sql ="SELECT COUNT(DISTINCT d.value ) AS qreg FROM {$CFG->prefix}enrol_unasus er INNER JOIN {$CFG->prefix}enrol_unasus_role_data d ON er.id = d.enrolid WHERE d.id >0 $wsql";
    		$r=get_record_sql($sql);
			return $r->qreg; 
				
			
    	}
    	
    	function search_by_cbo($param){
			global $CFG;
			
			$wsql=$this->search_by_cbo_param($param);
    		$sql ="SELECT d.value, COUNT( d.value ) AS qvalue FROM {$CFG->prefix}enrol_unasus er INNER JOIN {$CFG->prefix}enrol_unasus_role_data d ON er.id = d.enrolid WHERE d.id >0  $wsql GROUP BY d.value ORDER BY COUNT( d.value ) DESC ";
			return get_records_sql($sql);
				
			
    	}
    	
    	function search_by_cbo_param($param){
			$wsql="";
    		if(!empty($param->courseid)){
    			$wsql.= "AND er.courseid= $param->courseid ";
    		}
    		
    	if(!empty($param->userid)){
    			$wsql.= "AND er.userid= $param->userid ";
    		}
    	if(!empty($param->userid_sql_profile)){
    			$wsql.= " $param->userid_sql_profile ";
    		}
    	
    	if(!empty($param->status)){
    			$wsql.= "AND er.status= $param->status ";
    		}
    	if(!empty($param->statusinfo)){
    			$wsql.= "AND er.statusinfo= $param->statusinfo ";
    		}
    	if(!empty($param->statuscause)){
    			$wsql.= "AND er.statuscause= $param->statuscause";
    		}
    	
    		return $wsql;
				
			
    	}
	//SELECT courseid, COUNT(status) AS status,COUNT(statusinfo) AS statusinfo,COUNT(statuscause) as statuscause  FROM mdl_enrol_unasus  WHERE  id>0 GROUP BY courseid
	//SELECT d.value, COUNT( d.value ) AS qvalue FROM mdl_enrol_unasus e INNER JOIN mdl_enrol_unasus_role_data d ON e.id = d.enrolid WHERE d.id >0 GROUP BY d.value 
 }