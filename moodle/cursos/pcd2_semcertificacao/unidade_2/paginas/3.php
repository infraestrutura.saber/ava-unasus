<?php
               
    $pg_atual=3;
    registro($id,unid3_pg,$pg_atual,unid3_ev);
?>

<!-- colorbox exercicio2 -->
<div style='display:none'>
			<div id="caca_palavras" style='padding:10px; background:#fff;'>

			<div id="texto_color2"></div>
			<br><br>
			<a class="btn" href="index.php?pagina=3">Tentar novamente</a>
			<a class="btn" href="index.php?pagina=0">Estudar mais</a>
			<a class="inline2 btn" href="#feedback2">Mostrar respostas</a>
		
			<br><br>
			
			</div>
		</div>
<!-- fim colorbox exercicio1-->
	 
	<div style='display:none'>
		<div id="feedback2">
				
		<h4>Respostas</h4>
		<table>
			<tr>
				<td ><img style="border-radius: 10px;" style="text-align:center;" src="images/cacapalavrasnovo_resp.jpg"><br></td>
				<td style="padding: 10px;"><br>
		1.ILUMINAÇÃO  <br>
		2.VENTILADOS <br>
		3.MOBÍLIAS <br>
		4.CORRIMÃOS <br>
		</td>
			</tr>
		</table>
		<br><br>
		<style type="text/css">
			.inform{width: 500px;}
		</style>
		
		
			<a class="inline2 btn" href="#caca_palavras">Voltar</a>
		</div>
	</div>
<!-- colorbox exercicio2 final -->

   
	  
   <h4 class="titulo">Exercício</h4>
   
   <div id="ex1">
   <h5>Atividade 1</h5>
   Leia as frases apresentadas a seguir. Com o auxílio do caça-palavras, identifique a palavra que completa cada lacuna. <strong>A palavra deverá ser digitada na lacuna.</strong> Quando preencher todas as lacunas, clique em corrigir. Ao digitar as palavras, as regras da norma culta da língua portuguesa devem ser respeitadas.<br><br>
<b>1.</b> Uma <input type="text" id="p1"> adequada garante que os procedimentos sejam realizados com boa visibilidade e segurança. <br><br>

<b>2.</b> É importante observar se os ambientes são bem <input type="text" id="p2">, pois isso ajuda a prevenir doenças causadas por bactérias e fungos, principalmente. <br><br>

<b>3.</b> A disposição adequada das <input type="text" id="p3"> pode evitar acidentes e quedas, além de facilitar o uso de determinados equipamentos, como os de oxigenoterapia. <br><br>

<b>4.</b> É importante conversar com a família sobre adaptações a serem realizadas para auxiliar na mobilidade do paciente domiciliado. O uso de  <input type="text" id="p4"> e barras auxiliam na deambulação segura do paciente dentro dos cômodos do domicílio. Outras adaptações que podem ser sugeridas são a retirada de tapetes, e a construção de rampas de acesso. <br><br>


<img style="border-radius: 10px; margin-top: 19px;" src="images/cacapalavrasnovo.jpg" alt="Caça-Palavras">
 <button class="btn inline2" href="#caca_palavras" style="float: right;margin-right: 45px; margin-top: 70px;" type="button" onClick="respostasNovo();">Corrigir</button>


		
   
   
   
   </div>
   

   <style type="text/css">
   #legenda1 ul li, #legenda2 ul li,#legenda3 ul li,#legenda4 ul li {
		 list-style-type: disc;   
         margin-left: 10px;		
   }
   
   #quadro{
		margin-top: 25px;
		height: 282px;
		display: block;
   }
   
   #quadro img{
		width: 45%;
		cursor: pointer;
   }
   
   
   #feedback1, #feedback2{
			
			
	}
		
	#ex1 select{
		width: 65px;
	}
	
	#ex1{
		position: relative;
	}
	
	#ex2{
			margin-top: 60px;
			text-align: justify;
	}
   
   #legenda1,#legenda2,#legenda3,#legenda4{
		width: 200px;
		height: 300px;
		background-color: rgba(255, 255, 255, 0.88);
		display: none;
		position: absolute;
		top: 150;
		left: 330;
		border-radius: 10px;
		font-weight: bold;
	
   }
   
   #legenda2{
		top: 150;
		left: 690;
   }
   
    #legenda3{
		top: 445;
		left: 330;
   }
   
    #legenda4{
		top: 445;
		left: 690;
   }
    
    #legenda1 ul, #legenda2 ul,#legenda3 ul,#legenda4 ul{
		margin: 15px;
	
   }
   
   input[type="text"]{
		margin-top: 8px;
		height: 26px;
		width: 116px;
   }
   
   
   </style>
 
   
   <script type="text/javascript">
	 var user = $('#usuario b').html();


	function mostrarFeedback(id){
		document.getElementById("feedback" + id).style.display="block";
	}
   
   function respostasNovo(){
   
		var r1 = document.getElementById('p1').value;
		var r2 = document.getElementById('p2').value;
		var r3 = document.getElementById('p3').value;
		var r4 = document.getElementById('p4').value;

		
		
		if(r1 == "" || r2 == "" || r3 == "" || r4 == "" ){
			 $("#caca_palavras").html("<h4>Atenção!</h4> <br>Preencha todas as lacunas antes de clicar em corrigir.");
         	
			
		
		}else if( r1.toLowerCase() == "iluminação" && r2.toLowerCase() == "ventilados" && (r3.toLowerCase() == "mobílias" || r3.toLowerCase() == "mobilias") && r4.toLowerCase() == "corrimãos"){
			$(".inline2").colorbox({inline:true, width:"80%",onClosed:function(){ document.getElementById("feedback" + id).style.display="block"; }});
			$("#texto_color2").html("<h4>Resultado</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, você preencheu as lacunas corretamente!");
			$.post( "../apresentacao/functions/chamaExCerto.php", { exercicio: 14}, function() {
						
				});
			mostrarFeedback(2);
			
		}else{
			$(".inline2").colorbox({inline:true, width:"80%",onClosed:function(){ document.getElementById("feedback" + id).style.display="block"; }});
			$("#texto_color2").html("<h4>Resultado</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, você não preencheu as lacunas corretamente.");
			$.post( "../apresentacao/functions/chamaExErrado.php", { exercicio: 14}, function() {
						
				});
			mostrarFeedback(2);
		}		
		$(".inline2").colorbox({inline:true, width:"80%",onClosed:function(){ location.reload();}});
   }
   
   
   

   
   </script>
   
	<?php
	if ($_GET['ex']==1){ 

	}
	if ($_GET['ex']==2){ 

		echo '<script type="text/javascript">$("html,body").animate({scrollTop: $("#ex2").offset().top}, 1500);</script>';
	}
?>
   


