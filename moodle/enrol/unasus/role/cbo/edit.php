<?php

   require("../../../../config.php");
   require("$CFG->dirroot/enrol/unasus/lib/context.php");
   require("$CFG->dirroot/enrol/unasus/role/lib.php");
   require("$CFG->dirroot/enrol/unasus/role/cbo/form_edit.php");

require_login();
   //permiss�o
   require_capability('enrol/unasus:roleedit', get_context_instance(CONTEXT_SYSTEM), NULL, false);
    add_to_log(1, 'enrol_unasus', 'roleedit', "enrol/unasus/role/edit.php?contextid=$contextid", getremoteaddr());    
      
     $role =new role();
     $param=  new object();
    $param->contextid= required_param('contextid',PARAM_INT);
    $dto=$role->get_by_contextid($param->contextid);
   
    $form= new form_edit(); 
    
     $context =new context();
     $context_name=$context->get_name($param->contextid); 
  
   if ($form->is_cancelled()) {
        redirect($CFG->httpswwwroot.'/enrol/unasus/role/cbo/index.php') ;

    } else if ($formdata = $form->get_data()) {
    	$role->edit($formdata);
    	redirect($CFG->httpswwwroot.'/enrol/unasus/role/cbo/index.php') ;
     }
   /*
    $param->op= optional_param('op',0,PARAM_INT);
       if($param->op){
    	$param->id= required_param('id',PARAM_TEXT);
    	$param->title= required_param('title',PARAM_TEXT);
     	$param->value= required_param('value',PARAM_TEXT);
     	$param->description= optional_param('description',NULL,PARAM_TEXT);
     
   	 
   		echo "contextid: $param->contextid <br>";
    	echo "presentation: $param->presentation <br>";
    	echo "approval: $param->approval <br>";
    	echo "disapproval: $param->disapproval <br>";
    	echo "autreviewapproval: $param->autreviewapproval <br>";
    	echo "manualreviewapproval: $param->manualreviewapproval <br>";
    	echo "pendingapprovalmanager: ".$param->pendingapprovalmanager."<br>";
    
    	
		$role->edit($param);
		//$_SESSION['row_save_sucess']=get_string('row_save_sucess','enrol_unasus');
		header('Location: '.$CFG->wwwroot.'/enrol/unasus/role/cbo/index.php') ;
		exit;
    }
    */
   
     //Navega��o
   $navigation = build_navigation(array(array('name' => get_string('enrolname','enrol_unasus'), 'link' => "$CFG->wwwroot/enrol/unasus/index.php", 'type' => 'misc'),
                                     array('name' => get_string('role_manege','enrol_unasus'), 'link' => "$CFG->wwwroot/enrol/unasus/role/cbo/index.php", 'type' => 'misc'), array('name' => $context_name, 'link' => null, 'type' => 'misc'), array('name' => get_string('role_edit','enrol_unasus'), 'link' => null, 'type' => 'misc')));
     

     print_header(get_string('enrolname','enrol_unasus'),get_string('applicationsenrolment','enrol_unasusem'), $navigation);
   
 	
    $form->display();
  
    print_footer();
?>