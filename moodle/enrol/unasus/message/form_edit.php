<?php 

require_once $CFG->libdir.'/formslib.php';

class form_edit extends moodleform {

    function definition() {
    	global $dto;
    	
        $mform    =& $this->_form;
        $renderer =& $mform->defaultRenderer();

        $mform->addElement('header', '', get_string('message_edit','enrol_unasus'), '');
        
       
          $mform->addElement('hidden', 'id',  $dto->id);
        $mform->setType('id', PARAM_INT);
        
         $mform->addElement('hidden', 'contextid', $dto->contextid);
        $mform->setType('contextid', PARAM_INT);
        
        
         $mform->addElement('htmleditor', 'presentation', get_string('message_presentation','enrol_unasus'),'wrap="virtual" rows="7" cols="40"');
        $mform->setType('presentation', PARAM_TEXT);
        $mform->setDefault('presentation', $dto->presentation);
       $mform->setHelpButton('presentation', array('message_presentation', get_string('message_presentation', 'enrol_unasus'), 'enrol_unasus')); 
        	
         $mform->addElement('htmleditor', 'approval', get_string('message_approval','enrol_unasus'),'wrap="virtual" rows="7" cols="40"');
        $mform->setType('approval', PARAM_TEXT);
        $mform->setDefault('approval', $dto->approval);
        $mform->setHelpButton('approval', array('message_approval', get_string('message_approval', 'enrol_unasus'), 'enrol_unasus'));
        
         $mform->addElement('htmleditor', 'disapproval', get_string('message_disapproval','enrol_unasus'),'wrap="virtual" rows="7" cols="40"');
        $mform->setType('disapproval', PARAM_TEXT);
        $mform->setDefault('disapproval', $dto->disapproval);
        $mform->setHelpButton('disapproval', array('message_disapproval', get_string('message_disapproval', 'enrol_unasus'), 'enrol_unasus'));
        
         $mform->addElement('htmleditor', 'autreviewapproval', get_string('message_autreview','enrol_unasus'),'wrap="virtual" rows="7" cols="40"');
        $mform->setType('autreviewapproval', PARAM_TEXT);
         $mform->setDefault('autreviewapproval', $dto->autreviewapproval);
         $mform->setHelpButton('autreviewapproval', array('message_autreviewapproval', get_string('message_autreview', 'enrol_unasus'), 'enrol_unasus'));
         
         $mform->addElement('htmleditor', 'manualreviewapproval', get_string('message_manualreview','enrol_unasus'),'wrap="virtual" rows="7" cols="40"');
        $mform->setType('manualreviewapproval', PARAM_TEXT);
         $mform->setDefault('manualreviewapproval', $dto->manualreviewapproval);
         $mform->setHelpButton('manualreviewapproval', array('message_manualreviewapproval', get_string('message_manualreview', 'enrol_unasus'), 'enrol_unasus'));
         
       //  $mform->addElement('htmleditor', 'pendingapprovalmanager', get_string('message_pendingapprovalmanager','enrol_unasus'),'wrap="virtual" rows="7" cols="40"');
       // $mform->setType('pendingapprovalmanager', PARAM_TEXT);
     //   $mform->setDefault('pendingapprovalmanager', $dto->pendingapprovalmanager);
        
       
        $this->add_action_buttons(true,get_string('edit','enrol_unasus'));
    }

    function validation($data, $files) {
        global $CFG;

         $errors = parent::validation($data, $files);
        
      
     return $errors;
    }

}

?>