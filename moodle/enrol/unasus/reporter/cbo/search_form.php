<?php 

require_once $CFG->libdir.'/formslib.php';

class cbo_search_form extends moodleform {

    function definition() {
    	$util = new enrol_unasus_report_util();
    
    	global $param;
 		$mform    =& $this->_form;
        $renderer =& $mform->defaultRenderer();

        $mform->addElement('header', '',get_string('filter_enrol_data','enrol_unasus'), '');
        
      	
        $mform->addElement('select', 'courseid',get_string('course','enrol_unasus'),$util->get_courses_enronl_config_html_select());
        $mform->setType('courseid', PARAM_INT);
          $mform->setDefault('courseid', $param->courseid);
        
         $mform->addElement('select', 'userid', get_string('user','enrol_unasus'),$util->get_user_html_select());
        $mform->setType('userid', PARAM_INT);
          $mform->setDefault('userid', $param->userid);
        
       /* $mform->addElement('text', 'username', get_string('user','enrol_unasus'),'size="20"');
        $mform->setType('username', PARAM_TEXT);
        $mform->setDefault('username', $param->username);*/
     
        $mform->addElement('select', 'status', get_string('status','enrol_unasus'),$util->get_status_domain_table());
        $mform->setType('status', PARAM_INT);
          $mform->setDefault('status', $param->status);

        $mform->addElement('select', 'statusinfo', get_string('type','enrol_unasus'),$util->get_status_type_domain_table());
        $mform->setType('statusinfo', PARAM_INT);
          $mform->setDefault('statusinfo', $param->statusinfo);
        
    
        
        $mform->addElement('header', 'filter_profile_data',get_string('filter_profile_data','enrol_unasus'), '');
         $mform->setAdvanced('filter_profile_data');
         
        global $profile,$fields,$seleted_fields_filter;
    	
     foreach ($fields as $field){
    	if(isset($seleted_fields_filter[$field->id])) {
    		if($field->datatype=='menu'){
    			$mform->addElement('select', 'F_'.$field->id,$field->name,$profile->convert_menu_feild_data_to_array_for_form($field->param1));
        		$mform->setType('F_'.$field->id, PARAM_TEXT);
        		$mform->setDefault('F_'.$field->id,optional_param('F_'.$field->id,NULL,PARAM_TEXT));
    		}else{
    			 $mform->addElement('text', 'F_'.$field->id, $field->name);
    			 $mform->setType('F_'.$field->id, PARAM_TEXT);
    			 $mform->setDefault('F_'.$field->id,optional_param('F_'.$field->id,NULL,PARAM_TEXT));
    		}
         	 
    	}
      }	
      
      if(!empty($param->courseid)){
      	global $profilefield_field,$configcontext,  $manage;
      		$options_yes_not=$profilefield_field->get_general_html_option_yes_not();
      	$rolecontextid=  $manage->get_rolecontextid_by_course($param->courseid);
      	if(!empty($rolecontextid)){
      			$fields_enrolf= $profilefield_field->get_by_contextid($rolecontextid,1);
      	
      	 		$value_filter=$configcontext->get_value_by_contextid($rolecontextid,configcontext::$USER_PROFILE_ENROL_FILTER);
      	 		$seleted_fields_filter_enrolf=$profile->get_profile_config_in_array('',$value_filter);
      	 
      	 		//$mform->addElement('header', 'filter_enrol_data',get_string('filter_enrol_data','enrol_unasus'), '');
         		//$mform->setAdvanced('filter_enrol_data');
         
        		 foreach ($fields_enrolf as $field){
    					if(isset($seleted_fields_filter_enrolf[$field->id])) {
    						if($field->datatype=='menu'){
    							$mform->addElement('select', 'FE_'.$field->id,$field->name,$profile->convert_menu_feild_data_to_array_for_form($field->param1));
        						$mform->setType('FE_'.$field->id, PARAM_TEXT);
        						$mform->setDefault('FE_'.$field->id,optional_param('FE_'.$field->id,NULL,PARAM_TEXT));
    						}else if($field->datatype=='checkbox'){
    								$mform->addElement('select', 'FE_'.$field->id,$field->name,$options_yes_not);
        							$mform->setType('FE_'.$field->id, PARAM_TEXT);
        							$mform->setDefault('FE_'.$field->id,optional_param('FE_'.$field->id,NULL,PARAM_TEXT));
    						}else{
    							 $mform->addElement('text', 'FE_'.$field->id, $field->name);
    				 			$mform->setType('FE_'.$field->id, PARAM_TEXT);
    				 			$mform->setDefault('FE_'.$field->id,optional_param('FE_'.$field->id,NULL,PARAM_TEXT));
    						}
         	 
    					}
     	 			}	
      	}//if(!empty($rolecontextid)){
      	
      }// fim  if(!empty($param->courseid)){
      
    
        //data
        $this->add_action_buttons(true,get_string('research','enrol_unasus'));
    }

    function validation($data, $files) {
        global $CFG;

         $errors = parent::validation($data, $files);
         
      
     return $errors;
    }

}

?>