<?php
/*
 * Created on 24/01/2013
 *
 *Essa class tem as bibliotecas de gerenciamento
 * da configuração do curso. 
 */
 class course_sync_config{
 	
 	public static $TAG_USUARIO_NOME ="{USUARIO_NOME}";
 	public static $TAG_USUARIO_NOME_COMPLETO ="{USUARIO_NOME_COMPLETO}";
 	public static $TAG_CURSO_NOME ="{CURSO_NOME}";
 	public static $TAG_CURSO_ABREV ="{CURSO_ABREV}";
 	public static $TAG_CURSO_URL ="{CURSO_URL}";
 	public static $TAG_CERTIFICADO_URL ="{CERTIFICADO_URL}";
 	
 	//inserir regristro na base de dados. 
 	function save($dto) {
 		global $CFG;
                $dto->gradeapproved=$this->parse_to_double($dto->gradeapproved);
 		return insert_record('synciccarouca_course_config', $dto);
 	}
 
 //alteriar regristro na base de dados. 
 	function edit($dto) {
 		global $CFG;
                 $dto->gradeapproved=$this->parse_to_double($dto->gradeapproved);
 		return update_record('synciccarouca_course_config', $dto);
 	}

  //excluir regristro na base de dados. 
 function delete_by_id($id) {
 		global $CFG;
 		return delete_records_select('synciccarouca_course_config', "id=$id");
 	}
  //Retora a quantidade de registro cadastrada na base de dados
 function count() {
 		global $CFG;
		$sql ="SELECT COUNT(id)  AS qreg FROM {$CFG->prefix}synciccarouca_course_config ";
		$r=get_record_sql($sql);
		return $r->qreg;
 	}
 	function get_by_id($id) {
 		global $CFG;
		$sql ="SELECT id,mdlcourseid,arcidcurso,arcidoferta,concluinteferta,gradeapproved,timeupdate,messagemailcert,subjectmailcert  FROM {$CFG->prefix}synciccarouca_course_config WHERE id=$id";
		$r=get_record_sql($sql);
		return $r;
 	}
        function get_menssage($mdlcourseid) {
 		global $CFG;
		$sql ="SELECT messagemailcert,subjectmailcert  FROM {$CFG->prefix}synciccarouca_course_config WHERE mdlcourseid=$mdlcourseid";
		$r=get_record_sql($sql);
		return $r;
 	}
 	function get_all() {
 		global $CFG;
		$sql ="SELECT id,mdlcourseid,arcidcurso,arcidoferta,concluinteferta,gradeapproved,timeupdate,messagemailcert,subjectmailcert  FROM {$CFG->prefix}synciccarouca_course_config ";
		$r=get_records_sql($sql);
		return $r;
 	}
 	
        function search() {
 		global $CFG;
		$sql ="SELECT s.id,s.mdlcourseid,c.fullname,s.arcidcurso,s.arcidoferta,concluinteferta,s.gradeapproved,s.timeupdate,s.messagemailcert,s.subjectmailcert  FROM {$CFG->prefix}synciccarouca_course_config s INNER JOIN {$CFG->prefix}course c ON s.mdlcourseid=c.id ";
		$r=get_records_sql($sql);
		return $r;
 	}
 	 function exist_create($mdlcourseid) {
 		global $CFG;
		$sql ="SELECT COUNT(id)  AS qreg FROM {$CFG->prefix}synciccarouca_course_config WHERE mdlcourseid=".$mdlcourseid;
		$r=get_record_sql($sql);
		if($r->qreg>0) return true;
		else  return false;
	}
	 function exist_edit($id,$mdlcourseid) {
 		global $CFG;
		$sql ="SELECT COUNT(id)  AS qreg FROM {$CFG->prefix}synciccarouca_course_config WHERE id!=$id AND mdlcourseid=".$mdlcourseid;
                echo $sql;
		$r=get_record_sql($sql);
		if($r->qreg>0) return true;
		else  return false;
	}
 	
function certificate_send_mail_text_format ($dto) {
    if(!empty($dto->subjectmailcert) || !empty($dto->messagemailcert) ){
        $text="<b>".get_string('sujectcoursesync','report_synciccarouca')."</b>";
        $text.=" ".$dto->subjectmailcert."<br /><br />";
         $text.="<b>".get_string('messagemailcoursesync','report_synciccarouca')."</b>";
        $text.=" ".$dto->messagemailcert."<br /><br />";
        return $text;
    }
        return null;
    }		
function  get_course_name($id){
    global $CFG;
    $sql ="SELECT fullname FROM {$CFG->prefix}course WHERE id=$id";
   $r= get_record_sql($sql);
  
   return $r->fullname;
}

function fill_tag($messge,$course='',$user='',$urlcert='') {
 		global $CFG;
 		if(!empty($course)){
                    $url_curse=$CFG->httpswwwroot."/course/view.php?id=$course->id";
                    $messge=str_replace(self::$TAG_CURSO_URL, $url_curse, $messge);
                    $messge=str_replace(self::$TAG_CURSO_NOME, $course->fullname, $messge);
                    $messge=str_replace(self::$TAG_CURSO_ABREV, $course->shortname, $messge);
                }
                if(!empty($user)){
                     $messge=str_replace(self::$TAG_USUARIO_NOME, $user->firstname, $messge);
                    $nome_completo=$user->firstname;
                    if(!empty($user->lastname)){$nome_completo.=" ".$user->lastname;}
                    $messge=str_replace(self::$TAG_USUARIO_NOME_COMPLETO, $nome_completo, $messge);
               }
		
		 if(!empty($urlcert)){
                     $messge=str_replace(self::$TAG_CERTIFICADO_URL, $urlcert, $messge);
                 }
		return $messge;
	}
        
        public function parse_to_double($param){
 		 //$param=str_replace(".", "", $param) ;
 		$v=(double)str_replace(",", ".", $param);
 		return $v;
 	}
        
        public function option_yes_not(){
 		$op=array();
                $op[0]=get_string('no','report_synciccarouca');
                $op[1]=get_string('yes','report_synciccarouca');
                return $op;
 	}
        
        public function get_name_option_yes_not($param){
 		if(!empty($param)){
                    if($param==0)return get_string('no','report_synciccarouca');
                    else if($param==1)return get_string('yes','report_synciccarouca');
                }else return  get_string('no','report_synciccarouca');
               
 	}
 }