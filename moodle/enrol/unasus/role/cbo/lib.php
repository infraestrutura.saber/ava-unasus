<?php
/*
 * Created on 28/07/2012
 *
 *Essa class faz gerenciamento de CBO 
 */
 class unasus_cbo {
 	/*
 * Recebe CBO familia com 2231XX e retira XX e retorna 2231
 */
	function remove_family_prefix($cbo){
		return str_replace("XX", "", $cbo);
	
	}
	
	
	/*Recebe lista de CBO (separado por v�rgula) consultado no arouca e compara com a lista de CBO  (separado por v�rgula)  que s�o requisistos de inscri��o no curso
	 * e retorna lista de cbo que s�o requisitos de matr�cula que tem correspond�ncia na lista de cbo do arouca
 * 
 */
function get_cbo_enrol($list_cbo_arouca,$list_cbo_enrol_required){
	//echo "list_arouca: <br>";
	//print_r($list_cbo_arouca);
	
	//echo "list_cbo_enrol_required: <br>";
	//print_r($list_cbo_enrol_required);
	$cbos = array();
	//conver $list_cbo_enrol_required para array
	if(!empty($list_cbo_enrol_required)){
		//se tiver mais de um valor
		if(strstr($list_cbo_enrol_required,",")){
			$cbos_req = explode(",",$list_cbo_enrol_required); 
			foreach ($cbos_req as $cbor){
				if(strstr($list_cbo_arouca,$this->remove_family_prefix($cbor))){
				array_push($cbos,$cbor);
				}
			}
		}
		//se tiver s� um valor
		else{
			
			if(strstr($list_cbo_arouca,$this->remove_family_prefix($list_cbo_enrol_required))){
				array_push($cbos,$list_cbo_enrol_required);
			}
		}
	}
	//echo "list_cbo_enrol: <br>";
	//print_r($cbos);
return 	$cbos;
}
//retorna um array com nome de cada cbo
function get_cbo_name($list_cod,$list_name){
	$cbo_name=array();
	
	if(empty($list_cod))return $cbo_name;
		
	if(empty($list_name))return $cbo_name;
		
	$cod=explode(",", $list_cod); 
	$name=explode(",",$list_name); 
	
	if(count($cod)!=count($name))return $cbo_name;
	
	$cont=0;
	foreach ($cod as $c){
		$cbo_name[$c]=$name[$cont];
		$cont++;
	}
		
	return $cbo_name;
}


 }