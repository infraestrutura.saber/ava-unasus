<?php
               
    $pg_atual=4;
    registro($id,unid8_pg,$pg_atual,unid8_ev);
?>
    <!-- colorbox exercicio1 -->
<div style='display:none'>
			<div id="content_color1" style='padding:10px; background:#fff;'>

			<div id="texto_color1"></div>
			<br><br>
			<a class="btn" href="index.php?pagina=4">Tentar novamente</a>
			<a class="btn" href="index.php?pagina=0">Estudar mais</a>
			<a class="inline btn" href="#feedback1">Mostrar respostas</a>
		
			<br><br>
			
			</div>
		</div><!-- fim colorbox exercicio1-->
	 
	<div style='display:none'>
		<div id="feedback1">
			<h4>Respostas</h4>
		a) <b>Errado</b> - Aparadeira ou comadre não fazem parte dos equipamentos necessários para a coleta de sangue.<br>
		b) <b>Errado</b> - Esfigmomanômetro e estetoscópio não fazem parte dos equipamentos necessários para a coleta de material biológico.<br>
		c) <b>Errado</b> - Conjunto de pinças cirúrgicas não faz parte dos equipamentos necessários para a coleta de material biológico no domicílio.<br>
		d) <b>Correto</b><br>
		
		
			<br><br>
			<a class="inline btn" href="#content_color1">Voltar</a>
		</div>
	</div>
<!-- colorbox exercicio1 final -->
<!-- colorbox exercicio2 -->
<div style='display:none'>
			<div id="content_color2" style='padding:10px; background:#fff;'>

			<div id="texto_color2"></div>
			<br><br>
			<a class="btn" href="index.php?pagina=4">Tentar novamente</a>
			<a class="btn" href="index.php?pagina=0">Estudar mais</a>
			<a class="inline2 btn" href="#feedback2">Mostrar respostas</a>
		
			<br><br>
			
			</div>
		</div>
<!-- fim colorbox exercicio1-->
	 
	<div style='display:none'>
		<div id="feedback2">
				
		<h4>Respostas</h4>
		<table>
			<tr>
				<td class="inform"><img style="border-radius: 10px;" style="text-align:center;" src="images/cacapalavras-solucao.jpg"><br></td>
				<td ><br>
		1.BIOSSEGURANÇA  <br>
		2.PROTEÇÃO <br>
		3.PERFUROCORTANTE <br>
		4.GARROTE <br>
		5.AGULHAS <br>
		6.GASOMETRIA <br>
		7.VERDE <br>
		8.CINZA <br><br></td>
			</tr>
		</table>
		<br><br>
		<style type="text/css">
			.inform{width: 500px;}
		</style>
		
		
			<a class="inline2 btn" href="#content_color2">Voltar</a>
		</div>
	</div>
<!-- colorbox exercicio2 final -->

   
	  
   <h4 class="titulo">Exercícios</h4>

<div id="ex1">
   <h5>Atividade 1<b><i class=" icon-hand-up" title="Essa atividade possui interação."></i></b></h5>
   
   Analise as opções a seguir e identifique o conjunto que melhor representa 
os equipamentos e acessórios necessários para a coleta e o transporte de 
material biológico <b>(apenas sangue)</b> no domicílio.
   
   
   <div id="quadro">
		<img onmouseover="mostrar(1);" onmouseout="esconder(1)" style="float: left;margin-left: 25px;" src="images/Quadro a_ Atividade 1_ Unidade 8.png"/>
		<img onmouseover="mostrar(2);" onmouseout="esconder(2)" style="float: right;margin-right: 25px;" src="images/Quadro b_ Atividade 1_ Unidade 8.png"/>
   </div>

   <div id="quadro">
		<img onmouseover="mostrar(3);" onmouseout="esconder(3)" style="float: left;margin-left: 25px;" src="images/Quadro c_ Atividade 1_ Unidade 8.png"/>
		<img onmouseover="mostrar(4);" onmouseout="esconder(4)" style="float: right;margin-right: 25px;" src="images/Quadro d_ Atividade 1_ Unidade 8.png"/>
   </div>
   
   <div id="legenda1">
			<ul>
				<li>Álcool a 70%</li>
				<li>Luva de procedimento</li>
				<li>Maleta para transporte de material biológico</li>
				<li>Tubos coletores</li>
				<li>Jaleco</li>
				<li>Aparadeira</li>
				<li>Algodão hidrófilo</li>
				<li>Caixa térmica</li>
				<li>Máscara descartável de procedimento </li>
				<li>Garrote</li>
				<li>Agulha para coleta de sangue à vacuo</li>
			
			</ul>
   </div>
   
    <div id="legenda2">
			<ul>
				<li>Álcool a 70%</li>
				<li>Luva de procedimento</li>
				<li>Maleta para transporte de material biológico</li>
				<li>Tubos coletores</li>
				<li>Esfigmomanômetro</li>
				<li>Jaleco</li>
				<li>Algodão hidrófilo</li>
				<li>Caixa térmica</li>
				<li>Máscara descartável de procedimento </li>
				<li>Garrote</li>
				<li>Agulha para coleta de sangue à vacuo</li>
			
			</ul>
   </div>
   
   <div id="legenda3">
			<ul>
				<li>Álcool a 70%</li>
				<li>Pinça cirúgica</li>
				<li>Maleta para transporte de material biológico</li>
				<li>Tubos coletores</li>
				<li>Jaleco</li>
				<li>Algodão hidrófilo</li>
				<li>Caixa térmica</li>
				<li>Máscara descartável de procedimento </li>
				<li>Garrote</li>
				<li>Agulha para coleta de sangue à vacuo</li>
			
			</ul>
   </div>
   
    <div id="legenda4">
			<ul>
				<li>Álcool a 70%</li>
				<li>Luva de procedimento</li>
				<li>Maleta para transporte de material biológico</li>
				<li>Tubos coletores</li>
				<li>Jaleco</li>
				<li>Algodão hidrófilo</li>
				<li>Caixa térmica</li>
				<li>Máscara descartável de procedimento </li>
				<li>Garrote</li>
				<li>Agulha para coleta de sangue à vacuo</li>
			
			</ul>
   </div>
   
   Qual dos itens contém os equipamentos e acessórios necessários para a 
coleta e o transporte de material biológico (apenas sangue) no domicílio?
   <div style="margin-left: 293px;">Selecione a opção correta: 
   <select id="select1">
		<option value="-">-</option>
	  <option value="a">a)</option>
	  <option value="b">b)</option>
	  <option value="c">c)</option>
	  <option value="d">d)</option>
	</select>
	</div>
	
    <!--<button class="btn" style="float: right;margin-right: 45px;" type="button" onClick="respostas(1);">Corrigir</button>-->
	<a class='inline btn' style="float: right;margin-right: 45px;" type="button" href="#content_color1" onClick="respostas(1);" >Corrigir</a>

		
		
		
		<script>
	
		
		function mostrar(id){
			$("#legenda" + id).fadeIn();

		}
		
		function esconder(id){
			
			$('#legenda' + id).fadeOut("slow");
			
		
		}
 
	</script>

   </div>
   
   <div id="ex2">
   <h5>Atividade 2</h5>
   Leia as frases apresentadas a seguir. Com o auxílio do caça-palavras, identifique a palavra que completa cada lacuna. <strong>A palavra deverá ser digitada na lacuna.</strong> Quando preencher todas as lacunas, clique em concluir. Ao digitar as palavras, as regras da norma culta da língua portuguesa devem ser respeitadas.<br><br>
<b>1.</b> <input type="text" id="p1"> é o conjunto de ações voltadas para a prevenção de
doenças, a proteção do trabalhador e a diminuição de riscos inerentes às atividades de
pesquisa, ensino, desenvolvimento tecnológico e prestação de serviços, visando à
saúde do homem, dos animais, à preservação do meio ambiente e à qualidade dos
resultados.<br><br>
<b>2.</b> Sempre usar equipamento de <input type="text" id="p2"> individual ou coletivo
(dependendo do tipo de atividade) no manuseio dos resíduos de serviços de saúde e
nos casos de risco de contato com material biológico.<br><br>
<b>3.</b> Todo material <input type="text" id="p3"> deve ser descartado em recipiente
rígido exclusivo, resistente à punctura, à ruptura e com tampa. <br><br>
<b>4.</b> Durante o procedimento de punção venosa periférica para a coleta de
sangue, é necessário fazer uso do <input type="text" id="p4"> com a finalidade de localizar uma veia
viável para punção por meio do “represamento” do retorno venoso. <br><br>
<b>5.</b> Não reencapar, entortar, quebrar ou retirar manualmente as <input type="text" id="p5">
das seringas. <br><br>
<b>6.</b> <input type="text" id="p6"> arterial deve ser realizada preferencialmente na artéria
radial, utilizando uma seringa com anticoagulante, sendo um procedimento privativo
do enfermeiro, médico e biomédico. <br><br>
<b>7.</b> Os tubos utilizados para armazenamento e transporte de sangue possuem
cores padronizadas de acordo com a indicação de uso. Os tubos de coleta de sangue
que possuem tampa <input type="text" id="p7"> servem para armazenamento de plasma. <br><br>
<b>8.</b> Os tubos de coleta de sangue que possuem tampa <input type="text" id="p8"> são usados
para teste de glicemia, tolerância da glicose, prova de sobrecarga de lactose e teste de
tolerância da insulina. <br><br>
<img style="border-radius: 10px; margin-top: 19px; margin-left: 161px;" src="images/cacapalavras.jpg" alt="Caça-Palavras">
 <button class="btn inline2" href="#content_color2" style="float: right;margin-right: 45px; margin-top: 70px;" type="button" onClick="respostas2();">Corrigir</button>


		
   
   
   
   </div>
   

   <style type="text/css">
   #legenda1 ul li, #legenda2 ul li,#legenda3 ul li,#legenda4 ul li {
		 list-style-type: disc;   
         margin-left: 10px;		
   }
   
   #quadro{
		margin-top: 25px;
		height: 282px;
		display: block;
   }
   
   #quadro img{
		width: 45%;
		cursor: pointer;
   }
   
   
   #feedback1, #feedback2{
			
			
	}
		
	#ex1 select{
		width: 65px;
	}
	
	#ex1{
		position: relative;
	}
	
	#ex2{
			margin-top: 60px;
			text-align: justify;
	}
   
   #legenda1,#legenda2,#legenda3,#legenda4{
		width: 200px;
		height: 300px;
		background-color: rgba(255, 255, 255, 0.88);
		display: none;
		position: absolute;
		top: 150;
		left: 330;
		border-radius: 10px;
		font-weight: bold;
	
   }
   
   #legenda2{
		top: 150;
		left: 690;
   }
   
    #legenda3{
		top: 445;
		left: 330;
   }
   
    #legenda4{
		top: 445;
		left: 690;
   }
    
    #legenda1 ul, #legenda2 ul,#legenda3 ul,#legenda4 ul{
		margin: 15px;
	
   }
   
   input[type="text"]{
		margin-top: 8px;
		height: 26px;
		width: 116px;
   }
   
   
   </style>
 
   
   <script type="text/javascript">
	 var user = $('#usuario b').html();
	function respostas(id){

		var x = document.getElementById("select1").selectedIndex;
		
		
		if(x == 0){
		
			 
			 $("#content_color1").html("<h4>Atenção!</h4> <br>Preencha uma das sentenças antes de clicar em corrigir.");
         	

		
		}else if(x == 4){
			$("#texto_color1").html("<h4>Resultado</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, você acertou a alternativa.");
            	$(".inline").colorbox({inline:true, width:"50%",onClosed:function(){ document.getElementById("feedback" + id).style.display="block"; }});
			 $.post( "../apresentacao/functions/chamaExCerto.php", { exercicio: 11}, function() {
						
				});
			 mostrarFeedback(1);
		
		}else{
			$("#texto_color1").html("<h4>Resultado</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, você não acertou a alternativa.");
              
              $(".inline").colorbox({inline:true, width:"50%"});
			  $.post( "../apresentacao/functions/chamaExErrado.php", { exercicio: 11}, function() {
						
				});
			
			mostrarFeedback(1);
		}
	$(".inline").colorbox({inline:true, width:"50%",onClosed:function(){ location.reload();}});
}

	 function mostrarFeedback(id){
		document.getElementById("feedback" + id).style.display="block";
	}
   
   function respostas2(){
   
		var r1 = document.getElementById('p1').value;
		var r2 = document.getElementById('p2').value;
		var r3 = document.getElementById('p3').value;
		var r4 = document.getElementById('p4').value;
		var r5 = document.getElementById('p5').value;
		var r6 = document.getElementById('p6').value;
		var r7 = document.getElementById('p7').value;
		var r8 = document.getElementById('p8').value;
		
		
		if(r1 == "" || r2 == "" || r3 == "" || r4 == "" || r5 == "" || r6 == "" || r7 == "" || r8 == ""){
			 $("#content_color2").html("<h4>Atenção!</h4> <br>Preencha todas as lacunas antes de clicar em corrigir.");
         	
			
		
		}else if( r1.toLowerCase() == "biossegurança" && r2.toLowerCase() == "proteção" && r3.toLowerCase() == "perfurocortante" && r4.toLowerCase() == "garrote" && r5.toLowerCase() == "agulhas" && r6.toLowerCase() == "gasometria" && r7.toLowerCase() == "verde" && r8.toLowerCase() == "cinza"){
			$(".inline2").colorbox({inline:true, width:"50%",onClosed:function(){ document.getElementById("feedback" + id).style.display="block"; }});
			$("#texto_color2").html("<h4>Resultado</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, você preencheu as lacunas corretamente!");
			$.post( "../apresentacao/functions/chamaExCerto.php", { exercicio: 12}, function() {
						
				});
			mostrarFeedback(2);
			
		}else{
			$(".inline2").colorbox({inline:true, width:"50%",onClosed:function(){ document.getElementById("feedback" + id).style.display="block"; }});
			$("#texto_color2").html("<h4>Resultado</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, você não preencheu as lacunas corretamente.");
			$.post( "../apresentacao/functions/chamaExErrado.php", { exercicio: 12}, function() {
						
				});
			mostrarFeedback(2);
		}		
		$(".inline2").colorbox({inline:true, width:"50%",onClosed:function(){ location.reload();}});
   }
   
   
   

   
   </script>
   
	<?php
	if ($_GET['ex']==1){ 

	}
	if ($_GET['ex']==2){ 

		echo '<script type="text/javascript">$("html,body").animate({scrollTop: $("#ex2").offset().top}, 1500);</script>';
	}
?>
   


