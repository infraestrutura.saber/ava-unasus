<?php
/*
 * Created on 06/04/2012
 *
 *Essa class tem as bibliotecas de gerenciamento
 * da chave de autenticação do webservice. 
 */
 class message {
 	
 	public static $TAG_JUSTIFICATIVA ="{JUSTIFICATIVA}";
 	public static $TAG_USUARIO_NOME ="{USUARIO_NOME}";
 	public static $TAG_USUARIO_NOME_COMPLETO ="{USUARIO_NOME_COMPLETO}";
 	public static $TAG_CURSO_NOME ="{CURSO_NOME}";
 	public static $TAG_CURSO_ABREV ="{CURSO_ABREV}";
 	public static $TAG_CURSO_URL ="{CURSO_URL}";
 	
 	//inserir regristro na base de dados. 
 	function save($dto) {
 		global $CFG;
 		return insert_record('enrol_unasus_message', $dto);
 	}
 
 //alteriar regristro na base de dados. 
 	function edit($dto) {
 		global $CFG;
 		return update_record('enrol_unasus_message', $dto);
 	}

  //excluir regristro na base de dados. 
 function delete_by_id($id) {
 		global $CFG;
 		return delete_records_select('enrol_unasus_message', "id=$id");
 	}
  //Retora a quantidade de registro cadastrada na base de dados
 function count() {
 		global $CFG;
		$sql ="SELECT COUNT(id)  AS qreg FROM {$CFG->prefix}enrol_unasus_message ";
		$r=get_record_sql($sql);
		return $r->qreg;
 	}
 	function get_by_id($id) {
 		global $CFG;
		$sql ="SELECT id,contextid,presentation,approval,disapproval,autreviewapproval,manualreviewapproval,pendingapprovalmanager  FROM {$CFG->prefix}enrol_unasus_message WHERE id=$id";
		$r=get_record_sql($sql);
		return $r;
 	}
 	function get_by_contextid($contextid) {
 		global $CFG;
		$sql ="SELECT id,contextid,presentation,approval,disapproval,autreviewapproval,manualreviewapproval,pendingapprovalmanager  FROM {$CFG->prefix}enrol_unasus_message WHERE contextid=$contextid";
		$r=get_record_sql($sql);
		return $r;
 	}
 	function get_all() {
 		global $CFG;
		$sql ="SELECT id,contextid,presentation,approval,disapproval,autreviewapproval,manualreviewapproval,pendingapprovalmanager  FROM {$CFG->prefix}enrol_unasus_message ";
		$r=get_records_sql($sql);
		return $r;
 	}
 	function get_contextid_config() {
 		global $CFG;
		$sql ="SELECT contextid  FROM {$CFG->prefix}enrol_unasus_message ";
		$rows=get_records_sql($sql);
		
		if(!is_array($rows))return NULL;
		$conf = array();
		foreach ($rows as $row){ 
			$conf[$row->contextid]=TRUE;
		}
		return $conf; 
	 	}
	 	 	
	function fill_tag_user($messge,$user) {
		
		$messge=str_replace(self::$TAG_USUARIO_NOME, $user->firstname, $messge);
		$nome_completo=$user->firstname;
		if(!empty($user->lastname)){$nome_completo.=" ".$user->lastname;}
		$messge=str_replace(self::$TAG_USUARIO_NOME_COMPLETO, $nome_completo, $messge);
		return $messge;
	}
 	
 	function fill_tag_course($messge,$course) {
 		global $CFG;
 		$url_curse=$CFG->httpswwwroot."/course/view.php?id=$course->id";
		$messge=str_replace(self::$TAG_CURSO_NOME, $course->fullname, $messge);
		$messge=str_replace(self::$TAG_CURSO_ABREV, $course->shortame, $messge);
		$messge=str_replace(self::$TAG_CURSO_URL, $url_curse, $messge);
		return $messge;
	}
	function fill_tag_justify($messge,$justify) {
		$messge=str_replace(self::$TAG_JUSTIFICATIVA, $justify, $messge);
		return $messge;
	}
 }