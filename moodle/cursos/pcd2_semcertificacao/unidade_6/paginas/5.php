<?php
               
    $pg_atual=5;
     registro($id,unid6_pg,$pg_atual,unid6_ev);
?>

	<style type="text/css">
		table tr td{
			
			padding-left: 10px;
		}
		
		#lista{
            list-style-type: disc;  
            margin-left: 30px;
        }
	</style>
    
	<h4 class="titulo">Estoma de Alimentação</h4>
   <h5>Jejunostomia</h5><br/>
	<span style="margin-left: 409px;"><b>Localização anatômica da jejunostomia</b></span>
	<ul style="width: 370px;float: left;">
	É um procedimento cirúrgico de caráter temporário ou definitivo, por meio de endoscopia, laparotomia ou 
	laparoscopia, que estabelece o acesso ao jejuno (intestino) proximal através da parede abdominal, a fim de 
	evitar a passagem do alimento pelo estômago, sendo indicada para (SILVA, 2007):<br/><br/>
		<li id="lista">cirurgias abdominais de grande porte que envolvem ressecção gástrica;</li>
		<li id="lista">íleo paralítico em pós-operatório;</li>
		<li id="lista">fístulas digestivas.</li>
	</ul>

  <br/>
   <img style="float: right;margin-top: 2px;margin-right: 0px;" src="images/Fig06-jejunostomia.jpg" alt="Jejunostomia"> 
   <span style="position: absolute;top: 560;right: 438px;"><b>Fonte:</b> (UNA-SUS UFPE, 2014).</span>
   <br><br>
   <span style="clear:both">&#160</span>
<div style="width: 321px;float:left; clear:both; margin-top: 58px;">

	<div id="hide1" style="float:left;">
<table style="background-color:#FFE4CA"  class="table table-bordered "style="width: 300px; margin-left: 20px; float:left;">

		<tr bgcolor="#ccc">
		<td><b>Contraindicações da Jejunostomia</b></td>
	
	</tr>
	<tr >
		<td>Doença inflamatória</td>
	
	</tr>
	
	<tr>
		<td>Pós tratamento radioterápico</td>
	
	</tr>
	
	<tr >
		<td>Imunodepressão grave</td>
	
	</tr>
	
	<tr >
		<td>Asciste volumosa</td>
	
	</tr>
	
	<tr>
		<td>Peritonite</td>
	
	</tr>
	
	

</table>	
<span style="margin-left: 0px;float:left;"><b>Fonte:</b> (SILVA; HUGA-TANIGUCHI, 2010, adaptado).</span></div></div>

<div style="width: 384px; float:right; margin-right: 21px;margin-top: 58px;">

	<div id="hide2" style="float:right;">
<table style="background-color:#FFE4CA"  class="table table-bordered " style="width: 300px; margin-left: 20px; float:right;">
		<tr bgcolor="#ccc">
		<td ><b>Complicações da Jejunostomia</b></td>
	
	</tr>

	<tr>
		<td>Abcesso intrabdominal</td>
	
	</tr>
	
	<tr >
		<td>Sepse</td>
	
	</tr>
	
	<tr>
		<td>Hemorragia</td>
	
	</tr>
	
	<tr >
		<td>Peritonite</td>
	
	</tr>
	
	<tr >
		<td>Infecção localizada</td>
	
	</tr>
	
	<tr>
		<td>Saída acidental da sonda</td>
	
	</tr>
	
	<tr >
		<td>Obstrução do cateter</td>
	
	</tr>


</table>	
<span style="margin-left: 0px;float:left;"><b>Fonte:</b> (SMELTZER;BARE, 2005, adaptado).</span></div></div>
<div style="clear: both; height: 20px; display: block"></div>
  <h5 style="clear:both; margin-top: 10px;">Cuidados com a jejunostomia</h5>
  <ul>
		<li id="lista">Lavar as mãos antes e após manipular a sonda de jejunostomia;</li>
		<li id="lista">Realizar limpeza do local da inserção da sonda de jejunostomia com soro fisiológico a 0,9% ou água morna, sempre que necessário, e secar bem após a lavagem;</li>
		<li id="lista">Nunca tracionar a sonda;</li>
		<li id="lista">Manter a sonda sempre fechada enquanto não estiver em uso;</li>
		<li id="lista">Colocar duas gazes dobradas em cada lado da inserção da sonda, mantendo-as limpas e secas;</li>
		<li id="lista">Atentar para sinais flogísticos (inflamação) no sítio de inserção da sonda;</li>
		<li id="lista">Observar extravasamento de dieta pelo orifício da jejunostomia. Caso isso seja observado, comunicar imediatamente à equipe multiprofissional;
		<li id="lista">Durante e 60 minutos após a administração da dieta, manter o paciente em posição de Fowler;</li>
		<li id="lista">Lavar a sonda com 20 a 50ml de água filtrada após dietas e/ou medicações;</li>
		<li id="lista">Diluir bem as medicações antes de administrar (SMELTZER; BARE, 2005, adaptado). </li>
	</ul>
  <br>
<!--<a href="javascript:history.go(-1)" class="btn">Voltar</a>-->
<script>
	
		
		function esconder(id){
		
			if ($("#hide" + id).is(":hidden")) {
				$("#hide" + id).slideDown("");
				
			} else{
			$('#hide' + id).hide("");
			}
	
		}
 
	</script>
