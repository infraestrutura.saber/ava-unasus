<?php
/*
 * Created on 12/04/2012
 *
 *Essa class tem as bibliotecas de gerenciamento
 * da regra de inscri��o. 
 */
 class enrol_unasus_report_util {
 	
 	function get_courses_enrol_config(){
				global $CFG;
			$sql ="SELECT id,fullname FROM {$CFG->prefix}course WHERE enrol = 'unasus' ORDER BY fullname ";
			return get_records_sql($sql);
    	}

function get_courses_enronl_config_html_select(){
		$courses=	$this->get_courses_enrol_config();
		$options = array();
		$options['']="  ----  ";
		foreach ($courses as $course){ 
			$options[$course->id]=$course->fullname;
		}
		return 	$options ;
		} 
		
		function get_users(){
				global $CFG;
			$sql ="SELECT id,firstname,lastname FROM {$CFG->prefix}user ORDER BY firstname,lastname";
			return get_records_sql($sql);
    	}
    	
    	function get_user_html_select(){
		$users=	$this->get_users();
		$options = array();
		$options['']="  ----  ";
		foreach ($users as $user){ 
			
			$options[$user->id]=$user->firstname. " ".$user->lastname;
		}
		return 	$options ;
		}
		
	function get_status_domain_table(){
		
		$options = array();
		$options['']="  ----  ";
		$options[1]=get_string('aprove','enrol_unasus');
		$options[2]=get_string('reject','enrol_unasus');
		
		return 	$options ;
	}
	function get_status_domain_table_name($status_code){
		if($status_code==1) return get_string('aprove','enrol_unasus');
		else if($status_code==2) return get_string('reject','enrol_unasus');
		else return $status_code;
	}
function get_status_type_domain_table(){
		
		$options = array();
		$options['']="  ----  ";
		$options[1]=get_string('automatic','enrol_unasus');
		$options[2]=get_string('manual','enrol_unasus');
		
		return 	$options ;
		}
		
	function get_status_type_domain_table_name($status_code){
		if($status_code==1) return get_string('automatic','enrol_unasus');
		else if($status_code==2) return  get_string('manual','enrol_unasus');
		else return $status_code;
	}
		
function get_statuscause_domain_table_name($key){
		
		if($key==2) return get_string('msg_cpf_not_valid','enrol_unasus');
		else if($key==3) return get_string('msg_erro_arouca_conn','enrol_unasus');
		else if($key==4) return get_string('msg_no_cbo_arouca','enrol_unasus');
		else if($key==5) return get_string('msg_no_role_created','enrol_unasus');
		else if($key==6) return get_string('msg_no_cbo_in_role','enrol_unasus');
		else if($key==7) return get_string('msg_enrol_failed','enrol_unasus');
		else return "";
	}
	function get_string_option_yes_not($param){
 		
		if($param==1) return get_string('yes','enrol_unasus');
		else if($param==0)return get_string('not','enrol_unasus');
		return 	"";
	}
 }