<?php
/*
 * Created on 12/04/2012
 *
 *Essa class tem as bibliotecas de gerenciamento
 * da regra de inscri��o. 
 */
 class role {
 	
 	public static $TYPE_CBO ="CBO";
 	
 	//inserir regristro na base de dados. 
 	function save($dto) {
 		global $CFG;
 		return insert_record('enrol_unasus_role', $dto);
 	}
 
 //alteriar regristro na base de dados. 
 	function edit($dto) {
 		global $CFG;
 		return update_record('enrol_unasus_role', $dto);
 	}

 	
  //excluir regristro na base de dados. 
 function delete_by_id($id) {
 		global $CFG;
 		return delete_records_select('enrol_unasus_role', "id=$id");
 	}
  //Retora a quantidade de registro cadastrada na base de dados
 function count() {
 		global $CFG;
		$sql ="SELECT COUNT(id)  AS qreg FROM {$CFG->prefix}enrol_unasus_role ";
		$r=get_record_sql($sql);
		return $r->qreg;
 	}
 	function get_by_contextid($contextid) {
 		global $CFG;
		$sql ="SELECT id,title,contextid,type,name,value,description FROM {$CFG->prefix}enrol_unasus_role WHERE contextid=$contextid";
		$r=get_record_sql($sql);
		return $r;
 	}
 	function exist_by_contextid($contextid) {
 		global $CFG;
		$sql ="SELECT COUNT(id) AS qreg  FROM {$CFG->prefix}enrol_unasus_role WHERE contextid=$contextid";
		$r=get_record_sql($sql);
		return $r->qreg;
 	}
 	function get_cbo_by_contextid($contextid) {
 		global $CFG;
		$sql ="SELECT value FROM {$CFG->prefix}enrol_unasus_role WHERE  contextid=$contextid  AND type='".self::$TYPE_CBO."'";
		$r=get_record_sql($sql);
		return $r->value;
 	}
 	function get_all() {
 		global $CFG;
		$sql ="SELECT id,contextid,type,name,value,description  FROM {$CFG->prefix}enrol_unasus_role ";
		$r=get_records_sql($sql);
		return $r;
 	}
 	function get_type_html_option(){
 		$options = array();
		$options[self::$TYPE_CBO]=get_string('cbo','enrol_unasus');
		return 	$options ;
	}
	function  get_contextid_config(){
 		$context_value = array();
 		$rows=$this->get_all();
 		 $conf = array();
 		 if(is_array($rows)){
 		 	foreach ($rows as $row){ 
			$conf[$row->contextid]=TRUE;
		}
 		 }
		
		return $conf; 
 	
	}
 }