<?php $unidade = $_GET['view'];
session_start('config');
$id = $_SESSION['id'];
?>

<!--Desktop-->
<div class="span12 no_left bg1 topo_unidade hidden-phone" style="width:100%">

	<?php
	if ($unidade == 'unidade1') {
		$unid = 1;

	}
	if ($unidade == 'unidade2') {
		$unid = 2;

	}
	if ($unidade == 'unidade3') {
		$unid = 3;

	}

	// A PORCENTAGEM ESTA SENDO CHAMADA DESNTRO DESTA FUNCAO verifica_topico..
	verifica_topo($unidade, 0, $cpf);
	//
	?>
</div>

<?php
//CADASTRA O TOPICO
if ($_GET['view'] == 'unidade1') {

	$id_unidade = 1;
	$id_topico = $_GET['topico'];

	cadastrarTopico($id, $id_unidade, $id_topico);
}
if ($_GET['view'] == 'unidade2') {

	$id_unidade = 2;
	$id_topico = $_GET['topico'];
	cadastrarTopico($id, $id_unidade, $id_topico);
}
if ($_GET['view'] == 'unidade3') {

	$id_unidade = 3;
	$id_topico = $_GET['topico'];
	cadastrarTopico($id, $id_unidade, $id_topico);
}
?>

<?php
//VERIFICAR TOPICO VISUALIZADO

if ($_GET['view'] == 'unidade1') {

	$id_unidade = 1;
	$topico = $_GET['topico'];
	verificarTopico($id, $topico, $id_unidade);

	cadastrarTopico($id, $id_unidade, $id_topico);
}
if ($_GET['view'] == 'unidade2') {

	$id_unidade = 2;
	$topico = $_GET['topico'];
	verificarTopico($id, $topico, $id_unidade);
}
if ($_GET['view'] == 'unidade3') {

	$id_unidade = 3;
	$topico = $_GET['topico'];
	verificarTopico($id, $topico, $id_unidade);
}

?>

<!--Mobile-->
<div class="span12 no_left bg1 topo_unidade_phone visible-phone">
	<center>
		<?php
		if ($unidade == 'unidade1') {
			$unid = 1;

		}
		if ($unidade == 'unidade2') {
			$unid = 2;

		}
		if ($unidade == 'unidade3') {
			$unid = 3;

		}

		//$porc = 10;
		verifica_topo($unidade, 1, $porc);
		?>

	</center>
</div>

<div class="container margin_auto">

	<div class="row">

		<div class="span12 barra_up">
			<!--Normal-->
			<div class="span4 no_left barra_menu visible-desktop">
				<div class="label_menu marg_right">
					Navegação<img class="info" src="../imagens/conteudo/info.png" title="info" alt="info">
				</div>
				<?php verifica_unidade($unidade, 0); ?>
			</div>
			<!--Tablet-->
			<div class="span4 no_left barra_menu hidden-desktop">
				<div class="label_menu">
					Navegação<img class="info" src="../imagens/conteudo/info.png" title="info" alt="info">
				</div>
				<?php verifica_unidade($unidade, 1); ?>
			</div>

			<!--normal-->
			<div class="span4 no_left barra_menu visible-desktop">

				<?php verifica_unidade_ex($unidade, 0); ?>

			</div>

			<div class="span4 no_left barra_menu hidden-desktop">
				<?php verifica_unidade_ex($unidade, 1); ?>

			</div>

			<div class="span4 no_left">
				<div class="span2 no_left barra_menu bg">
					<center>
						<div class="label_top">
							Continuar os estudos
						</div>
					</center>
					<?php
					if ($unidade == 'unidade1') {
						$id_unidade = 1;

					}
					if ($unidade == 'unidade2') {
						$id_unidade = 2;

					}
					if ($unidade == 'unidade3') {
						$id_unidade = 3;

					}

					$id = 10;
					//$id=$_SESSION['id'];
					ultimoAcesso($id, $id_unidade);
					?>
				</div>
				<div class="span2 no_left barra_menu bg_1">
					<center>
						<div class="label_top">
							<a  href="#content_color" onClick="confirmacao();"><span id="loading">Recomeçar os estudos</span></a>
						</div>
					</center>

					<?php
					if ($unidade == 'unidade1') {
						$unid = 1;
						//deletarEvolucao($cpf, $unid);
					}
					if ($unidade == 'unidade2') {
						$unid = 2;
						//deletarEvolucao($cpf, $unid);
					}
					if ($unidade == 'unidade3') {
						$unid = 3;
						//deletarEvolucao($cpf, $unid);
					}
					?>
				</div>
			</div>
		</div>

		<!--Conteudo-->
		<div class="span12 no_left box_conteudo">
			<div class="alertConfirmacao" >
				<strong><img src="../imagens/warning.svg"> Confirmação!</strong>
				<br>
				<br>
				<?php echo $_SESSION['firstname']; ?>,
				você tem certeza que deseja recomeçar os estudos desta unidade?
				Se confirmar a opção, todos os registros de navegação pelas páginas desta unidade serão
				eliminados e você não saberá quais páginas já visitou.
				<br>
				<br>

				<a class="btn" onClick="cancelar();">Cancelar</a>
				<a class="btn btn-warning" onClick="apagar_confirmar();">Recomeçar</a>
			</div>
			<div class="span2 no_left"></div>
			<div class="span8 conteudo_interno">
				<!--Conteudo entra aqui-->
				<span style="color: #8d55a1;font-weight: bold;font-size: 20px;"><!-- Implantação da Coleta de Dados Simplificada (CDS) --></span>
				<br>
				<br>
				<?php

				$resource = $_GET['resource'];
				$topico = $_GET['topico'];
				$unidade = $_GET['view'];
				conteudo($resource);
				echo "<br><br><center>";
				paginacao($unidade,$topico);
				echo "</center>";
				?>

			</div>
			<div class="span2"></div>
		</div>

	</div>

</div>
<script>
	var url = location.search.split("?");
	var valor_parte1 = url[1].split("=");
	var valor_parte0 = valor_parte1[1].split("&");
	var valor_parte2 = valor_parte0[0].split("e");
	var unidade = valor_parte2[1];

	if (unidade == '1') {
		var iconCarregando = $('<img src="../imagens/loader_unid1.gif">');
	}
	if (unidade == '2') {
		var iconCarregando = $('<img src="../imagens/loader_unid2.gif">');
	}
	if (unidade == '3') {
		var iconCarregando = $('<img src="../imagens/loader_unid3.gif">');
	}
	function confirmacao() {
		$('.alertConfirmacao').slideDown(500);
		//$('#loading').html(iconCarregando);
	}

	function cancelar() {
		$('.alertConfirmacao').slideUp(500);
		//$(iconCarregando).remove();
		//$('#loading').html('Recomeçar os estudos');
	}

	function apagar_confirmar() {

		$.ajax({
			type : 'post',

			url : '../functions/apagar_tudo.php',

			data : 'unidade=' + unidade,

			dataType : 'html',
			beforeSend : function() {
				$('#loading').html(iconCarregando);
			},
			complete : function() {
				$(iconCarregando).remove();
				$('#loading').html('Recomeçar os estudos');

			},
			error : function(xhr, er) {
				alert('erro');

			},
			success : function(data) {
				location.reload();
				//alert(data);

			}
		});

	}
</script>

