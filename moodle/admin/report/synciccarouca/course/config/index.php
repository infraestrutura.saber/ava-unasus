<?php

   require("../../../../../config.php");
   require("$CFG->dirroot/admin/report/synciccarouca/course/config/lib.php");
    require("$CFG->dirroot/admin/report/synciccarouca/tab.php");
 require_login();

    if(!isadmin()) require_capability('report/synciccarouca:configview', get_context_instance(CONTEXT_SYSTEM), NULL, false);
     add_to_log(1, 'report_synciccarouca', 'configview', "admin/report/synciccarouca/course/config/index.php", getremoteaddr());    
       
    $navigation = build_navigation(array(array('name' => get_string('synciccarouca','report_synciccarouca'), 'link' => "$CFG->wwwroot/admin/report/synciccarouca/index.php", 'type' => 'misc'),
                                     array('name' => get_string('manegewscoursesync','report_synciccarouca'), 'link' => null, 'type' => 'misc')));
  
    
    $lib =new course_sync_config();
    
   
   
    print_header(get_string('synciccarouca','report_synciccarouca'),get_string('synciccarouca','report_synciccarouca'), $navigation);
    print_tabs($tabs, $currenttab, $inactive, $activated);
 
   
     get_table_view();
     print_footer();
    
  
   function get_table_view(){
   		global $lib;
   		 $rows_count = $lib->count();
   		 $rows = $lib->search();
   		 echo  "<center><a href='add.php'>".get_string('addnewcoursesync','report_synciccarouca')."</a></centar>";
   		 echo "<center>".get_string('rowcount','report_synciccarouca') ." " . $rows_count."</centar>";
   		 $table = new object();
                 if(!empty($rows)){
   			 
    	  		$table->head  = array(get_string('id','report_synciccarouca'),get_string('mdlcoursesync','report_synciccarouca'),get_string('aroucacoursesync','report_synciccarouca'),get_string('aroucaofertaync','report_synciccarouca'),get_string('coursesync_aroucaconcluinteoferta','report_synciccarouca'),get_string('approvedgrade','report_synciccarouca'),get_string('sendmailcoursesync','report_synciccarouca'), get_string('edit'));
    	  		$table->align = array('left', 'left', 'left','left','left','left','left','right');
    	  		$table->width = '95%';
         		 $table->class = 'generaltable';
                         
          		$table->data = array();
				foreach ($rows as $row) {
                                    $txt_yesnot=$lib->get_name_option_yes_not($row->concluinteferta);
                                    if($row->arcidcurso==0){$row->arcidcurso="";}
           		 	    $table->data[] = array($row->id,$row->fullname,$row->arcidcurso,$row->arcidoferta,$txt_yesnot,$row->gradeapproved,$lib->certificate_send_mail_text_format($row),icons ($row));
                                }
        		}
   				 print_table($table);
   		}
   		

   function icons ($dto) {
   	global $CFG;
  	    $strdelete   = get_string('delete');
        $strmoveup   = get_string('moveup');
        $stredit     = get_string('edit');

    /// Edit
    $editstr = '<a title="'.$stredit.'" href="edit.php?id='.$dto->id.'"><img src="'.$CFG->pixpath.'/t/edit.gif" alt="'.$stredit.'" class="iconsmall" /></a> ';
 	$editstr .= '<img src="'.$CFG->pixpath.'/spacer.gif" alt="" class="iconsmall" /> ';
    /// Delete
    $editstr .= '<a title="'.$strdelete.'" href="delete_confirm.php?id='.$dto->id.'"';
    $editstr .= '"><img src="'.$CFG->pixpath.'/t/delete.gif" alt="'.$strdelete.'" class="iconsmall" /></a> ';
     $editstr .= '<img src="'.$CFG->pixpath.'/spacer.gif" alt="" class="iconsmall" /> ';

    return $editstr;
}
?>