<?php
               
    $pg_atual=6;
     registro($id,unid4_pg,$pg_atual,unid4_ev);
?>




<!-- colorbox exercicio1 -->
<div style='display:none'>
			<div id="content_color1" style='padding:10px; background:#fff;'>

			<div id="texto_color1"></div>
			<br><br>
			<a class="btn" href="index.php?pagina=6">Tentar novamente</a>
			<a class="btn" href="index.php?pagina=0">Estudar mais</a>
			<a class="inline btn" href="#feedback1">Mostrar respostas</a>
		
			<br><br>
			
			</div>
		</div><!-- fim colorbox exercicio1-->
	 
	<div style='display:none'>
		<div id="feedback1">
			
				<h4>Respostas</h4>
				<br>
				<b>1. Verdadeiro</b><br>
				<b>2. Falso</b> - Para o alívio da retenção urinária aguda ou crônica, após prescrição médica, pode-se realizar a sondagem vesical utilizando a sonda de Foley. Esse procedimento é realizado pelo técnico de enfermagem capacitado e sob a supervisão do enfermeiro.<br>
				<b>3. Falso</b> - A sondagem vesical de demora não tem apenas a função de esvaziamento da bexiga. Também objetiva controlar o volume urinário; preparar o paciente para cirurgias, principalmente abdominais; promover drenagem urinária dos pacientes com incontinência urinária; e auxiliar no diagnóstico das lesões traumáticas do trato urinário. A sonda pode permanecer por dias no paciente, sendo trocada apenas em casos de obstrução ou por indicação médica.<br>
				<b>4. Falso</b> - A avaliação do ambiente domiciliar é de extrema importância para a realização do procedimento de sondagem vesical, principalmente no que se refere à redução do risco de infecção.<br>
				<br>
		
			<a class="inline btn" href="#content_color1">Voltar</a>
		</div>
	</div>
<!-- colorbox exercicio1 final -->
<!-- colorbox exercicio2 -->
<div style='display:none'>
			<div id="content_color2" style='padding:10px; background:#fff;'>

			<div id="texto_color2"></div>
			<br><br>
			<a class="btn" href="index.php?pagina=6">Tentar novamente</a>
			<a class="btn" href="index.php?pagina=0">Estudar mais</a>
			<a class="inline2 btn" href="#feedback2">Mostrar respostas</a>
		
			<br><br>
			
			</div>
		</div>
		<div style='display:none'>
		<div id="feedback2">
			
			
				<h4>Respostas</h4>
				<br>
				<b>A sequência correta é:</b> infecção, procedimento invasivo, asséptica, equipamento de proteção individual, tamanho, tipo, balonete, preenchido, tração, desinsuflado.
				<br><br>
		
				<a class="inline btn" href="#content_color2">Voltar</a>
		</div>
	</div>
<!-- fim colorbox exercicio2-->



	 <h4 class="titulo">Exercícios </h4>
	  <h5>Atividade 1</h5>
	  
	  <div id="ex1">
			
			<p style="text-align: justify;">
			A assistência multiprofissional em saúde prestada aos indivíduos com doenças crônicas no domicílio representa um grande avanço para melhorar a qualidade de vida, humanizar a assistência, promover a saúde, além de estimular uma reflexão crítica acerca da efetividade dessas práticas e da organização dos serviços de saúde.  </p>
			
			<p>Sobre as indicações e finalidades da sondagem vesical, marque (V) verdadeiro ou (F) falso nas sentenças a seguir:</p>
			
	<table  style="width: 640px;margin-left: 45px;margin-top: 33px;margin-bottom: 20px;">
	
		
	<tr bgcolor="#F6CF29">	
	<td><center><div class="d1"><h5 style="color:#fff;">Sentenças</h5></div> </center><center> <div class="d3"><h5 style="color:#fff;">Verdadeiro | Falso</h5></div> </center></td>
	</tr>
	
	
	<tr bgcolor="#F0E68C"  >
		<td><div class="d1"><p>1. São indicações da sondagem vesical: colher urina asséptica para exames; controlar o débito urinário; proporcionar conforto a indivíduos com incontinência urinária; aliviar a retenção urinária aguda ou crônica, quando as medidas para estimular a micção forem ineficazes.</p></div>
		<div class="d2"><p><input type="radio" name="1" value="V"></p><span class="boolean">V</span></div>
		<div class="d3"><p><input type="radio" name="1" value="F"></p><span class="boolean">F</span> </div></td>
	
	</tr>
	
	<tr bgcolor="#F8D946" >
		<td><div class="d1"><p>2. Para o alívio da retenção urinária aguda ou crônica, após prescrição médica, pode-se realizar a sondagem vesical utilizando a sonda de Foley. Esse procedimento é realizado pelo técnico de enfermagem sem a necessidade de supervisão do enfermeiro.</p></div>
		<div class="d2"><p><input type="radio" name="2" value="V"></p><span class="boolean">V</span></div>
		<div class="d3"><p><input type="radio" name="2" value="F"></p><span class="boolean">F</span></div></td>
	
	</tr>
	
	<tr bgcolor="#F0E68C" >
		<td><div class="d1"><p>3. A sondagem vesical de demora é utilizada para esvaziamento imediato da bexiga, não permanecendo no paciente.</p></div>
		<div class="d2"><p><input type="radio" name="3" value="V"></p><span class="boolean">V</span></div>
		<div class="d3"><p><input type="radio" name="3" value="F"></p><span class="boolean">F</span></div></td>
	
	</tr>
	
	<tr bgcolor="#F8D946">
		<td><div class="d1"><p>4. Para a realização do procedimento de sondagem vesical, é dispensável a avaliação do ambiente domiciliar.</p></div>
		<div class="d2"><p><input type="radio" name="4" value="V"></p><span class="boolean">V</span></div>
		<div class="d3"><p><input type="radio" name="4" value="F"></p><span class="boolean">F</span></div></td>
	
	</tr>
	
	
		
</table>


		  <a class='inline btn' style="float: right;margin-right: 45px;" type="button" href="#content_color1" onClick="respostas(1);" >Corrigir</a>

		
		
</div>



<div id="ex2">
	  <div id="questao">
   <h5>Atividade 2</h5>
      <br>
	  Leia atentamente as frases abaixo. Em seguida, selecione as opções que completam as sentenças corretamente.  
<br><br>
	  
	  <p style="width:710px; border: 1px dotted#ccc; padding: 7px; "><b>a)</b>	Apesar de o ambiente de cuidado ser o domicílio, ainda assim a equipe de saúde deve se  preocupar com a 
	  <select id="s1">
		  <option value="0">-</option>
	  	  <option value="1">infecção</option>
		  <option value="9">tração</option>
		  <option value="6">tipo</option>
		  <option value="4">equipamento de proteção individual</option>
		  <option value="5">tamanho</option>
		  <option value="3">procedimento invasivo</option>
		  <option value="7">balonete</option>
		  <option value="8">desinsuflado</option>
		  <option value="2">asséptica</option>
		  <option value="10">preenchido</option>
	  </select>
	 , que pode ocorrer com a realização de <select id="s2">
		  <option value="0">-</option>
	  	  <option value="1">infecção</option>
		  <option value="9">tração</option>
		  <option value="6">tipo</option>
		  <option value="4">equipamento de proteção individual</option>
		  <option value="5">tamanho</option>
		  <option value="3">procedimento invasivo</option>
		  <option value="7">balonete</option>
		  <option value="8">desinsuflado</option>
		  <option value="2">asséptica</option>
		  <option value="10">preenchido</option>
	  </select> , como a sondagem vesical.</p>
	  <br>
	  <p style="width:710px; border: 1px dotted#ccc; padding: 7px;"><b>b)</b>	Durante a realização do cateterismo vesical, para que haja redução das possíveis complicações desse procedimento, é importante que o profissional fique atento aos cuidados fundamentais, como: usar rigorosamente a técnica  <select id="s3">
		  <option value="0">-</option>
	  	  <option value="1">infecção</option>
		  <option value="9">tração</option>
		  <option value="6">tipo</option>
		  <option value="4">equipamento de proteção individual</option>
		  <option value="5">tamanho</option>
		  <option value="3">procedimento invasivo</option>
		  <option value="7">balonete</option>
		  <option value="8">desinsuflado</option>
		  <option value="2">asséptica</option>
		  <option value="10">preenchido</option>
	  </select> , paramentando-se sempre com  <select id="s4">
		  <option value="0">-</option>
	  	  <option value="1">infecção</option>
		  <option value="9">tração</option>
		  <option value="6">tipo</option>
		  <option value="4">equipamento de proteção individual</option>
		  <option value="5">tamanho</option>
		  <option value="3">procedimento invasivo</option>
		  <option value="7">balonete</option>
		  <option value="8">desinsuflado</option>
		  <option value="2">asséptica</option>
		  <option value="10">preenchido</option>
	  </select> durante o procedimento. Além disso, é importante selecionar o  <select id="s5">
		  <option value="0">-</option>
	  	  <option value="1">infecção</option>
		  <option value="9">tração</option>
		  <option value="6">tipo</option>
		  <option value="4">equipamento de proteção individual</option>
		  <option value="5">tamanho</option>
		  <option value="3">procedimento invasivo</option>
		  <option value="7">balonete</option>
		  <option value="8">desinsuflado</option>
		  <option value="2">asséptica</option>
		  <option value="10">preenchido</option>
	  </select> e o  <select id="s6" >
		 <option value="0">-</option>
	  	  <option value="1">infecção</option>
		  <option value="9">tração</option>
		  <option value="6">tipo</option>
		  <option value="4">equipamento de proteção individual</option>
		  <option value="5">tamanho</option>
		  <option value="3">procedimento invasivo</option>
		  <option value="7">balonete</option>
		  <option value="8">desinsuflado</option>
		  <option value="2">asséptica</option>
		  <option value="10">preenchido</option>
	  </select> de sonda apropriada. É preciso que o <select id="s7" >
		 <option value="0">-</option>
	  	  <option value="1">infecção</option>
		  <option value="9">tração</option>
		  <option value="6">tipo</option>
		  <option value="4">equipamento de proteção individual</option>
		  <option value="5">tamanho</option>
		  <option value="3">procedimento invasivo</option>
		  <option value="7">balonete</option>
		  <option value="8">desinsuflado</option>
		  <option value="2">asséptica</option>
		  <option value="10">preenchido</option>
	  </select> seja testado antes da inserção da sonda de Foley e  <select id="s8" >
		 <option value="0">-</option>
	  	  <option value="1">infecção</option>
		  <option value="9">tração</option>
		  <option value="6">tipo</option>
		  <option value="4">equipamento de proteção individual</option>
		  <option value="5">tamanho</option>
		  <option value="3">procedimento invasivo</option>
		  <option value="7">balonete</option>
		  <option value="8">desinsuflado</option>
		  <option value="2">asséptica</option>
		  <option value="10">preenchido</option>
	  </select> conforme indicação do fabricante. Após o término da sondagem vesical de demora, um dos objetivos de uma fixação adequada é evitar  <select id="s9">
		  <option value="0">-</option>
	  	  <option value="1">infecção</option>
		  <option value="9">tração</option>
		  <option value="6">tipo</option>
		  <option value="4">equipamento de proteção individual</option>
		  <option value="5">tamanho</option>
		  <option value="3">procedimento invasivo</option>
		  <option value="7">balonete</option>
		  <option value="8">desinsuflado</option>
		  <option value="2">asséptica</option>
		  <option value="10">preenchido</option>
	  </select> . Antes da remoção da sonda de Foley, o balonete deverá ser  <select id="s10">
		 <option value="0">-</option>
	  	  <option value="1">infecção</option>
		  <option value="9">tração</option>
		  <option value="6">tipo</option>
		  <option value="4">equipamento de proteção individual</option>
		  <option value="5">tamanho</option>
		  <option value="3">procedimento invasivo</option>
		  <option value="7">balonete</option>
		  <option value="8">desinsuflado</option>
		  <option value="2">asséptica</option>
		  <option value="10">preenchido</option>
	  </select> .
	  
	  
	  </p>
     

  </div>
 <br>
	
	
	

<!--<button class="btn" style="float: right;margin-top: 20px;margin-right: 45px;" type="button" onClick="respostas2(2);">Corrigir</button>-->
<a class='inline btn' style="float: right;margin-top: 5px;margin-right: 45px;" type="button" href="#content_color2" onClick="respostas2(2);" >Corrigir</a>


</div>



<script type="text/javascript">


		 function respostas(id){


        var p1 = document.getElementsByName('1');
        var p2 = document.getElementsByName('2');
        var p3 = document.getElementsByName('3');
        var p4 = document.getElementsByName('4');
        var gabarito = new Array('V','F','F','F');


        var resp = new Array(p1,p2,p3,p4);
        var completo = true;


        for(var x = 0; x <5; x++){

            if(resp[x] != null){
                 if(resp[x][0].checked == true){
                           resp[x] = p1[0].value;
                }else if(resp[x][1].checked == true){
                            resp[x] = p1[1].value;

                }else{
                    completo = false;
                }
            }
        }

        if(completo){

			if(compare(resp,gabarito) == true){
            	$("#texto_color1").html("<h4>Resultado</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, parabéns! Você acertou todas as alternativas.");
            	$(".inline").colorbox({inline:true, width:"50%"});
				$.post( "../apresentacao/functions/chamaExCerto.php", { exercicio: 3}, function() {
						
				});

            }else{
            	$("#texto_color1").html("<h4>Resultado</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, você não acertou todas as alternativas.");
               	$(".inline").colorbox({inline:true, width:"50%",onClosed:function(){  }});
				$.post( "../apresentacao/functions/chamaExErrado.php", { exercicio: 3}, function() {
						
				});
               
            }
			
			mostrarFeedback(id);

         }else{

           $("#content_color1").html("<h4>Atenção!</h4> <br>Preencha todas as sentenças antes de clicar em concluir.");
         	
         }
		 $(".inline").colorbox({inline:true, width:"50%",onClosed:function(){ location.reload();}});
           
}

 function compare(x, y) {
            if (x.length != y.length) {
                return false;
            }
            for (key in x) {
                if (x[key] !== y[key]) {
                    return false;
                }
            }
            return true;
        }
		
	function mostrarFeedback(id){
		document.getElementById("feedback" + id).style.display="block";
	}
	
    function respostas2(id){
	
		var s1 = document.getElementById("s1").selectedIndex;
		var s2 = document.getElementById("s2").selectedIndex;
		var s3 = document.getElementById("s3").selectedIndex;
		var s4 = document.getElementById("s4").selectedIndex;
		var s5 = document.getElementById("s5").selectedIndex;
		var s6 = document.getElementById("s6").selectedIndex;
		var s7 = document.getElementById("s7").selectedIndex;
		var s8 = document.getElementById("s8").selectedIndex;
		var s9 = document.getElementById("s9").selectedIndex;
		var s10 = document.getElementById("s10").selectedIndex;
		
		
		
		$(".inline2").colorbox({inline:true, width:"50%",onClosed:function(){ document.getElementById("feedback" + id).style.display="block"; }});
	
		 
		 if(s1 == 1 && s2 == 6 && s3 == 9 && s4 ==4 && s5 == 5 && s6 == 3 && s7 == 7 && s8 == 10 && s9 == 2 && s10 == 8){
		 
		 	$(".inline").colorbox({inline:true, width:"50%",onClosed:function(){ document.getElementById("feedback" + id).style.display="block"; }});
			$("#texto_color2").html("<h4>Resultado</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, você completou as sentenças corretamente!");
			$.post( "../apresentacao/functions/chamaExCerto.php", { exercicio: 4}, function() {
						
				});
		 	
			mostrarFeedback(2);
		}else if(s1 == 0 || s2 == 0 || s3 == 0 || s4 == 0 || s5 == 0 || s6 == 0 || s7 == 0 || s8 == 0 || s9 == 0 || s10 == 0){
		
			$("#content_color2").html("<h4>Resultado</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, selecione uma opção válida em todos os campos.");
         	
			
		}else {
			$(".inline").colorbox({inline:true, width:"50%",onClosed:function(){ document.getElementById("feedback" + id).style.display="block"; }});
			$("#texto_color2").html("<h4>Resultado</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, você não completou as sentenças corretamente.");
			$.post( "../apresentacao/functions/chamaExErrado.php", { exercicio: 4}, function() {
						
				});
			
			mostrarFeedback(2);
		}
		$(".inline").colorbox({inline:true, width:"50%",onClosed:function(){ location.reload();}});
	}
	
	
	
</script>

<style type="text/css">
		
		select{
			width:  257px !important;
		}
		
		table tr td{
			font-size: 15px;
			border: 1px solid #fff;
		}
		
		.d1{

			width: 469px;
			float: left;
			margin-right: 16px;
			border-right: 1px solid #fff;
			
		}
		
		.d1 p{
			margin-top: 8px;
			margin-left: 10px;
		}
		
		.d2{
			float: left;
			padding-right: 15px;
			padding-left: 15px;
			
		
		}
		
		.d2 p{
			margin-top: 5px;
			margin-left: 10px;
		
		}
		
		.d3{
			float: left;
			padding-right: 15px;
			
			
		}
		
		.d3 p{
		
			margin-top: 5px;
			margin-left: 10px;
		}
		
		.boolean{
			margin-top: -24px;
			margin-left: 29px;
			float: right;
		}
		
				
		#feedback1, #feedback2{
			
			
		}
		
		#op1 input , #op2 input, #op3 input, #op4 input{
			float: left;
			margin-right: 10px;
			margin-left: 10px;
		}
		
			#ex2{
		clear: both;
		margin-top: 100px;
	
	}
	
	  #ex2 p{
		margin: 0 !important;
	  }

	  #parte1{
		width: 482px;
	  }

	  #parte2{
		margin-top: -20px !important;
		margin-left: 475px !important;

	  }

	  #parte3{
		margin-top: 13px !important;
	  }

		
</style>		
	<?php
	if ($_GET['ex']==1){ 

	}
	if ($_GET['ex']==2){ 

		echo '<script type="text/javascript">$("html,body").animate({scrollTop: $("#ex2").offset().top}, 1500);</script>';
	}
?>
