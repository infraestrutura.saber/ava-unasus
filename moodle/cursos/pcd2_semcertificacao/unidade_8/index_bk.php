﻿<!DOCTYPE html!>
<meta charset="utf-8">
<html lang="pt-br">
<head>

		
		<link rel="stylesheet" type="text/css" href="../css/style.css">
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<link rel="stylesheet" type="text/css" href="../lib/bootstrap/css/bootstrap-responsive.css">
		<link rel="stylesheet" type="text/css" href="../lib/bootstrap/css/bootstrap-responsive.min.css">
		<link rel="stylesheet" type="text/css" href="../lib/bootstrap/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="../lib/bootstrap/css/bootstrap.min.css">
		<script type="text/javascript" src="../lib/bootstrap/js/bootstrap.js"></script>
		<script type="text/javascript" src="../lib/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="../lib/bootstrap/js/bootstrap-dropdown.js"></script>
		<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
		<link rel="stylesheet" type="text/css" href="../lib/colorbox/colorbox.css">
		<script type="text/javascript" src="../lib/colorbox/jquery.colorbox.js"></script>
		<title>Unidade 2</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>




	
			<body>
						<script type="text/javascript">
			$(document).ready(function(){
			 $("#menu li a").mouseover(function(){
			 var index = $("#menu li a").index(this);
			 $("#menu li").eq(index).children("ul").slideDown(100);
			 if($(this).siblings('ul').size() > 0){
			 return false;
			 }
			 });
			 $("#menu li").mouseleave(function(){
			 var index = $("#menu li").index(this);
			 $("#menu li").eq(index).children("ul").slideUp(100);
			 });
			});


		</script>
				<?php 
				require_once('../../../config.php');
				require_once('../apresentacao/functions/conectar.php');	require_once('../apresentacao/functions/functions.php');conectar();
				$id=$USER->id; 
				$cpf=$USER->username;
				


				function b($id){
					echo "ID: " .$id;
				}
				
				//redirecionar caso nao tenha permissao
				if($id==""){
					header('Location: http://ufpe.unasus.gov.br/testes/homl_moodle_unasus/login/index.php');
					//header('Location: http://ufpe.unasus.gov.br/adteste/login/index.php');

				}
				
				if(empty($_GET['pagina'])){
					//se variavél pagina não existir 
					$temp = '0';
					

				}

				if ($_GET['pagina']==1){ 
					$temp = 1;
					
				}
				if ($_GET['pagina']==2){ 
					$temp = 2;
					
				}if ($_GET['pagina']==3){ 
					$temp = 3;
					
				}
				
				?>
				<div id="geral">
					<div id="fundo">
							<div id="barra_topo">
							  	<?php
										include('../includes/menu.php'); 

								?>
					</div>

					<div id="disciplina">
						<p>Cuidados na Avaliação do Ambiente Domiciliar  </p>
						<div class="info"><img src="../images/info.png" title="Título da unidade didática." alt="Título da unidade didática."></div>
					</div>
					
					<div id="usuario">
							  			  		<p>Olá, <b><span style="margin-right: 8px;"><?php echo ($USER->firstname ."</span></b>"."   |  "); ?></p>
					</div>

			<div id="barra_evolucao">
				<div id="evolucao">
					<p class="descricao">Evolução<img src="../images/info.png" title="Contabiliza a quantidade de páginas visualizadas. A proposta é lhe dar 
a visão do percentual de conteúdos já acessados." alt="Contabiliza a quantidade de páginas visualizadas. A proposta é lhe dar 
a visão do percentual de conteúdos já acessados."></p>
					
					<div class="navbar">			          
			            <div class="progress evolucao">
			               <div class="bar" style="width: <?php porcentagem_unid(2,$id); ?> "></div>
			            </div>

			            

			      	</div>
				</div>
					
					<div id="exercicios">
					<p class="descricao">Exercício(s)<img src="../images/info.png" title="Atividade(s) formativa(s) relacionada(s) a esta unidade didática. 
Servem para testar seus conhecimentos, mas não valem nota." alt="Atividade(s) formativa(s) relacionada(s) a esta unidade didática. 
Servem para testar seus conhecimentos, mas não valem nota."></p>
						<ul>
							<li>
								<?php
								  verificaExercicio($id,$cpf, 14, 3, 1);
								?>
							</li>
							
							
						</ul>
				</div>
								
			</div>


			</div> <!--daf-->


			<div id="central">
				
				<div id="conteudo">
					<?php
						if($temp==0){
							include('paginas/0.php'); 
						}

						if($temp == 1){
							include('paginas/1.php');
						}
						if($temp == 2){
							include('paginas/2.php');
						}
						if($temp == 3){
							include('paginas/3.php');
						}
					?>
				</div>

				<div id="indice">
					<div id="titulo">
						<h3>Índice</h3>
						<div class="info"><img src="../images/info.png" title="Acesso ao(s) conteúdos(s) disponível(is) nesta unidade didática." alt="Acesso ao(s) conteúdos(s) disponível(is) nesta unidade didática."></div>
					</div>

					<ul>
					      	<li><a href="index.php?pagina=0">Página inicial da unidade</a><?php verificarTopico($id,$cpf, 0, 2); ?></li>
							<?php
					      			//require_once('../apresentacao/functions/functions.php');
					      			if($_GET['pagina'] == 0) {
					      				//$id = ID do usuario , $id_unidade = ID da unidade
					      				cadastrarTopico($id,$cpf, 2);
					      			}
					      		?>
							
							
					      	<li><a href="index.php?pagina=1">Ambiente domiciliar: o que<br>observar?</a>
							<?php verificarTopico($id,$cpf, 1, 2); ?>
							</li>
							
							 <?php
							    	require_once('../apresentacao/functions/functions.php');							
									if($_GET['pagina'] == 1) {
										cadastrarTopico($id,$cpf, 2);
									}
					      		?>

					      	<li><a href="index.php?pagina=3">Exercício</a><?php verificarTopico($id,$cpf, 2, 2);?></li>
							<?php
									if($_GET['pagina'] == 2) {										
										cadastrarTopico($id,$cpf,2);
									}
								?>

							
					      	<li><a href="index.php?pagina=2">Referências</a><?php verificarTopico($id,$cpf, 2, 2);?></li>
							<?php
									if($_GET['pagina'] == 2) {										
										cadastrarTopico($id,$cpf,2);
									}
								?>

								
					</ul>
				</div>

			</div>


		</div>


	</body>

	</html>
