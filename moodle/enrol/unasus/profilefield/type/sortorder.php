<?php

   require("../../../../config.php");
   require("$CFG->dirroot/enrol/unasus/profilefield/lib.php");

  
 require_login();
   
    require_capability('enrol/unasus:profilefieldedit', get_context_instance(CONTEXT_SYSTEM), NULL, false);
  
    
  
   $profilefield_field = new profilefield_field(); 
    $param=  new object();
    $param->id= required_param('itemid',PARAM_INT);
    $param->contextid= required_param('contextid',PARAM_INT);
     $param->seq= required_param('seq',PARAM_INT);
    $param->operation= required_param('operation',PARAM_TEXT);
    $param->categoryid= required_param('categoryid',PARAM_TEXT);
  
   add_to_log(1, 'enrol_unasus', 'rofilefieldedit', "enrol/unasus/profilefield/sortorder.php?id $param->id", getremoteaddr());
     
    $profilefield_field->update_order($param->categoryid,$param->seq,$param->operation);
    header('Location: '.$CFG->wwwroot.'/enrol/unasus/profilefield/manage.php?contextid='.$param->contextid) ;
?>