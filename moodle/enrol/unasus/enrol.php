<?php
require("$CFG->dirroot/enrol/unasus/manage.php");
require("$CFG->dirroot/enrol/unasus/log/lib.php");
require("$CFG->dirroot/enrol/unasus/role/lib.php");
require("$CFG->dirroot/enrol/unasus/role/data/lib.php");
require("$CFG->dirroot/enrol/unasus/role/cbo/lib.php");
require("$CFG->dirroot/enrol/unasus/message/lib.php");
require("$CFG->dirroot/enrol/unasus/lib/context.php");
require("$CFG->dirroot/enrol/unasus/profilefield/category/lib.php");
require("$CFG->dirroot/enrol/unasus/profilefield/lib.php");
require("$CFG->dirroot/enrol/unasus/profilefield/data/lib.php");
require("$CFG->dirroot/enrol/unasus/profilefield/info_form.php");
require("$CFG->dirroot/enrol/unasus/arouca.php");
require("$CFG->dirroot/enrol/unasus/cpf.php");
require("$CFG->dirroot/enrol/unasus/lib/configcontext.php");
class enrolment_plugin_unasus {
   var $strloginto=null;
   var $strcourses=null;
   var $navigation=null;
   var $navlinks=null;
   var $contextid=null;
   var $contextid_role=null;
   
   var $role=null;
   var $message=null;
   var $message_dto=null;
   var $arouca_cbos=null; //cbo de usu�rio consultado no arouca
   var $cbo_role=null; //cbo que s�o requisitos da matr�cula
   var $cbo_enrol=null; // cbo homologado para mat�cula. Resulta da compara��o do cbo do usu�rio no arouca e lista de cbo que s�o requisitos da matr�cula
   
   var $enrol_manage=null; 
   var $enrol_log=null;
   public function config_form( $formdata ){
   		global $CFG;
       include ("$CFG->dirroot/enrol/unasus/config.html");
    }
    public function process_config( $formdata ){
        return true;
    }
    
    public function setup_enrolments( $user ){
          return;
    }
    
    public function print_entry($course){
        
    	// 1� passo - verificar a validade do cpf e verificar se existe  regra definida no curso
    	$this->check_role_exist($course);
        
    	$this->validate_cpf($course);
    	//2� passo - verificar se existe algum hist�rico de solicita��o
    	
    	//3� passo - apresentar texto de aprensenta��o e formul�rio de cadastro
    	$this->show_enrol_information($course);
    	
    	//4� passo - finalize processo caso o usu�rio cancelar a solicita��o
    	$this->cancel_request();
    	
    	//5� passo - caso us�rio confirme a inscri��o cadastre dados do formul�rio na base
    	$this->save_data_info_form();
        
    	//6� passo - caso us�rio confirme a inscri��o verifique o cbo no arouca
    	$this->get_arouca_cbo($course);
    	
    	//7� passo - caso cbo n�o for confirmado, registre a solicita��o e exibe mensagem de recusa
    	$this->no_cbo_in_arouca($course);
       
    	$this->no_cbo_for_enrol($course);
    	
    	//8� passo - caso cbo  for confirmado, registre a solicita��o e cbo que foi homologado, efetive a matr�cula  e exibe mensagem de aceita��o
    	 $this->process_enrol($course);
    }
    public function check_group_entry( $courseid, $password ){
       
    }
    
    public function get_access_icons( $course ){
        return 'put icons here';
    }
    
    public function cron(){
        $this->log = 'Print this in the cron log unasus!';
        $this->review_enrol_by_cron();
    }
    
    public function check_role_exist($course){
        global $USER;
    	$exist_role=$this->get_role()->exist_by_contextid($this->get_contextid($course->id));
    	if(!$exist_role){
                //registrar solicita��o da matr�cula
    		$enrol_dto= new object();
    		$enrol_dto->userid=$USER->id;
    		$enrol_dto->courseid=$course->id;
    		$enrol_dto->rolecontextid=$this->get_contextid($course->id);
    		$enrol_dto->time=time();
    		//$enrol_dto->timeupdate=null;
    		$enrol_dto->status=enrol_unasus_manage::$STATUS_ENROL_NOT_ACCEPT;
    		$enrol_dto->statusinfo=enrol_unasus_manage::$STATUS_ENROL_TYPE_MANUAL;
    		$enrol_dto->statuscause=enrol_unasus_log::$STATUS_NO_ROLE_ENROL_IN_DATABASE;
    		//$enrol_dto->author=NULL;
    		//$enrol_dto->justify=NULL;
    		//$enrol_dto->description=NULL;
    		$this->get_enrol_manage()->save($enrol_dto);
    		
    		//registrar log de solicita��o
    		$log_dto = new object();
    		$log_dto->userid=$USER->id;
    		$log_dto->courseid=$course->id;
    		$log_dto->rolecontextid=$this->get_contextid($course->id);
    		$log_dto->time=time();
    		$log_dto->status=enrol_unasus_log::$STATUS_NO_ROLE_ENROL_IN_DATABASE;
    		//$log_dto->error=null;
    		$log_dto->type=enrol_unasus_log::$TYPE_EXEC_MANUAL;
    		
    		$this->get_enrol_log()->save($log_dto);
    		 print_header($this->get_strloginto($course), $course->fullname, $this->get_navigation($course));
        	print_box(get_string('role_norole_defined','enrol_unasus'));
            print_footer();
            exit;
    	}
    }
    
    public function show_enrol_information($course){
		if (empty($_GET['confirm']) && empty($_GET['cancel'])){
			$msg=$this->get_message_dto($course->id);
		
		 	print_header($this->get_strloginto($course), $course->fullname, $this->get_navigation($course));
        	print_box($msg->presentation);
        	$this->get_form_info($course);
            print_footer();
            exit;
		}
			
		}
		
      public function get_strloginto($course){
    	if($this->strloginto==null){
    		$this->strloginto=get_string('loginto', '', $course->shortname);
    	}
    	return $this->strloginto;
      }
        public function get_strcourses(){
    	if($this->strcourses==null){
    		$this->strcourses= get_string('courses');
    	}
    	return $this->strcourses;
      }
    public function get_navigation($course){
    	if($this->navigation==null){
    		$navlinks = array();
    		$navlinks[] = array('name' => $this->strcourses, 'link' => ".", 'type' => 'misc');
    		$navlinks[] = array('name' => $this->get_strloginto($course), 'link' => null, 'type' => 'misc');
    		$this->navigation = build_navigation($navlinks);
    		
    	}
    	return $this->navigation;
    }
    public function get_contextid($courseid){
    	if($this->contextid==null){
    		$this->contextid=-1;
    		$context_levels=$this->get_enrol_manage()->get_context_level_decreasing($courseid);
    		if(!empty($context_levels)){
    			 foreach ($context_levels as $ctx){ 
    			 	if($this->get_role()->exist_by_contextid($ctx)){
    			 		$this->contextid=$ctx;
    			 		break;
    			 	}
    			 }
    			
    		}
    		
    	}
    	return  $this->contextid;
    }
    public function get_contextid_role($courseid){
    	if($this->contextid_role==null){
    		$this->contextid_role= $this->get_contextid($courseid);
    	}
    	return  $this->contextid_role;
    }
    public function get_role(){
    	if($this->role==null) $this->role=new role();
    	return $this->role;
    }
    
    public function get_message(){
    	if($this->message==null) $this->message= new message();
    	return $this->message;
    }
     public function get_form_info($course){
     	//$contextid=$this->get_contextid($course->id);
    	$form= new infofield_form();
    	$form->display();
   	
    }
    public function get_message_dto($courseid){
    	if($this->message_dto==null) $this->message_dto= $this->get_message()->get_by_contextid($this->get_contextid($courseid));
    	return $this->message_dto;
    }
    
    public function save_data_info_form(){
    
    	global $USER;
    	if (!empty($_GET['confirm']) && empty($_GET['cancel'])){
    		//echo "SALVAR<BR>";
    		foreach($_GET as $keyname => $value) {
    		if(is_numeric($keyname)){
    			$param=  new object();
    			$param->userid=$USER->id;
    			$param->fieldid=$keyname;
    			$param->data=$value;
    			$data_infofield =new data_infofield();
    			$data_infofield->create($param);
    			//echo("$keyname --&gt; $value.<br />\n");
    		}
			
			}
    	}
    	
    }
     public function cancel_request(){
     	global $CFG;
    	if (!empty($_GET['confirm']) && !empty($_GET['cancel'])){
    		 redirect($CFG->wwwroot);
    	}
    }
    public function validate_cpf($course){
    
    	global $USER;
    	global $CFG;
    	$cpf=new unasus_cpf(); 
    	if(!$cpf->validate($this->get_enrol_manage()->get_cpf($USER->id))){
    		
    		//registrar solicita��o da matr�cula
    		$enrol_dto= new object();
    		$enrol_dto->userid=$USER->id;
    		$enrol_dto->courseid=$course->id;
    		$enrol_dto->rolecontextid=$this->get_contextid($course->id);
    		$enrol_dto->time=time();
    		//$enrol_dto->timeupdate=null;
    		$enrol_dto->status=enrol_unasus_manage::$STATUS_ENROL_NOT_ACCEPT;
    		$enrol_dto->statusinfo=enrol_unasus_manage::$STATUS_ENROL_TYPE_AUTOMATIC;
    		$enrol_dto->statuscause=enrol_unasus_log::$STATUS_CPF_NOT_VALID;
    		//$enrol_dto->author=NULL;
    		//$enrol_dto->justify=NULL;
    		//$enrol_dto->description=NULL;
    		$this->get_enrol_manage()->save($enrol_dto);
    		
    		//registrar log de solicita��o
    		$log_dto = new object();
    		$log_dto->userid=$USER->id;
    		$log_dto->courseid=$course->id;
    		$log_dto->rolecontextid=$this->get_contextid($course->id);
    		$log_dto->time=time();
    		$log_dto->status=enrol_unasus_log::$STATUS_CPF_NOT_VALID;
    		//$log_dto->error=null;
    		$log_dto->type=enrol_unasus_log::$TYPE_EXEC_MANUAL;
    		
    		$this->get_enrol_log()->save($log_dto);
    		
    		print_header($this->strloginto, $course->fullname, $this->get_navigation($course));
        	print_box(get_string('msg_cpf_not_valid','enrol_unasus'));
        	print_continue($CFG->wwwroot, $return=false);
        	print_footer();
            exit;
    	}
    	
    }
    
   
	public function get_arouca_cbo($course){
    	if (!empty($_GET['confirm']) && empty($_GET['cancel'])){
    		global $USER;
    		global $CFG;
			$arouca=new arouca();
			$this->arouca_cbos=$arouca->get_cbo_arouca($this-> get_enrol_manage()->get_cpf($USER->id));
			if($arouca->WS_AROUCA_FAILURE){
				
					
    		 	//registrar solicita��o da matr�cula
    			$enrol_dto= new object();
    			$enrol_dto->userid=$USER->id;
    			$enrol_dto->courseid=$course->id;
    			$enrol_dto->rolecontextid=$this->get_contextid($course->id);
    			$enrol_dto->time=time();
    			$enrol_dto->status=enrol_unasus_manage::$STATUS_ENROL_NOT_ACCEPT;
    			$enrol_dto->statusinfo=enrol_unasus_manage::$STATUS_ENROL_TYPE_AUTOMATIC;
    			$enrol_dto->statuscause=enrol_unasus_log::$STATUS_CONNECTION_AROUCA_FAILED;
	    		$this->get_enrol_manage()->save($enrol_dto);
    		
    			//registrar log de solicita��o
    			$log_dto = new object();
    			$log_dto->userid=$USER->id;
    			$log_dto->courseid=$course->id;
    			$log_dto->rolecontextid=$this->get_contextid($course->id);
    			$log_dto->time=time();
    			$log_dto->status=enrol_unasus_log::$STATUS_CONNECTION_AROUCA_FAILED;
    			$log_dto->type=enrol_unasus_log::$TYPE_EXEC_MANUAL;
    			$log_dto->error=addslashes($arouca->WS_AROUCA_FAILURE_MSG);
    			$this->get_enrol_log()->save($log_dto);
    			
				print_header($this->strloginto, $course->fullname, $this->get_navigation($course));
        		print_box(get_string('msg_connhttp_arouca_failed','enrol_unasus'));
        		print_continue($CFG->wwwroot, $return=false);
        		print_footer();
           		exit;
			}
		}
			
	}
	
	public function no_cbo_in_arouca($course){
		
		if (!empty($_GET['confirm']) && empty($_GET['cancel'])){
    		
    		global $CFG;
    		global $USER;
    		$msg=$this->get_message_dto($course->id);
    		$cbos_arouca_string_list=$this->conver_array_to_strig($this->arouca_cbos);
    		if(!$this->exist_value_in_array($this->arouca_cbos)){
		   
		   		//registrar solicita��o da matr�cula
    			$enrol_dto= new object();
    			$enrol_dto->userid=$USER->id;
    			$enrol_dto->courseid=$course->id;
    			$enrol_dto->rolecontextid=$this->get_contextid($course->id);
    			$enrol_dto->time=time();
    			$enrol_dto->status=enrol_unasus_manage::$STATUS_ENROL_NOT_ACCEPT;
    			$enrol_dto->statusinfo=enrol_unasus_manage::$STATUS_ENROL_TYPE_AUTOMATIC;
    			$enrol_dto->statuscause=enrol_unasus_log::$STATUS_NO_COBO_IN_AROUCA;
	    		$this->get_enrol_manage()->save($enrol_dto);
    		
    			//registrar log de solicita��o
    			$log_dto = new object();
    			$log_dto->userid=$USER->id;
    			$log_dto->courseid=$course->id;
    			$log_dto->rolecontextid=$this->get_contextid($course->id);
    			$log_dto->time=time();
    			$log_dto->status=enrol_unasus_log::$STATUS_NO_COBO_IN_AROUCA;
    			$log_dto->type=enrol_unasus_log::$TYPE_EXEC_MANUAL;
                        
    			$this->get_enrol_log()->save($log_dto);
				print_header($this->strloginto, $course->fullname, $this->get_navigation($course));
        		print_box($msg->disapproval);
        		print_continue($CFG->wwwroot, $return=false);
        		print_footer();
           		exit;
			}
		}
			
	}
	public function no_cbo_for_enrol($course){
		if (!empty($_GET['confirm']) && empty($_GET['cancel'])){
    		
    		global $CFG;
    		global $USER;
    		$msg=$this->get_message_dto($course->id);
    		$cbo_enrol=$this->get_cbo_enrol($course->id);
    		
    	    if(!$this->exist_value_in_array($cbo_enrol)){
		   		
		   		//registrar solicita��o da matr�cula
    			$enrol_dto= new object();
    			$enrol_dto->userid=$USER->id;
    			$enrol_dto->courseid=$course->id;
    			$enrol_dto->rolecontextid=$this->get_contextid($course->id);
    			$enrol_dto->time=time();
    			$enrol_dto->status=enrol_unasus_manage::$STATUS_ENROL_NOT_ACCEPT;
    			$enrol_dto->statusinfo=enrol_unasus_manage::$STATUS_ENROL_TYPE_AUTOMATIC;
    			$enrol_dto->statuscause=enrol_unasus_log::$STATUS_NO_CBO_IN_ROLE;
	    		$this->get_enrol_manage()->save($enrol_dto);
    		
    			//registrar log de solicita��o
    			$log_dto = new object();
    			$log_dto->userid=$USER->id;
    			$log_dto->courseid=$course->id;
    			$log_dto->rolecontextid=$this->get_contextid($course->id);
    			$log_dto->time=time();
    			$log_dto->status=enrol_unasus_log::$STATUS_NO_CBO_IN_ROLE;
    			$log_dto->type=enrol_unasus_log::$TYPE_EXEC_MANUAL;
                        $this->get_enrol_log()->save($log_dto);
				print_header($this->strloginto, $course->fullname, $this->get_navigation($course));
        		print_box($msg->disapproval);
        		print_continue($CFG->wwwroot, $return=false);
        		print_footer();
           		exit;
			}
		}
			
	}
	public  function process_enrol($course){
		if (!empty($_GET['confirm']) && empty($_GET['cancel'])){
    		$msg=$this->get_message_dto($course->id);
    		$cbo_enrol=$this->get_cbo_enrol($course->id);
    		global $CFG;
    		global $USER;
    		 if($this->exist_value_in_array($cbo_enrol)){
    		 	$result_enrol=$this->get_enrol_manage()->save_enrol_course($USER->id,$course->id);
    		 	//$result_enrol=enrol_into_course($course,$USER, 'unasus');
    		 	
    		 	//registrar solicita��o da matr�cula
    			$enrol_dto= new object();
    			$enrol_dto->userid=$USER->id;
    			$enrol_dto->courseid=$course->id;
    			$enrol_dto->rolecontextid=$this->get_contextid($course->id);
    			$enrol_dto->time=time();
    			$enrol_dto->status=enrol_unasus_manage::$STATUS_ENROL_ACCEPT;
    			$enrol_dto->statusinfo=enrol_unasus_manage::$STATUS_ENROL_TYPE_AUTOMATIC;
    			$enrol_dto->statuscause=enrol_unasus_log::$STATUS_ENROL_COMPLETED_WITH_SUCCESS;
	    		$enrol_dto->id=$enrol_dto->id=$enrol_dto->id=$this->get_enrol_manage()->save($enrol_dto);
    			
    			//registrar cbo de matricula homologado
    			$role_data = new unasus_role_data();
    			foreach ($cbo_enrol as $cbo){ 
    				$role_data_dto= new object();
    				$role_data_dto->enrolid=$enrol_dto->id;
    				$role_data_dto->type=unasus_role_data::$TYPE_CBO;
    				$role_data_dto->value=$cbo;
    				$role_data->save($role_data_dto);
    			}
    			
    			//registrar log de solicita��o
    			$log_dto = new object();
    			$log_dto->userid=$USER->id;
    			$log_dto->courseid=$course->id;
    			$log_dto->rolecontextid=$this->get_contextid($course->id);
    			$log_dto->time=time();
    			$log_dto->status=enrol_unasus_log::$STATUS_ENROL_COMPLETED_WITH_SUCCESS;
    			$log_dto->type=enrol_unasus_log::$TYPE_EXEC_MANUAL;
    			$this->get_enrol_log()->save($log_dto);
    			print_header($this->strloginto, $course->fullname, $this->get_navigation($course));
        		print_box($msg->approval);
        		print_continue("$CFG->wwwroot/course/view.php?id=$course->id", $return=false);
        		print_footer();
           		exit;
    		 }
		}
	}
	
	public function get_cbo_role($courseid){
		if($this->cbo_role==null){
			$this->cbo_role=$this->get_role()->get_cbo_by_contextid($this->get_contextid($courseid));
		}
		
		return $this->cbo_role; 
    	
	}
	
		public function get_cbo_enrol($courseid){
			
		if($this->cbo_enrol==null){
			$unasus_cbo=new unasus_cbo();
			$this->cbo_enrol= $unasus_cbo->get_cbo_enrol($this->conver_array_to_strig($this->arouca_cbos),$this->get_cbo_role($courseid));
		
		}
		
		return $this->cbo_enrol; 
    	
	}
	public function get_enrol_manage(){
		if($this->enrol_manage==null){
			$this->enrol_manage=new enrol_unasus_manage();
		}
		return $this->enrol_manage;
		
	}
		public function get_enrol_log(){
		if($this->enrol_log==null){
			$this->enrol_log=new enrol_unasus_log();
		}
		return $this->enrol_log;
		
	}
	
	public function conver_array_to_strig($list){
    		$cont=0;
    		
    		$result="";
    		if(empty($list)) return $result;
    		 foreach ($list as $v){ 
    		 	$cont++;
    		 	if($cont==1){$result=$v;}
    		 	else{$result.=",".$v."";}
    		 	
    		 }
    		 return $result;
    	}
    	
    	
	public  function exist_value_in_array($list){
		
		if(!is_array($list)) return FALSE;
		$exist_value=FALSE;
		 foreach ($list as $v){ 
		 	if(!empty($v)){
		 		$exist_value=TRUE;
		 		break; 
		 	}
		 }
		return $exist_value;
	}
	public  function review_enrol_by_cron(){
		
	   // 1�  extrair lista de contexto que a matr�cula deve ser revisada
	  $list_rolecontextid= $this->get_enrol_log()->get_rolecontextid();
         
	   if(empty($list_rolecontextid)) return NULL;
	    
	  
	   // 2�  fazer loop de cada contexto
	  
	   $configcontext=new configcontext();
	   $contextid=NULL;
	    foreach ($list_rolecontextid as $rc){
	    	
        	  $contextid=$rc->rolecontextid;
        	
        	 // 2.1�  verificar se o cron pode ser processado pela config do contexto
        	   
	  
        	$process_cron=$this->review_enrol_process_cron($contextid);
        	
        	// 2.2�  extrair lista de log que deve ser reprocessado
        	   $list_log= $this->review_enrol_get_log($contextid,$process_cron);
	   		
	   		//2.2� - processar revis�o para cada item do log
	   		if(!empty($list_log)){
	   			 foreach ($list_log as $log){
	   			 	 //2.2.1� - extrair cpf
	   			 	 $cpf=$this->get_cpf_review_enrol($log);
	   			 	 
	   			 	//2.2.2� - extrair cbo no arouca
	   			 	$cbo_arouca=$this->get_cbo_in_arouca_review_enrol($log,$cpf);
	   			 	
	   			 		//2.2.3� - extrair regra da matr�cula  
	   			 	$cbo_role=$this->get_cbo_role_review_enrol($log);
	   			 	 
	   			 	//2.2.4� - extrair cbo homologado
	   			 	$cbo_enrol=$this->get_cbo_enrol_review($log,$cbo_arouca,$cbo_role);
	   			 	 
	   			 	//2.2.5� - processar inscri��o
	   			 	$result_enrol=$this->process_enrol_review($log,$cbo_enrol);
	   			 	
	   			 	//2.2.6� - enviar mensagem
	   			 	$result_msg=$this->send_message_enrol_approve_review($log,$result_enrol);
	   			 	
	   			 	
	   			 } //fim  foreach ($list_log as $log)
	   		}//if(!empty($list_log))
	   		
	   		$action="cron_".$contextid;
	   		 add_to_log(1, 'unasus', $action, '', getremoteaddr());
	    }// fim foreach ($list_rolecontextid as $rc)
	   
	   
	}   
        
       public  function review_enrol_process_cron($contextid){
       		  $configcontext=new configcontext();
       		  
       		  //verificar se processamento autom�tico est� configurado
       		  $process_mode=$configcontext->get_by_contextid($contextid,configcontext::$PROCESSING_MODE);
            
                
                 
       		  if(empty($process_mode)) {return FALSE;}
                  else if(empty($process_mode->value)){return FALSE;}
                  else if($process_mode->value=='manual'){return FALSE;}
       		 
       		  
                       
       		  //extrair intervalo de cron em hora da base de dados pelo contexto
       		  $time_cron_hour_frequence=$configcontext->get_by_contextid($contextid,configcontext::$CRON_PROCESS_FREQUENCY);
       		
             
        	if(empty($time_cron_hour_frequence->value)) return TRUE;
        	
        	//2.2� - extrair data do �ltimo cron processado no log 
        	
        	$count_hour_last_exec_cron=$this->get_enrol_manage()->get_count_hour_last_exec_cron($contextid);
        	
        	if(empty($count_hour_last_exec_cron) || $count_hour_last_exec_cron==-1)return TRUE;
        	
        	$process_cron=FALSE;
        	
        	if($count_hour_last_exec_cron>$time_cron_hour_frequence->value	){$process_cron=TRUE;}
        	else{$process_cron=FALSE;}
        	
        	return $process_cron;
        	
       }
       public  function review_enrol_get_log($contextid,$process_cron){
       		  if(!$process_cron) return null;
       		 
       		  //extrair config de status que deve ser reprocessado
       		  $configcontext=new configcontext();
       		  $status_review=$configcontext->get_value_by_contextid($contextid,configcontext::$CRON_PROCESS_ITEM);
       		
       		  if(empty($status_review)) return null;
       		  
       		  $list_log= $this->get_enrol_log()->get_by_status($contextid,$status_review);
       		  return $list_log;
       }
       
        public function get_cpf_review_enrol($log){
        		$cpf_number= $this-> get_enrol_manage()->get_cpf($log->userid);
    			$cpf=new unasus_cpf(); 
    	
    			if(!$cpf->validate($cpf_number)){
    				//registrar log de solicita��o
    				$log->time=time();
    				$log->status=enrol_unasus_log::$STATUS_CPF_NOT_VALID;
    				$log->type=enrol_unasus_log::$TYPE_EXEC_CRON;
		    		$this->get_enrol_log()->edit($log);
		    		return null;
    			}
    			return $cpf_number;
    	}
    	
    	public function get_cbo_in_arouca_review_enrol($log,$cpf){
			if(empty($cpf))return null;
			
			$arouca=new arouca();
			$list_cbo=$arouca->get_cbo_arouca($cpf);
			
			if($arouca->WS_AROUCA_FAILURE){
				//registrar log de solicita��o
    			$log->time=time();
    			$log->status=enrol_unasus_log::$STATUS_CONNECTION_AROUCA_FAILED;
    			$log->type=enrol_unasus_log::$TYPE_EXEC_CRON;
		    	$this->get_enrol_log()->edit($log);
		    	return null;
			}	
			
			//se n�o tiver cbo no arauca
			if(!$this->exist_value_in_array($list_cbo)){
				//registrar log de solicita��o
    			$log->time=time();
    			$log->status=enrol_unasus_log::$STATUS_NO_COBO_IN_AROUCA;
    			$log->type=enrol_unasus_log::$TYPE_EXEC_CRON;
		    	$this->get_enrol_log()->edit($log);
		    	return null;
			}
			return $list_cbo;
        }
			
	public function get_cbo_role_review_enrol($log){
		 $cbo_role=$this->get_role()->get_cbo_by_contextid($log->rolecontextid);
		 
		 if(empty($cbo_role)){
				//registrar log de solicita��o
    			$log->time=time();
    			$log->status=enrol_unasus_log::$STATUS_NO_ROLE_ENROL_IN_DATABASE;
    			$log->type=enrol_unasus_log::$TYPE_EXEC_CRON;
		    	$this->get_enrol_log()->edit($log);
		    	return null;
			}
		return $cbo_role;	
	}
     public function get_cbo_enrol_review($log,$cbo_arouca,$cbo_role){
     	$unasus_cbo=new unasus_cbo();
     	$cbo_enrol= $unasus_cbo->get_cbo_enrol($this->conver_array_to_strig($cbo_arouca),$cbo_role);
     	if(!$this->exist_value_in_array($cbo_enrol)){
     		//registrar log de solicita��o
    			$log->time=time();
    			$log->status=enrol_unasus_log::$STATUS_NO_CBO_IN_ROLE;
    			$log->type=enrol_unasus_log::$TYPE_EXEC_CRON;
		    	$this->get_enrol_log()->edit($log);
		    	return null;
     	}
     	return $cbo_enrol;
     }
     
     public  function process_enrol_review($log,$cbo_enrol){
     
     	if(empty($cbo_enrol))return FALSE;
     	
      	$result_enrol=$this->get_enrol_manage()->save_enrol_course($log->userid,$log->courseid);
     
      
      
    	if($result_enrol>0){
    		//registrar solicita��o da matr�cula
    		$enrol_dto= new object();
    		$enrol_dto->userid=$log->userid;
    		$enrol_dto->courseid=$log->courseid;
    		$enrol_dto->rolecontextid=$log->rolecontextid;
    		$enrol_dto->time=time();
    		$enrol_dto->status=enrol_unasus_manage::$STATUS_ENROL_ACCEPT;
    		$enrol_dto->statusinfo=enrol_unasus_manage::$STATUS_ENROL_TYPE_AUTOMATIC;
    		$enrol_dto->statuscause=enrol_unasus_log::$STATUS_ENROL_COMPLETED_WITH_SUCCESS;
	   		$enrol_dto->id=$enrol_dto->id=$enrol_dto->id=$this->get_enrol_manage()->save($enrol_dto);
	    
	    	//registrar cbo de matricula homologado
	    	
    		$role_data = new unasus_role_data();
    		foreach ($cbo_enrol as $cbo){ 
    			$role_data_dto= new object();
    			$role_data_dto->enrolid=$enrol_dto->id;
    			$role_data_dto->type=unasus_role_data::$TYPE_CBO;
    			$role_data_dto->value=$cbo;
    			$role_data->save($role_data_dto);
    		}
	    	//registrar log de solicita��o
	    	$log->time=time();
    		$log->status=enrol_unasus_log::$STATUS_ENROL_COMPLETED_WITH_SUCCESS;
    		$log->type=enrol_unasus_log::$TYPE_EXEC_CRON;
			$this->get_enrol_log()->edit($log);
    		return TRUE;
    		
    	}else if($result_enrol==0){ //matricula j� existe
    	   	//registrar log de solicita��o
	    	$log->time=time();
    		$log->status=enrol_unasus_log::$STATUS_ENROL_COMPLETED_WITH_SUCCESS;
    		$log->type=enrol_unasus_log::$TYPE_EXEC_CRON;
			$this->get_enrol_log()->edit($log);
    		return FALSE;
    	}
    	return FALSE;
	}
	public  function send_message_enrol_approve_review($log,$result_enrol){
		if(!$result_enrol) return FALSE;
	
       	$courseinfo=get_record("course", "id", $log->courseid);
       	$siteinfo=get_record("course", "id", 1);
       	$userinfo=get_record("user", "id", $log->userid);
       	$admininfo=get_record("user", "id", 2);
       	$a->site = $siteinfo->shortname;
        $a->course = $courseinfo->shortname;        
        $subject = get_string('message_enrol_aproved_subject','enrol_unasus',$a);
        $message= new message();
        $msg=$message->get_by_contextid($log->rolecontextid);
        $body = $msg->autreviewapproval;
        $result=email_to_user($userinfo, $siteinfo->shortname, $subject, $body);
        return $result;
	}   							
}
?>
