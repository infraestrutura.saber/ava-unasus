<?php
require("$CFG->dirroot/enrol/unasus/wskey/lib.php");
class arouca {
		//controle de falha de websevice
		var $WS_AROUCA_FAILURE=FALSE;
		var $WS_AROUCA_FAILURE_MSG='';
		var $wsdto=NULL;	
		//homologacação
		//var $WSDL="https://150.164.2.204:9443/plataformaarouca/services/AroucaWebService?wsdl";
		
		//produção
	//  var $WSDL="https://arouca.unasus.gov.br:8443/plataformaarouca/services/AroucaWebService?wsdl";
		
	//	var $CERTIFICADO="/enrol/unasusem/certs/seunasuskeystore.pem";
	//	var $KEY="unasuskey";
		
	function get_cbo_arouca($cpf){
    	$this->WS_AROUCA_FAILURE=FALSE;
    	$this->WS_AROUCA_FAILURE_MSG='';
    	global $CFG;
     	$cbos = array();
     	
    	try {
       		 global $CFG;
        	$client = new SoapClient($this->get_wsdto()->wsdl,    
                array('trace'=>true,
                      'cache_wsdl'=>WSDL_CACHE_NONE,
                      'exceptions'=>true,
                      'local_cert' =>$CFG->dataroot."/".$this->get_wsdto()->filekey,
                      'passphrase' =>$this->get_wsdto()->wskey));
         
         	try {
          		$res=$client->recuperarPessoa(array('cpf' =>$cpf));
           		$x= NULL;
           		if(!empty($res->return)){$x=$res->return;}
           		if (!empty($x) && is_array($x->cbos)) {
           			 foreach($x->cbos as $chave){
               		array_push($cbos,$chave->codigo);
               		}
            	} else {
              		if (!empty($x))array_push($cbos,$x->cbos->codigo);
            	}
                
             }
        catch (Exception $e) {
            $this->WS_AROUCA_FAILURE=TRUE;
            $this->WS_AROUCA_FAILURE_MSG=$e->getMessage();
   	 }
   
    } catch (Exception $e) {
              $this->WS_AROUCA_FAILURE=TRUE;
            $this->WS_AROUCA_FAILURE_MSG=$e->getMessage();
        }
      return $cbos;
  }
	
	function  get_wsdto(){
		if($this->wsdto==NULL){
			$wskey =new wskey();
			$this->wsdto=$wskey->get_arouca();
		}
		return $this->wsdto;
	}	

}
?>
