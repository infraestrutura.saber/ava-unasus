<?php
class ingr_conc_sync_couse {
	public static $STATUS_INGRESSANTE_INSCRITO_MOODLE =11;
	public static $STATUS_INGRESSANTE_SINCRONIZADO_AROUCA =12;
	public static $STATUS_INGRESSANTE_SINCRONIZADO_AROUCA_FALHA =13;
	
	public static $STATUS_CONCLUINTE_HOMOLOGADO_MOODLE =21;
	public static $STATUS_CONCLUINTE_SINCRONIZADO_AROUCA =22;
	public static $STATUS_CONCLUINTE_SINCRONIZADO_AROUCA_FALHA =23;
	public static $STATUS_CONCLUINTE_HOMOLOGADO_GESTOR =25;
	
	public static $STATUS_CERTIFICADO_EMITIDO =31;
	public static $STATUS_CERTIFICADO_ENVIADO =32;
	
	public static $STATUS_CERTIFICADO_EMITIDO_AROUCA_FALHA =33;
	
	
	
	function salvar($dto) {
 		return insert_record('synciccarouca_course', $dto);
 	}
 	function editar($dto) {
 		return update_record('synciccarouca_course', $dto);
 	}
 	function delete_by_id($id) {
 		return delete_records('synciccarouca_course','id',$id);
 	}
 	
 	function pesquisa_countar($param) {
 		global $CFG;
				
				$wsql=$this->pesquisa_param($param);
			$sql ="SELECT COUNT(s.id) as qreg FROM {$CFG->prefix}synciccarouca_course s INNER JOIN {$CFG->prefix}user u ON u.id=s.mdluserid  LEFT JOIN {$CFG->prefix}user ua ON ua.id=s.mdluseridaut  INNER JOIN {$CFG->prefix}course c ON c.id=s.mdlcourseid WHERE s.mdlcourseid >1 $wsql ";
			$resp=get_record_sql($sql);
			return $resp->qreg;
 	}
 	function pesquisa($param) {
 		global $CFG;
			$wsql=$this->pesquisa_param($param);
			$sql ="SELECT s.id,u.firstname, u.lastname, u.email, u.idnumber, u.username,ua.firstname as firstnameua, ua.lastname as lastnameua, c.fullname,s.arcidoferta,s.status,s.dataingresso,s.dataconclusao,s.urlcertificado,s.faultstringwsarouca,s.mdluseridaut FROM {$CFG->prefix}synciccarouca_course s INNER JOIN {$CFG->prefix}user u ON u.id=s.mdluserid  LEFT JOIN {$CFG->prefix}user ua ON ua.id=s.mdluseridaut INNER JOIN {$CFG->prefix}course c ON c.id=s.mdlcourseid WHERE s.mdlcourseid >1  $wsql ORDER BY s.dataingresso DESC";
			//echo $sql;
			
			return get_records_sql($sql,$param->page*$param->perpage, $param->perpage);
 	}
 

    	function pesquisa_param($param){
    		$wsql="";
    		if(!empty($param->courseid)){
    			$wsql.= "AND s.mdlcourseid= $param->courseid ";
    		}
    		
    	if(!empty($param->userid)){
    			$wsql.= "AND s.mdluserid= $param->userid ";
    		}
    	
    	if(!empty($param->status)){
    			$wsql.= "AND s.status= $param->status ";
    		}
    	
    	return $wsql;
    	}
 	function existe_pelo_id($id) {
 		
 	}
function get_status_tabela_dominio(){
		
		$options = array();
		$options['']="  ----  ";
		$options[11]=get_string('status_ingres_inscrito_moodle','report_synciccarouca');
		$options[12]=get_string('status_ingres_sinc_arouca','report_synciccarouca');
		$options[13]=get_string('status_ingres_sinc_falhaws_arouca','report_synciccarouca');
		
		$options[21]=get_string('status_concl_homol_moodle','report_synciccarouca');
		$options[25]=get_string('status_concl_homol_gestor','report_synciccarouca');
		$options[22]=get_string('status_concl_sinc_arouca','report_synciccarouca');
		$options[23]=get_string('status_concl_sinc_falhaws_arouca','report_synciccarouca');
		
		$options[31]=get_string('status_concl_cert_emitido','report_synciccarouca');
		$options[32]=get_string('status_concl_cert_enviado','report_synciccarouca');
		$options[33]=get_string('status_concl_cert_falhaws_arouca','report_synciccarouca');
		
		return 	$options ;
		}
	
	
	function recuperarMatriculaHomologadaNaoRegistrado($idCurso){
		global $CFG;
		$sql ="SELECT DISTINCT userid,created FROM {$CFG->prefix}report_synciccarouca WHERE status=1 AND courseid=$idCurso AND userid NOT IN (SELECT mdluserid FROM {$CFG->prefix}synciccarouca_course WHERE mdlcourseid=$idCurso )";
		return get_records_sql($sql); 
	}
function get_student_profile() {
 		global $CFG;
		$sql ="SELECT value FROM {$CFG->prefix}config WHERE name = 'gradebookroles' ";
		$r=get_record_sql($sql);
		return $r->value;
 	}
	function recuperarMatriculaNoCursoNaoRegistrado($idCurso){
		global $CFG;
		
		$wsqlr=$this->get_student_profile();
		if(!empty($wsqlr)){$wsqlr=" AND rs.roleid IN ($wsqlr) ";}
		else {$wsqlr=" AND rs.roleid IN (-1) ";}
		
		$sql ="SELECT DISTINCT rs.userid, rs.timestart AS created,rs.enrol  FROM {$CFG->prefix}role_assignments rs  INNER JOIN {$CFG->prefix}context e ON rs.contextid=e.id WHERE e.contextlevel=50 $wsqlr  AND e.instanceid= $idCurso AND rs.userid NOT IN (SELECT mdluserid FROM {$CFG->prefix}synciccarouca_course WHERE mdlcourseid=$idCurso )";
		//echo $sql;
                //echo "<BR>";
		return get_records_sql($sql); 
	}
	function existeIngressante($idUsuario,$idCursoMoodle){
		
			global $CFG;
			$sql ="SELECT COUNT(id) AS qreg FROM {$CFG->prefix}synciccarouca_course WHERE mdluserid=$idUsuario AND mdlcourseid=$idCursoMoodle"; 
			$r=get_record_sql($sql);
			return $r->qreg;
		}
	function cadastrarIngressante($idUsuario,$idCursoMoodle,$idCursoArouca,$idOfertaArouca,$dataIngresso,$metodoInscricao){
		if(!$this->existeIngressante($idUsuario,$idCursoMoodle)){
                        if(empty($dataIngresso)){$dataIngresso=strtotime('2012-09-01');}
			$dto=new object();
			$dto->mdluserid=$idUsuario;
			$dto->mdlcourseid=$idCursoMoodle;
			$dto->arcidcurso=$idCursoArouca;
			$dto->arcidoferta=$idOfertaArouca;
			$dto->dataingresso=$dataIngresso;
			$dto->status= self::$STATUS_INGRESSANTE_INSCRITO_MOODLE;
			$dto->mdlenrol=$metodoInscricao;
			return $this->salvar($dto);
	 	}else return FALSE;
	}
	function recuperarIngressantesPeloStatus($idCursoMoodle,$statusIngresante){
			global $CFG;
			$sql ="SELECT s.id,u.firstname, u.lastname, u.email, u.username AS cpf,s.arcidoferta,s.dataingresso FROM {$CFG->prefix}synciccarouca_course s INNER JOIN {$CFG->prefix}user u ON u.id=s.mdluserid  WHERE s.mdlcourseid=$idCursoMoodle AND s.status IN ($statusIngresante) ";
			return get_records_sql($sql); 	
		 
		}
		
		function recuperarIngressantesAprovados($idCursoMoodle,$notaFinalCurso){
			global $CFG;
			$sql ="SELECT s.id ,g.userid,g.finalgrade,g.timecreated,g.timemodified FROM {$CFG->prefix}grade_grades g INNER JOIN {$CFG->prefix}grade_items i ON g.itemid=i.id  INNER JOIN {$CFG->prefix}synciccarouca_course s ON s.mdluserid=g.userid WHERE i.itemtype='course' AND  s.mdlcourseid=i.courseid AND  i.courseid=$idCursoMoodle AND s.status=". self::$STATUS_INGRESSANTE_SINCRONIZADO_AROUCA ." AND g.finalgrade >= $notaFinalCurso";
			return get_records_sql($sql); 	
		 
		}
		
		function recuperarConcluintePeloStatus($idCursoMoodle,$statusConcluinte){
			global $CFG;
			$sql ="SELECT s.id,u.firstname, u.lastname, u.email, u.username AS cpf,s.arcidcurso,s.arcidoferta,s.dataconclusao FROM {$CFG->prefix}synciccarouca_course s INNER JOIN {$CFG->prefix}user u ON u.id=s.mdluserid  WHERE s.mdlcourseid=$idCursoMoodle AND s.status IN ($statusConcluinte) ";
			return get_records_sql($sql); 	
		 
		}
		
		function recuperarConcluinteParaHomologarcaoManual($idCursoMoodle,$statusConcluinte){
			global $CFG;
			$sql ="SELECT DISTINCT s.id,s.mdluserid,u.firstname, u.lastname, u.email, u.username AS cpf,s.arcidcurso,s.dataingresso,s.dataconclusao,g.finalgrade,s.mdlenrol FROM {$CFG->prefix}synciccarouca_course s INNER JOIN {$CFG->prefix}user u ON u.id=s.mdluserid INNER JOIN {$CFG->prefix}grade_grades g ON g.userid=u.id   INNER JOIN {$CFG->prefix}grade_items i ON g.itemid=i.id  WHERE i.itemtype='course' AND s.mdlcourseid=i.courseid AND s.mdlcourseid=$idCursoMoodle AND s.status IN ($statusConcluinte) ORDER BY u.firstname, u.lastname";
			return get_records_sql($sql); 	
		 
		}
		function contarConcluinteParaHomologarcaoManual($idCursoMoodle,$statusConcluinte){
			global $CFG;
			$sql ="SELECT COUNT(s.mdluserid) AS qreg  FROM {$CFG->prefix}synciccarouca_course s INNER JOIN {$CFG->prefix}user u ON u.id=s.mdluserid INNER JOIN {$CFG->prefix}grade_grades g ON g.userid=u.id   INNER JOIN {$CFG->prefix}grade_items i ON g.itemid=i.id  WHERE i.itemtype='course' AND s.mdlcourseid=i.courseid AND s.mdlcourseid=$idCursoMoodle AND s.status IN ($statusConcluinte) ORDER BY u.firstname, u.lastname";
			$r=get_record_sql($sql); 	
			
			return $r->qreg;
		 
		}
		function recuperarCertificadoPeloStatus($idCursoMoodle,$statusConcluinte){
			global $CFG;
			$sql ="SELECT id,mdluserid, urlcertificado FROM {$CFG->prefix}synciccarouca_course  WHERE mdlcourseid=$idCursoMoodle AND status IN ($statusConcluinte) ";
			return get_records_sql($sql); 	
		 
		}
		
		  function dataConcluintePelaNota($dataLancamentoNota,$dataAtualizacaoNota){
   			if(!empty($dataAtualizacaoNota) && $dataAtualizacaoNota>0) {
   				return $dataAtualizacaoNota;
   			}
   			if(!empty($dataLancamentoNota) && $dataLancamentoNota>0) {
   				return $dataLancamentoNota;
   			}
   		return time();
   }


   
   
    function registrarIgressante($idCursoMoodle,$idCursoArouca,$idOfertaArouca){
   		 
     	 $arouca_ws=new arouca_ws();
     	 
     	  //1 PASSO - EXTRAIR MATRICULAS HOMOLOGADAS NO AROUCA E AINDA N�O CATALOGADOS COMO INGRESSANTES 
     	 // $ingressante_nao_registrado=$this->recuperarMatriculaHomologadaNaoRegistrado($idCursoMoodle);
     	  $ingressante_nao_registrado=$this->recuperarMatriculaNoCursoNaoRegistrado($idCursoMoodle);
     	 // print_r( $ingressante_nao_registrado);
     	  //2� PASSO - REGISTRAR MATRICULAS DO MOODLE NA TABELA DE INGRESSANTES
          if(!empty($ingressante_nao_registrado)){
              foreach($ingressante_nao_registrado as $inh){
      			$this->cadastrarIngressante($inh->userid,$idCursoMoodle,$idCursoArouca,$idOfertaArouca,$inh->created,$inh->enrol);
      		}
          }
       	
     	//3� PASSO EXTRAIR INGRESSANES N�O SINCRONIZADOS
     	//definir status separado por v�rgula
     	$listaStatus=self::$STATUS_INGRESSANTE_INSCRITO_MOODLE.",".self::$STATUS_INGRESSANTE_SINCRONIZADO_AROUCA_FALHA;
     	$ingressante_nao_sync=$this->recuperarIngressantesPeloStatus($idCursoMoodle,$listaStatus);  
     
     	 //4� EFETUAR SINCRONIZA��O DOS INGRESSANTES AINDA N�O SINCRONIZADOS
     	 if(!empty($ingressante_nao_sync)){
             foreach($ingressante_nao_sync as $inh){
      			$dataIngresso= date('d/m/Y',$inh->dataingresso);
      	      	$arouca_ws->cadastrarIngressante($inh->cpf,$inh->arcidoferta, date('d/m/Y',$inh->dataingresso));
      	      	$dto=new object();
      			$dto->id=$inh->id;
      			if($arouca_ws->WS_AROUCA_FAILURE){
      				//verificar se ingressante j� est� cadastrado
      			
      				//print_r($arouca_ws->WS_AROUCA_FAILURE_MSG);
      				//echo "existe:". strpos($arouca_ws->WS_AROUCA_FAILURE_MSG, 'ngressante');
      				//echo "<br>";
      				if(strpos($arouca_ws->WS_AROUCA_FAILURE_MSG, 'ngressante') && strpos($arouca_ws->WS_AROUCA_FAILURE_MSG, 'cadastrado')){
      					$dto->status=self::$STATUS_INGRESSANTE_SINCRONIZADO_AROUCA;
      					$dto->faultstringwsarouca='';
      				}
      				//EFETUAR CADASTRO DE PESSAO SE N�O TIVER
      				//echo "<br>";
      				else if(strpos($arouca_ws->WS_AROUCA_FAILURE_MSG, 'essoa n') && strpos($arouca_ws->WS_AROUCA_FAILURE_MSG, 'o encontrada')){
      					
      					$arouca_ws1=new arouca_ws();
      					$nomePessoa=$inh->firstname.' '.$inh->lastname;
      					$nomePessoa=trim($nomePessoa);
      					$arouca_ws1->cadastrarPessoa($inh->cpf, $nomePessoa,'', '','', '', $inh->email, '');
      					if($arouca_ws1->WS_AROUCA_FAILURE){
      						$dto->status=self::$STATUS_INGRESSANTE_SINCRONIZADO_AROUCA_FALHA;
      						$dto->faultstringwsarouca=addslashes($arouca_ws1->WS_AROUCA_FAILURE_MSG);
      						
      						}
      					else{
      						//$dto->status=self::$STATUS_INGRESSANTE_SINCRONIZADO_AROUCA;
      						$dto->faultstringwsarouca='';
      						
      						}
      				}
      				else{
      					
      					$dto->status=self::$STATUS_INGRESSANTE_SINCRONIZADO_AROUCA_FALHA;
      					$dto->faultstringwsarouca=addslashes($arouca_ws->WS_AROUCA_FAILURE_MSG);
      				}
      				
	      		}else{
      				$dto->status=self::$STATUS_INGRESSANTE_SINCRONIZADO_AROUCA;
      				$dto->faultstringwsarouca='';
      				
      			} 
      			$this->editar($dto);
     	  }
         } 
        
   }
   
  
    function registrarConcluinte($idCursoMoodle,$notaFinalCurso,$naOferta=FALSE){
   		 
     	 $arouca_ws=new arouca_ws();
     	 
     	 //1� EXTRAIR INGRESSANTES SINCRONIZADOS QUE AINDA N�O FORAM REGISTRADOS COMO CONCLUINTES E QUE J� ATENDEM O CRIT�IRO DE CONCLUS�O. 
     		$ingr_aprv=$this->recuperarIngressantesAprovados($idCursoMoodle,$notaFinalCurso);
     		
     	 //2� REGISTRAR CONCLUITES NA TABELA QUE ATENDEM CRITERIO DE  NOTA NO MOODLE 
    	 if(!empty($ingr_aprv)){
              foreach($ingr_aprv as $ingap){
     			$dto=new object();
      			$dto->id=$ingap->id;	
      			$dto->status=self::$STATUS_CONCLUINTE_HOMOLOGADO_MOODLE;
      			$dto->dataconclusao=$this->dataConcluintePelaNota($ingap->timecreated,$ingap->timemodified);
      			$this->editar($dto);
     		}
         }
             
     	
     	//3�  EXTRAIR CONCLUINTES HOMOLOGADOS PARA SINCRONIZA��O NO AROUCA
     	//definir status separado por v�rgula
     	$listaStatus=self::$STATUS_CONCLUINTE_HOMOLOGADO_GESTOR.",".self::$STATUS_CONCLUINTE_SINCRONIZADO_AROUCA_FALHA;
     	$concluinte_nao_sync=$this->recuperarConcluintePeloStatus($idCursoMoodle,$listaStatus);  
     	
     	
     	 //4� EFETUAR SINCRONIZA��O DOS CONCLUINTES AINDA N�O SINCRONIZADOS
     	if(!empty($concluinte_nao_sync)){
            foreach($concluinte_nao_sync as $conh){
      			$dataConclusao= date('d/m/Y',$conh->dataconclusao);
      			$nomeConcluinte=$conh->firstname;
      			if($conh->lastname!=null){$nomeConcluinte.= " ".$conh->lastname;}
      			
      			if($naOferta)$arouca_ws->cadastrarConcluinteOferta($conh->cpf,$conh->arcidoferta,$dataConclusao,$nomeConcluinte);
                        else $arouca_ws->cadastrarConcluinte($conh->cpf,$conh->arcidcurso,$dataConclusao,$nomeConcluinte);
      	      	$dto=new object();
      			$dto->id=$conh->id;
      			if($arouca_ws->WS_AROUCA_FAILURE){
      				if(strpos($arouca_ws->WS_AROUCA_FAILURE_MSG, 'oncluinte') && strpos($arouca_ws->WS_AROUCA_FAILURE_MSG, 'cadastrado')){
      					$dto->status=self::$STATUS_CONCLUINTE_SINCRONIZADO_AROUCA;
      				}else{
      					$dto->status=self::$STATUS_CONCLUINTE_SINCRONIZADO_AROUCA_FALHA;
      					$dto->faultstringwsarouca=addslashes($arouca_ws->WS_AROUCA_FAILURE_MSG);
      				}
      				
	      		}else{
      				$dto->status=self::$STATUS_CONCLUINTE_SINCRONIZADO_AROUCA;
      				$dto->faultstringwsarouca='';
      				$dto->faultstringwsarouca='';
      			}
      			$this->editar($dto);
     	  }
        }  
        
     	
     
    }
   
 
   
    function recuperarCertificado($idCursoMoodle,$naOferta=FALSE){
    	
     	 $arouca_ws=new arouca_ws();
     	 
    	// 1� RECUPERAR CONCLUINTES SINCRONIZADOS CUJO CERTIFICADO N�O FOI RECUPERADO
    	//definir status separado por v�rgula
     	$listaStatus=self::$STATUS_CONCLUINTE_SINCRONIZADO_AROUCA.",".self::$STATUS_CERTIFICADO_EMITIDO_AROUCA_FALHA;
  
    	$concluinte_sync=$this->recuperarConcluintePeloStatus($idCursoMoodle,$listaStatus);  
    	
    	//print_r($concluinte_sync);
    
    	//4� EFETUAR SINCRONIZA��O DOS CONCLUINTES AINDA N�O SINCRONIZADOS
     	if(!empty($concluinte_sync)){
            foreach($concluinte_sync as $cons){
      			$dataConclusao= date('d/m/Y',$cons->dataconclusao);
                        $url_certificado=null;
                        if($naOferta){$url_certificado=$arouca_ws->recuperarCertificadoConcluinteOferta($cons->cpf,$cons->arcidoferta, $dataConclusao);}
                        else {$url_certificado=$arouca_ws->recuperarCertificadoConcluinte($cons->cpf,$cons->arcidcurso, $dataConclusao);}
      			
      	       	$dto=new object();
      			$dto->id=$cons->id;
      			if($arouca_ws->WS_AROUCA_FAILURE){
      				$dto->status=self::$STATUS_CERTIFICADO_EMITIDO_AROUCA_FALHA;
      				$dto->faultstringwsarouca=addslashes($arouca_ws->WS_AROUCA_FAILURE_MSG);
      			}else{
      				$dto->faultstringwsarouca='';
      				$dto->status=self::$STATUS_CERTIFICADO_EMITIDO;
      				$dto->urlcertificado=$url_certificado;
      				$dto->faultstringwsarouca='';
      			}
      			$this->editar($dto);
     	  }
        } 
        
     	  
    }
    
function enviarCertificado($idCursoMoodle){
    	//1° EXTRAIR LISTA DE USUÁRIOS COM CETIFICADO EMITIDO
    	$listaStatus=self::$STATUS_CERTIFICADO_EMITIDO;
     
    	$concluinte_cert=$this->recuperarCertificadoPeloStatus($idCursoMoodle,$listaStatus);  
    	//2°FAZER LOOP  E ENVIAR CERTIFICADO POR EMAIL PARA CADA USUÁRIO
        $course_sync_config =new course_sync_config();
        
    	$courseinfo=$this->get_course($idCursoMoodle);
    	$siteinfo=$this->get_course(1);
    	$admininfo=$this->get_user(2);
        $config_message=$course_sync_config->get_menssage($idCursoMoodle);
        if(!empty($concluinte_cert) && !empty($config_message->subjectmailcert) && !empty($config_message->messagemailcert)){
            foreach($concluinte_cert as $cert){
    	 	
    	 	//echo "<br> id: ".$cert->id;
    	 	//echo "<br> cert: ".$cert->urlcertificado;
    	 	
                
       		$userinfo=$this->get_user($cert->mdluserid);
                $userinfo->mailformat=1;
                $subject = $course_sync_config->fill_tag($config_message->subjectmailcert,$courseinfo,$userinfo,$cert->urlcertificado);
        	 $body =$course_sync_config->fill_tag($config_message->messagemailcert,$courseinfo,$userinfo,$cert->urlcertificado);
        	$msg_enviada=email_to_user($userinfo, $siteinfo->shortname, $subject,'', $body);
        	if($msg_enviada){
        		$dto=new object();
      			$dto->id=$cert->id;
      			$dto->status=self::$STATUS_CERTIFICADO_ENVIADO;
      			$this->editar($dto);
        	}
    	 }
        }
    	 
    }
   
function get_course($id){
				global $CFG;
			global $USER;
			$sql ="SELECT id,fullname,shortname	 FROM {$CFG->prefix}course WHERE id=$id";
			 return get_record_sql($sql);
			}	
                        
                        
                        
function get_user($id){
				global $CFG;
			$sql ="SELECT id,firstname,lastname,email FROM {$CFG->prefix}user WHERE id= $id";
			
			return get_record_sql($sql);
    	}
        
        
        
        function get_courses(){
				global $CFG;
			
			$sql ="SELECT id,fullname FROM {$CFG->prefix}course WHERE id> 1  ORDER BY fullname";
			return get_records_sql($sql);
    	}

function get_courses_html_select(){
		$courses=	$this->get_courses();
		$options = array();
		$options['']="  ----  ";
		foreach ($courses as $course){ 
			 
			$options[$course->id]=$course->fullname;
		}
		return 	$options ;
		}
 function get_users(){
				global $CFG;
			$sql ="SELECT id,firstname,lastname FROM {$CFG->prefix}user ORDER BY firstname,lastname";
			return get_records_sql($sql);
    	}               
function get_user_html_select(){
		$users=	$this->get_users();
		$options = array();
		$options['']="  ----  ";
		foreach ($users as $user){ 
			
			$options[$user->id]=$user->firstname. " ".$user->lastname;
		}
		return 	$options ;
		}     
                
                
              
}

?>