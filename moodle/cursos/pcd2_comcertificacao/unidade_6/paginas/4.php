<?php
               
    $pg_atual=4;
    registro($id,unid6_pg,$pg_atual,unid6_ev);
?>
    
	<style type="text/css">
		
		.gastros{
			position: absolute;
			top: 163px;
			right: 298px;
		
		}
		
		.foley{
			position: absolute;
			top: 687px;
			right: 314px;
			cursor:pointer;
			margin-top: 61px;
			border: 1px solid #F68C23;
			
		}
		
		#foley_modal{
			width: 340px;
			position: absolute;
			top: 574px;
			right: 467px;
			border: 1px solid #F68C23;
			padding:5px;
			background-color: #FFF0E2;
			display: none;
			margin-top: 61px;
		}
		
		table tr td{
			
			
		}
		
		#lista{
            list-style-type: disc;  
            margin-left: 30px;
        }
		
		#hide1, #hide2{
			display:block;
		
		}
		
	</style>
	
	<script type="text/javascript">
		
		function toggle_show(){
			
			document.getElementById("foley_modal").style.display="block";
			
		
		}
		
		function toggle_hide(){
			
			document.getElementById("foley_modal").style.display="none";
			
		
		}
	
	</script>
	<h4 class="titulo">Estoma de Alimentação</h4>
    <h5 >Gastrostomia</h5>
		
		É um procedimento cirúrgico no qual a sonda gástrica percutânea é passada através da parede abdominal 
		diretamente para o estômago, com a finalidade de promover nutrição enteral, sendo associada mais frequentemente às seguintes 
		situações clínicas (SANTOS et al, 2011; SILVA, 2007):  <br/>
		<span style="margin-left: 368px;"><b>Localização anatômica da gastrostomia</b></span>
		<ul style="margin-bottom: 150px;">
		<li id="lista">acidente vascular encefálico;</li>
		<li id="lista">doença do neurônio motor;</li>
		<li id="lista">esclerose múltipla;</li>
		<li id="lista">doença de Parkinson;</li>
		<li id="lista">paralisia cerebral;</li>
		<li id="lista">doenças demenciais;</li>
		<li id="lista">traumatismo crânio encefálico;</li>
		<li id="lista">pacientes em tratamento intensivo;</li>
		<li id="lista">neoplasia de orofaringe;</li>
		<li id="lista">neoplasia de esôfago;</li>
		<li id="lista">queimaduras;</li>
		<li id="lista">fístulas digestivas;</li>
		<li id="lista">síndrome do intestino curto;</li>
		<li id="lista">descompressão gástrica;</li>
		<li id="lista">nutrição prolongada.</li>
		</ul>
		
		<img class="gastros" src="images/Fig04-gastrostomia.jpg" alt="gastrostomia" title="gastrostomia">
		<span style="position: absolute;top: 586;right: 438px;"><b>Fonte:</b> (UNA-SUS UFPE, 2014).</span>

<br/><br/>
<div class="box">
        <img src="../images/img_vocesabiaque_ad.png" alt="Você sabia">
        <span class="titulo_box">Você sabia que...</span>
         <hr>
        
        <p style="width: 500px;">A utilização da sonda de Foley nas gastrostomias é uma adaptação feita pelas 
        equipes da atenção domiciliar na ausência da sonda gástrica percutânea, a qual possui um balão que evita 
        a migração da sonda pelos movimentos peristálticos.<br/></p>
        “Na ausência de sonda adequada, pode-se usar de maneira transitória e em caráter <br>EMERGENCIAL uma sonda vesical do tipo Foley” (SÃO PAULO, 2012).
        <i class=" icon-hand-up" style="float: right" title="Esse quadro possui interação."></i> 
		<img onclick="toggle_show();" class="foley" src="images/Fig05.1-vocesabia.jpg" alt="Sonda de Foley" title="Sonda de Foley">
		<div id="foley_modal">
			<p style="float:right; font-weight:bold; margin: 0;font-size: 18px;cursor:pointer;" onclick="toggle_hide();">x</p>
			<img src="images/Fig05.2-vocesabia.jpg" alt="Sonda de Foley" title="Sonda de Foley">
		</div>
 <br><br><br><br><br>
</div>
<br/>
<br><br>
<div style="width: 321px;float:left; ">



	<div id="hide1" style="float:left;">
<table style="background-color:#FFE4CA" class="table table-bordered " style="width: 300px; margin-left: 20px; float:left;">
		<tr bgcolor="#ccc">
		<td><b>Contraindicações da Gastrostomia</b></td>
	
	</tr>

	<tr >
		<td>Obesidade</td>
	
	</tr>
	
	<tr>
		<td>Dificuldades respiratórias</td>
	
	</tr>
	
	<tr  >
		<td>Coagulopatia não corrigida</td>
	
	</tr>
	
	<tr >
		<td>Estenose de esôfago</td>
	
	</tr>
	
	<tr >
		<td>Úlcera gástrica</td>
	
	</tr>
	
	<tr >
		<td>Gastrectomia</td>
	
	</tr>
	
	<tr>
		<td>Cirurgias do abdomen superior</td>
	
	</tr>
	
	<tr > 
		<td>Neoplasia gástrica</td>
	
	</tr>

</table>	


<span style="margin-left: 0px;float:left;"><b>Fonte:</b> (SILVA; HUGA-TANIGUCHI, 2010, adaptado).</span></div></div>

<div style="width: 384px; float:right; margin-right: 21px;">

	<div id="hide2" style="float:right;">
<table style="background-color:#FFE4CA" class="table table-bordered " style="width: 300px; margin-left: 20px; float:right;">

		<tr bgcolor="#ccc">
		<td><b>Complicações da Gastrostomia</b></td>
	
	</tr>
	<tr >
		<td>Dor</td>
	
	</tr>
	
	<tr >
		<td>Inflamação</td>
	
	</tr>
	
	<tr >
		<td>Peritonite</td>
	
	</tr>
	
	<tr>
		<td>Granuloma</td>
	
	</tr>
	
	<tr >
		<td>Perfuração Intestinal</td>
	
	</tr>
	
	<tr >
		<td>Infecção Localizada</td>
	
	</tr>
	
	<tr >
		<td>Sangramento pela Fístula</td>
	
	</tr>
	
	<tr > 
		<td>Eritema ao redor da inserção</td>
	
	</tr>
	
	<tr> 
		<td>Encurtamento da sonda fora do abdome</td>
	
	</tr>

</table>	
<span style="margin-left: 0px;float:left;"><b>Fonte:</b> (SMELTZER; BARE, 2005, adaptado).</span></div></div>

<!-- tabela de Josi -->
<h5 style="clear:both; ">Cuidados com a gastrostomia</h5>
  <ul>
		<li id="lista">Lavar as mãos antes e após manipular a sonda de gastrostomia;</li>
		<li id="lista">Realizar limpeza do local da inserção da sonda de gastrostomia com soro 
		fisiológico a 0,9% ou água morna, sempre que necessário, e secar bem após a lavagem;</li>
		<li id="lista">Nunca tracionar a sonda;</li>
		<li id="lista">Manter a sonda sempre fechada enquanto não estiver em uso;</li>
		<li id="lista">Colocar duas gazes dobradas em cada lado da inserção da sonda, mantendo-as limpas e secas;</li>
		<li id="lista">Atentar para sinais flogísticos (inflamação) no sítio de inserção da sonda;</li>
		<li id="lista">Observar extravasamento de dieta pelo orifício da gastrostomia. Caso isso 
		seja observado, comunicar imediatamente à equipe multiprofissional;
		<li id="lista">Durante e 60 minutos após a administração da dieta, manter o paciente em posição de Fowler;</li>
		<li id="lista">Lavar a sonda com 20 a 50ml de água filtrada após dietas e/ou medicações;</li>
		<li id="lista">Diluir bem as medicações antes de administrar (SMELTZER; BARE, 2005, adaptado).</li>
	</ul>
	<!-- -->
<!--<a href="javascript:history.go(-1)" class="btn">Voltar</a>-->

<script>
	
		
		function esconder(id){
		
			if ($("#hide" + id).is(":hidden")) {
				$("#hide" + id).slideDown("");
				
			} else{
			$('#hide' + id).hide("");
			}
	
		}
 
	</script>
