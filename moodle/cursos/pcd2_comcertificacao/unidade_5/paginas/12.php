<?php
               
    $pg_atual=12;
     registro($id,unid6_pg,$pg_atual,unid6_ev);
?>
    
   

   <h4 class="titulo">Referências</h4>
   <br><br>
  ACADEMIA NACIONAL DE CUIDADOS PALIATIVOS (ANCP). <strong>Manual de cuidados paliativos.</strong> Rio de Janeiro: Diagraphic, 2009.  320p. Disponível em: &#60<a href="http://www.nhu.ufms.br/Bioetica/Textos/Morte e o Morrer/MANUAL DE CUIDADOS PALIATIVOS.pdf" target="_blank">http://www.nhu.ufms.br/Bioetica/Textos/Morte%20e%20o%20Morrer/MANUAL%20DE%20CUIDADOS%20PALIATIVOS.pdf</a>&#62. Acesso em: 3 jun. 2013.


<br/><br/>
BRASIL. Ministério da Saúde. Instituto Nacional de Câncer - INCA. Divisão de Comunicação Social. <strong>Orientações sobre ostomias.</strong> Rio de Janeiro: INCA, 2003. Disponível em: &#60<a href="http://www.inca.gov.br/publicacoes/ostomias.pdf" target="_blank">http://www.inca.gov.br/publicacoes/ostomias.pdf</a>&#62. Acesso em: 2 maio 2013.

<br/><br/>

BRASIL. Decreto Nº 5.296, de 02 de dezembro de 2004. Regulamenta as leis Nº 10.048, de 08 de novembro de 2000, que dá prioridade de atendimento às pessoas que especifica, e Nº 10.098, de 19 de dezembro de 2000, que estabelece normas gerais e critérios básicos para a promoção da acessibilidade das pessoas portadoras de deficiência ou com mobilidade reduzida, e dá outras providências. <strong>Diário Oficial [da] República Federativa do Brasil</strong>, Poder Executivo, Brasília, DF, 3 dez. 2004. Seção 1, p. 5. Disponível em: &#60<a href="http://www2.camara.leg.br/legin/fed/decret/2004/decreto-5296-2-dezembro-2004-534980-normaatualizada-pe.html" target="_blank">http://www2.camara.leg.br/legin/fed/decret/2004/decreto-5296-2-dezembro-2004-534980-normaatualizada-pe.html</a>&#62. Acesso em: 2 maio 2013.


<br/><br/>
BRASIL. Ministério da Saúde. Instituto Nacional de Câncer-INCA. Divisão de Comunicação Social. <strong>Cuidados com a sua estomia:</strong> orientações aos pacientes. Rio de Janeiro: INCA, 2010. Disponível em: &#60<a href="http://www1.inca.gov.br/inca/Arquivos/cuidados_com_a_sua_estomia.pdf" target="_blank">http://www1.inca.gov.br/inca/Arquivos/cuidados_com_a_sua_estomia.pdf</a>&#62.  Acesso em: 2 maio 2013.


<br/><br/>
BRASIL. Ministério da Saúde. Gabinete do Ministro.<strong> Portaria Nº 2.029, de 24 de agosto de 2011.</strong> Institui a Atenção Domiciliar no âmbito do Sistema Único de Saúde (SUS). 2011. Disponível em: &#60<a href="http://bvsms.saude.gov.br/bvs/saudelegis/gm/2011/prt2029_24_08_2011.html" target="_blank">http://bvsms.saude.gov.br/bvs/saudelegis/gm/2011/prt2029_24_08_2011.html</a>&#62. Acesso em: 15 mar. 2013.


<br/><br/>
BRASIL. Ministério da Saúde.<strong> Instrutivo ostomia</strong>. Ref. Portaria GM 793 de 24 de abril de 2012 e Portaria GM 835 de 25 de abril de 2012. [2012]. Disponível em: &#60<a href="http://portalsaude.saude.gov.br/portalsaude/arquivos/pdf/2012/Jul/16/ostomia_160712.pdf" target="_blank">http://portalsaude.saude.gov.br/portalsaude/arquivos/pdf/2012/Jul/16/ostomia_160712.pdf</a>&#62. Acesso em: 13 mar. 2013.


<br/><br/>
BRASIL. Ministério da Saúde. Secretaria de Atenção à Saúde. Departamento de Atenção Básica.
<strong>Caderno de Atenção Domiciliar</strong>. Volume 2. Brasília: Ed. Ministério da Saúde, 2013. <br/><br/>
CEARÁ (Estado). Conselho Regional de Medicina do Estado do Ceará (CREMEC).<strong> Parecer CREMEC Nº 19/2007.</strong> A aposição e substituição de sonda naso-enteral, de cistostomia, de gastrostomia e traqueóstomo é procedimento compartilhado com os profissionais de enfermagem. A prescrição é do médico, que tem a responsabilidade maior. Fortaleza, 2007. Disponível em: &#60<a href="http://www.cremec.com.br/pareceres/2007/par1907.htm" target="_blank">http://www.cremec.com.br/pareceres/2007/par1907.htm</a>&#62. Acesso em: 29 abr. 2013.
 

<br/><br/>
COLOGNA, A. J. Cistostomia. <strong>Medicina</strong>, Ribeirão Preto, v. 44, n.1, p. 57-62, jan./mar., 2011. Disponível em: &#60<a href="http://revista.fmrp.usp.br/2011/vol44n1/Simp6_cistotomia.pdf" target="_blank">http://revista.fmrp.usp.br/2011/vol44n1/Simp6_cistotomia.pdf</a>&#62. Acesso em: 2 maio 2013.


<br/><br/>
FARIAS, G. M. de; FREIRE, I. L. S; RAMOS, C. da S. Aspiração endotraqueal: estudo em pacientes de uma unidade de urgência e terapia intensiva de um hospital da região metropolitana de Natal - RN. <strong>Rev Eletr Enf</strong>, Goiânia v. 8, n. 1, p. 63-69, 2006. Disponível em: &#60<a href="http://www.fen.ufg.br/fen_revista/revista8_1/pdf/v8n1a09.pdf" target="_blank">http://www.fen.ufg.br/fen_revista/revista8_1/pdf/v8n1a09.pdf</a>&#62. Acesso em: 2 maio 2013.


<br/><br/>
NETTINA, S. M. <strong>Prática de enfermagem</strong>. 7. ed. Rio de Janeiro: Guanabara Koogan, 2003. <br/><br/>
PAULA, R. A. B. de; SANTOS, V. L. C. de G. Estudo retrospectivo sobre as complicações do estoma e da pele periestoma em ostomizados da cidade de São Paulo. <strong>Rev Esc Enferm USP</strong>, São Paulo, v. 33 (número especial), p.63-73, 1999. Disponível em: &#60<a href="http://www.ee.usp.br/reeusp/upload/pdf/787.pdf" target="_blank">http://www.ee.usp.br/reeusp/upload/pdf/787.pdf</a>&#62. Acesso em: 3 maio2013.


<br/><br/>
POLETTO, D.; GONÇALVES, M. I.; BARROS, M. T. de T.<strong> Como cuidar de criança com estoma.</strong> Florianópolis: [s. n.], 2010. Disponível em: &#60<a href="http://www.abraso.org.br/Como%20cuidar%20da%20crian%C3%A7a%20com%20estoma%20(COR).pdf" target="_blank">http://www.abraso.org.br/Como%20cuidar%20da%20crian%C3%A7a%20com%20estoma%20(COR).pdf</a>&#62. Acesso em: 2 maio 2013.


<br/><br/>
POTTER, P. A.; PERRY, A. G.<strong> Fundamentos de enfermagem.</strong> 6. ed. Rio de Janeiro: 
Elsevier, 2006. <br/><br/>
ROCHA, J. J. R. da. Estomas intestinais (ileostomias e colostomias) e anastomoses
intestinais. <strong>Medicina</strong>, Ribeirão Preto, v. 44, n.1, p. 51-56, 2011. Disponível em: &#60<a href="http://revista.fmrp.usp.br/2011/vol44n1/Simp5_Estomas%20intestinais.pdf" target="_blank">http://revista.fmrp.usp.br/2011/vol44n1/Simp5_Estomas%20intestinais.pdf</a>&#62. Acesso em: 3 maio 2013.


<br/><br/>
RONDÔNIA (Estado). Conselho Regional de Enfermagem de Rondônia (COREN-RO). <strong>Parecer técnico Nº 001/2012.</strong> Competência do enfermeiro para realizar o procedimento de troca de sonda de Gastrostomia. Porto Velho, 2012. Disponível em: &#60<a href="http://www.coren-ro.org.br/site/index.php?option=com_content&amp;view=article&amp;id=633:parecer-tecnico-no-0012012-competencia-do-enfermeiro-para-realizar-o-procedimento-de-troca-de-sonda&amp;catid=11:decretos&amp;itemid=13" target="_blank">http://www.coren-ro.org.br/site/index.php?option=com_content&view=article&id=633:parecer-tecnico-no-0012012-competencia-do-enfermeiro-para-realizar-o-procedimento-de-troca-de-sonda&catid=11:decretos&itemid=13</a>&#62. Acesso em: 27 abr. 2013.


<br/><br/>
SANTOS, J. S. dos. et al. Gastrostomia e jejunostomia: aspectos da evolução técnica e da ampliação das indicações. <strong>Medicina</strong>, Ribeirão Preto, v. 44, n.1, p. 39-50, jan./mar., 2011. Disponível em: &#60<a href="http://revista.fmrp.usp.br/2011/vol44n1/Simp4_Gastrostomia_e_jejunostomia%20atual.pdf" target="_blank">http://revista.fmrp.usp.br/2011/vol44n1/Simp4_Gastrostomia_e_jejunostomia%20atual.pdf </a>&#62. Acesso em: 3 maio 2013.


<br/><br/>
SÃO PAULO (Estado). Conselho Regional de Enfermagem de São Paulo (COREN-SP).<strong> Parecer COREN-SP Nº 045/2012-CT</strong>. Dispõem sobre a troca de sonda de gastrostomia e jejunostomia. Respaldo legal e competência do enfermeiro. São Paulo, 2012. Disponível em: &#60<a href="http://portal.coren-sp.gov.br/sites/default/files/Parecer%20045-2012.pdf" target="_blank">http://portal.coren-sp.gov.br/sites/default/files/Parecer%20045-2012.pdf</a>&#62. Acesso em: 2 maio 2013.


<br/><br/>
SILVA, L. da S. <strong>Atuação da equipe de enfermagem na adesão do paciente ostomizado para o autocuidado. </strong>2007. 63 f. Monografia (Bacharel em Enfermagem)–Centro Universitário Feevale. Novo Hamburgo, 2007. Disponível em: &#60<a href="ged.feevale.br/bibvirtual/monografia/MonografiaLeticiaSilva.pdf" target="_blank">ged.feevale.br/bibvirtual/monografia/MonografiaLeticiaSilva.pdf</a>&#62. Acesso em: 28 abr. 2013.


<br/><br/>
SILVA, D.; HUGA-TANIGUCHI, K. T. Cuidados contínuos com sondas e ostomias. In: YAMAGUCHI, A. M. et al. <strong>Assistência domiciliar</strong>: uma proposta interdisciplinar. Barueri, SP: Manole, 2010. <br/><br/>
SMELTZER, S. C.; BARE, B. G. <strong>Brunner e Suddarth</strong>: tratado de enfermagem médico-cirúrgica. 10. ed., v. 1. Rio de Janeiro: Guanabara Koogan, 2005. <br/><br/>

SONOBE, H. M.; BARICHELLO, E.; ZAGO, M. M. F. A visão do colostomizado sobre o uso da bolsa da colostomia. <strong>Revista Brasileira de Cancerologia</strong>, Brasília, n. 48, n. 3, p.341-348, 2002. Disponível em: &#60<a href="www.inca.gov.br/rbc/n_48/v03/pdf/artigo2.pdf" target="_blank">www.inca.gov.br/rbc/n_48/v03/pdf/artigo2.pdf</a>&#62. Acesso em: 2 maio 2013.

<br/><br/>

SOUZA, J. L. de. et al. O preparo do familiar para o cuidado à pessoa com estomia. <strong>Rev Enferm UFPE On Line</strong>, Recife, v. 7, n. 1, p. 649-56, mar., 2013. Disponível em: &#60<a href="www.revista.ufpe.br/revistaenfermagem/index.php/revista/article/download/3731/5628" target="_blank">www.revista.ufpe.br/revistaenfermagem/index.php/revista/article/download/3731/5628</a> &#62. Acesso em: 3 maio 2013.

<br><br/>

VIANNA, A.; PALAZZO, R. F.; ARAGON, C . Traqueostomia: uma revisão atualizada. <strong>Pulmão RJ</strong>, v. 20, n. 3, p. 39-42, 2011. Disponível em: &#60<a href="http://www.sopterj.com.br/revista/2011_20_3/09.pdf" target="_blank">http://www.sopterj.com.br/revista/2011_20_3/09.pdf</a>&#62. Acesso em: 3 maio 2013.


