<?php
/*
 * Created on 06/04/2012
 *
 *Essa class tem as bibliotecas de gerenciamento
 * do modo de processamento. 
 */
 class processmode  {
	public static $MANUAL ="manual";
	public static $AUTOMATIC ="automatic";
	public static $MANUAL_AUTOMATIC ="manual_automatic";
 	
 	function get_cofig_html_option(){
 		$options = array();
 		$options['']='----';
		$options[self::$MANUAL]=get_string('processmode_manual','enrol_unasus');
		$options[self::$AUTOMATIC]=get_string('processmode_auto','enrol_unasus');
		$options[self::$MANUAL_AUTOMATIC]=get_string('processmode_manual_auto','enrol_unasus');
		return 	$options ;
	}
 }