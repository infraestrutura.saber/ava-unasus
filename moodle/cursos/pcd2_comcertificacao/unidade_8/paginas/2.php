<?php
               
    $pg_atual=2;
    registro($id,unid8_pg,$pg_atual,unid8_ev);
?>
    <style type="text/css">
        #lista li, .box ul li{
            list-style-type: disc; 
             margin-left: 30px;
        }
		
		.box ul li ul li{
			list-style-type: circle !important; 
		}
		
		
		
    </style>
   <!-- <script src = "https://www.youtube.com/iframe_api"></script> -->

   <h4 class="titulo">Acondicionamento e transporte do material biológico</h4>
	Garantir o acondicionamento, conservação e transporte do material biológico 
	até a recepção pelo laboratório executor dos exames é o objetivo de uma coleta 
	satisfatória.<br><br>
	Ao final do período da coleta, é importante fazer a conferência dos pedidos 
	com os frascos, verificar se todas as amostras estão perfeitamente tampadas 
	e identificadas, incluindo a data da coleta, certificando-se de que o material 
	não cairá durante o transporte (colocar calço ou fixar com fita adesiva) e 
	ficará protegido do sol e da umidade. Para realizar este procedimento, até o 
	devido acondicionamento de todo o material coletado, é fundamental que o 
	profissional esteja paramentado. <br><br>
	É preciso lembrar que todo material encaminhado ao laboratório deve ser 
	acompanhado de uma via de solicitação de exames laboratoriais, na qual são 
	inseridos o nome do usuário atendido, o número do registro e os exames 
	solicitados, ficando a outra via na unidade para controle do retorno dos 
	resultados e relatório estatístico (PARANÁ, 2012; SÃO PAULO, 2006).<br><br>
	O acondicionamento do material coletado deverá ser tecnicamente apropriado, 
	segundo a natureza de cada material a ser transportado, de forma a impedir a 
	exposição dos profissionais da saúde, assim como dos profissionais da frota 
	que transportam o material.<br><br>
	O profissional que realizar a coleta de material biológico deverá manter, no 
	mínimo, dois jogos de caixas para transporte a fim de facilitar a higienização 
	e as trocas, sendo que cada jogo é composto por duas caixas: uma para transportar
	 sangue e outra para transportar fezes/urina/escarro. As amostras de sangue 
	 deverão ser acondicionadas em recipientes rígidos, constituídos de materiais 
	 apropriados para tal finalidade, dotados de dispositivos pouco flexíveis e 
	 impermeáveis para fechamento sob pressão, como as caixas térmicas 
	 (PARANÁ, 2012; SÃO PAULO, 2006).<br><br>

<br>
	
<div style="background-color: #e9cac0;padding: 5px;border-radius: 10px;">
	<h5>Caixas térmicas</h5> São recipientes de segurança para transporte, destinados à acomodação 
	das estantes e grades com tubos contendo as amostras biológicas. Estas caixas devem, obrigatoriamente, 
	ser rígidas, resistentes e impermeáveis, revestidas internamente com material liso, durável, lavável
	 e resistente às soluções desinfetantes, devendo ainda ser dotadas de dispositivos de fechamento externo. 
	 Sacos, pastas ou envelopes não devem ser colocados dentro das caixas térmicas. Os mesmos devem ser 
	 afixados na parte superior destas (PARANÁ, 2012). 
	<br><br>
	Como medida de segurança para o transporte, na parte externa das caixas térmicas, deverá ser fixado o símbolo 
	de material infectante e inscrito, com destaque, o título de identificação: <b>MATERIAL INFECTANTE.</b> Também 
	deverá ser inscrito o desenho de seta indicativa vertical apontada para cima, de maneira a caracterizar a 
	disposição vertical, com as extremidades de fechamento voltadas para cima. As caixas térmicas devem ser 
	protegidas do calor e da exposição solar (PARANÁ, 2012).<br><br>
	<b>Exemplo de caixa térmica adequada para transporte de material biológico </b><i class=" icon-hand-up" title="Essa imagem possui interação."></i>
	<br><br>
	
	<div id="interacao">
		<img src="images/fig03-p8_imgbase.png" alt="Caixa Térmica"><br>
		<div class="um_action"></div>
		<div class="um_i"><img src="images/fig03-p8_quadro3.png"></div>
		<div class="dois_action"></div>
		<div class="dois_i"><img src="images/fig03-p8_quadro4.png"></div>
		<div class="tres_action"></div>
		<div class="tres_i"><img src="images/fig03-p8_quadro1.png"></div>
		<div class="quatro_action"></div>
		<div class="quatro_i"><img src="images/fig03-p8_quadro2.png"></div>
	</div>
	
	<b>Fonte:</b> (UNA-SUS UFPE, 2014) <!--(UFPE, 2014)-->.<br><br>
</div>
<br>
<div style="background-color: #e9cac0;padding: 5px;border-radius: 10px;">
	<h5>Estantes e grades</h5> 
	São recipientes de suporte utilizados para acondicionar tubos e 
	frascos coletores contendo amostras biológicas. Deverão ser rígidas e resistentes, não quebráveis, 
	permitir a fixação em posição vertical, com a extremidade de fechamento (tampa) voltada para cima, impedindo 
	o tombamento do material.<br><br>
	<b>Modelo de estante e grade para transporte de tubos</b><i class=" icon-hand-up" title="Essa imagem possui interação."></i><br>
	<!-- interacao -->
	<div id="imagem_base">

		<img  src="images/Grade-transp-tubos_pag32-cap_6a8.jpg" alt="Estante de tubos"><br>
		<div class="descricao_vermelho"><b>Descrição: </b>tubo seco, sem coagulantes.<br><b>Indicação: </b>usado para sorologia, dosagens bioquímicas (íons) e para obtenção de plasma.</div>
		<div class="vermelho"></div>
		<div class="descricao_amarelo"><b>Descrição: </b>contém ativador de coágulo jateado na parede do tubo, que acelera o processo de coagulação e gel separador para obtenção de soro.<br><b>Indicação:</b> são utilizados para análises de bioquímica (rotina e especiais), sorologia, imunologia, marcadores tumorais e marcadores cardíacos, hormônios específicos e drogas terapêuticas.</div>
		<div class="amarelo"></div>
		<div class="descricao_preto"><b>Descrição: </b>contêm solução de citrato de sódio.<br> <b>Indicação: </b>são utilizados para exames de Velocidade de Hemossedimentação (VHS).</div>
		<div class="preto"></div>
		<div class="descricao_azul"><b>Descrição: </b>tubo com anticoagulante (citrato trissódico 3,2 a 3,8%).<br><b>Indicação: </b>usado para a realização de coagulograma, exames de tempo de atividade da 
		protrombina, tempo de tromboplastina e antitrombina III.</div>
		<div class="azul"></div>
		<div class="descricao_verde"><b>Descrição: </b>tubo com anticoagulante (heparina sódica, lítica ou amônica).<br><b>Indicação: </b>usado para a obtenção de plasma.</div>
		<div class="verde"></div>
		<div class="descricao_roxo"><b>Descrição: </b>tubo com EDTA-K2, EDTA-K3 ou EDTA 8%.<br><b>Indicação: </b>usado para exames hematológicos, tais como contagem de plaqueta, eletroforese de hemoglobina, grupo sanguíneo, fator Rh.</div>
		<div class="roxo"></div>
		<div class="descricao_cinza"><b>Descrição: </b>tubo com oxalato de potássio, estabilizador, ou fluoreto de sódio, EDTA.<br><b>Indicação: </b>usado para glicemia, teste de tolerância da glicose, prova de sobrecarga de lactose, teste de tolerância da insulina.</div>
		<div class="cinza"></div>
	</div>
	<td colspan="3" style="background-color:#ccc"><b> </b></td>
	
	<br>
	<b>Fonte: </b> (UNA-SUS UFPE, 2014) <!--(SÃO PAULO, 2006, adaptado)-->. 
</div>
	<br><br>
<div id="player_video" class="box">
         <img src="../images/img_vocesabiaque_ad.png" alt="Você sabia" >
        <span class="titulo_box">Você sabia que...</span>
        <br>
        <hr/>
<table >
		<tr>
			<td width="420"> <div id="player"></div>
        </td>
			<td class="josi2"> A coleta de gasometria arterial deve ser realizada preferencialmente na artéria radial, utilizando uma seringa com anticoagulante, sendo um procedimento privativo do enfermeiro ou do médico (COFEN, 2011).

Antes de realizar a punção na artéria radial, é importante realizar a avaliação da circulação colateral através do teste de Allen. 
Assista ao vídeo a seguir, que orienta a execução do teste.</td>

		</tr>
	</table >
	<br>
</div>
<br>
<br>
Para um transporte seguro de material biológico e/ou resíduos infectantes, bem como para a sua chegada em tempo hábil e em condições adequadas, é necessário que haja uma sintonia entre o remetente, transportador e 
laboratório de destino (SÃO PAULO, 2006).
<br><br>
<div class="box">
        <img src="../images/img_atencao_ad.png"  alt="Atenção">
        <span class="titulo_box">Atenção!</span>
        <br>
        <hr/>
		<ul id="lista">
		<li>É vedado, em qualquer hipótese, o transporte de recipientes contendo resíduos infectantes no compartimento dianteiro dos veículos automotores;</li> 
<li>O fato de a amostra de urina ser de fácil obtenção pode induzir a um descuido no tratamento da amostra após sua coleta. Sendo assim:<br>
<ul>
<li>esta amostra deve ser colhida em recipiente descartável, limpo e seco. No caso de urocultura, o frasco deve ser estéril;</li>
<li>lembrar de inserir no recipiente de coleta a identificação com nome do paciente, data e hora da coleta;</li>
<ul>
<li style="list-style-type: square !important; ">sua entrega ao laboratório para análise deve ocorrer no máximo 1 hora a partir da coleta e, caso não seja possível, refrigerar para prevenir a decomposição da urina e a proliferação bacteriana na amostra. A amostra não deve ser congelada (SÃO PAULO, 2006).</li></ul>
</ul></li>
 <br>

</div>
<style type="text/css">
			.josi2{
				line-height: 22px;
			}
           .um_descricao , .dois_descricao, .tres_descricao, .quatro_descricao, .cinco_descricao{
            color: #e9cac0;

           }
           .um ,.dois, .tres, .quatro,.cinco{
            cursor: pointer;
           }
           .um_action{
				background-color: #B62C31;
				position: absolute;
				height: 7px;
				top: 233px;
				left: 225px;
				width: 7px;
				z-index: 7;
				border: 2px solid #fff;
				border-radius: 30px 30px 30px 30px;
				cursor: pointer;
			}
			 .um_i{
				
				position: absolute;
				top: 163px;
				left: 233px;
				z-index: 7;
				display: none;
								
			}
			    .dois_action{
				background-color: #B62C31;
				position: absolute;
				height: 7px;
				top: 265px;
				left: 287px;
				width: 7px;
				z-index: 7;
				border: 2px solid #fff;
				border-radius: 30px 30px 30px 30px;
				cursor: pointer;
			}
			 .dois_i{
				
				position: absolute;
				top: 276px;
				left: 297px;
				z-index: 7;
				display: none;
								
			}
				.tres_action{
				background-color: #B62C31;
				position: absolute;
				height: 7px;
				top: 136px;
				left: 207px;
				width: 7px;
				z-index: 7;
				border: 2px solid #fff;
				border-radius: 30px 30px 30px 30px;
				cursor: pointer;
			}
			 .tres_i{
				
				position: absolute;
				top: 20px;
				left: 212px;
				z-index: 7;
				display: none;
								
			}
				.quatro_action{
				background-color: #B62C31;
				position: absolute;
				height: 7px;
				top: 100px;
				left: 207px;
				width: 7px;
				z-index: 7;
				border: 2px solid #fff;
				border-radius: 30px 30px 30px 30px;
				cursor: pointer;
			}
			.quatro_i{
				
				position: absolute;
				top: 115px;
				left: 212px;
				z-index: 7;
				display: none;
								
			}

			#interacao{
				position: relative;
			}
			.table td {
                border-top: 1px solid #DDDDDD;
                line-height: 20px;
                padding: 8px;
                text-align: left;
                vertical-align: middle;
}
/*novo ------------------*/
			#imagem_base{
				position: relative;
			}
			.descricao_vermelho{
				padding: 20px;
				background-color: #B62C31;
				color: #fff;
				border: 2px solid #fff;
				border-radius: 7px;
				position: absolute;
				top: 20px;
				left: 50px;
				display: none;

			}
				.vermelho{
				background-color: #B62C31;
				position: absolute;
				height: 7px;
				top: 219px;
				left: 138px;
				width: 7px;
				z-index: 7;
				border: 2px solid #fff;
				border-radius: 30px 30px 30px 30px;
				cursor: pointer;

			}
				.descricao_amarelo{
				padding: 20px;
				background-color: #daa520;
				border: 2px solid #fff;
				border-radius: 7px;
				position: absolute;
				top: 20px;
				left: 50px;
				display: none;
				color: #fff;

			}
					.amarelo{
				background-color: #B62C31;
				position: absolute;
				height: 7px;
				top: 219px;
				left: 222px;
				width: 7px;
				z-index: 7;
				border: 2px solid #fff;
				border-radius: 30px 30px 30px 30px;
				cursor: pointer;

			}
			.preto{
				background-color: #B62C31;
				position: absolute;
				height: 7px;
				top: 213px;
				left: 299px;
				width: 7px;
				z-index: 7;
				border: 2px solid #fff;
				border-radius: 30px 30px 30px 30px;
				cursor: pointer;
			}
			.descricao_preto{
				color: #fff;
				padding: 20px;
				background-color: #000;
				border: 2px solid #fff;
				border-radius: 7px;
				position: absolute;
				top: 20px;
				left: 50px;
				z-index: 7;
				display: none;

			}
			.verde{
				background-color: #B62C31;
				position: absolute;
				height: 7px;
				top: 213px;
				left: 375px;
				width: 7px;
				z-index: 7;
				border: 2px solid #fff;
				border-radius: 30px 30px 30px 30px;
				cursor: pointer;
			}
				.descricao_verde{
				color: #fff;
				padding: 20px;
				background-color: #17A86E;
				border: 2px solid #fff;
				border-radius: 7px;
				position: absolute;
				top: 20px;
				left: 50px;
				z-index: 7;
				display: none;

			}
			.descricao_azul{
				color: #fff;
				padding: 20px;
				background-color: #2A9AE1;
				border: 2px solid #fff;
				border-radius: 7px;
				position: absolute;
				top: 20px;
				left: 50px;
				z-index: 7;
				display: none;

			}
			.azul{
				background-color: #B62C31;
				position: absolute;
				height: 7px;
				top: 209px;
				left: 453px;
				width: 7px;
				z-index: 7;
				border: 2px solid #fff;
				border-radius: 30px 30px 30px 30px;
				cursor: pointer;
			}
				.descricao_roxo{
				color: #fff;
				padding: 20px;
				background-color: #601352;
				border: 2px solid #fff;
				border-radius: 7px;
				position: absolute;
				top: 20px;
				left: 50px;
				z-index: 7;
				display: none;

			}
			.roxo{
				background-color: #B62C31;
				position: absolute;
				height: 7px;
				top: 202px;
				left: 531px;
				width: 7px;
				z-index: 7;
				border: 2px solid #fff;
				border-radius: 30px 30px 30px 30px;
				cursor: pointer;
			}
				.descricao_cinza{
				color: #fff;
				padding: 20px;
				background-color: #8A8583;
				border: 2px solid #fff;
				border-radius: 7px;
				position: absolute;
				top: 20px;
				left: 50px;
				z-index: 7;
				display: none;

			}
			.cinza{
				background-color: #B62C31;
				position: absolute;
				height: 7px;
				top: 202px;
				left: 603px;
				width: 7px;
				z-index: 7;
				border: 2px solid #fff;
				border-radius: 30px 30px 30px 30px;
				cursor: pointer;
			}
           </style>
           <script type="text/javascript">
           //interacao figura
             $(".um_action").click(function(){
                $(".um_i").fadeIn(1500);
              });
            $(".dois_action").click(function(){
                $(".dois_i").fadeIn(1500);
              });
           $(".tres_action").click(function(){
                $(".quatro_i").fadeIn(1500);
              });
            $(".quatro_action").click(function(){
                $(".tres_i").fadeIn(1500);
              });
            //interacao quadros
            $(".vermelho").click(function(){
            	limpar();
                $(".descricao_vermelho").fadeIn(1500);
              });

            $(".amarelo").click(function(){
            	limpar();
                $(".descricao_amarelo").fadeIn(1500);
              });
           $(".verde").click(function(){
           	limpar();
                $(".descricao_verde").fadeIn(1500);
              });
            $(".preto").click(function(){
            	limpar();
                $(".descricao_preto").fadeIn(1500);
              });
            $(".azul").click(function(){
            	limpar();
                $(".descricao_azul").fadeIn(1500);
              });
            $(".roxo").click(function(){
            	limpar();
                $(".descricao_roxo").fadeIn(1500);
              });
            $(".cinza").click(function(){
            	limpar();
                $(".descricao_cinza").fadeIn(1500);
              });

            function limpar(){
            	 $(".descricao_amarelo, .descricao_preto, .descricao_azul, .descricao_verde, .descricao_vermelho, .descricao_cinza ,.descricao_roxo").fadeOut(1000);
            };
			
			//onYouTubePlayerAPIReady
 // 2. This code loads the IFrame Player API code asynchronously.
      var tag = document.createElement('script');

      tag.src = "https://www.youtube.com/iframe_api";
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

      // 3. This function creates an <iframe> (and YouTube player)
      //    after the API code downloads.
      var player;
      function onYouTubeIframeAPIReady() {
        player = new YT.Player('player', {
          height: '250',
          width: '400',
          videoId: 'SzNSUhfcjVI',
          events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
          }
       
        });
      }
      // 4. The API will call this function when the video player is ready.
      function onPlayerReady(event) {
        //event.target.playVideo();
      }

      // 5. The API calls this function when the player's state changes.
      //    The function indicates that when playing a video (state=1),
      //    the player should play for six seconds and then stop.
      var done = false;
      function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.PLAYING && !done) {
        $.post( "../apresentacao/functions/chamaCadVideo.php", { video: 9}, function() {
						
				});
          done = true;
        }
      }
       </script>
	<?php
//cadastrarVideo($id,9);
	if ($_GET['video']==1){ 

		echo '<script type="text/javascript">$("html,body").animate({scrollTop: $("#player_video").offset().top}, 1500);</script>';
	}
	
?>

