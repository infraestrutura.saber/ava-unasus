<?php

   require("../../../../config.php");
   require("$CFG->dirroot/enrol/unasus/profilefield/category/lib.php");

   require("$CFG->dirroot/enrol/unasus/profilefield/category/form_add.php");

require_login();

   //permiss�o
    require_capability('enrol/unasus:profilefieldcategorycreate', get_context_instance(CONTEXT_SYSTEM), NULL, false);
    add_to_log(1, 'enrol_unasus', 'profilefieldcategorycreate', "enrol/unasus/profilefield/category/add.php?contextid=$contextid", getremoteaddr());    
    //Navega��o
   $navigation = build_navigation(array(array('name' => get_string('enrolname','enrol_unasus'), 'link' => "$CFG->wwwroot/enrol/unasus/index.php", 'type' => 'misc'),
                                     array('name' => get_string('cron_config','enrol_unasus'), 'link' => "$CFG->wwwroot/enrol/unasus/cron/index.php", 'type' => 'misc'), array('name' => get_string('cron_config_add','enrol_unasus'), 'link' => null, 'type' => 'misc')));
    
    $param=  new object();
    $param->contextid= required_param('contextid',PARAM_INT);
     $profilefield_category =new profilefield_category();
    $form= new form_add();

       if ($form->is_cancelled()) {
        redirect($CFG->httpswwwroot.'/enrol/unasus/profilefield/manage.php?contextid='.$param->contextid) ;

    } else if ($formdata = $form->get_data()) {
    	$param->name=$formdata->name;
    	
    	$profilefield_category->save($param);
    	redirect($CFG->httpswwwroot.'/enrol/unasus/profilefield/manage.php?contextid='.$param->contextid) ;
    	  
    	
    }
 
     

     print_header(get_string('enrolname','enrol_unasus'),get_string('applicationsenrolment','enrol_unasusem'), $navigation);
   
 	
    $form->display();
  
    print_footer();
    
?>