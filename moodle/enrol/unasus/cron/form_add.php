<?php

require_once $CFG->libdir.'/formslib.php';

class form_add extends moodleform {

    function definition() {
    	global $param;
        $mform    =& $this->_form;
        $renderer =& $mform->defaultRenderer();

        $mform->addElement('header', '', get_string('cron_situation_review','enrol_unasus'), '');
         $mform->addElement('hidden', 'contextid', $param->contextid);
        $mform->setType('contextid', PARAM_INT);
        
        $mform->addElement('checkbox', 'erro_arouca_conn', get_string('msg_erro_arouca_conn','enrol_unasus'));
        $mform->addElement('checkbox', 'no_cbo_arouca', get_string('msg_no_cbo_arouca','enrol_unasus'));
        $mform->addElement('checkbox', 'no_role_created', get_string('msg_no_role_created','enrol_unasus'));
        $mform->addElement('checkbox', 'no_cbo_in_role', get_string('msg_no_cbo_in_role','enrol_unasus'));
       
       $mform->addElement('header', '', get_string('cron_frequency_process','enrol_unasus'), '');
      $mform->addElement('text', 'interval_teme_process', get_string('cron_interval_teme_process','enrol_unasus'),'size="8"');
        $mform->setType('interval_teme_process', PARAM_INT);
        $mform->setHelpButton('interval_teme_process', array('cron_frequency_process', get_string('cron_frequency_process', 'enrol_unasus'), 'enrol_unasus'));
        
        
        $this->add_action_buttons(true,get_string('save','enrol_unasus'));
    }

    function validation($data, $files) {
       $errors = parent::validation($data, $files);
       /* if(!is_int($data['interval_teme_process']) ){
        	$errors['interval_teme_process'] = get_string('required_int','enrol_unasus');
        }*/
     return $errors;
    }

}

?>