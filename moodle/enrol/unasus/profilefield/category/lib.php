<?php
/*
 * Created on 19/05/2012
 *
 *Essa class tem as bibliotecas de gerenciamento
 * da categoria do perfil do usu�rio. 
 */
 class profilefield_category {
 	
 	//inserir regristro na base de dados. 
 	function save($dto) {
 		global $CFG;
 		$dto->sortorder=$this->get_next_order($dto->contextid);
 		return insert_record('enrol_unasus_info_category', $dto);
 	}
 
 //alteriar regristro na base de dados. 
 	function edit($dto) {
 		global $CFG;
 		return update_record('enrol_unasus_info_category', $dto);
 	}

  //excluir regristro na base de dados. 
 function delete_by_id($id) {
 		global $CFG;
 		return delete_records_select('enrol_unasus_info_category', "id=$id");
 	}
  //Retora a quantidade de registro cadastrada na base de dados
 function count() {
 		global $CFG;
		$sql ="SELECT COUNT(id)  AS qreg FROM {$CFG->prefix}enrol_unasus_info_category ";
		$r=get_record_sql($sql);
		return $r->qreg;
 	}
 	function get_by_id($id) {
 		global $CFG;
		$sql ="SELECT id,contextid,name,sortorder,description  FROM {$CFG->prefix}enrol_unasus_info_category WHERE id=$id";
		$r=get_record_sql($sql);
		return $r;
 	}
 	function get_all() {
 		global $CFG;
		$sql ="SELECT id,contextid,name,sortorder,description   FROM {$CFG->prefix}enrol_unasus_info_category  ORDER BY sortorder";
		$r=get_records_sql($sql);
		return $r;
 	}
 	function get_by_contextid($contextid) {
 		global $CFG;
		$sql ="SELECT id,contextid,name,sortorder,description   FROM {$CFG->prefix}enrol_unasus_info_category WHERE contextid=$contextid ORDER BY sortorder";
		$r=get_records_sql($sql);
		return $r;
 	}
 	function get_name_by_id($id) {
 		global $CFG;
		$sql ="SELECT name  FROM {$CFG->prefix}enrol_unasus_info_category WHERE id=$id";
		$r=get_record_sql($sql);
		return $r->name;
 	}
 	function exist_create_name($contextid,$name) {
 		global $CFG;
		$sql ="SELECT COUNT(id)  AS qreg   FROM {$CFG->prefix}enrol_unasus_info_category WHERE contextid=$contextid AND name='".$name."'";
		$r=get_record_sql($sql);
		if( $r->qreg==0) return FALSE;
		if( $r->qreg==1) return TRUE;
		return TRUE;
 	}
 	function exist_edit_tname($id, $contextid,$name) {
 		global $CFG;
		$sql ="SELECT COUNT(id)  AS qreg   FROM {$CFG->prefix}enrol_unasus_info_category WHERE id!=$id AND contextid=$contextid AND name='".$name."'";
		$r=get_record_sql($sql);
		if( $r->qreg==0) return FALSE;
		if( $r->qreg==1) return TRUE;
		return TRUE;
 	}
 	
 	function get_last_order($contextid) {
 		global $CFG;
		$sql ="SELECT MAX(sortorder) AS maxorder   FROM {$CFG->prefix}enrol_unasus_info_category WHERE contextid=$contextid ";
		$r=get_record_sql($sql);
		return $r->maxorder;
 	}
 	function get_next_order($contextid) {
 		$last=$this->get_last_order($contextid);
 		if(empty($last)) return 1;
 		else return $last+1;
 	}
 	function get_order($contextid,$sortorder) {
 		global $CFG;
		$sql ="SELECT id,sortorder   FROM {$CFG->prefix}enrol_unasus_info_category WHERE contextid=$contextid AND sortorder=$sortorder";
		$r=get_record_sql($sql);
		return $r;
 	}
 	function get_all_in_sort_by_order($contextid) {
 		global $CFG;
		$sql ="SELECT id,sortorder   FROM {$CFG->prefix}enrol_unasus_info_category  WHERE contextid=$contextid ORDER BY sortorder ";
		$rows=get_records_sql($sql);
		$result=array();
		if(empty($rows) )return $result;
		$cont=0;
		foreach ($rows as $row) {
			$cont++;
			$result[$cont]=$row;
		}
		return $result;
 	}
 	function update_order($contextid,$seq,$operation) {
 		global $CFG;
 		$list_cat=$this->get_all_in_sort_by_order($contextid);
 		
 		$selected_cat=$list_cat[$seq];
 		$selected_cat_ref=null;
 		print_r($list_cat);
 		if($operation=='up'){
 			$selected_cat->sortorder--;
 			$selected_cat_ref=$list_cat[$seq-1];
 			$selected_cat_ref->sortorder++;
 			update_record('enrol_unasus_info_category', $selected_cat);
 			update_record('enrol_unasus_info_category', $selected_cat_ref);
 			
 		}
		else if($operation=='down'){
			$selected_cat->sortorder++;
 			$selected_cat_ref=$list_cat[$seq+1];
 			$selected_cat_ref->sortorder--;
 			update_record('enrol_unasus_info_category', $selected_cat);
 			update_record('enrol_unasus_info_category', $selected_cat_ref);
		}
	
 	}
 	
 
 		function get_html_option($contextid){
 		$categories=$this->get_by_contextid($contextid);
 		
 		$options = array();
 		//$options['']='----';
 		if(empty($categories))return 	$options ;
 		foreach ($categories as $category){
			$options[$category->id]=$category->name;
 		};
		return 	$options ;
	}
	
 }