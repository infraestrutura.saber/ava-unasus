﻿<?PHP 
$string['synciccarouca']='Sync Arouca';


$string['cpfinvalidolog'] = 'CPF inválido';
$string['connhttparoucafailedlog'] = 'Falha de conexão com a Plataforma Arouca/CNES';

$string['save']='Salvar';
$string['delete']='Excluir';
$string['edit']='Editar';
$string['add']='Cadastar';
$string['view']='Visualizar';
$string['confirm']='Confirmar';
$string['search'] = 'Pesquisa';
$string['cancel'] = 'Cancelar';
$string['id'] = 'ID';
$string['course'] = 'Curso';
$string['user'] = 'Usuário';
$string['enroldate'] = 'Data de Inscrição';
$string['enroldateupdate'] = 'Data de Atualização';
$string['status'] = 'Status';
$string['type'] = 'Tipo';
$string['rule'] = 'Regra';
$string['rule1'] = 'Regra I';
$string['key'] = 'Chave';
$string['value'] = 'Valor';
$string['source'] = 'Origem';
$string['amount'] = 'Total';
$string['entorldate'] = 'Dados da Inscrição';
$string['reporter'] = 'Relatório';
$string['homologar_concluintes'] = 'Homologar Concluintes ';
$string['requiredfiedl'] = 'Campo obrigatório'; 
$string['require_number']='Digite valor numérico. Exemplo 6,5';
$string['rowcount'] = 'Quant. de Registro: ';
$string['rowfilter'] = 'Registro Filtrado: '; 
$string['config']='Configuração';
$string['research']='Pesquisar';

$string['manual_aprove'] = 'Homologação Manual da Matrícula';
$string['manual_aprove_by'] = 'Homologação por';
$string['justify'] = 'Justificativa';
$string['goon'] = 'Avançar';
$string['observation'] = 'Observação';
$string['description'] = 'Descrição';
$string['yes'] = 'Sim';
$string['no'] = 'Não';

$string['integracao_arouca'] = 'Integração com a Plataforma Arouca';
$string['relatorio_sinc'] = 'Relatório de Sincronização';
$string['relatorio_sinc_arouca'] = 'Relatório de Sincronização com a Plataforma Arouca';
$string['homologacao_concluintes'] = 'Homologação de Concluintes ';

$string['status_ingres_inscrito_moodle']='Ingressante registrado no curso do Moodle';
$string['status_ingres_sinc_arouca']='Ingressante sincronizado com a Plataforma Arouca';
$string['status_ingres_sinc_falhaws_arouca']='Ingressante não sincronizado com a Plataforma Arouca- Falha de Web Service';

$string['status_concl_homol_moodle']='Concluinte homologado na Plataforma Moodle';
$string['status_concl_sinc_arouca']='Concluinte sincronizado com a Plataforma Arouca';
$string['status_concl_sinc_falhaws_arouca']='Concluinte  não sincronizado com a Plataforma Arouca- Falha de Web Service';
$string['status_concl_homol_gestor']='Concluinte homologado pelo gestor'; 

$string['status_concl_cert_emitido']='Certificado emitido'; 
$string['status_concl_cert_enviado']='Certificado enviado';
$string['status_concl_cert_falhaws_arouca']='Certificado não emitido - Falha de Web Service';

$string['cpf']='CPF';
$string['data_ingresso']='Data de Ingresso';
$string['data_conclusao']='Data de Conclusão';
$string['certificado']='Certificado';
$string['msg_falha_ws']='Mensagem de Falha  WS';
$string['homologado_por']='Homologado Por';

$string['concluintes_nhomologacao'] = 'Concluintes não Homologados';
$string['num_concluintes_nhomologados'] = 'N° de concluintes não Homologados: ';
$string['voltar_selecionar_outro_curso'] = 'Voltar e selecionar outro  curso';
$string['avancar'] = 'Avançar';
$string['homologar'] = 'Homologar';
$string['nota_final'] = 'Nota Final';
$string['email'] = 'E-mail';
$string['uf'] ='UF';
$string['cbo'] ='CBO';
$string['metodo_matricula'] ='Método de Matrícula';

#synccourse
$string['manegewscoursesync']='Gerenciar Sincronização de Curso';
$string['addnewcoursesync']='Cadastrar Nova Sincronização de Curso';
$string['editcoursesync']='Editar Sincronização de Cursoe';
$string['deletecoursesync']='Apagar Sincronização de Curso';

$string['sync']='Sincronização';
$string['mdlcoursesync']='Curso na Plataforma Moodle';
$string['aroucacoursesync']='Id do curso na Plataforma Arouca';
$string['aroucacoursesyncrequiredcf']='Id do curso na Plataforma Arouca é um campo obrigatório caso o cadastro do concluinte não for na oferta.';
$string['aroucaofertaync']='Id da oferta na Plataforma Arouca';
$string['approvedgrade']='Nota de aprovação';

$string['sendmailcoursesync']='Envio de Certificado por E-mail';
$string['sujectcoursesync']='Assunto';
$string['messagemailcoursesync']='Mensagem';

$string['coursesync_duplication']='Duplicação de  sincronização de curso';
$string['coursesync_aroucaconcluinteoferta']='Cadastrar concluinte na oferta';

?>