<?php
   require("../../../../config.php");
   require("$CFG->dirroot/enrol/unasus/manage.php");
    require("$CFG->dirroot/enrol/unasus/reporter/lib.php");
   require("$CFG->dirroot/enrol/unasus/reporter/general/search_form.php");
   require("$CFG->dirroot/enrol/unasus/tab.php");
   require("$CFG->dirroot/enrol/unasus/lib/userprofile.php");
   require("$CFG->dirroot/enrol/unasus/profilefield/lib.php");
    require("$CFG->dirroot/enrol/unasus/profilefield/data/lib.php");
    require("$CFG->dirroot/enrol/unasus/lib/configcontext.php");

 require_login();
   // Acesso permitido apenas ao usu�rio admin
   
     require_capability('enrol/unasus:viewreporterenrol', get_context_instance(CONTEXT_SYSTEM), NULL, false);
     add_to_log(1, 'enrol_unasus', 'view', 'reporter/general/search.php', getremoteaddr());    
    //Navega��o
     $navigation = build_navigation(array(array('name' => get_string('enrolname','enrol_unasus'), 'link' => "$CFG->wwwroot/enrol/unasus/index.php", 'type' => 'misc'),
                                     array('name' => get_string('search','enrol_unasus'), 'link' => null, 'type' => 'misc')));
     //tab menu
     $currenttab = 'search';
        
    $manage=new enrol_unasus_manage();
    $profile =new enrol_unasus_userprofile();
    $profilefield_field = new profilefield_field();
    $profilefield_data = new data_infofield();  
    
     $configcontext =new configcontext();
    
    //perfil do usu�rio
    $fields= $profile->get_fields_user_profile();
    $seleted_fields_filter=$profile->get_profile_config_in_array(enrol_unasus_userprofile::$USER_PROFILE_FILTER);
    $seleted_fields_reporter=$profile->get_profile_config_in_array(enrol_unasus_userprofile::$USER_PROFILE_REPORTER);
    $profile_data= null; 
  
  
    //parâmetros
    $param=  new object();
    $param->courseid= optional_param('courseid', NULL,PARAM_INT);
    $param->userid= optional_param('userid', NULL,PARAM_INT);
     $param->username= optional_param('username', NULL,PARAM_TEXT);
    $param->status= optional_param('status', NULL,PARAM_INT);
    $param->statusinfo= optional_param('statusinfo', NULL,PARAM_INT);
   // $param->role= optional_param('role', NULL,PARAM_INT);
    $param->userid_sql_profile="";
    $param->page= optional_param('page', 0,PARAM_INT);
    $param->perpage= optional_param('perpage', 100,PARAM_INT);
    $param->paginar=TRUE; 
  
   $param->show_profile_reporter=optional_param('show_profile_reporter', NULL,PARAM_INT);

$profile_data= $profile->get_data_fields_user_profile_in_array($seleted_fields_reporter);
 if($param->courseid){ 
  	$rolecontextid=  $manage->get_rolecontextid_by_course($param->courseid);
  	if(!empty($rolecontextid)){
  		$fields_enrol= $profilefield_field->get_by_contextid($rolecontextid,1);
  		$data_fields_enrol=$profile->get_data_profile_field($fields_enrol,'FE_');
  		
  		 $param->userid_sql_profile.=$profilefield_data->get_sql_filter_user_data($fields_enrol,$data_fields_enrol);
  	 
  		 $value_filter_enrol=$configcontext->get_value_by_contextid($rolecontextid,configcontext::$USER_PROFILE_ENROL_FILTER);
  		$value_filter_enrol=$profile->get_profile_config_in_array('',$value_filter_enrol);
  		
  		if($param->show_profile_reporter){
  			
  			$seleted_fields_reporter_enrol=$configcontext->get_value_by_contextid($rolecontextid,configcontext::$USER_PROFILE_ENROL_REPORTER);
  			$seleted_fields_reporter_enrol=$profile->get_profile_config_in_array('',$seleted_fields_reporter_enrol);
  			$profile_data_enrol= $profilefield_data->get_data_fields_user_profile_in_array($seleted_fields_reporter_enrol);
	  		
	   	}
  	}
  	
  }
  
   $data_fields=$profile->get_data_profile_field($fields,'F_');
   		$param->userid_sql_profile.=$profile->get_sql_filter_user_data($fields,$data_fields);
   
   //form
    $form= new general_search_form();
    
    function get_table_view(){
   	global $manage,$param;
    $rows = $manage->search($param);
     $rows_count = $manage->search_count($param);
     $util = new enrol_unasus_report_util();
      echo "<center>".get_string('rowcount','enrol_unasus') ." " . $rows_count."</centar>";
       paging($rows_count);
   	  if(!empty($rows)){
   			  $table = new object();
    	  		$table->head  = array(get_string('id','enrol_unasus'),get_string('name','enrol_unasus'),get_string('course','enrol_unasus'),get_string('enroldate','enrol_unasus'),get_string('status','enrol_unasus'),get_string('type','enrol_unasus'),get_string('observation','enrol_unasus'));
    	  		$table->align = array('left', 'left','left','left','left','left','left','left');
    	  		$table=add_head_report_field_for_profile($table);
    	  		$table->width = '95%';
         		 $table->class = 'generaltable';
          		$table->data = array();
				foreach ($rows as $row) {
					$user_name=$row->firstname.' '.$row->lastname;
					$date_enrol=date('d/m/Y H:i:s',$row->time);
					
					$status=$util->get_status_domain_table_name($row->status);
					$type=$util->get_status_type_domain_table_name($row->statusinfo);
			
       				$observation=$util->get_statuscause_domain_table_name($row->statuscause);
       				$link=$link="<a href=\"detail.php?enrolid=$row->id\">$row->id</a>";
           		 	$datas=array($link,$user_name,$row->fullname,$date_enrol,$status,$type,$observation);
           		 	//$table->data[] = array($link,$user_name,$row->fullname,$date_enrol,$status,$type,$observation);
           		 	
           		 	$table->data[]=add_data_report_field_for_profile($datas,$row->userid);
        		}
        		
   				 print_table($table);
   		}
   		 paging($rows_count);
   }
     
     
    print_header(get_string('applicationsenrolment','enrol_unasusem'),get_string('applicationsenrolment','enrol_unasusem'), $navigation);
   
 	print_tabs($tabs, $currenttab, $inactive, $activated);
    $form->display();
  
  get_table_view();
  
 
    print_footer();
    
     function paging($totalcount){
  		global $CFG,$param;
  		 print_paging_bar($totalcount, $param->page, $param->perpage, "$CFG->wwwroot/enrol/unasus/reporter/general/search.php?".get_url_param_profile_filter()."".get_url_param_profile_enrol_filter()."&courseid=$param->courseid&userid=$param->userid&status=$param->status&statusinfo=$param->statusinfo&username=$param->username&show_profile_reporter=$param->show_profile_reporter&perpage=$param->perpage&");
  }
   
 function get_url_param_profile_filter(){
 	global $fields;
    global $seleted_fields_filter;
    if(empty($fields)) return "";
    $param_profile="";
    foreach ($fields as $field){
    	if(isset($seleted_fields_filter[$field->id])) {
    		$param_profile.="&F_".$field->id."=".optional_param('F_'.$field->id,'',PARAM_TEXT);
    	}
    } 
    return $param_profile;
 }
  function get_url_param_profile_enrol_filter(){
 	global $fields_enrol;
 	global $value_filter_enrol;
    global $param;
   
    $param_profile="";
    if($param->courseid){
    	 if(empty($fields_enrol)) return "";
  		 foreach ($fields_enrol as $field){
  		 	if(isset($value_filter_enrol[$field->id])) {
    			$param_profile.="&FE_".$field->id."=".optional_param('FE_'.$field->id,'',PARAM_TEXT);
  		 	}
    	}
   	 }
    return $param_profile;
 }
 function add_head_report_field_for_profile($table){
 	global $fields;
 	global $seleted_fields_reporter;
 	global $param;
 	
 	foreach ($fields as $field){
 		if(isset($seleted_fields_reporter[$field->id])) {
 			array_push($table->head,$field->name);
 			array_push($table->align,'left');
 		}
 	}
 	if(!$param->show_profile_reporter) return $table;
 	
 	if($param->courseid && !isset($fields_enrol)){
 		global $fields_enrol;
 		global $seleted_fields_reporter_enrol;
 		global $profile_data_enrol;
 		
 		foreach ($fields_enrol as $field){
 		  if(isset($seleted_fields_reporter_enrol[$field->id])) {
 			array_push($table->head,$field->name);
 			array_push($table->align,'left');
 		 }
 	}
 	}
 	
 	return $table;
 }
 
 function add_data_report_field_for_profile($datas,$userid){
 	global $fields;
 	global $seleted_fields_reporter;
 	global $profile_data;
 	global $param;
 	 $util = new enrol_unasus_report_util();
 	
 	foreach ($fields as $field){
 		$key=$field->id."/".$userid;
 		if(isset($seleted_fields_reporter[$field->id])) {
 			$value="";
 			if(array_key_exists($key,$profile_data)){
 				$value=$profile_data[$key];
 				if($field->datatype=='checkbox'){$value=$util->get_string_option_yes_not($value);}
 				}
 			array_push($datas,$value);
 			
 		}
 	}
 	if(!$param->show_profile_reporter) return $datas;
 	if($param->courseid && !isset($fields_enrol)){
 		global $fields_enrol;
 		global $seleted_fields_reporter_enrol;
 		global $profile_data_enrol;
 		
 		foreach ($fields_enrol as $field){
 		$key=$field->id."/".$userid;
 		if(isset($seleted_fields_reporter_enrol[$field->id])) {
 			$value="";
 			if(array_key_exists($key,$profile_data_enrol)){
 				$value=$profile_data_enrol[$key];
 				if($field->datatype=='checkbox'){$value=$util->get_string_option_yes_not($value);}
 				}
 			
 			array_push($datas,$value);
 			
 			}
 		}
 	}
 	return $datas;
 }
?>