<?php
  require("../../../../config.php");
   require("$CFG->dirroot/enrol/unasus/lib/userprofile.php");
   require("$CFG->dirroot/enrol/unasus/reporter/conf/profile_form.php");
    require("$CFG->dirroot/enrol/unasus/tab.php");

   //permiss�o
   require_capability('enrol/unasus:configreporterenrol', get_context_instance(CONTEXT_SYSTEM), NULL, false);
    add_to_log(1, 'enrol_unasus', 'configreporterenrol', "enrol/unasus/reporter/conf/profile.php", getremoteaddr());    
    //Navega��o
    
     $navigation = build_navigation(array(array('name' => get_string('enrolname','enrol_unasus'), 'link' => "$CFG->wwwroot/enrol/unasus/index.php", 'type' => 'misc'),
                                     array('name' => get_string('config_reporter_profile_conf','enrol_unasus'), 'link' => null, 'type' => 'misc')));
   
        //tab menu
     $currenttab = 'config';
     $secondrow = array();
     $secondrow[] = new tabobject('wskey',"$CFG->wwwroot/enrol/unasus/wskey/index.php",get_string('wskeyauth','enrol_unasus'));
     $secondrow[] = new tabobject('message',"$CFG->wwwroot/enrol/unasus/message/index.php",get_string('message','enrol_unasus'));
  	 $secondrow[] = new tabobject('processmode',"$CFG->wwwroot/enrol/unasus/processmode/index.php",get_string('processmode','enrol_unasus'));
  	 $secondrow[] = new tabobject('cron',"$CFG->wwwroot/enrol/unasus/cron/index.php",get_string('cron','enrol_unasus'));
  	 $secondrow[] = new tabobject('profilefield',"$CFG->wwwroot/enrol/unasus/profilefield/index.php",get_string('profilefield','enrol_unasus'));
  	  $secondrow[] = new tabobject($currenttab,"$CFG->wwwroot/enrol/unasus/reporter/conf/profile.php",get_string('config_reporter_profile','enrol_unasus'));
  	  $secondrow[] = new tabobject('reporter_profile_enrol',"$CFG->wwwroot/enrol/unasus/reporter/conf/enrol/index.php",get_string('config_reporter_profile_enrol','enrol_unasus'));
  
  	 $tabs = array($firstrow, $secondrow);
    //form
     $profile =new enrol_unasus_userprofile();
    $form= new enrol_unasus_profile_form();
   
   
    
    //par�metros
    $param=  new object();
    
     if ($form->is_cancelled()) {
        redirect($CFG->httpswwwroot.'/enrol/unasus/reporter/conf/profile.php') ;

    } else if ($formdata = $form->get_data()) {
    	$fields= $profile->get_fields_user_profile();
    	$seleted_fields_filter=$profile->get_selected_profile_field($fields,"F_");
    	$seleted_fields_reporter=$profile->get_selected_profile_field($fields,"R_");
    	
    	//echo "fiter: ".$seleted_fields_filter;
    	//echo "reporter: ".$seleted_fields_reporter;
    	//exit;
    	$profile->updadte_profile_config(enrol_unasus_userprofile::$USER_PROFILE_FILTER,$seleted_fields_filter);
    	$profile->updadte_profile_config(enrol_unasus_userprofile::$USER_PROFILE_REPORTER,$seleted_fields_reporter);
    	redirect($CFG->httpswwwroot.'/enrol/unasus/reporter/conf/profile.php') ;
     }
     
   
     print_header(get_string('enrolname','enrol_unasus'),get_string('applicationsenrolment','enrol_unasusem'), $navigation);
    print_tabs($tabs, $currenttab, $inactive, $activated);
 	
    $form->display();
  
    print_footer();
?>