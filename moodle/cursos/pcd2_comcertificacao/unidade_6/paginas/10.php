<?php
               
    $pg_atual=10;
     registro($id,unid6_pg,$pg_atual,unid6_ev);
?>
    
	<style type="text/css">
	
		
		table tr td{
			
			
		}
		
		.d1{

			width: 109px;
			height: 70px;
			float: left;
			
		}
		
		.d1 p{
			margin-top: 22px;
		}
		
		.d2{
			width: 351px;
			height: 70px;
			float: left;
			padding-right: 15px;
		
		
		}
		
		.d2 p{
			margin-top: 22px;
			margin-left: 10px;
		
		}
		
		.d3{
			height: 70px;
			line-height: 65px;
			
		}
		
		.d3 p{
		
			margin-left: 508px;
		}
		
		#lista{
            list-style-type: disc;  
            margin-left: 30px;
        }
		
		
	</style>
   

   <h4 class="titulo">Responsabilidades da equipe de enfermagem na execução dos procedimentos</h4><br/>
   
  A responsabilidade de manutenção e monitoramento dos estomas é de responsabilidade
   das três modalidades de atenção domiciliar (veja mais detalhes no quadro a seguir). Portanto, a partir do 
   conhecimento de todo panorama das complicações e das possíveis repercussões 
   sobre a qualidade de vida do paciente estomizado, é de fundamental relevância 
   a atuação de uma equipe interdisciplinar, que busque não apenas prestar a 
   assistência biológica, mas também que facilite o processo de aceitação, de 
   reabilitação e a capacidade de autocuidado do indivíduo. <br/> <br/>
	As intervenções se ampliam precocemente, ou seja, no período pré-operatório 
	(com cuidados como a demarcação do local do estoma, preparo intestinal e 
	nutricional e suporte emocional), medidas intra-operatórias (com técnica 
	cirúrgica e suprimento sanguíneo adequados), pós-operatórias (com a escolha 
	dos dispositivos adequados e a capacitação para o autocuidado no domicílio). 
	Jamais poderemos deixar de lembrar que a família compõe de forma integral o 
	trabalho de equipe, na busca da reabilitação e reintegração social do estomizado 
	(PAULA; SANTOS, 1999). Além disso, lembre-se:<br/> <br/>
<ul><li id="lista">a indicação dos estomas é de competência da equipe médica, 
	porém a troca da sonda de jejunostomia é de competência do enfermeiro (SÃO PAULO, 2012);</li>
<li id="lista">o enfermeiro exerce todas as atividades de enfermagem, cabendo-lhe privativamente os cuidados de enfermagem de maior complexidade técnica e que exijam conhecimentos de base científica e capacidade de tomar decisões imediatas (COREN/SP, 2012);</li>
<li id="lista">cabe ao enfermeiro, na condição de estomaterapeuta, a troca de sonda de gastrostomia (SÃO PAULO, 2012).</li></ul>
<br><br>
  <center><b>Responsabilidades do cuidado dos estomas nas modalidades de atenção domiciliar</b></center>

  <table class="table table-bordered tabela"  style="width: 640px;margin-left: 50px;margin-top: 10px;margin-bottom: 20px; ">
	
	<tr>
		<td bgcolor="#F68C23"><h5 style="color:#ffffff;text-align:center;">Estomas</h5></td>
		
	</tr>
	
	<tr>	
	<td bgcolor="#F68C23" style="border-top:3px solid #FFF0E2"><p style="color:#000; text-align:center;">Procedimentos e cuidados em AD&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Modalidades</p></td>
	</tr>
	
	
	<tr>
		<td bgcolor="#FFE4CA" style="border-bottom:3px solid #F68C23; padding: 14px;">Observar as características de normalidade do estoma, o efluente e a pele. Reforçar
e/ou orientar a prevenção de dermatite periestoma ou ainda tratar as afecções
cutâneas instaladas.</td>
	</tr>	
	
	
	<tr bgcolor="#FFF0E2">
		<td><div class="d1"><p>Traqueostomia</p></div>
		<div class="d2"><p>Troca de cânula (com cilindro de oxigênio),
higienização, aspiração e descanulação.</p></div>
		<div class="d3"><p>AD1 AD2 AD3</p></div></td>
	
	</tr>
	
	<tr bgcolor="#FFE4CA">
		<td><div class="d1"><p>Gastrostomia</p></div>
		<div class="d2"><p>Troca de sonda, curativo e retirada de sonda.</p></div>
		<div class="d3"><p>AD1 AD2 AD3</p></div></td>
	
	</tr>
	
	<tr bgcolor="#FFF0E2">
		<td><div class="d1"><p>Jejunostomia</p></div>
		<div class="d2"><p>Troca de sonda, curativo e retifixação.</p></div>
		<div class="d3"><p>AD1 AD2 AD3</p></div></td>
	
	</tr>
	
	<tr bgcolor="#FFE4CA">
		<td><div class="d1"><p>Colostomia</p></div>
		<div class="d2"><p>Troca de bolsa, higienização e curativos.</p></div>
		<div class="d3"><p>AD1 AD2 AD3</p></div></td>
	
	</tr>
	
	<tr bgcolor="#FFF0E2">
		<td><div class="d1"><p>Ileostomia</p></div>
		<div class="d2"><p>Troca de bolsa, higienização e curativos.</p></div>
		<div class="d3"><p>AD1 AD2 AD3</p></div></td>
	
	</tr>
	
	<tr bgcolor="#FFE4CA">
		<td><div class="d1"><p>Cistostomia definitiva</p></div>
		<div class="d2"><p>Troca de bolsa e higienização.</p></div>
		<div class="d3"><p>AD1 AD2 AD3</p></div></td>
	
	</tr>
	
	<tr bgcolor="#FFF0E2">
		<td><div class="d1"><p>Cistostomia provisória</p></div>
		<div class="d2"><p>Troca de sonda e curativo.</p></div>
		<div class="d3"><p>AD1 AD2 AD3</p></div></td>
	
	</tr>
	
	<tr bgcolor="#FFE4CA">
		<td><div class="d1"><p>Ureterostomia</p></div>
		<div class="d2"><p>Troca de bolsa e higienização.</p></div>
		<div class="d3"><p>AD1 AD2 AD3</p></div></td>
	
	</tr>
		
</table>
	<span style="margin-left: 300px;"><b>Fonte:</b> (BRASIL, 2013).</span>
	<br><br>  
  
<div class="box">
       <img src="../images/img_saibamais_ad.png" alt="Saiba mais" >
        <span class="titulo_box">Saiba mais...</span>
         <hr>
        
        Sobre as competências das equipes nas três modalidades de atenção domiciliar, clique <a target="blank" href= "http://bvsms.saude.gov.br/bvs/saudelegis/gm/2011/prt2029_24_08_2011.html">aqui</a>.<br/><br>
       

</div>
<br><br>

   