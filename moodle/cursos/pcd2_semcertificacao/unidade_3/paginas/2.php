<?php
               
    $pg_atual=2;
     registro($id,unid3_pg,$pg_atual,unid3_ev);
?>
	<style type="text/css">

		#lista li{
			list-style-type: disc;
       margin-left: 30px;   

		}
	
</style> 

   <h4 class="titulo">Responsabilidades  da equipe na execução do procedimento</h4>
   É importante salientar que a educação
    em saúde deve estar presente em todos os momentos da assistência domiciliar. 
    Então, faz-se necessário, durante o processo de cura ou controle da condição 
    crônica em saúde, que os indivíduos sejam agentes ativos neste processo, por 
    meio de adesão às mudanças necessárias ao seu estilo de vida. Na perspectiva 
    do cuidado no domicílio, a equipe multiprofissional sempre irá atuar de forma 
    que suas ações reflitam atitudes de acolhimento, respeito e segurança, seja ao
     paciente, à família, ao cuidador ou à comunidade (DUARTE, 2000).
<br><br>
No contexto da atuação da enfermagem,
 a abordagem aos pacientes que necessitam de assistência à saúde no domicílio deve ser
  coerente com as leis e normas que regem a profissão, estar articulada por protocolos
   clínicos e embasada por um referencial teórico e científico atualizado que contemple 
   as singularidades dos sujeitos e de sua família.
<br><br>
Tendo por base essa ideia, cabe à equipe
 de enfermagem, na execução do procedimento, estar ciente de que <br>o enteroclisma pode ser 
 realizado por técnicos e auxiliares de enfermagem sob supervisão do enfermeiro ou pelo
  próprio enfermeiro, sendo de competência privativa deste último a consulta de 
  enfermagem, que tem como objetivo avaliar (Artigo 11, do Decreto 94.406/87 que 
  regulamenta a Lei 7.498/86 do exercício profissional da enfermagem):

<ul id="lista">
<li>os possíveis sinais e sintomas de impactação das fezes (fecaloma);</li>
<li>distensão abdominal;</li>
<li>flatulência e outros distúrbios gastrointestinais;</li>
<li>pacientes críticos;</li>
    <li>casos de maior complexidade técnica que exijam conhecimentos de base 
    	científica e necessidade de tomar decisões imediatas.</li>
</ul>
<br><br>
<div class="box">
      
       <img src="../images/img_atencao_ad.png" >
        <span class="titulo_box">Atenção!</span>
        <br>
        <hr/>
        Uma parte importante dos cuidados prestados pela equipe de enfermagem ao paciente domiciliado é ajudar o paciente a superar as dificuldades de eliminação intestinal. É importante que a equipe oriente a reeducação intestinal tão logo o paciente possa se alimentar sozinho ou com ajuda de cuidadores, através da ingestão de líquidos, dieta rica em fibras vegetais ou alimentos laxantes naturais (em caso de constipação) e atividade física diária (se possível) (DUARTE, 2000; SCRAMIN e MACHADO, 2006).<br>
        <br>Além destes cuidados, algumas dicas podem ser dadas ao paciente para efetivar a reeducação intestinal, como: <br><br>

        <ul id="lista">
          <li>responder sem demora ao desejo de defecar; </li>
          <li>tentar estabelecer uma rotina com horário fixo, de preferência após uma das refeições, mesmo que sem vontade de eliminar as fezes. Oriente o paciente a permanecer de 15 a 20 minutos no sanitário, sem aplicar força para evacuar; </li>
          <li>realizar manobras de massagem (como a Manobra de Rosing); </li>
          <li>manter a privacidade do paciente em momentos de necessidade de evacuação (NETTINA, 2003; POTTER, 2009). </li>

        </ul>
        Problemas na eliminação intestinal causada por falta de privacidade (devido a presença de familiares) e limitações funcionais podem causar futuros problemas na alimentação; na mobilidade, devido a dor; lesão da região perianal causadas por fezes líquidas; e alteração de sono e conforto (DUARTE, 2000; SCRAMIN e MACHADO, 2006).

        <br><br>
       <br><br>
</div>


   
