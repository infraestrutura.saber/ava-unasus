<?php //$Id: forgot_password_form.php,v 1.7.2.1 2007/11/23 22:12:35 skodak Exp $

require_once $CFG->libdir.'/formslib.php';

class homologar_concluinte_form extends moodleform {

    function definition() {
    	 global $lista_ingresantes,$cpf,$param;
 
        $mform    =& $this->_form;
        $renderer =& $mform->defaultRenderer();

        $mform->addElement('header', '',get_string('concluintes_nhomologacao','report_synciccarouca'), '');
        
      	
    
    	foreach ($lista_ingresantes as $ing){
    	
         	 $mform->addElement('checkbox', $ing->id, $cpf->format($ing->idnumber).' '.$ing->firstname.' '.$ing->lastname);
         	$mform->setDefault( $ing->id, 1);
         	 
         }
		$mform->addElement('hidden', 'courseid',$param->courseid);
        $mform->setType('courseid', PARAM_INT);	
        //data
        $this->add_action_buttons(true,get_string('homologar','report_synciccarouca'));
    }

    function validation($data, $files) {
        global $CFG;

         $errors = parent::validation($data, $files);
         
      
     return $errors;
    }

}

class selecionar_curso_form extends moodleform {

    function definition() {
    	global $syc_ing_conc;
 
        $mform    =& $this->_form;
        $renderer =& $mform->defaultRenderer();

        $mform->addElement('header', '',get_string('homologacao_concluintes','report_synciccarouca'), '');
        
      	
        $mform->addElement('select', 'courseid',get_string('course','report_synciccarouca'),$syc_ing_conc->get_courses_html_select());
        $mform->setType('courseid', PARAM_INT);
        $mform->addRule('courseid', get_string('requiredfiedl','report_synciccarouca'), 'required', null, 'client');
        
        $this->add_action_buttons(true,get_string('avancar','report_synciccarouca'));
    }

    function validation($data, $files) {
        global $CFG;

         $errors = parent::validation($data, $files);
         
      
     return $errors;
    }

}
?>