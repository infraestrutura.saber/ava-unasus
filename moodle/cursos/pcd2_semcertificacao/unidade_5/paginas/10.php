<?php
               
    $pg_atual=10;
    registro($id,unid5_pg,$pg_atual,unid5_ev);
?>
  <h4 class="titulo">Referências</h4>
   <br/>


ANVISA (AGÊNCIA NACIONAL DE VIGILÂNCIA SANITÁRIA). <b>Resolução da Diretoria 
Colegiada - RCD Nº 63, de 6 de julho de 2000.</b>
Disponível em: <a href=" http://portal.anvisa.gov.br/wps/wcm/connect/61e1d380474597399f7bdf3fbc4c6735/RCD+N%C2%B0+63-2000.pdf?MOD=AJPERES" target="_blank">
http://portal.anvisa.gov.br/wps/wcm/connect/61e1d380474597399f7bdf3fbc4c6735/RCD+N%C2%B0+63-2000.pdf?MOD=AJPERES</a>. 
Acesso em: 19 abr. 2013.
<br/><br/>

ANVISA (Agência Nacional de Vigilância Sanitária). Consulta Pública nº 75, de 23 de dezembro de 2008. 
Disponível em: <a href="http://www.inp.org.br/pt/downloads/CP75_21fev.pdf" target="_blank">http://www.inp.org.br/pt/downloads/CP75_21fev.pdf</a>. Acesso em: 12 mar. 2013.
<br/><br/>
ATZINGEN, M.C.V.; SILVA, M.E.M.P. Desenvolvimento e análise de custo de 
   dietas enterais artesanais à base de hidrolisado protéico de carne. <b>Rev Bras 
   Nutr Clin</b>, v. 22, n. 3, p. 210-13, jul./set. 2007.<br/><br/>
BENTO, A. P. L.; JORDÃO JÚNIOR, A. A.; GARCIA, R. W. D. <b>Manual do paciente em 
terapia nutricional enteral domiciliar</b>. [2011]. 51 p. Disponível em: 
<a href="http://www.crn8.org.br/audiovisual/publicacoes/2011/manual-do-paciente/Manual-do-paciente-em-terapia-nutricional-enteral-domiciliar.pdf" target="_blank">
	www.crn8.org.br/audiovisual/publicacoes/2011/manual-do-paciente/Manual-do-paciente-em-terapia-nutricional-enteral-domiciliar.pdf</a>. 
	 Acesso em: 3 abr. 2013.
<br/><br/>

BRASIL. Lei Nº 7.498 de 25 de junho de 1986. Dispõe sobre a regulamentação do 
exercício da enfermagem, e dá outras providências. <b>Diário Oficial [da] 
República Federativa do Brasil</b>, Poder Legislativo, Brasília, DF, 26 jun. 
1986. Seção 1, p. 9.273 a 9.275. Disponível em: 
<a href="http://www.planalto.gov.br/ccivil_03/leis/L7498.htm" target="_blank">http://www.planalto.gov.br/ccivil_03/leis/L7498.htm</a>. 
Acesso em: 8 abr. 2013. 
<br/><br/>

BRASIL. Ministério da Saúde. Secretaria de Atenção à Saúde. Departamento de 
Atenção Básica.
<b>Caderno de Atenção Domiciliar</b>. Brasília: Ed. Ministério da Saúde, 2013.
<br/><br/>

BRASÍLIA. Conselho Regional de Enfermagem da Unidade Federativa (COREN-DF).
<b>Parecer COREN-DF Nº 009/2011:</b> o enfermeiro que presta assistência ao paciente 
 crônico no domicílio, pode passar sonda nasogástrica ou nasoenteral e administrar
  alimentação ou medicamentos por esta via. Brasília, 2011. Disponível em:
   <a href="http://www.coren-df.org.br/portal/index.php/pareceres/parecer-coren/1191-no-0092011-o-enfermeiro-que-presta-assistencia-ao-paciente-cronico-no-domicilio-pode-passar-sonda-nasogastrica-ou-nasoenteral-e-administrar-alimentacao-ou-medicamentos-por-esta-via" target="_blank">
   	http://www.coren-df.org.br/portal/index.php/pareceres/parecer-coren/1191-no-0092011-o-enfermeiro-que-presta-assistencia-ao-paciente-cronico-no-domicilio-pode-passar-sonda-nasogastrica-ou-nasoenteral-e-administrar-alimentacao-ou-medicamentos-por-esta-via</a>. Acesso em: 19 abr. 2013.
<br/><br/>

CINTRA, E. de A.; NISHIDE, V. M.; NUNES, W. A. <b>Assistência de enfermagem ao paciente crítico</b>. 
São Paulo: Atheneu, 2000. 671 p.
<br/><br/>

COELHO, S. <b>Cateter nasogástrico.</b> 2007. Disponível em: 
<a href="http://www.youtube.com/watch?v=VuyBL-L-A1A" target="_blank">http://www.youtube.com/watch?v=VuyBL-L-A1A</a>. 
Acesso em: 19 abr. 2013.
<br/><br/>

DOVERA, T. M. D. da S. <b>Nutrição aplicada ao curso de enfermagem.</b> Rio de Janeiro: 
Guanabara Koogan, 2007.
<br/><br/>

DREYER, E. et al. <b>Nutrição enteral domiciliar:</b> manual do usuário: como preparar
 e administrar a dieta por sonda. 2. ed. rev. Campinas, SP: Hospital das Clínicas 
 da UNICAMP, 2011. 33 p. Disponível em: <a href="http://www.hc.unicamp.br/servicos/emtn/Manual_paciente.pdf"‎ target="_blank">
 www.hc.unicamp.br/servicos/emtn/Manual_paciente.pdf</a>.  Acesso em: 15 abr. 2013. 
<br/><br/>

KNOBEL, E. <b>Terapia intensiva.</b> São Paulo: Atheneu, 2006.

<br/><br/>
NETTINA, S. M. <strong>Prática de enfermagem</strong>. 7. ed. Rio de Janeiro: Guanabara Koogan, 2003. 
<br><br>
POTTER, P. A. PERRY, A. G. <b>Fundamentos da enfermagem</b>. 7. ed. Rio de Janeiro: 
Elsevier, 2009.
<br/><br/>

RIBEIRÃO PRETO. Secretaria da Saúde. Divisão de Enfermagem. <b>Procedimento 
Operacional Padrão - POP.</b> Ciclo 1: Técnicas de Enfermagem. Sondagem Nasogástrica 
POP 012. Sondagem Nasoenteral POP 004. Ribeirão Preto, 2012. Disponível em:
 <a href="http://www.ribeiraopreto.sp.gov.br/ssaude/saudepessoal/enferm/po-sondagem_nasogastrica_nasoenteral_2012.pdf" target="_blank">
 	http://www.ribeiraopreto.sp.gov.br/ssaude/saudepessoal/enferm/po-sondagem_nasogastrica_nasoenteral_2012.pdf</a>. 
 	Acesso em: 3 maio 2013.
<br/><br/>

SANTOS, V. F. N. dos; BOTTONI, A.; MORAIS, T. B. Qualidade nutricional e 
microbiológica de dietas enterais artesanais padronizadas preparadas nas 
residências de pacientes em terapia nutricional domiciliar. <b>Rev Nutr,</b>, Campinas,
  v. 26,  n. 2, p. 205-214, mar./abr. 2013.<br/> Disponível em: 
  <a href="http://dx.doi.org/10.1590/S1415-52732013000200008" target="_blank">http://dx.doi.org/10.1590/S1415-52732013000200008</a>. 
  Acesso em: 19 abr. 2013.
 <br/><br/>

SOCIEDADE BRASILEIRA DE NUTRIÇÃO PARENTERAL E ENTERAL. Terapia nutricional 
domiciliar. <b>Rev Assoc Med Bras</b>, São Paulo, v. 58, n. 4, p. 408-411, 2012. 
Disponível em: <a href="http://www.scielo.br/scielo.php?pid=S0104-42302012000400008&script=sci_arttext
" target="_blank">http://www.scielo.br/pdf/ramb/v58n4/v58n4a08.pdf</a>. 
Acesso em: 20 abr. 2013.
<br/><br/>

SOCIEDADE BRASILEIRA DE NUTRIÇÃO PARENTAL E ENTERAL; FEDERAÇÃO BRASILEIRA DE 
GASTROENTEROLOGIA; ASSOCIAÇÃO BRASILEIRA DE NUTROLOGIA. <b>Recomendações para 
preparo da nutrição enteral.</b> [São Paulo]: AMB; [Brasília]: CFM, 2011. Disponível 
em: <a href="http://www.projetodiretrizes.org.br/9_volume/recomendacoes_para_preparo_da_nutricao_enteral.pdf" target="_blank">
http://www.scielo.br/scielo.php?pid=S0104-42302012000400008&script=sci_arttext
</a>. 
Acesso em: 19 abr. 2013.
<br/><br/>

SMELTIZER, S. C.; BARE, B. G. <b>Tratado de enfermagem médico-cirúrgica.</b> 10. ed. v. 2. 
 Rio de Janeiro: Guanabara Koogan, 2005.
<br/><br/>
