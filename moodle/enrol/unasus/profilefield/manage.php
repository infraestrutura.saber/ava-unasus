<?php

   require("../../../config.php");
   require("$CFG->dirroot/enrol/unasus/lib/context.php");
   require("$CFG->dirroot/enrol/unasus/profilefield/category/lib.php");
   require("$CFG->dirroot/enrol/unasus/profilefield/lib.php");
  require_login();

$param=  new object();
$context =new context();

$param->contextid= required_param('contextid',PARAM_INT);
 $context_name=$context->get_name($param->contextid);   
   //permiss�o
    require_capability('enrol/unasus:profilefieldview', get_context_instance(CONTEXT_SYSTEM), NULL, false);
    add_to_log(1, 'enrol_unasus', 'add', "enrol/unasus/profilefield/manage.php?contextid=".$param->contextid, getremoteaddr());    
    //Navega��o
   $navigation = build_navigation(array(array('name' => get_string('enrolname','enrol_unasus'), 'link' => "$CFG->wwwroot/enrol/unasus/index.php", 'type' => 'misc'),
                                     array('name' => get_string('profilefield','enrol_unasus'), 'link' => "$CFG->wwwroot/enrol/unasus/profilefield/index.php", 'type' => 'misc'), array('name' => $context_name, 'link' => null, 'type' => 'misc')));
    
    
    $profilefield_category =new profilefield_category();
    $profilefield_field = new profilefield_field(); 
    
   
    
     print_header(get_string('enrolname','enrol_unasus'),get_string('applicationsenrolment','enrol_unasusem'), $navigation);
   	view_fields();
 	form_add_item();
   
    print_footer();
    function view_fields(){
    	global $profilefield_category;
    	global $param;
    	$rows = $profilefield_category->get_by_contextid($param->contextid);
    	$output="";
    	if(!empty($rows)){
    		$cont=0;
    		$total=count($rows);
	    	foreach ($rows as $row){
	    		$cont++;
	    		 print_heading(format_string($row->name) .' '.profile_category_icons($row,$cont,$total));
	    		//$output .= "<center><h1>$row->name</h1></centar>"; 
	    		form_view_field($row->id);
	    		
	    	}
    	}
    	//echo $output;
    }
    function form_add_item(){
    	global  $param,$CFG;
    	$output="<FORM NAME=\"nav\">";
    	$output.="<div>";
		$output.="<select onchange=\"document.location.href=document.nav.SelectURL.options[document.nav.SelectURL.selectedIndex].value\" name=\"SelectURL\">";
		$output.="<option selected=\"\" value=\"#\">".get_string('profilefield_select_type','enrol_unasus')."</option>";
				$output.="<option value=\"$CFG->wwwroot/enrol/unasus/profilefield/type/checkbox/add.php?contextid=".$param->contextid."\">".get_string('profilefield_checkbox','enrol_unasus')."</option>";
				$output.="<option value=\"$CFG->wwwroot/enrol/unasus/profilefield/type/menu/add.php?contextid=".$param->contextid."\">".get_string('profilefield_option','enrol_unasus')."</option>";
				$output.="<option value=\"$CFG->wwwroot/enrol/unasus/profilefield/type/text/add.php?contextid=".$param->contextid."\">".get_string('profilefield_text','enrol_unasus')."</option>";
				$output.="<option value=\"$CFG->wwwroot/enrol/unasus/profilefield/type/textarea/add.php?contextid=".$param->contextid."\">".get_string('profilefield_textarea','enrol_unasus')."</option>";
				$output.="</select>";
				$output.="</div>";
    	
		$output.="<INPUT TYPE=\"BUTTON\" VALUE=\"".get_string('profilefieldcategory_add','enrol_unasus')."\" ONCLICK=\"window.location.href='category/add.php?contextid=".$param->contextid."'\">";
		$output.="</FORM>";
		echo $output;
    }
   /* 
    function move_ordem($categoryid){
    	global $profilefield_field;
    	global $profilefield_category;
    	 $param=  new object();
    	 $param->operation= optional_param('operation',NULL,PARAM_TEXT);
    	 
    	 if(!empty($param->operation)){
    	 	$param->itemid= required_param('itemid',PARAM_INT);
    	 	$param->typeitem= required_param('typeitem',PARAM_TEXT);
    	 	
    	 	if($param->typeitem=='category'){
    	 		$profilefield_category->update_order($param->itemid,$param->operation);
    	 	}else  if($param->typeitem=='field'){
    	 		$profilefield_field->update_order($param->itemid,$param->operation);
    	 	}
    	 }
    	 
    	 
    }*/
    function form_view_field($categoryid){
    	global $profilefield_field;
   		 $rows = $profilefield_field->get_all_by_category($categoryid);
   		
   		
   		if(!empty($rows)){
   			  $table = new object();
    	  		$table->head  = array(get_string('profilefield', 'admin'), get_string('edit'));
    	  		$table->align = array('left', 'right');
    	  		$table->width = '95%';
         		 $table->class = 'generaltable profilefield';
          		$table->data = array();
          		$contf=0;
    			$totalf=count($rows);
				foreach ($rows as $row) {
					$contf++;
           		 	$table->data[] = array(format_string($row->name), profile_field_icons ($row,$contf,$totalf));
        		}
   				 print_table($table);
   		}
   		
    }
    
    
/**
 * Gera a string html com link de gerenciamento editar | apagar | mover de cada campo
 * @param   object   objeto do campo
 * @return  string   retorna string HTML
 */
function profile_field_icons ($dto,$cont,$total) {
    global $CFG, $USER,$rows;

  	     $strdelete   = get_string('delete');
        $strmoveup   = get_string('moveup');
        $strmovedown = get_string('movedown');
        $stredit     = get_string('edit');

    /// Edit
    $editstr = '<a title="'.$stredit.'" href="type/'.$dto->datatype.'/edit.php?id='.$dto->id.'&contextid='.$dto->contextid.'"><img src="'.$CFG->pixpath.'/t/edit.gif" alt="'.$stredit.'" class="iconsmall" /></a> ';
 	$editstr .= '<img src="'.$CFG->pixpath.'/spacer.gif" alt="" class="iconsmall" /> ';
    /// Delete
    $editstr .= '<a title="'.$strdelete.'" href="type/delete_confirm.php?id='.$dto->id.'"';
    $editstr .= '"><img src="'.$CFG->pixpath.'/t/delete.gif" alt="'.$strdelete.'" class="iconsmall" /></a> ';
     $editstr .= '<img src="'.$CFG->pixpath.'/spacer.gif" alt="" class="iconsmall" /> ';

/// Move up
    if ($cont > 1) {
        $editstr .= '<a title="'.$strmoveup.'" href="type/sortorder.php?seq='.$cont.'&contextid='.$dto->contextid.'&categoryid='.$dto->categoryid.'&operation=up&itemid='.$dto->id.'"><img src="'.$CFG->pixpath.'/t/up.gif" alt="'.$strmoveup.'" class="iconsmall" /></a> ';
     } else {
        $editstr .= '<img src="'.$CFG->pixpath.'/spacer.gif" alt="" class="iconsmall" /> ';
    }
    
    /// Move down
     if ($cont <$total) {
    	$editstr .= '<a title="'.$strmovedown.'" href="type/sortorder.php?seq='.$cont.'&contextid='.$dto->contextid.'&categoryid='.$dto->categoryid.'&operation=down&itemid='.$dto->id.'"><img src="'.$CFG->pixpath.'/t/down.gif" alt="'.$strmovedown.'" class="iconsmall" /></a> ';
    } else {
        $editstr .= '<img src="'.$CFG->pixpath.'/spacer.gif" alt="" class="iconsmall" /> ';
    }
    return $editstr;
}

function profile_category_icons ($dto,$cont,$total) {
    global $CFG, $USER,$rows;
   
          $strdelete   = get_string('delete');
        $strmoveup   = get_string('moveup');
        $strmovedown = get_string('movedown');
        $stredit     = get_string('edit');


     /// Edit
    $editstr = '<a title="'.$stredit.'" href="category/edit.php?id='.$dto->id.'&contextid='.$dto->contextid.'"><img src="'.$CFG->pixpath.'/t/edit.gif" alt="'.$stredit.'" class="iconsmall" /></a> ';

    /// Delete
    $editstr .= '<a title="'.$strdelete.'" href="category/delete_confirm.php?id='.$dto->id.'"';
    $editstr .= '"><img src="'.$CFG->pixpath.'/t/delete.gif" alt="'.$strdelete.'" class="iconsmall" /></a> ';

   /// Move up
    if ($cont > 1) {
        $editstr .= '<a title="'.$strmoveup.'" href="category/sortorder.php?seq='.$cont.'&contextid='.$dto->contextid.'&operation=up&itemid='.$dto->id.'"><img src="'.$CFG->pixpath.'/t/up.gif" alt="'.$strmoveup.'" class="iconsmall" /></a> ';
     } else {
        $editstr .= '<img src="'.$CFG->pixpath.'/spacer.gif" alt="" class="iconsmall" /> ';
    }
    
    /// Move down
    if ($cont <$total) {
    	$editstr .= '<a title="'.$strmovedown.'" href="category/sortorder.php?seq='.$cont.'&contextid='.$dto->contextid.'&operation=down&itemid='.$dto->id.'"><img src="'.$CFG->pixpath.'/t/down.gif" alt="'.$strmovedown.'" class="iconsmall" /></a> ';
    } else {
        $editstr .= '<img src="'.$CFG->pixpath.'/spacer.gif" alt="" class="iconsmall" /> ';
    }
    return $editstr;
}
?>