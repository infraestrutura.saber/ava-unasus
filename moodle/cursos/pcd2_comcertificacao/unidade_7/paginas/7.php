<?php
               
    $pg_atual=7;
     registro($id,unid7_pg,$pg_atual,unid7_ev);
?>


<!-- colorbox exercicio1 -->
<div style='display:none'>
			<div id="content_color1" style='padding:10px; background:#fff;'>

			<div id="texto_color1"></div>
			<br><br>
			<a class="btn" href="index.php?pagina=7">Tentar novamente</a>
			<a class="btn" href="index.php?pagina=0">Estudar mais</a>
			<a class="inline btn" href="#feedback1">Mostrar respostas</a>
		
			<br><br>
			
			</div>
		</div><!-- fim colorbox exercicio1-->
	 
	<div style='display:none'>
		<div id="feedback1">
			<h4>Respostas</h4>
			<br>
			1. <b>Verdadeiro</b><br>
			2. <b>Falso</b> - Os equipamentos usados na oxigenoterapia (cilindros, máscaras, suportes) não são de propriedade do paciente e não podem ser destruídos, emprestados ou vendidos.<br>
			3. <b>Falso</b> – Nunca se deve manusear equipamentos de oxigênio com as mãos sujas de óleos e graxa, sob risco de combustão.<br>
			4. <b>Falso</b> - É de responsabilidade do médico determinar a forma de administração do O2, por meio de uma prescrição detalhada, mediante avaliação e reavaliações do paciente.<br>
			5. <b>Falso</b> - A oxigenoterapia é indicada para pacientes que possuem doença pulmonar obstrutiva crônica avançada, pneumonias, enfisema pulmonar e bronquite crônica, sendo sua indicação e prescrição de responsabilidade do médico.<br>
		6. <b>Verdadeiro</b><br>
			<br><br>
			<a class="inline btn" href="#content_color1">Voltar</a>
		</div>
	</div>
<!-- colorbox exercicio1 final -->
<!-- colorbox exercicio2 -->
<div style='display:none'>
			<div id="content_color2" style='padding:10px; background:#fff;'>

			<div id="texto_color2"></div>
			<br><br>
			<a class="btn" href="index.php?pagina=7">Tentar novamente</a>
			<a class="btn" href="index.php?pagina=0">Estudar mais</a>
			<a class="inline2 btn" href="#feedback2">Mostrar respostas</a>
		
			<br><br>
			
			</div>
		</div>
<!-- fim colorbox exercicio2-->
	 
	<div style='display:none'>
		<div id="feedback2">
				
		<h4>Respostas</h4>
		a)	<b>Errado</b> - Apenas médicos, enfermeiros e biomédicos têm habilitação para colher material para gasometria arterial. A posição em que o paciente deve ser colocado, nesse caso, é a de Fowler.<br>
		b)	<b>Correto</b><br>
		c)	<b>Errado</b> - Nesse caso, o paciente deve ser colocado na posição de Fowler.<br>
		d)	<b>Errado</b> - Apenas médicos, enfermeiros e biomédicos têm habilitação para colher material para gasometria arterial.<br>
		e)	<b>Errado</b> - Apenas médicos, enfermeiros e biomédicos têm habilitação para colher material para gasometria arterial. Nesse caso, o paciente deve ser colocado na posição de Fowler.<br><br>
<br>

			<br><br>
			<a class="inline2 btn" href="#content_color2">Voltar</a>
		</div>
	</div>
<!-- colorbox exercicio2 final -->



<h4 class="titulo">Exercícios </h4>
 
 <div id="ex1">
	 <h5>Atividade 1</h5>
	 <i>A oxigenoterapia é uma intervenção não farmacológica comprovada na eficácia do aumento da sobrevida de pacientes que possuem insuficiência respiratória crônica. 
O oxigênio é um gás com indicações e dosagens específicas, necessitando de cuidados durante o seu manuseio. Sendo assim, a equipe multiprofissional deverá sempre monitorar o impacto dessa terapia no cotidiano do paciente e familiares.</i>
<br><br>
De acordo com o conteúdo apresentado na unidade, marque verdadeiro (V) ou falso (F) nas sentenças abaixo:

<table  style="width: 640px;margin-left: 45px;margin-top: 33px;margin-bottom: 20px;">
	
		
	
	<tr bgcolor="#6B4932">	
	<td><center><div class="d1"><h5 style="color:#fff;">Sentenças</h5></div> </center><center> <div class="d3"><h5 style="color:#fff;">Verdadeiro | Falso</h5></div> </center></td>
	</tr>
	
	<tr bgcolor="#BB9D89"  >
		<td><div class="d1"><p>1. A equipe de enfermagem é responsável pela administração do oxigênio e pelo uso correto dos equipamentos utilizados, bem como pelas orientações ao paciente e ao cuidador quanto ao uso do oxigênio.</p></div>
		<div class="d2"><p><input type="radio" name="1" value="V"></p><span class="boolean">V</span></div>
		<div class="d3"><p><input type="radio" name="1" value="F"></p><span class="boolean">F</span> </div></td>
	
	</tr>
	
	<tr bgcolor="#AA8165" >
		<td><div class="d1"><p>2. Os equipamentos usados na oxigenoterapia (cilindros, máscaras, suportes) são de propriedade do paciente e podem ser destruídos, emprestados ou vendidos, caso o paciente ou familiares assim o desejar.</p></div>
		<div class="d2"><p><input type="radio" name="2" value="V"></p><span class="boolean">V</span></div>
		<div class="d3"><p><input type="radio" name="2" value="F"></p><span class="boolean">F</span></div></td>
	
	</tr>
	
	<tr bgcolor="#BB9D89" >
		<td><div class="d1"><p>3. Deve-se evitar o manuseio dos equipamentos de oxigênio com as mãos sujas de óleos e graxa para evitar sujá-los. Entretanto, se isso for feito, não há risco de combustão. </p></div>
		<div class="d2"><p><input type="radio" name="3" value="V"></p><span class="boolean">V</span></div>
		<div class="d3"><p><input type="radio" name="3" value="F"></p><span class="boolean">F</span></div></td>
	
	</tr>
	
	<tr bgcolor="#AA8165">
		<td><div class="d1"><p>4. O paciente/cuidador pode aumentar o fluxo de oxigênio sempre que necessário.</p></div>
		<div class="d2"><p><input type="radio" name="4" value="V"></p><span class="boolean">V</span></div>
		<div class="d3"><p><input type="radio" name="4" value="F"></p><span class="boolean">F</span></div></td>
	
	</tr>
	
	<tr bgcolor="#BB9D89" >
		<td><div class="d1"><p>5. A oxigenoterapia é indicada para pacientes que possuem doença pulmonar obstrutiva crônica avançada, pneumonias, enfisema pulmonar e bronquite crônica, sendo sua indicação e prescrição  de responsabilidade do médico e do enfermeiro. </p></div>
		<div class="d2"><p><input type="radio" name="5" value="V"></p><span class="boolean">V</span></div>
		<div class="d3"><p><input type="radio" name="5" value="F"></p><span class="boolean">F</span></div></td>
	
	</tr>
	
	<tr bgcolor="#AA8165">
		<td><div class="d1"><p>6. É necessário avaliar diariamente a mucosa das narinas e a pele do paciente para prevenir possíveis lesões.</p></div>
		<div class="d2"><p><input type="radio" name="6" value="V"></p><span class="boolean">V</span></div>
		<div class="d3"><p><input type="radio" name="6" value="F"></p><span class="boolean">F</span></div></td>
	
	</tr>
	
		
</table>


		 
		 <a class='inline btn' style="float: right;margin-right: 45px;" type="button" href="#content_color1" onClick="respostas(1);" >Corrigir</a>


	
</div>


 
 <div id="ex2">
	 <h5>Atividade 2</h5>
	 <table width="100%" ><td align="center"><img src="images/Senhor Frederico-01.jpg" alt="Exercício 2 Frederico"/></td></table><br><br>
	 <br>
	 <p style="text-align: justify;">
	Seu Frederico tem 75 anos, é aposentado, possui doença pulmonar obstrutiva crônica e  
	recebeu recentemente alta hospitalar após tratamento de um acidente vascular encefálico 
	com sequelas motoras. Foi solicitada a avaliação pela Equipe Multiprofissional de Atenção 
	Domiciliar – EMAD para a inclusão do paciente no programa. Ao ser inserido no 
	Programa Melhor em Casa, recebeu a primeira visita no domicílio 24 horas após 
	sua alta. No referido momento, encontrava-se respirando sem suporte de oxigênio, 
	lúcido, cooperativo, acamado, em posição dorsal, ofegante, com cianose de 
	extremidade. Ao ser avaliado pela equipe, verificou-se pressão arterial de 160x100 mmHg, 
	frequência cardíaca de 110 bpm, frequência respiratória de 30 rpm, temperatura 
	axilar de 37,2 ºC e oximetria de pulso em 84% de oxigênio. O médico da equipe prescreveu 
	a fonte de oxigênio complementar (gás ou líquido), o tipo de sistema de liberação 

	(cateteres, máscaras, cilindros, concentradores ou oxigênio líquido), a duração 
	e o fluxo (ao repouso, no exercício e durante o sono) e a oferta de oxigênio 
	em casos de saturação abaixo de 88%. 
	<br><br>
	Mediante os sinais e sintomas apresentados, identifique a alternativa que melhor representa as intervenções a serem realizadas pela equipe multiprofissional:
	</p>
	
	<div id="op1">
	<input type="radio" name="ex2" value="a">
	<p>
	<b>a)</b> Colher material para gasometria arterial; iniciar oxigenoterapia por cateter nasal; colocar o paciente em posição de Sims; verificar novamente, após oxigenoterapia, os sinais vitais.
	</p>
	</div>
	
	<div id="op2">
	<input type="radio" name="ex2" value="b">
	
	<p>
	<b>b)</b> Mediante prescrição médica em casos de intercorrência, colocar o paciente em posição de Fowler, elevando sua cabeceira com travesseiros ou cobertores; iniciar oxigenoterapia; verificar sinais vitais após a oxigenoterapia.
</p>
</div>


<div id="op3">
<input type="radio" name="ex2" value="c">
<p>
<b>c)</b> Iniciar oxigenoterapia por cateter nasal; colocar o paciente em posição de Trendelemburg, elevando seus membros com travesseiros ou cobertores; verificar novamente, após oxigenoterapia, os sinais vitais.
</p>
</div>


<div id="op4">
<input type="radio" name="ex2" value="d">
<p>
<b>d)</b>	Colher material para gasometria arterial; iniciar oxigenoterapia por cateter nasal; colocar o paciente em posição de Fowler, elevando sua cabeceira com travesseiros ou cobertores; verificar novamente, após oxigenoterapia, os sinais vitais.

</p>
</div>

<div id="op5">
<input type="radio" name="ex2" value="e">
<p>
<b>e)</b>	Colher material para gasometria arterial; iniciar oxigenoterapia; colocar o paciente em posição de Trendelemburg, verificar novamente, após oxigenoterapia, os sinais vitais.

</p>
</div>
	
	 
	 <button class="btn inline2" href="#content_color2" style="float: right;margin-right: 45px; margin-top: 70px;" type="button" onClick="respostas2(2);">Corrigir</button>
	

	 
</div>


  <style type="text/css">

		ul li ul{
		 list-style-type: disc;   

		}
		
		#ex1{
			text-align: justify;
		
		}		
		
		#ex2{
			margin-top: 60px;
			text-align: justify;
		}

		table tr td{
			font-size: 15px;
			border: 1px solid #f6f6f6;
		}
		
		.d1{

			width: 469px;
			float: left;
			margin-right: 16px;
			border-right: 1px solid #fff;
			
		}
		
		.d1 p{
			margin-top: 8px;
			margin-left: 10px;
		}
		
		.d2{
			float: left;
			padding-right: 15px;
			padding-left: 15px;
			
		
		}
		
		.d2 p{
			margin-top: 5px;
			margin-left: 10px;
		
		}
		
		.d3{
			float: left;
			padding-right: 15px;
			
			
		}
		
		.d3 p{
		
			margin-top: 5px;
			margin-left: 10px;
		}
		
		.boolean{
			margin-top: -24px;
			margin-left: 29px;
			float: right;
		}
		
				
		#feedback1, #feedback2{
			
			
		}
		
		#op1 input , #op2 input, #op3 input, #op4 input, #op5 input{
			float: left;
			margin-right: 10px;
			margin-left: 10px;
		}


</style> 

<script type="text/javascript">
    var user = $('#usuario b').html();
	
	    function respostas(id){

        var p1 = document.getElementsByName('1');
        var p2 = document.getElementsByName('2');
        var p3 = document.getElementsByName('3');
        var p4 = document.getElementsByName('4');
		var p5 = document.getElementsByName('5');
		var p6 = document.getElementsByName('6');
        var gabarito = new Array('V','F','F','F','F','V');


        var resp = new Array(p1,p2,p3,p4,p5,p6);
        var completo = true;


        for(var x = 0; x <7; x++){

            if(resp[x] != null){
                 if(resp[x][0].checked == true){
                           resp[x] = p1[0].value;
                }else if(resp[x][1].checked == true){
                            resp[x] = p1[1].value;

                }else{
                    completo = false;
                }
            }
        }

        if(completo){

			if(compare(resp,gabarito) == true){
            	$("#texto_color1").html("<h4>Resultado</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, você acertou todas as alternativas.");
            	$(".inline").colorbox({inline:true, width:"50%",onClosed:function(){ document.getElementById("feedback" + id).style.display="block"; }});
				$.post( "../apresentacao/functions/chamaExCerto.php", { exercicio: 9}, function() {
						
				});
                
            }else{
            	$("#texto_color1").html("<h4>Resultado</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, você não acertou todas as alternativas.");
              $(".inline").colorbox({inline:true, width:"50%",onClosed:function(){ document.getElementById("feedback" + id).style.display="block"; }});
			  $.post( "../apresentacao/functions/chamaExErrado.php", { exercicio: 9}, function() {
						
				});
                
               
            }
			
			mostrarFeedback(id);

         }else{
           $("#content_color1").html("<h4>Atenção!</h4> <br>Preencha todas as sentenças antes de clicar em concluir.");
         	
         }
		 $(".inline").colorbox({inline:true, width:"50%",onClosed:function(){ location.reload();}});
           
}

 function compare(x, y) {
            if (x.length != y.length) {
                return false;
            }
            for (key in x) {
                if (x[key] !== y[key]) {
                    return false;
                }
            }
            return true;
        }
		
	function mostrarFeedback(id){
		document.getElementById("feedback" + id).style.display="block";
	}
	
	//Exercício 2
	
		function respostas2(id){
		
		 var p1 = document.getElementsByName('ex2');
		 
		 if(p1[1].checked == true){
			$(".inline2").colorbox({inline:true, width:"50%",onClosed:function(){ document.getElementById("feedback" + id).style.display="block"; }});
			$("#texto_color2").html("<h4>Resultado</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, você selecionou a alternativa correta.");
			$.post( "../apresentacao/functions/chamaExCerto.php", { exercicio: 10}, function() {
						
				});
			mostrarFeedback(2);
		}else if(p1[0].checked == true){

			$(".inline2").colorbox({inline:true, width:"50%",onClosed:function(){ document.getElementById("feedback" + id).style.display="block"; }});
			$("#texto_color2").html("<h4>Resultado</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, você selecionou a alternativa incorreta.");
			$.post( "../apresentacao/functions/chamaExErrado.php", { exercicio: 10}, function() {
						
				});
			mostrarFeedback(2);
		}else if(p1[2].checked == true){
			$(".inline2").colorbox({inline:true, width:"50%",onClosed:function(){ document.getElementById("feedback" + id).style.display="block"; }});
			$("#texto_color2").html("<h4>Resultado</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, você selecionou a alternativa incorreta.");
			$.post( "../apresentacao/functions/chamaExErrado.php", { exercicio: 10}, function() {
						
				});
			mostrarFeedback(2);
		}else if(p1[3].checked == true){
			$(".inline2").colorbox({inline:true, width:"50%",onClosed:function(){ document.getElementById("feedback" + id).style.display="block"; }});
			$("#texto_color2").html("<h4>Resultado</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, você selecionou a alternativa incorreta.");
			$.post( "../apresentacao/functions/chamaExErrado.php", { exercicio: 10}, function() {
						
				});
			mostrarFeedback(2);
		}else if(p1[4].checked == true){
			$(".inline2").colorbox({inline:true, width:"50%",onClosed:function(){ document.getElementById("feedback" + id).style.display="block"; }});
			$("#texto_color2").html("<h4>Resultado</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, você selecionou a alternativa incorreta.");
			$.post( "../apresentacao/functions/chamaExErrado.php", { exercicio: 10}, function() {
						
				});
			mostrarFeedback(2);	
		}else{
			$("#content_color2").html("<h4>Atenção!</h4> <br>Marque uma das alternativas!");
         	
		}
		$(".inline2").colorbox({inline:true, width:"50%",onClosed:function(){ location.reload();}});
	}

	
</script>


	<?php
	if ($_GET['ex']==1){ 

	}
	if ($_GET['ex']==2){ 

		echo '<script type="text/javascript">$("html,body").animate({scrollTop: $("#ex2").offset().top}, 1500);</script>';
	}
?>
