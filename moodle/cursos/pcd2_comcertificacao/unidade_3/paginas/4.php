

<!-- colorbox exercicio1 -->
<div style='display:none'>
			<div id="content_color1" style='padding:10px; background:#fff;'>

			<div id="texto_color1"></div>
			<br><br>
			<a class="btn" href="index.php?pagina=4">Tentar novamente</a>
			<a class="btn" href="index.php?pagina=0">Estudar mais</a>
			<a class="inline btn" href="#feedback1">Mostrar respostas</a>
		
			<br><br>
			
			</div>
		</div><!-- fim colorbox exercicio1-->
	 
	<div style='display:none'>
		<div id="feedback1">
			<h4>Respostas</h4>
			<br>
			<b>1. Falso</b> – A retirada de fezes impactadas manualmente, sem supervisão, pode causar muitos danos ao paciente. O fecaloma é uma situação que requer intervenção médica, e o diagnóstico tardio de fezes impactadas pode exigir, por exemplo, cirurgia.<br>
			<b>2. Verdadeiro</b><br>
			<b>3. Falso</b> – Para facilitar o processo de defecação, o paciente também deve ser orientado a ficar sentado e, caso o equilíbrio permita, inclinado para frente. O paciente e/ou cuidador devem ser orientados a realizar a manobra de Rosing. <br>
			<b>4. Verdadeiro</b><br>
			<br><br>
			<a class="inline btn" href="#content_color1">Voltar</a>
		</div>
	</div>
<!-- colorbox exercicio1 final -->
<!-- colorbox exercicio2 -->
<div style='display:none'>
			<div id="content_color2" style='padding:10px; background:#fff;'>

			<div id="texto_color2"></div>
			<br><br>
			<a class="btn" href="index.php?pagina=4">Tentar novamente</a>
			<a class="btn" href="index.php?pagina=0">Estudar mais</a>
			<a class="inline2 btn" href="#feedback2">Mostrar respostas</a>
		
			<br><br>
			
			</div>
		</div>
<!-- fim colorbox exercicio2-->
	 
	<div style='display:none'>
		<div id="feedback2">
				
		<h4>Respostas</h4>
		<span style="color:#F00">(alternativas em vermelho estão incorretas).</span><br><br>
		a)	<b>Pré-execução:</b><br><br>
		<span style="color:#F00">•	Não há necessidade de verificação da frequência cardíaca e da pressão arterial;</span><br>
		•	Reunião do material;<br>
		•	Orientação ao paciente e cuidador sobre o procedimento;<br>
		•	Checagem da prescrição.<br><br>
		<i>Feedback: Antes da realização do procedimento, devido ao risco da ocorrência de distúrbios hidroeletrolíticos, a equipe de enfermagem deverá avaliar as condições de hidratação e parâmetros clínicos do paciente, como frequência cardíaca e pressão arterial, bem como condições da rede venosa, que podem ser afetados por diminuição de volume de líquido circulante. Pode ser necessária a obtenção de acesso vascular para reposição hídrica.</i>
		<br><br>
		c)	<b>Fase de infusão da solução</b><br><br>
		<span style="color:#F00">•	Infusão da solução de modo rápido e indolor;<br>
		•	Em caso de cólicas, continuar a infusão da solução e solicitar ao paciente para inspirar profundamente;<br>
		•	Após o término da infusão, retirar a sonda rapidamente e comprimir as nádegas;<br></span>
		•	 Oferecer a comadre, colocar fralda ou encaminhar o paciente ao vaso sanitário, conforme suas condições clínicas.<br><br>
		<i>Feedback: A infusão deve ser lenta e indolor. Em caso de disfunção abdominal, suspender a infusão imediatamente. Após o término da infusão, retirar a sonda lentamente e comprimir as nádegas.</i>
		<br><br>
		
		d)	<b>Fase de pós-execução</b><br><br>
		<span style="color:#F00">•	Se a equipe achar conveniente, deverá realizar anotações referentes à quantidade e às características da eliminação intestinal;<br></span>
		•	A equipe deve garantir conforto ao paciente;<br>
		•	Organização do ambiente.<br><br>
		<i>Feedback: A equipe de enfermagem deve sempre registrar como foi realizado o procedimento. É importante anotar a data, o horário, a quantidade de líquido infundido e a característica da eliminação intestinal, assim como as intercorrências que venham a existir.</i>
		<br><br>
		
			<br><br>
			<a class="inline2 btn" href="#content_color2">Voltar</a>
		</div>
	</div>
<!-- colorbox exercicio2 final -->



       <h4 class="titulo">Exercícios</h4>
	  <h5>Atividade 1</h5>
	  <!-- span que recebera a função para registrar exercicio 1-->

	  <div id="ex1">
	  	<table width="100%" ><td align="center"><img src="images/Dona josefina-atividade01.jpg" alt="Exercício 1 Josefina"/></td></table><br><br>
			
			<p style="text-align: justify; margin-top: 10px;">
			Josefina tem 65 anos, possui o 1º grau incompleto, é negra, dona de casa, portadora de miocardiopatia e diabetes tipo II e reside em Sumaré - São Paulo, com o esposo e uma irmã. Há um mês, sofreu queda da própria altura, evoluindo com dor intensa, sudorese e limitação coxo femoral. Encaminhada ao hospital, diagnosticou-se fratura no trocanter do fêmur esquerdo. Foi submetida a tratamento cirúrgico e, devido às complicações pós-operatórias, necessitou de internação em uma Unidade de Tratamento Intensivo (UTI) por 20 dias. Passado esse período, ficou 10 dias na unidade de clínica cirúrgica. Recebeu alta hospitalar e ficou sob a responsabilidade da Equipe Multiprofissional de Atenção Domiciliar (EMAD), com orientações para manter-se acamada até posterior liberação da equipe médica. Durante a visita da equipe de enfermagem, Josefina queixou-se de inapetência, de dor abdominal e de estar sem evacuar há 10 dias.  </p>
			
			<p>Diante do exposto, marque V (verdadeiro) ou F (falso) nas sentenças a seguir, levando em consideração as condutas que devem ser adotadas pela equipe de saúde:</p>
			
	<table  style="width: 640px;margin-left: 45px;margin-top: 33px;margin-bottom: 20px;">
	
		
	<tr bgcolor="#009ec8">	
	<td><center><div class="d1"><h5 style="color:#fff;">Sentenças</h5></div> </center><center> <div class="d3"><h5 style="color:#fff;">Verdadeiro | Falso</h5></div> </center></td>
	</tr>
	
	<tr bgcolor="#E2EBF7"  >
		<td><div class="d1"><p>1. Orientar o cuidador para que, na ausência da equipe, realize a retirada manual das fezes impactadas.</p></div>
		<div class="d2"><p><input type="radio" name="1" value="V"></p><span class="boolean">V</span></div>
		<div class="d3"><p><input type="radio" name="1" value="F"></p><span class="boolean">F</span> </div></td>
	
	</tr>
	
	<tr bgcolor="#C6E1E9" >
		<td><div class="d1"><p>2. Conversar com o médico e avaliar a necessidade de realizar o procedimento de enteroclisma, conforme prescrição.</p></div>
		<div class="d2"><p><input type="radio" name="2" value="V"></p><span class="boolean">V</span></div>
		<div class="d3"><p><input type="radio" name="2" value="F"></p><span class="boolean">F</span></div></td>
	
	</tr>
	
	<tr bgcolor="#E2EBF7" >
		<td><div class="d1"><p>3. Orientar a paciente a posicionar-se na comadre para forçar a saída das fezes.</p></div>
		<div class="d2"><p><input type="radio" name="3" value="V"></p><span class="boolean">V</span></div>
		<div class="d3"><p><input type="radio" name="3" value="F"></p><span class="boolean">F</span></div></td>
	
	</tr>
	
	<tr bgcolor="#C6E1E9">
		<td><div class="d1"><p>4. Indagar a respeito dos hábitos intestinais anteriores à internação, ingesta hídrica e alimentação.</p></div>
		<div class="d2"><p><input type="radio" name="4" value="V"></p><span class="boolean">V</span></div>
		<div class="d3"><p><input type="radio" name="4" value="F"></p><span class="boolean">F</span></div></td>
	
	</tr>
	
	
		
</table>


		 <!--<button class="btn" style="float: right;margin-right: 45px;" type="button" onClick="respostas(1);">Corrigir</button>-->
		  <a class='inline btn' style="float: right;margin-right: 45px;" type="button" href="#content_color1" onClick="respostas(1);" >Corrigir</a>

	
</div>
<hr style="margin-top: 70px;">
<div id="ex2" style="margin-top: 10px;">
	
	<h5>Atividade 2</h5>
	
	<p style="text-align: justify;">
	Em relação às fases (pré-execução, execução, infusão da solução e pós-execução) que compõem a realização do procedimento de enteroclisma, observe as opções abaixo e assinale aquela que apresenta ações coerentes com a fase descrita: 
	
	</p>
	
	<div id="op1">
	<input type="radio" name="ex2" value="a">
	<img src="images/enfermeiro.jpg" alt="Enfermeiro">
	<p>
	a)	<strong>Pré-execução:</strong><br>
&nbsp;&nbsp;&nbsp;&nbsp;•	Não há necessidade de verificação da frequência cardíaca e da pressão arterial;<br>
&nbsp;&nbsp;&nbsp;&nbsp;•	Reunião do material;<br>
&nbsp;&nbsp;&nbsp;&nbsp;•	Orientação ao paciente e cuidador sobre o procedimento;<br>
&nbsp;&nbsp;&nbsp;&nbsp;•	Checagem da prescrição.<br>
	</p>
	</div>
	
	<div id="op2">
	<input type="radio" name="ex2" value="b">
	<img src="images/enfermeiro.jpg" alt="Enfermeiro">
	<p>
	b)	<strong>Fase de execução:</strong><br>
&nbsp;&nbsp;&nbsp;&nbsp;•	Orientação ao paciente/cuidador;<br>
&nbsp;&nbsp;&nbsp;&nbsp;•	Promoção da privacidade ao paciente;<br>
&nbsp;&nbsp;&nbsp;&nbsp;•	Lavagem das mãos;<br>
&nbsp;&nbsp;&nbsp;&nbsp;•	Fazer uso de Equipamentos de Proteção Individual (EPIs);<br>
&nbsp;&nbsp;&nbsp;&nbsp;•	Proteção do leito;<br>
&nbsp;&nbsp;&nbsp;&nbsp;•	Posicionamento do paciente na posição de Sims;<br>
&nbsp;&nbsp;&nbsp;&nbsp;•	Conexão do equipo à sonda retal;<br>
&nbsp;&nbsp;&nbsp;&nbsp;•	Lubrificação da ponta distal da sonda retal com a própria solução;<br>
&nbsp;&nbsp;&nbsp;&nbsp;•	Introdução da sonda cerca de 7 a 10 cm no adulto e 5 cm em crianças.<br>
</p>
</div>


<div id="op3">
<input type="radio" name="ex2" value="c">
<img src="images/enfermeiro.jpg" alt="Enfermeiro">

<p>
c)	<strong>Fase de infusão da solução</strong><br>
&nbsp;&nbsp;&nbsp;&nbsp;•	Infusão da solução de modo rápido e indolor;<br>
&nbsp;&nbsp;&nbsp;&nbsp;•	Em caso de cólicas, continuar a infusão da solução e solicitar ao paciente para inspirar profundamente;<br>
&nbsp;&nbsp;&nbsp;&nbsp;•	Após o término da infusão, retirar a sonda rapidamente e comprimir as nádegas;<br>
&nbsp;&nbsp;&nbsp;&nbsp;•	 Oferecer a comadre, colocar fralda ou encaminhar o paciente ao vaso sanitário, conforme suas condições clínicas.<br>
</p>
</div>


<div id="op4">
<input type="radio" name="ex2" value="d">
<img src="images/enfermeiro.jpg" alt="Enfermeiro">
<p>
	d)	<strong>Fase de pós-execução</strong><br>
&nbsp;&nbsp;&nbsp;&nbsp;•	Se a equipe achar conveniente, deverá realizar anotações referentes à quantidade e às características da eliminação intestinal;<br>
&nbsp;&nbsp;&nbsp;&nbsp;•	A equipe deve garantir conforto ao paciente;<br>
&nbsp;&nbsp;&nbsp;&nbsp;•	Organização do ambiente.<br>

</p>
</div>
	
	 <button class="btn inline2" href="#content_color2" style="float: right;margin-right: 45px; margin-top: 70px;" type="button" onClick="respostas2(2);">Corrigir</button>
	

	

</div>
<script type="text/javascript">

</script>

<script type="text/javascript">
	//pegando nome do usuario
    var user = $('#usuario b').html();
    function respostas(id){


        var p1 = document.getElementsByName('1');
        var p2 = document.getElementsByName('2');
        var p3 = document.getElementsByName('3');
        var p4 = document.getElementsByName('4');
        var gabarito = new Array('F','V','F','V');


        var resp = new Array(p1,p2,p3,p4);
        var completo = true;


        for(var x = 0; x <5; x++){

            if(resp[x] != null){
                 if(resp[x][0].checked == true){
                           resp[x] = p1[0].value;
                }else if(resp[x][1].checked == true){
                            resp[x] = p1[1].value;

                }else{
                    completo = false;
                }
            }
        }

        if(completo){


          
            if(compare(resp,gabarito) == true){
            	
            	$("#texto_color1").html("<h4>Resultado</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, parabéns! Você acertou todas as alternativas.");
            	$(".inline").colorbox({inline:true, width:"50%",onClosed:function(){ document.getElementById("feedback" + id).style.display="block"; }});
				$.post( "../apresentacao/functions/chamaExCerto.php", { exercicio: 1}, function() {
						
				});
                
               
            }else{
            	$("#texto_color1").html("<h4>Resultado</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, você não acertou todas as alternativas.");
                
              $(".inline").colorbox({inline:true, width:"50%",onClosed:function(){ document.getElementById("feedback" + id).style.display="block"; }});
			  $.post( "../apresentacao/functions/chamaExErrado.php",  { exercicio: 1}, function() {
					
				});
            }
			
			mostrarFeedback(id);
			

         }else{
         	$("#content_color1").html("<h4>Atenção!</h4> <br>Preencha todas as sentenças antes de clicar em concluir.");
         	

            
         }
		 $(".inline").colorbox({inline:true, width:"50%",onClosed:function(){ location.reload();}});
           
}

 function compare(x, y) {
            if (x.length != y.length) {
                return false;
            }
            for (key in x) {
                if (x[key] !== y[key]) {
                    return false;
                }
            }
            return true;
        }
		
	function mostrarFeedback(id){
		document.getElementById("feedback" + id).style.display="block";
	}
	
	//-----EXERCÍCIO 2 --------//
	
	function respostas2(id){
		
		 var p1 = document.getElementsByName('ex2');
		 
		 if(p1[1].checked == true){
		 	$(".inline2").colorbox({inline:true, width:"50%",onClosed:function(){ document.getElementById("feedback" + id).style.display="block"; }});
			$("#texto_color2").html("<h4>Resultado</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, você selecionou a alternativa correta.");
           $.post( "../apresentacao/functions/chamaExCerto.php", { exercicio: 2}, function() {
						
			});
			
			
			
			mostrarFeedback(2);
		}else if(p1[0].checked == true){
			$(".inline2").colorbox({inline:true, width:"50%",onClosed:function(){ document.getElementById("feedback" + id).style.display="block"; }});
			$("#texto_color2").html("<h4>Resultado</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, você selecionou a alternativa incorreta.");
			$.post( "../apresentacao/functions/chamaExErrado.php", { exercicio: 2}, function() {
						
				});
			
			
			mostrarFeedback(2);
		}else if(p1[2].checked == true){
			$(".inline2").colorbox({inline:true, width:"50%",onClosed:function(){ document.getElementById("feedback" + id).style.display="block"; }});
			$("#texto_color2").html("<h4>Resultado</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, você selecionou a alternativa incorreta.");
			$.post( "../apresentacao/functions/chamaExErrado.php", { exercicio: 2}, function() {
						
				});
			
			
			mostrarFeedback(2);
		}else if(p1[3].checked == true){
			$(".inline2").colorbox({inline:true, width:"50%",onClosed:function(){ document.getElementById("feedback" + id).style.display="block"; }});
			$("#texto_color2").html("<h4>Resultado</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, você selecionou a alternativa incorreta.");
			$.post( "../apresentacao/functions/chamaExErrado.php", { exercicio: 2}, function() {
						
				});
			
			mostrarFeedback(2);
		}else{
			$("#content_color2").html("<h4>Atenção!</h4> <br><?php echo $USER->firstname; ?></b>, marque uma das alternativas!");
         	
			
		}
		$(".inline2").colorbox({inline:true, width:"50%",onClosed:function(){ location.reload();}});
	}

</script>


	<style type="text/css">
	
		
		table tr td{
			font-size: 15px;
			border: 1px solid #f6f6f6;
		}
		
		.d1{

			width: 469px;
			float: left;
			margin-right: 16px;
			border-right: 1px solid #fff;
			
		}
		
		.d1 p{
			margin-top: 8px;
			margin-left: 10px;
		}
		
		.d2{
			float: left;
			padding-right: 15px;
			padding-left: 15px;
			
		
		}
		
		.d2 p{
			margin-top: 5px;
			margin-left: 10px;
		
		}
		
		.d3{
			float: left;
			padding-right: 15px;
			
			
		}
		
		.d3 p{
		
			margin-top: 5px;
			margin-left: 10px;
		}
		
		.boolean{
			margin-top: -24px;
			margin-left: 29px;
			float: right;
		}
		
		.enviar{
			float: right;
			position: absolute;
			right: 338px;
			background-color: #2c2c2c;
background-image: -moz-linear-gradient(top, #333333, #222222);
background-image: -ms-linear-gradient(top, #333333, #222222);
background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#333333), to(#222222));
background-image: -webkit-linear-gradient(top, #333333, #222222);
background-image: -o-linear-gradient(top, #333333, #222222);
background-image: linear-gradient(top, #333333, #222222);
background-repeat: repeat-x;
filter: progid:DXImageTransform.Microsoft.Gradient(startColorstr='#333333', endColorstr='#222222', GradientType=0);
-webkit-transform: translatez(0);
-moz-transform: translatez(0);
-ms-transform: translatez(0);
-o-transform: translatez(0);
transform: translatez(0);
z-index: 2;
background-repeat: repeat-x;
			color: white;
			font-weight: bold;
			padding: 5px;
			border-radius: 15px;
			font-size: 12px;
			height: 32px;
			width: 100px;
		
		}
		
		#feedback1, #feedback2{
			
			
		}
		
		#op1 , #op2 , #op3 , #op4 {
			clear: both;
			margin-top: 20px;
		}
		
		#op3{
			margin-top: 50px;
			
		}
		
		#op1 p, #op2 p, #op3 p, #op4 p{
			margin-left: 127px;
			width: 579px;
			padding: 10px;
			border-radius: 10px;
		}
		
		#op1 img, #op2 img, #op3 img, #op4 img{
			width: 14%;
			float: left;
			height: auto;
			max-width: 100%;
			vertical-align: middle;
			margin-left: 10px;
		}
		
		#op1 p{
			background-color: #BACAFD;
		}
		
		#op2 p{
			background-color: #7CF881;
		}
		
		#op3 p{
			background-color: #FFF262;
		}
		
		#op4 p{
			background-color: #B4FAEF;
		}
		
		#op1 input, #op2 input, #op3 input,#op4 input{
			float: left;
			margin-top: 15px;
		}
		
	</style>
	
	<?php
	if ($_GET['ex']==1){ 
	
					
	}
	if ($_GET['ex']==2){ 

		echo '<script type="text/javascript">$("html,body").animate({scrollTop: $("#ex2").offset().top}, 1500);</script>';
	}
?>