<?php
               
    $pg_atual=6;
     registro($id,unid6_pg,$pg_atual,unid6_ev);
?>
    <style type="text/css">
	
	#tab_3{

		
	}
	
	#lin_1{
		width: 710px;
		background-color: #F68C23;
	
	}
	
	#lin_1 p{
		margin: 0;
		padding: 5px;
		padding-left: 225px;
		color: #fff;
		
		font-weight: bold;
	}
	
	#lin_4, #linha5, #linha6{
		margin-top: 0px;
	
	}
	
	#t1{
		width: 257px;	
		float: left;
		background-color: #FFE4CA;
		text-align: center;
		font-weight: bold;
		margin-top: 0px;
		margin-bottom: 1px;
	}
	
	#t2{
		float: right;
		width: 452px;
		background-color: #FFE4CA;
		text-align: center;
		font-weight: bold;
		margin-top: 0px;
		margin-left: 0px;
		margin-bottom: 1px;
		margin-right: 5px;
	
	}
	
	
	#t3, #t5, #t7{
		margin-top: 0px;
		width: 257px;
		height: 205px;
		vertical-align: middle;
		line-height: 167px;
		float: left;
		background-color: #FFF0E2;
	
	}
	
	
	#t4, #t6, #t8{
		width: 418px;
		height: 188px;
		margin-top: 0px;
		margin-right: 5px;
		padding-top: 17px;
		float: right;
		padding-left: 20px;
		padding-right: 14px;
		background-color: #FFF0E2;
	}
	
	#t4 p , #t6 p{
		width: 218px;
		float: left;
		margin-top: 50px;
	}
	
	#t4 img, #t6 img , #t8 img{
		float: right;
		border: 1px solid #F68C23;
	}
	
	#t5, #t6{
		background-color: #FFE4CA;
		margin-top: 0px;
	}
	
	#t7 p, #t8 p, #t9 p{
		margin-top: 25px;

	}
	
	#t8 p{
		width: 134px;
		float: left;
	}
	
	#t8 img{
		width: 62%;
		float: right;
	}
	
	#t3 p, #t5 p, #t7 p{
		margin-left: 45px;
		margin-top: 18px;
	
	}
	
	#lista{
            list-style-type: disc;  
            margin-left: 30px;
        }
		
	#lin_4, #lin_5, #lin_6, #lin_8, #lin_9{
		display: none;
	
	
	}
	
	#tipos_col{
		margin-left: 94px;
		
	}
	
	#tipos_col li{
		color: white;
		float: left;
		font-weight: bold;
		background-color: #F68C23;
		padding: 5px;
		border: 1px solid #fff;
		border-top: 0px;
		border-radius: 0px 0px 10px 10px;
		cursor: pointer;
	}
	.tabela{
        	width: 400px;
        	margin-left: 154px;
        }
      .center{
      	margin-left: 85px;
      }
      .fontes{
        	margin-left: 154px;	
        }
        .imgtabs2{
        	width: 529px;
        }
</style>
 <script>
	
		
		function change(id){
		
			if ($("#lin_" + id).is(":hidden")) {
				
				$('#lin_3').fadeOut("");
				$('#lin_4').fadeOut("");
				$('#lin_5').fadeOut("");
				$('#lin_6').fadeOut("");
			
				$("#lin_" + id).fadeIn(1000);
			}
			
			
		}
		
		function change2(id){
		
			if ($("#lin_" + id).is(":hidden")) {
				
				$('#lin_7').fadeOut("");
				$('#lin_8').fadeOut("");
				$('#lin_9').fadeOut("");
							
				$("#lin_" + id).fadeIn(1000);
			}
			
			
		}
 
</script>
	<h4 class="titulo">Estoma de Eliminação</h4>
   <h5>Colostomia</h5><br/>
   É um tipo de estoma de eliminação no qual, por meio de uma intervenção cirúrgica, ocorre a comunicação do 
   cólon (intestino grosso) com o exterior, podendo ser permanente ou temporária, terminal (uma boca) ou em 
   dupla boca (próximas - ambas em mesma colostomia, ou afastadas - uma boca na alça e outra terminal). É 
   indicada nos seguintes casos (ACADEMIA NACIONAL DE CUIDADOS PALIATIVOS, 2009):
   <ul>
    	<li id="lista">carcinoma de cólon e reto;</li>
	 	<li id="lista">retocolite ulcerativa;</li>
	 	<li id="lista">doença de Crohn;</li>
	 	<li id="lista">doença de Chagas e perfurações causadas por armas de fogo ou objetos perfuro-cortantes;</li>
	 	<li id="lista">fístulas colorretais; </li>
		<li id="lista">proteção de uma anastomose retocólica baixa;</li>
	 	<li id="lista">fístula retovaginal.</li>
</ul>
<br>


 <span style="margin-left: 241px;margin-top: 43px;margin-left: 0px;"><b>Classificação e tipos de colostomias quando o cólon é ligado à pele</b> <i class=" icon-hand-up" title="Esse quadro possui interação."></i> 
</span>
<div class="tabContainer" >
  <ul class="digiTabs" id="sidebarTabs">
    <li  id="tab1" class="selected"  onclick="tabs(this);">Colostomia ascendente</li>
    <li id="tab2" onclick="tabs(this);">Colostomia transversa</li>
    <li id="tab3"  onclick="tabs(this);">Colostomia descendente</li>
    <li id="tab4"  onclick="tabs(this);">Colostomia sigmoide</li>
  </ul>

  <div id="tabContent">
    

<table>
<tr>
<td class="td_texto"><b>Características:</b><br><br><br>O estoma é feito na alça ascendente, no lado direito do abdômen e as fezes têm consistência semilíquida.</td>
<td><img src="images/Fig07.1_colostomia_ascendente.jpg" alt="Colostomia ascendente" /></td>
</tr>
</table>
<br><br><b> Fonte:</b> (UNA-SUS UFPE, 2014).
  </div>
</div>
 
<div id="tab1Content" style="display:none;">
<table>
<tr>
<td class="td_texto"><b>Características:</b><br><br><br>O estoma é feito na alça ascendente, no lado direito do abdômen e as fezes têm consistência semilíquida.</td>
<td><img src="images/Fig07.1_colostomia_ascendente.jpg" alt="Colostomia ascendente"/></td>
</tr>
</table>
<br><br><b> Fonte:</b> (UNA-SUS UFPE, 2014) <!--(NETTINA, 2003; POTTER; PERRY, 2006; SMELTZER; BARE, 2005; SONOBE; BARICHELLO; ZAGO, 2002, adaptado)-->.
</div>

<div id="tab2Content" style="display:none;">
<table>
<tr>
<td class="td_texto"><b>Características:</b><br><br><br>O estoma é feito na alça do cólon
transverso, no lado esquerdo ou direito
do abdômen, e as fezes têm consistência pastosa.</td>
<td><img src="images/Fig07.2_colostomia_transversa.jpg" alt="Colostomia transversa"/></td>
</tr>
</table>
<br><br><b> Fonte:</b> (UNA-SUS UFPE, 2014) <!--(NETTINA, 2003; POTTER; PERRY, 2006; SMELTZER; BARE, 2005; SONOBE; BARICHELLO; ZAGO, 2002, adaptado)-->.
</div>
<div id="tab3Content" style="display:none;">
 <table>
<tr>
<td class="td_texto"><b>Características:</b><br><br><br>O estoma é feito na alça descendente, no lado esquerdo do abdômen, e as fezes têm consistência semissólida. </td>
<td><img src="images/Fig07.3_colostomia_descedente.jpg" alt="Colostomia descendente" /></td>
</tr>
</table>
<br><br><b> Fonte:</b> (UNA-SUS UFPE, 2014) <!--(NETTINA, 2003; POTTER; PERRY, 2006; SMELTZER; BARE, 2005; SONOBE; BARICHELLO; ZAGO, 2002, adaptado)-->.
</div>
<div id="tab4Content" style="display:none;">
  <table>
<tr>
<td class="td_texto"><b>Características:</b><br><br><br>Envolve o cólon sigmoide, e as fezes são firmes e sólidas. </td>
<td><img src="images/Fig07.5_colostomias_sigmoide.jpg" alt="Colostomia sigmoide"/></td>
</tr>
</table>
<br><br><b> Fonte:</b> (UNA-SUS UFPE, 2014) <!--(NETTINA, 2003; POTTER; PERRY, 2006; SMELTZER; BARE, 2005; SONOBE; BARICHELLO; ZAGO, 2002, adaptado)-->.
</div>


<style>
  .tabContainer{margin:10px 0;width:760px;font-size: 12px;font-size: 12px;font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;}
  .tabContainer .digiTabs{font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;list-style:none;display:block;overflow:hidden;margin:0;padding:0px;position:relative;top:1px;}
  .tabContainer .digiTabs li{font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;float:left;background-color:#b5b5b5;border:1px solid #e1e1e1;padding:5px!important;cursor:pointer;border-bottom:none;margin-right:10px;font-family:verdana;font-size:.8em;font-weight:bold;color:#fff;border-top-left-radius: 6px;border-top-right-radius: 6px;}
  .tabContainer .digiTabs .selected{font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;background-color:#fff;color:#393939;}
  #tabContent{font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;padding:10px;background-color:#F6F6F6;overflow:hidden;float:left;margin-bottom:10px;border-top: 1px solid #E1E1E1;border-bottom: 1px solid #E1E1E1;border-left: 1px solid #E1E1E1;border-right: 1px solid #E1E1E1;width:93%;background-color: #fff;border-top-right-radius: 6px;border-bottom-left-radius: 6px;border-bottom-right-radius: 6px;}
  .td_texto{width: 550px;}
  .td_texto2{width: 450px;}

  }
</style>
<script type="text/javascript">
  function tabs(x)
  {
    var lis=document.getElementById("sidebarTabs").childNodes; //gets all the LI from the UL
 
    for(i=0;i<lis.length;i++)
    {
      lis[i].className=""; //removes the classname from all the LI
    }
    x.className="selected"; //the clicked tab gets the classname selected
    var res=document.getElementById("tabContent");  //the resource for the main tabContent
    var tab=x.id;
    switch(tab) //this switch case replaces the tabContent
    {
      case "tab1":
        res.innerHTML=document.getElementById("tab1Content").innerHTML;
        break;
 
      case "tab2":
        res.innerHTML=document.getElementById("tab2Content").innerHTML;
        break;
      case "tab3":
        res.innerHTML=document.getElementById("tab3Content").innerHTML;
        break;
        case "tab4":
        res.innerHTML=document.getElementById("tab4Content").innerHTML;
        break;
      default:
        res.innerHTML=document.getElementById("tab1Content").innerHTML;
        break;
 
    }
  }
 
</script>



  <div id="tab_3" >
	
	

</div>


<br><br><br><br> <span style="margin-left: 241px;margin-top: 43px;margin-left: 0px;"><b>Classificação e tipos de colostomias quando à exteriorização no abdômen</b> <i class=" icon-hand-up" title="Esse quadro possui interação."></i> 
</span>
<!-- inicio tabs2-->
  <div class="tabContainer2" >
  <ul class="digiTabs2" id="sidebarTabs2">
    <li  id="tab1.2" class="selected2"  onclick="tabs2(this);">Colostomia em alça</li>
    <li id="tab2.2" onclick="tabs2(this);">Colostomia duplo barril</li>
    <li id="tab3.2"  onclick="tabs2(this);">Colostomia terminal</li>
   
  </ul>
  <div id="tabContent2">
   <table>
   	<tr>                  
            <td class="imgtabs2"><b>Características:</b><br><br><br>Normalmente são temporários e situados no cólon transverso. 
            	A alça do intestino transverso é fixada no abdômen. Possui 
            	duas aberturas, a extremidade proximal drena fezes, e a porção 
            	distal drena muco.</td>  
            <td ><img src="images/Fig07.4_alca.jpg" alt="Colostomia em alça" /> </td>   
            
    <tr>
    </table>
   <br><br><b> Fonte:</b> (UNA-SUS UFPE, 2014) <!--(NETTINA, 2003; POTTER; PERRY, 2006; SMELTZER; BARE, 2005; SONOBE; BARICHELLO; ZAGO, 2002, adaptado)-->.
  </div>
</div>
 
<div id="tab1Content2" style="display:none;">
    <table>
   	<tr>                  
            <td class="td_texto"><b>Características:</b><br><br><br>Normalmente são temporários e situados no cólon transverso. 
            	A alça do intestino transverso é fixada no abdômen. Possui 
            	duas aberturas, a extremidade proximal drena fezes, e a porção 
            	distal drena muco.</td>  
            <td ><img src="images/Fig07.4_alca.jpg" alt="Colostomia em alça" /> </td>   
            
    <tr>
    </table>
    <br><br><b> Fonte:</b> (UNA-SUS UFPE, 2014) <!--(NETTINA, 2003; POTTER; PERRY, 2006; SMELTZER; BARE, 2005; SONOBE; BARICHELLO; ZAGO, 2002, adaptado)-->.
</div>

<div id="tab2Content2" style="display:none;">
<table>
   	<tr>                  
            <td class="td_texto"><b>Características:</b><br><br><br>O intestino é totalmente separado, e as duas 
            	porções finais são trazidas para a parede abdominal. São 
            	construídos dois estomas distintos, o estoma de funcionamento 
            	proximal e o estoma distal, que não funciona.</td>  
            <td ><img src="images/Fig07.6_duplo-barril.jpg" alt="Colostomia duplo-barril" /></td>   
            
    <tr>
    </table>
    <br><br><b> Fonte:</b> (UNA-SUS UFPE, 2014) <!--(NETTINA, 2003; POTTER; PERRY, 2006; SMELTZER; BARE, 2005; SONOBE; BARICHELLO; ZAGO, 2002, adaptado)-->.
</div>
<div id="tab3Content2" style="display:none;">
 <table>
   	<tr>                  
            <td class="td_texto2"><b>Características:</b><br><br><br>O estoma é feito na alça do cólon transverso, no 
            	lado esquerdo ou direito do abdômen, e as fezes têm consistência 
            	pastosa.</td>  
            <td ><img src="images/Fig07.7_terminal.jpg" alt="Colostomia terminal" /> </td>   
            
    <tr>
    </table>
<br><br><b> Fonte:</b> (UNA-SUS UFPE, 2014) <!--(NETTINA, 2003; POTTER; PERRY, 2006; SMELTZER; BARE, 2005; SONOBE; BARICHELLO; ZAGO, 2002, adaptado)-->.
</div>


<style>
  .tabContainer2{margin:10px 0;width:760px;font-size: 12px;font-size: 12px;font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;}
  .tabContainer2 .digiTabs2{font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;list-style:none;display:block;overflow:hidden;margin:0;padding:0px;position:relative;top:1px;}
  .tabContainer2 .digiTabs2 li{font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;float:left;background-color:#b5b5b5;border:1px solid #e1e1e1;padding:5px!important;cursor:pointer;border-bottom:none;margin-right:10px;font-family:verdana;font-size:.8em;font-weight:bold;color:#fff;border-top-left-radius: 6px;border-top-right-radius: 6px;}
  .tabContainer2 .digiTabs2 .selected2{font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;background-color:#fff;color:#393939;}
  #tabContent2{font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;padding:10px;background-color:#F6F6F6;overflow:hidden;float:left;margin-bottom:10px;border-top: 1px solid #E1E1E1;border-bottom: 1px solid #E1E1E1;border-left: 1px solid #E1E1E1;border-right: 1px solid #E1E1E1;width:93%;background-color: #fff;border-top-right-radius: 6px;border-bottom-left-radius: 6px;border-bottom-right-radius: 6px;}
</style>
<script type="text/javascript">
  function tabs2(x)
  {
    var lis=document.getElementById("sidebarTabs2").childNodes; //gets all the LI from the UL
 
    for(i=0;i<lis.length;i++)
    {
      lis[i].className=""; //removes the classname from all the LI
    }
    x.className="selected2"; //the clicked tab gets the classname selected
    var res=document.getElementById("tabContent2");  //the resource for the main tabContent
    var tab=x.id;
    switch(tab) //this switch case replaces the tabContent
    {
      case "tab1.2":
        res.innerHTML=document.getElementById("tab1Content2").innerHTML;
        break;
 
      case "tab2.2":
        res.innerHTML=document.getElementById("tab2Content2").innerHTML;
        break;
      case "tab3.2":
        res.innerHTML=document.getElementById("tab3Content2").innerHTML;
        break;
        case "tab4.2":
        res.innerHTML=document.getElementById("tab4Content2").innerHTML;
        break;
      default:
        res.innerHTML=document.getElementById("tab1Content2").innerHTML;
        break;
 
    }
  }
 
</script>

<br><br>

  
  <table class="table table-bordered tabela " width="300px" style="background-color:#ffe4ca">
   	<tr>
   		<td style="background-color:#ccc"  colspan="2"><b class="center">Complicações da Colostomia</b></td>
   		
   	</tr>	
   	<tr>
   		<td><b>Precoce</b></td>
   		<td><b>Tardia</b></td>
   	</tr>	
   		<tr>
   		<td style="width: 178px;">Sangramento<br>
			Infecção da ferida<br>
			Edema<br>
			Dermatite peri-estomal<br>
			Isquemia ou necrose da
			alça exteriorizada<br>
			Disfagia<br></td>
   		<td>Estenose e obstrução<br>
			Prolapso<br>
			Hérnia para-estomal<br>
			Fístulas<br></td>
   	</tr>	
   </table>
   <span class="fontes"><b>Fonte:</b> (ROCHA, 2011, adaptado).</span>
   <br>
   <!-- excluído conforme solicitado
<div class="box" style="clear:both;margin-top: 25px;">
        <img src="../images/img_atencao_ad.png" >
        <span class="titulo_box">Atenção!</span>
         <hr>
      
      Quanto maior for a atividade funcional do intestino grosso, mais sólidas são as fezes, porque mais água foi absorvida pelo organismo. Portanto, a consistência da evacuação, que vem de uma colostomia, depende da região em que o intestino foi interrompido.
</div>
<br>-->
<h5 style="clear:both">Cuidados com a colostomia</h5>
  
  Para a maioria das pessoas, a perda da continência urinária ou fecal pode causar vários desequilíbrios, levando, assim, além dos problemas de ordem cirúrgica e física, a problemas de ordem psicológica e social. Dentro desse contexto, a equipe multiprofissional deve sempre orientar o paciente e o cuidador sobre a necessidade de (BRASIL, 2003):<br/>
  <ul>
<li id="lista">a bolsa estar adequada ao tamanho do estoma intestinal;</li>
<li id="lista">acondicionar as bolsas reservas em lugar arejado, limpo, seco e fora do alcance da luz solar;</li>
<li id="lista">esvaziar a bolsa de ileostomia e urostomia quando alcançar 1/3 de seu espaço preenchido;</li>
<li id="lista">proteger a bolsa durante o banho com um plástico e fitas adesivas;</li>
<li id="lista">trocar a bolsa, quando esta se apresentar saturada (placa protetora se torna quase completamente branca);</li>
<li id="lista">observar sempre a cor (deve ser vermelho vivo), o brilho, a umidade, a presença de muco, o tamanho e a forma do estoma;</li>
<li id="lista">realizar a limpeza do estoma delicadamente, não sendo necessário nem aconselhável esfregá-lo;</li>
<li id="lista">realizar a limpeza da pele ao redor do estoma com água e sabonete neutro, sem esfregar com força, nem usar esponjas ásperas;</li>
<li id="lista">aparar os pelos ao redor do estoma com tesoura;</li>
<li id="lista">expor a pele ao redor do estoma (sempre que possível) ao sol da manhã, de 15 a 20 minutos por dia, tendo cuidado de proteger o estoma com gaze umedecida;</li>
<li id="lista">não utilizar substâncias agressivas à pele, como álcool, benzina, colônias, tintura de benjoim, mercúrio, digluconato de clorexidina, pomadas e cremes, pois estes produtos podem ressecar a pele, ferindo-a e causando reações alérgicas.</li>
</ul>
<br><br>

<br>
<!--<a href="javascript:history.go(-1)" class="btn">Voltar</a>-->
