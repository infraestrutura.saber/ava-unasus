<?php

   require("../../../config.php");
    require("$CFG->dirroot/enrol/unasus/lib/context.php");
   require("$CFG->dirroot/enrol/unasus/message/lib.php");
   require("$CFG->dirroot/enrol/unasus/message/form_add.php");
 require_login();
   //permiss�o
    require_capability('enrol/unasus:messagecreate', get_context_instance(CONTEXT_SYSTEM), NULL, false);
    add_to_log(1, 'enrol_unasus', 'messagecreate', "enrol/unasus/message/add.php?contextid=$contextid", getremoteaddr());    
    
    //form
     $param=  new object();
     $param->contextid= required_param('contextid',PARAM_TEXT);
     
     $context =new context();
     $context_name=$context->get_name($param->contextid);   
    $form= new form_add();
    $message =new message();
   
    
    if ($form->is_cancelled()) {
        redirect($CFG->httpswwwroot.'/enrol/unasus/message/index.php') ;

    } else if ($formdata = $form->get_data()) {
    	$message->save($formdata);
    	redirect($CFG->httpswwwroot.'/enrol/unasus/message/index.php') ;
     }
   
    //Navega��o
   $navigation = build_navigation(array(array('name' => get_string('enrolname','enrol_unasus'), 'link' => "$CFG->wwwroot/enrol/unasus/index.php", 'type' => 'misc'),
                                     array('name' => get_string('message_maneger','enrol_unasus'), 'link' => "$CFG->wwwroot/enrol/unasus/message/index.php", 'type' => 'misc'), array('name' => $context_name, 'link' => null, 'type' => 'misc'), array('name' => get_string('message_add','enrol_unasus'), 'link' => null, 'type' => 'misc')));
   

     print_header(get_string('enrolname','enrol_unasus'),get_string('applicationsenrolment','enrol_unasusem'), $navigation);
   
 	
    $form->display();
  
    print_footer();
?>