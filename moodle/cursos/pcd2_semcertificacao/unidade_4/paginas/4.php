<?php
               
    $pg_atual=4;
     registro($id,unid4_pg,$pg_atual,unid4_ev);
?>

<style type="text/css">

#lista li{
 list-style-type: disc;   
  margin-left: 15px;
}

#imagens{
	width: 711px;
	height: 440px;
}
#desc{
	width: 280px;
	text-align: center;
	margin-left: 144px;
	position: absolute;
	top: 349;
}

</style> 

   <h4 class="titulo">Autocateterismo</h4>

   O autocateterismo tem como objetivo promover <b>a independência do paciente</b>, 
   reduzindo complicações e melhorando a sua autoestima e a sua qualidade de vida. 
    <br> <br>
Para a realização do autocateterismo no domicílio, pode-se utilizar de técnica limpa (não estéril), pois 
o risco de contaminação cruzada é menor neste ambiente. Além disso, pode reutilizar 
a sonda conforme protocolo de cada município, devendo esta ser higienizada com sabão 
antibacteriano líquido e enxaguada com água morna. Após limpa e seca, a sonda deverá 
ser armazenada em seu próprio recipiente ou saco plástico, semelhante ao utilizado 
para armazenar alimento (SMELTZER; BARE, 2005).
    <br> <br>
        É importante orientar o paciente e o cuidador que o esquema de cateterismo 
        intermitente limpo deve ser realizado em períodos de 4 em 4 ou 6 em 6 horas 
        e antes de dormir (SMELTZER; BARE, 2005). 

<br><br><br>
    

    <div class="box">
       <img src="../images/img_saibamais_ad.png" alt="Saiba mais" >
        <span class="titulo_box">Saiba mais</span>
        <br>
        <hr/>
 Para saber mais sobre autocateterismo, consulte o infográfico a seguir.<br><br>
 <b>Passo a passo para realização do autocateterismo</b> <i class=" icon-hand-up" title="Essa figura possui interação."></i>
<br><br>Oriente o paciente a reunir todos os materiais e higienizar o órgão genital. Depois 
ele deverá executar os passos a seguir e, ao final, limpar corretamente os materiais 
utilizados. 
<br><br><br>
	      <div id="imagens">
			<img class="josi" style="margin-left: 274px;" src="images/Fig01.1.jpg" alt="Autocateterismo"><br> <!-- <i class=" icon-hand-up" title="Essa figura possui interação."></i> -->
			<div class="um_action"></div>
		
			<div class="dois_action"></div>
			<div class="tres_action"></div>
			
			<div style="width:160px;margin-top: 30px;float:left;">
				<img class="dois" src="images/Fig01.2.jpg" alt="Autocateterismo">
				<img class="tres"  src="images/Fig01.3.jpg" alt="Autocateterismo"><br>
			</div>
			<div style="width:160px;margin-top: 30px;float:right;margin-right: 78px;">
				<img class="quatro"  src="images/Fig01.4.jpg" alt="Autocateterismo">
				<img class="cinco"  src="images/Fig01.5.jpg" alt="Autocateterismo">
			</div>
			<div id="desc">
					<div class="box_observar">
					É importante observar presença de:<br>
					febre, urina turva, sedimentos, muco, sangramento e/ou dor.
					<br>Deseja saber mais sobre como orientar o paciente a respeito do autocateterismo? Clique <a target="_blank" href="http://www.londrina.pr.gov.br/dados/images/stories/Storage/sec_saude/protocolos_clinicos_saude/instrucao_de_trabalho_acvi_tl_2011.pdf">aqui</a>  (SOUZA, MOROOKA, GONÇALVEZ, 2011).
					</ul>
				</div>
			</div>
		  </div>
    
			
	<br>
	<div style="clear: both; margin-left: 20px; margin-top: 30px; text-align: center;">
	
	<b>Fonte:</b> (UNA-SUS UFPE, 2014).	
	</div>
	<div class="box_um"><img src="images/quadro_aviso1.png"></div>
	<div class="box_dois"><img src="images/quadro_aviso2.png"></div>
	<div class="box_tres"><img src="images/quadro_aviso2.png"></div>
    <br><br>
    
      
   
</div>

<style type="text/css">
.josi{
	border: solid 2px #fff;
}
.um.1{
	border: 2px solid #fff;
}
.box_observar{
	padding: 5px;
	display: none;
	background-color: #F5D278;
	width: 455px;
margin-left: -18px;
}
#imagens{
	position: relative;

}
.dois{
	display: none;
	border: 2px solid #fff;
	margin-left: 78px;
}

.tres{
	display: none;
	margin-left: 78px;
	border: 2px solid #fff;
}
.quatro{
	display: none;
	margin-right: 78px;
	border: 2px solid #fff;
}
.cinco{
	display: none;
	margin-right: 78px;
	border: 2px solid #fff;
}

.um_action{
	background-color: #DAA520;
border: 2px solid #FFFFFF;
border-radius: 30px 30px 30px 30px;
cursor: pointer;
height: 10px;
left: 374px;
position: absolute;
top: 59px;
width: 10px;
z-index: 3;

}
.dois_action{
	background-color: #DAA520;
border: 2px solid #FFFFFF;
border-radius: 30px 30px 30px 30px;
cursor: pointer;
height: 10px;
left: 162px;
position: absolute;
top: 176px;
width: 10px;
z-index: 1;
display: none;
}
.tres_action{
	background-color: #DAA520;
border: 2px solid #FFFFFF;
border-radius: 30px 30px 30px 30px;
cursor: pointer;
height: 10px;
left: 586px;
position: absolute;
top: 196px;
width: 10px;
z-index: 3;
display: none;
}
.box_um{
left: 402px;
position: absolute;
top: 573px;
display: none;


}
.box_dois{
left: 200px;
position: absolute;
top: 746px;
display: none;

}
.box_tres{
	left: 236px;
position: absolute;
top: 826px;
display: none;

}
</style>

<script type="text/javascript">

$('.um_action').click(function(){
	$('.box_um').fadeIn(500);
	$('.dois').fadeIn(1000);
	$('.quatro').fadeIn(2000);
	$('.dois_action').fadeIn(2500);
	$('.tres_action').fadeIn(2500);
});
$('.dois_action').click(function(){
	$('.box_dois').fadeIn(500);
	$('.tres').fadeIn(2000);
	$('.cinco').fadeIn(1000);
	$('.box_observar').fadeIn(2000);
	
});
$('.tres_action').click(function(){
	$('.box_dois').fadeIn(500);
	$('.tres').fadeIn(2000);
	$('.cinco').fadeIn(1000);
	$('.box_observar').fadeIn(2000);

	
});

</script>
