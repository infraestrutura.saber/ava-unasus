<?php
   require("../../../../config.php");
   require("$CFG->dirroot/enrol/unasus/manage.php");
    require("$CFG->dirroot/enrol/unasus/reporter/lib.php");
   require("$CFG->dirroot/enrol/unasus/reporter/course/search_form.php");
   require("$CFG->dirroot/enrol/unasus/tab.php");
   require("$CFG->dirroot/enrol/unasus/lib/userprofile.php");
   require("$CFG->dirroot/enrol/unasus/profilefield/lib.php");
    require("$CFG->dirroot/enrol/unasus/profilefield/data/lib.php");
    require("$CFG->dirroot/enrol/unasus/lib/configcontext.php");

 require_login();
      require_capability('enrol/unasus:viewreporterenrol', get_context_instance(CONTEXT_SYSTEM), NULL, false);
     add_to_log(1, 'enrol_unasus', 'view', 'reporter/course/search.php', getremoteaddr());    
    //Navega��o
     $navigation = build_navigation(array(array('name' => get_string('enrolname','enrol_unasus'), 'link' => "$CFG->wwwroot/enrol/unasus/index.php", 'type' => 'misc'),
                                     array('name' => get_string('reporter_consolidate_by_course','enrol_unasus'), 'link' => null, 'type' => 'misc')));
     //tab menu
     $currenttab = 'reporter';
    $secondrow = array();
     $secondrow[] = new tabobject($currenttab,"$CFG->wwwroot/enrol/unasus/reporter/course/search.php",get_string('course','enrol_unasus'));
     $secondrow[] = new tabobject('cbo',"$CFG->wwwroot/enrol/unasus/reporter/cbo/search.php",get_string('cbo','enrol_unasus'));
    // $secondrow[] = new tabobject('enrol_data',"$CFG->wwwroot/enrol/unasus/reporter/enrolprofile/index.php",get_string('enrol_data','enrol_unasus'));
  	 $tabs = array($firstrow, $secondrow);
  
    
    $manage=new enrol_unasus_manage();
    $profile =new enrol_unasus_userprofile();
    $profilefield_field = new profilefield_field();
    $profilefield_data = new data_infofield();  
    
     $configcontext =new configcontext();
    
    //perfil do usu�rio
    $fields= $profile->get_fields_user_profile();
    $seleted_fields_filter=$profile->get_profile_config_in_array(enrol_unasus_userprofile::$USER_PROFILE_FILTER);
    $seleted_fields_reporter=$profile->get_profile_config_in_array(enrol_unasus_userprofile::$USER_PROFILE_REPORTER);
    $profile_data= null; 
  
  
    //parâmetros
    $param=  new object();
     $param->userid= optional_param('userid', NULL,PARAM_INT);
     $param->username= optional_param('username', NULL,PARAM_TEXT);
    $param->statusinfo= optional_param('statusinfo', NULL,PARAM_INT);
    $param->userid_sql_profile="";
    
 
   $data_fields=$profile->get_data_profile_field($fields,'F_');
   		//echo "Data Fied: <br>";
   		//print_r($data_fields);
   		$param->userid_sql_profile.=$profile->get_sql_filter_user_data($fields,$data_fields,'userid');
   		//echo "<br><br>SQL PAM: $param->userid_sql_profile<br> ";
    
   //form
    $form= new couse_search_form();
    
    function get_table_view(){
   	global $manage,$param;
    $rows = $manage->search_by_course_user($param);
    $rows_count = $manage->count_search_by_course($param);
    $aprove = $manage->search_by_course_status($param,enrol_unasus_manage::$STATUS_ENROL_ACCEPT);
    $reject = $manage->search_by_course_status($param,enrol_unasus_manage::$STATUS_ENROL_NOT_ACCEPT);
   
      echo "<center>".get_string('rowcount','enrol_unasus') ." " . $rows_count."</centar>";
    
   	  if(!empty($rows)){
   	  		$course_name=$manage->get_course_name($rows);
   			  $table = new object();
    	  		$table->head  = array(get_string('id','enrol_unasus'),get_string('course','enrol_unasus'),get_string('count_enrol_request','enrol_unasus'),get_string('count_enrol_request_aprove','enrol_unasus'),get_string('count_enrol_request_reject','enrol_unasus'));
    	  		$table->align = array('left', 'left','left','left','left');
    	  		$table->width = '95%';
         		 $table->class = 'generaltable';
          		$table->data = array();
				foreach ($rows as $row) {
					$table->data[] = array($row->courseid,$course_name[$row->courseid],$row->userid,$aprove[$row->courseid], $reject[$row->courseid]);
           		}
        		
   				 print_table($table);
   		}
   		
   }
     
     
    print_header(get_string('applicationsenrolment','enrol_unasusem'),get_string('applicationsenrolment','enrol_unasusem'), $navigation);
   
 	print_tabs($tabs, $currenttab, $inactive, $activated);
    $form->display();
  
  get_table_view();
   
    print_footer();
    
?>