<?php

   require("../../../../config.php");
   require("$CFG->dirroot/enrol/unasus/profilefield/category/lib.php");
   require("$CFG->dirroot/enrol/unasus/profilefield/lib.php");
  
 require_login();
   
    require_capability('enrol/unasus:profilefielddelete', get_context_instance(CONTEXT_SYSTEM), NULL, false);
  
    
  
   $profilefield_field = new profilefield_field();
    $param=  new object();
    $param->id= required_param('id',PARAM_INT);
    $param->contextid= required_param('contextid',PARAM_INT);
    
     add_to_log(1, 'enrol_unasus', 'profilefielddelete', "enrol/unasus/profilefield/delete.php", getremoteaddr());
     
    $profilefield_field->delete_by_id($param->id);
   header('Location: '.$CFG->wwwroot.'/enrol/unasus/profilefield/manage.php?contextid='.$param->contextid) ;
?>