<div style='display:none'>
			<div id="content_color1" style='padding:10px; background:#fff;'>

			<div id="texto_color"></div>
			<br><br>
			<a class="btn inline" href="index.php?pagina=0" onClick="apagar_confirmar();" >Sim, desejo recomeçar os estudos.</a>
			<a class="btn" href="index.php?pagina=0">Não, quero continuar.</a>
				
			<br><br>
			
			</div>
</div>

<style type="text/css">
        #lista{
            list-style-type: disc; 
             margin-left: 30px;
        }
    </style>
        <p>
            <h4 class='titulo'>Apresentação da Unidade</h4>
        
        </p>
		A alimentação, além de ser uma necessidade fisiológica, é uma atividade 
    importante na prevenção de doenças e na recuperação da saúde do paciente que 
    necessita de cuidados domiciliares. Em algumas situações, devido às enfermidades, 
    aos processos cirúrgicos ou até mesmo ao processo de envelhecimento, ela pode 
    se apresentar prejudicada. <br><br>
Nesta unidade, veremos as indicações e os tipos de sondagem para fins nutricionais, 
os cuidados no manuseio dos materiais, as responsabilidades da equipe de enfermagem 
na execução do procedimento. Também estudaremos as orientações para o seguimento
 nutricional no domicílio e as possíveis intercorrências que poderão ocorrer 
 durante a sondagem e a nutrição enteral.<br>

		<br>
        <br>
       <b>Objetivos educacionais da unidade </b>
            <li id="lista">Especificar os tipos de sondas; </li>
           <li id="lista">Reconhecer as indicações, cuidados no manuseio e as responsabilidades da equipe na execução do procedimento;</li>
           <li id="lista">Descrever os procedimentos necessários para medição e fixação da sonda nasoentérica, os cuidados no preparo e administração de dietas, orientações quanto ao seguimento nutricional, métodos de administração nutricional e intercorrências na realização do procedimento.</li>
           
           <br><br>
          


       
        <br>
        <img src="images/abertura.png" alt="Imagem de abertura Unidade 5">
		
		<script type="text/javascript">
			
		function apagar(){
			$("#texto_color").html("<h4>Confirmação</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, você tem certeza que deseja recomeçar os estudos desta unidade? Todos os seus registros serão zerados.");
            $(".inline").colorbox({inline:true, width:"50%",onClosed:function(){}});
		}
		
		function apagar_confirmar(){
			
			$.post( "../apresentacao/functions/apagar_tudo.php", { unidade: 5 }, function() {
				location.reload();
				});
			
			
		
		}
		
		</script>
