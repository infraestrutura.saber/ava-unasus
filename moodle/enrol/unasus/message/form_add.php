<?php //$Id: forgot_password_form.php,v 1.7.2.1 2007/11/23 22:12:35 skodak Exp $

require_once $CFG->libdir.'/formslib.php';

class form_add extends moodleform {

    function definition() {
    	global $param;
    	
        $mform    =& $this->_form;
        $renderer =& $mform->defaultRenderer();

        $mform->addElement('header', '', get_string('message_add','enrol_unasus'), '');
        
         $mform->addElement('hidden', 'contextid', $param->contextid);
        $mform->setType('contextid', PARAM_INT);
        
         $mform->addElement('htmleditor', 'presentation', get_string('message_presentation','enrol_unasus'),'wrap="virtual" rows="7" cols="40"');
        $mform->setType('presentation', PARAM_TEXT);
         $mform->setHelpButton('presentation', array('message_presentation', get_string('message_presentation', 'enrol_unasus'), 'enrol_unasus'));
        
         $mform->addElement('htmleditor', 'approval', get_string('message_approval','enrol_unasus'),'wrap="virtual" rows="7" cols="40"');
        $mform->setType('approval', PARAM_TEXT);
        $mform->setHelpButton('approval', array('message_approval', get_string('message_approval', 'enrol_unasus'), 'enrol_unasus'));
        
         $mform->addElement('htmleditor', 'disapproval', get_string('message_disapproval','enrol_unasus'),'wrap="virtual" rows="7" cols="40"');
        $mform->setType('disapproval', PARAM_TEXT);
        $mform->setHelpButton('disapproval', array('message_disapproval', get_string('message_disapproval', 'enrol_unasus'), 'enrol_unasus'));
        
         $mform->addElement('htmleditor', 'autreviewapproval', get_string('message_autreview','enrol_unasus'),'wrap="virtual" rows="7" cols="40"');
        $mform->setType('autreviewapproval', PARAM_TEXT);
         $mform->setHelpButton('autreviewapproval', array('message_autreviewapproval', get_string('message_autreview', 'enrol_unasus'), 'enrol_unasus'));
       
         $mform->addElement('htmleditor', 'manualreviewapproval', get_string('message_manualreview','enrol_unasus'),'wrap="virtual" rows="7" cols="40"');
        $mform->setType('manualreviewapproval', PARAM_TEXT);
        $mform->setHelpButton('manualreviewapproval', array('message_manualreviewapproval', get_string('message_manualreview', 'enrol_unasus'), 'enrol_unasus'));
        
       //  $mform->addElement('htmleditor', 'pendingapprovalmanager', get_string('message_pendingapprovalmanager','enrol_unasus'),'wrap="virtual" rows="7" cols="40"');
        //$mform->setType('pendingapprovalmanager', PARAM_TEXT);
        
       
        $this->add_action_buttons(true,get_string('save','enrol_unasus'));
    }

    function validation($data, $files) {
        global $CFG;

         $errors = parent::validation($data, $files);
        
      
     return $errors;
    }

}

?>