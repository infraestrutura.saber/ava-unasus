<?php

    require("../../../../config.php");
    
	 require("$CFG->dirroot/admin/report/synciccarouca/course/relatorio_form.php");
	 require("$CFG->dirroot/admin/report/synciccarouca/course/lib.php");
 	 require("$CFG->dirroot/admin/report/synciccarouca/tab.php");
 	  require("$CFG->dirroot/admin/report/synciccarouca/cpf.php");


  // Acesso permitido apenas ao usu�rio admin
    require_capability('moodle/legacy:admin', get_context_instance(CONTEXT_SYSTEM), NULL, false);
    
    //Navega��o
    $navlinks = array();
    $navlinks[] = array('name' => get_string('synciccarouca','report_synciccarouca'), 'link' => ".", 'type' => 'misc');
    $navigation = build_navigation($navlinks);
    
   //tab menu
     $currenttab = 'reporter';
  
  
     
  //regras da sinc
   $syc_ing_conc=new ingr_conc_sync_couse();
  $cpf=new cpf();
    
    
      
    //parâmetros
   $param=  new object();
    $param->courseid= optional_param('courseid', NULL,PARAM_INT);
    $param->userid= optional_param('userid', NULL,PARAM_INT);
    $param->status= optional_param('status', NULL,PARAM_INT);
    $param->page= optional_param('page', 0,PARAM_INT);
    $param->perpage= optional_param('perpage', 100,PARAM_INT);
    //$param->paginar=TRUE; 
    $form= new relatorio_form();
 /*echo "courseid: $param->courseid <br>";
    echo "userid: $param->userid <br>";
    echo "status: $param->status <br>";*/
   function get_table_view(){
  	global $syc_ing_conc,$param,$cpf;
    $rows = $syc_ing_conc->pesquisa($param);
    $rows_count = $syc_ing_conc->pesquisa_countar($param);
    //echo  sizeof($syc_ing_conc->pesquisa_countar($param));
    
     
       echo ' <center>'.get_string('rowfilter','report_synciccarouca') .' '. $rows_count.' </center><br>';
     paging($rows_count);
     
     $output = ' <center><table class="generaltable   border="1"  boxaligncenter" width="95%" cellspacing="1" cellpadding="5">';
    $output .= '<tr  bgcolor="#F6F6F6">
                <th align="left">'.get_string('name').'</th>
				<th align="left">'.get_string('cpf','report_synciccarouca').'</th>
				<th align="left">'.get_string('course','report_synciccarouca').'</th>
				<th align="left">'.get_string('status','report_synciccarouca').'</th>
				<th align="left">'.get_string('homologado_por','report_synciccarouca').'</th>
				<th align="left">'.get_string('data_ingresso','report_synciccarouca').'</th>
				<th align="left">'.get_string('data_conclusao','report_synciccarouca').'</th>
				<th align="left">'.get_string('certificado','report_synciccarouca').'</th>
				<th align="left">'.get_string('msg_falha_ws','report_synciccarouca').'</th>
                
                </tr>';
               
  
    if(!empty($rows)){
    	$status=$syc_ing_conc->get_status_tabela_dominio();
    	$cont=0;
        foreach ($rows as $row){
        	$cont++;
        	$resto=$cont%2;
        	$fundo=" ";
        	if($resto==0){$fundo=" bgcolor=\"#F6F6F6\" ";}
        	$nstatus=$status[$row->status];
        	
        	$dataconclusao="";
        	if($row->dataconclusao) $dataconclusao=date('d/m/Y',$row->dataconclusao);
        	
        	$dataingresso="";
        	if($row->dataingresso) $dataingresso=date('d/m/Y',$row->dataingresso);
        	
        	 $output .= '<tr '.$fundo.'>';
			$output .= '<td>'.$row->firstname.' '.$row->lastname.'</td>';
			$output .= '<td>'.$cpf->format($row->username).'</td>';
            $output .= "<td>$row->fullname</td>";
            $output .= "<td>$nstatus</td>";
            $output .= '<td>'.$row->firstnameua.' '.$row->lastnameua.'</td>';
           $output .= "<td>".$dataingresso."</td>";
            $output .= "<td>".$dataconclusao."</td>";  
            $output .= "<td>$row->urlcertificado</td>";            
             $output .= "<td>$row->faultstringwsarouca</td>";
            $output .= '</tr>';   
    	}
    }  
    
    $output .= '</table></center>';
      echo  $output;
     paging($rows_count);

   }
      
    print_header(get_string('synciccarouca','report_synciccarouca'),get_string('synciccarouca','report_synciccarouca'), $navigation);
   
 	print_tabs($tabs, $currenttab, $inactive, $activated);
    $form->display();
  
    get_table_view();
   
    print_footer();
    
     function paging($totalcount){
  		global $CFG,$param;
  		 print_paging_bar($totalcount, $param->page, $param->perpage, "$CFG->wwwroot/admin/report/synciccarouca/course/relatorio.php?courseid=$param->courseid&userid=$param->userid&status=$param->status&perpage=$param->perpage&");
  }
?>