<?php
               
    $pg_atual=6;
     registro($id,unid7_pg,$pg_atual,unid7_ev);
?>

<style type="text/css">

#lista li{
            list-style-type: disc;   
      // margin-left: 30px;




}
</style> 

   <h4 class="titulo">Intercorrências  e contraindicações para a administração 
    do oxigênio domiciliar</h4>

    Cada vez mais os pacientes são assistidos dentro dos seus domicílios enquanto 
    ainda necessitam de ventiladores mecânicos, com cânula de traqueostomia ou sob 
    oxigenoterapia. Este cuidado pode ser realizado com relativo sucesso, sendo 
    importante a família e o paciente estarem informados e emocional e fisicamente
    preparados para compartlihar essa modalidade de assistência. A equipe de 
    cuidado domiciliar deve estar disponível, e o domicílio deve ser avaliado 
    periodicamente para que seja determinado se está adequado ao desenvolvimento 
    seguro, tanto para o paciente quanto para o profissional, das atividades que 
    envolvem a oxigenoterapia (LIMA; VARGAS, 2004). <br> <br>
    As atividades de assistência à saúde não são isentas de riscos. O papel da 
    equipe multiprofissional é minimizar os fatores predisponentes, por meio da 
    capacitação profissional, sistematização da assistência de enfermagem e 
    construção de protocolos embasados nas melhores evidências científicas (ANVISA, 2009).

  <br> <br>
   Podemos citar três categorias de riscos associadas à oxigenoterapia domiciliar 
   (SOCIEDADE BRASILEIRA DE PNEUMOLOGIA E TISIOLOGIA, 2000), conforme descritas na 
   figura a seguir.
<br><br>

<!--tabs-->


 <div class="tabContainer2">
  <ul class="digiTabs2" id="sidebarTabs2">
    <li  id="tab1.2" class="selected2"  onclick="tabs2(this);">Físicos</li>
    <li id="tab2.2" onclick="tabs2(this);">Funcionais</li>
    <li id="tab3.2"  onclick="tabs2(this);">Químicos</li>
   
  </ul>
  <div id="tabContent2">
   
   <ul id="lista">
          <li><b>Incêndios</b> - na maioria das vezes, ocasionados pelo hábito de fumar 
            durante o uso do oxigênio;</li>
          <li><b>Explosões</b> - geralmente ocorrem por golpes ou quedas dos cilindros 
            e/ou por manipulação inadequada dos redutores de pressão;</li>
          <li><b>Traumas</b> - ocasionados pelo cateter ou máscara;</li>
          <li><b>Ressecamento de secreções</b> - devido à umidificação inadequada.</li>
    </ul>
  </div>
</div>
 <br><br><br><br>
<div id="tab1Content2" style="display:none;">
      <ul id="lista">
          <li><b>Incêndios</b> - na maioria das vezes, ocasionados pelo hábito de fumar 
            durante o uso do oxigênio;</li>
          <li><b>Explosões</b> - geralmente ocorrem por golpes ou quedas dos cliindros 
            e/ou por manipulação inadequada dos redutores de pressão;</li>
          <li><b>Traumas</b> - ocasionados pelo cateter ou máscara;</li>
          <li><b>Ressecamento de secreções</b> - devido à umidificação inadequada.</li>
    </ul>
</div>
<br><br>
<div id="tab2Content2" style="display:none;">
 <b>Retenção de CO<sub>2</sub> e atelectasias</b> - ocasionadas pelo aumento 
                  da PaO<sub>2</sub>, resultante da administração de oxigênio, que  pode 
                  aumentar o espaço morto devido à reversão da vasoconstrição 
                  hipóxica pulmonar, aumentando a perfusão de áreas com pequena 
                  ventilação e desviando sangue de áreas bem ventiladas, resultando 
                  em alterações da relação ventliação/perfusão, aumento do 
                  espaço morto e, consequentemente, da Pressão Parcial de Gás 
                  Carbônico - PaCO<sub>2</sub>.
</div>
<br><br>
<div id="tab3Content2" style="display:none;">
 A toxicidade de oxigênio é decorrente da produção exagerada de 
                radicais livres desse gás, lesando a mucosa pulmonar. Afeta, 
                sobretudo, os pulmões e o sistema nervoso. Sintomas como 
                tremores, contrações, letargia e convulsões podem caracterizar 
                toxicidade do sistema nervoso central. Essas complicações estão
                 relacionadas a: <ul id="lista"><li>frações inspiradas do gás oxigênio 
                 superiores a 50% por longos períodos;</li>
                <li>doenças neuromusculares;</li>
                <li>doenças genéticas.</li>
            </ul>

</div>
<br><br>
<style>
  .tabContainer2{margin:10px 0;width:760px;font-size: 12px;}
  .tabContainer2 .digiTabs2{list-style:none;display:block;overflow:hidden;margin:0;padding:0px;position:relative;top:1px;}
  .tabContainer2 .digiTabs2 li{float:left;background-color:#b5b5b5;border:1px solid #e1e1e1;padding:5px!important;cursor:pointer;border-bottom:none;margin-right:10px;font-size:12px;font-weight:bold;color:#fff;border-top-left-radius: 6px;border-top-right-radius: 6px;}
  .tabContainer2 .digiTabs2 .selected2{background-color:#fff;color:#393939;}
  #tabContent2{padding:10px;background-color:#F6F6F6;overflow:hidden;float:left;margin-bottom:10px;border-top: 1px solid #E1E1E1;border-bottom: 1px solid #E1E1E1;border-left: 1px solid #E1E1E1;border-right: 1px solid #E1E1E1;width:93%;background-color: #fff;border-top-right-radius: 6px;border-bottom-left-radius: 6px;border-bottom-right-radius: 6px;}
</style>
<script type="text/javascript">
  function tabs2(x)
  {
    var lis=document.getElementById("sidebarTabs2").childNodes; //gets all the LI from the UL
 
    for(i=0;i<lis.length;i++)
    {
      lis[i].className=""; //removes the classname from all the LI
    }
    x.className="selected2"; //the clicked tab gets the classname selected
    var res=document.getElementById("tabContent2");  //the resource for the main tabContent
    var tab=x.id;
    switch(tab) //this switch case replaces the tabContent
    {
      case "tab1.2":
        res.innerHTML=document.getElementById("tab1Content2").innerHTML;
        break;
 
      case "tab2.2":
        res.innerHTML=document.getElementById("tab2Content2").innerHTML;
        break;
      case "tab3.2":
        res.innerHTML=document.getElementById("tab3Content2").innerHTML;
        break;
        case "tab4.2":
        res.innerHTML=document.getElementById("tab4Content2").innerHTML;
        break;
      default:
        res.innerHTML=document.getElementById("tab1Content2").innerHTML;
        break;
 
    }
  }
 
</script>



                
                
                    
                     <div class="box">
                      <img src="../images/img_vocesabiaque_ad.png" alt="Você sabia" >
                        <span class="titulo_box">Você sabia que...</span>
                        <br>
                        <hr/>
                         <span class="subtitulo"> Não existem contraindicações 
                          para a administração do oxigênio domiciliar, entretanto,
                           alguns aspectos podem ser limitantes desta terapia no 
                           domicílio, como:</span><br><br>
                          <b>Enfermidades associadas</b>
                         <ul id="lista">   
                              <li>enfermidades psiquiátricas graves. </li></ul>
                                 
                                  <b>Características dos pacientes:</b>
                         <ul id="lista">
                          <li>dificuldades do paciente e de familiares para 
                            entender os riscos associados 
                            ao tratamento e em manter as medidas de segurança 
                            adequadas;</li>
                          <li>persistência do tabagismo;</li>
                          <li>não aderência ao tratamento farmacológico instituído.</li>
                        </ul>   
                        <b>Aspectos sociais e demográficos</b>
                        <ul id="lista">
                          <li>residência em locais de difícil acesso e com problemas no 
                              fornecimento de energia elétrica.</li></ul>
                              <br>
                                <span class="credito"> <b>Fonte: </b>(SCANLAN; WILKINS; STOLLER, 
                      2009, adaptado).</span>
                    </div>
                  
           
           <style type="text/css">

           .um_descricao , .dois_descricao, .tres_descricao {
            color: #d5c9c1;

           }
           .um ,.dois, .tres{
            cursor: pointer;
           }
           </style>
           <script type="text/javascript">
            $(".um").mouseover(function(){
                $(".um_descricao").css('color','#000');
              });
            $(".dois").mouseover(function(){
                $(".dois_descricao").css('color','#000');
              });
            $(".tres").mouseover(function(){
                $(".tres_descricao").css('color','#000');
              });

           </script>
           
           
    
  
