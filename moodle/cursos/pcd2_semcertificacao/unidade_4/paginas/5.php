<?php
               
    $pg_atual=5;
     registro($id,unid4_pg,$pg_atual,unid4_ev);
?>
<!-- <script src = "http://www.youtube.com/player_api"></script> -->
<style type="text/css">

	#lista li{
	 list-style-type: disc; 
      margin-left: 30px;  

	}
</style> 

   <h4 class="titulo">Cuidados  no manuseio dos equipamentos </h4>

   Durante a realização do cateterismo 
   vesical, com intuito de reduzir as possíveis complicações deste procedimento, o 
   profissional deve atentar para cuidados fundamentais que regem a técnica, como (LENZ, 2006):
<ul id="lista"> 
    <li>usar rigorosamente a técnica asséptica;</li>
    <li>fazer uso de Equipamentos de Proteção Individual (EPIs) durante o procedimento;</li>
<li>escolher o tamanho e tipo de sonda apropriado;</li>
<li>evitar a contaminação do orifício de drenagem (local de conexão entre a sonda com a bolsa coletora);</li>
<li>evitar a contaminação da bolsa coletora de sistema fechado, orientando o cuidador a evitar contato da bolsa coletora com o piso ou com animais domésticos;</li>
<li>testar o balonete da sonda vesical de demora antes da sua inserção;</li>
<li>preencher o balonete da sonda vesical de demora conforme indicação do fabricante;</li>
<li>desinflar o balonete da sonda vesical de demora antes de sua retirada;</li>
<li>evitar o manuseio da sonda vesical de demora além do necessário, evitando traumas;</li>
<li>fixar adequadamente a sonda vesical de demora evitando pressão, tensão e tração sobre a bexiga ou uretra devido risco de lesão;</li>
<li>alternar o lado de fixação da bolsa coletora a fim de evitar lesão no meato urinário (entrada da uretra), local onde a sonda se apoia;</li>
<li>não elevar a bolsa coletora acima da linha média da cintura do paciente para que não haja retorno da urina, evitando, assim, infecção do trato urinário.</li>

    </ul>
<br><br>
    

    <div class="box">
        <img src="../images/img_atencao_ad.png" alt="Atenção" >
        <span class="titulo_box">Atenção!</span>
        <br>
        <hr/>
 Apesar de o ambiente de cuidado 
 ser o domiciliar, deve-se orientar o cuidador e o paciente sobre a 
 importância do controle de infecção na realização de procedimentos invasivos, como a sondagem. O paciente e o cuidador devem 
     receber informações quanto à necessidade do uso da técnica correta 
     de lavagem das mãos e cuidados para evitar a contaminação dos materiais necessários para o procedimento
     de sondagem. A higiene das mãos é essencial antes e após manusear qualquer parte da sonda vesical ou
     sistema de drenagem, assim como a higiene íntima pré e pós-cateterismo (BRASIL, 2013).
<br><br>
    
   
</div>
<br>
<div class="box">
    <br>
        <img src="../images/img_saibamais_ad.png" alt="Saiba mais">
        <span class="titulo_box">Saiba mais</span>
        <hr/>
 
      Para saber mais sobre a higiene 
     íntima pré e pós-cateterismo, assista ao vídeo  a seguir.
      <br><br>
    <table width="100%" ><td align="center"><div id="player"></div></td></table><br>

      
   
</div>

<script type="text/javascript">

//onYouTubePlayerAPIReady

// 2. This code loads the IFrame Player API code asynchronously.
      var tag = document.createElement('script');

      tag.src = "https://www.youtube.com/iframe_api";
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

      // 3. This function creates an <iframe> (and YouTube player)
      //    after the API code downloads.
      var player;
      function onYouTubeIframeAPIReady() {
        player = new YT.Player('player', {
          height: '250',
          width: '400',
          videoId: 'BWx_M8prBss',
          events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
          }
       
        });
      }
      // 4. The API will call this function when the video player is ready.
      function onPlayerReady(event) {
        //event.target.playVideo();
      }

      // 5. The API calls this function when the player's state changes.
      //    The function indicates that when playing a video (state=1),
      //    the player should play for six seconds and then stop.
      var done = false;
      function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.PLAYING && !done) {
     $.post( "../apresentacao/functions/chamaCadVideo.php", { video: 5}, function() {
						
						});
          done = true;
        }
      }

</script>
    
		<?php

	if ($_GET['video']==1){ 

		echo '<script type="text/javascript">$("html,body").animate({scrollTop: $("#player").offset().top}, 1500);</script>';
	}
	
?>


   
