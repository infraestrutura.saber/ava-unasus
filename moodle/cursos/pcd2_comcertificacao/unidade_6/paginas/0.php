<div style='display:none'>
			<div id="content_color1" style='padding:10px; background:#fff;'>

			<div id="texto_color"></div>
			<br><br>
			<a class="btn inline" href="index.php?pagina=0" onClick="apagar_confirmar();" >Sim, desejo recomeçar os estudos.</a>
			<a class="btn" href="index.php?pagina=0">Não, quero continuar.</a>
				
			<br><br>
			
			</div>
</div>
<style type="text/css">
        #lista{
            list-style-type: disc;  
            margin-left: 30px;
        }
        #lista2
        {
            list-style-type: square; 
            margin-left: 50px;
        }
         #lista3
        {
            
            margin-left: 60px;
        }
    </style>
        <p>
            <h4 class='titulo'>Apresentação da Unidade</h4>
        
        </p>
		Como você já sabe, a pele é uma barreira mecânica contra a invasão dos microrganismos causadores de doença. 
        Quando lesionada, torna-se uma porta de entrada para patógenos, pondo em risco a saúde do indivíduo. 
        Portanto, especial atenção deve ser dada ao paciente estomizado.<br><br>
Nesta unidade, não somente estudaremos os tipos de estomas, como também identificaremos as responsabilidades da 
equipe de enfermagem na realização do procedimento e os dispositivos e acessórios necessários para realização de 
procedimentos no paciente estomizado. Por fim, refletiremos sobre as condições facilitadoras e dificultadoras do 
cuidado ao paciente estomizado no domicílio.<br>

		<br>

        <br>
      <b>Objetivos educacionais da unidade: </b>
           <li id="lista">Classificar os tipos de estomas e com qual sistema do corpo humano se relacionam;</li>
           <li id="lista">Identificar as responsabilidades da equipe, os dispositivos e acessórios necessários 
           para a realização dos procedimentos de enfermagem no paciente estomizado;</li>
            <li id="lista">Refletir sobre as condições adjuvantes e facilitadoras durante o cuidado com paciente 
            estomizado no domicílio.</li>
           
           <br><br>
          


        
        <br>
        <img src="images/abertura.png" alt="Imagem de abertura Unidade 6">
		<script type="text/javascript">
			
		function apagar(){
			$("#texto_color").html("<h4>Confirmação</h4> <br><br> <b> <?php echo $USER->firstname; ?></b>, você tem certeza que deseja recomeçar os estudos desta unidade? Todos os seus registros serão zerados.");
            $(".inline").colorbox({inline:true, width:"50%",onClosed:function(){}});
		}
		
		function apagar_confirmar(){
			
			$.post( "../apresentacao/functions/apagar_tudo.php", { unidade: 6 }, function() {
						location.reload();
				});
			
		
		}
		
		</script>