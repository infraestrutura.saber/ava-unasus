<?php //$Id: forgot_password_form.php,v 1.7.2.1 2007/11/23 22:12:35 skodak Exp $

require_once $CFG->libdir.'/formslib.php';

class enrol_unasus_manual_accept_form extends moodleform {

    function definition() {
    	global $enrolid;
	  $mform    =& $this->_form;
        $renderer =& $mform->defaultRenderer();

        $mform->addElement('header', '', get_string('manual_aprove','enrol_unasus'), '');
        
      	
        $mform->addElement('textarea', 'justify', get_string('justify','enrol_unasus'),'wrap="virtual" rows="15" cols="80"');
        $mform->setType('justify', PARAM_TEXT);
        $mform->addRule('justify', get_string('requiredfield','enrol_unasus'), 'required', null, 'client');

         $mform->addElement('hidden', 'enrolid',$enrolid);
        $mform->setType('enrolid', PARAM_INT);	
        //data
        $this->add_action_buttons(true, get_string('save','enrol_unasus'));
    }

    function validation($data, $files) {
        global $CFG;

         $errors = parent::validation($data, $files);
         
      
     return $errors;
    }

}

?>