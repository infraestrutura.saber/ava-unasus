<?php
/*
 * Created on 18/07/2012
 *
 *Essa class tem as bibliotecas de gerenciamento
 * da dados do perfil do usu�rio na matr�cula 
 */
 class data_infofield {
 		
 	//inserir regristro na base de dados. 
 	function save($dto) {
 		global $CFG;
 		return insert_record('enrol_unasus_info_data', $dto);
 	}
 //inserir regristro na base de dados se n�o existe e atualize se j� existe 
 	function create($dto) {
 		$countreg=$this->exist_by_user($dto->userid,$dto->fieldid);
 		if($countreg==1){
 			$dto->id=$this->get_id_by_user($dto->userid,$dto->fieldid);
 			 return $this->edit($dto);
 		}else if($countreg>1){
 			$this->delete_by_user($dto->userid,$dto->fieldid);
 			return $this->save($dto);
 		}else if($countreg==0){
 			 return $this->save($dto);
 		}
 		
 		
 	}
 //alteriar regristro na base de dados. 
 	function edit($dto) {
 		global $CFG;
 		return update_record('enrol_unasus_info_data', $dto);
 	}

  //excluir regristro na base de dados. 
 function delete_by_user($userid,$fieldid) {
 		global $CFG;
 		return delete_records_select('enrol_unasus_info_data', "userid=$userid AND fieldid=$fieldid");
 	}
 	/*
function get_by_user($userid,$contextid) {
 		global $CFG;
		$sql ="SELECT d.id,d.userid,d.fieldid,d. data  FROM {$CFG->prefix}enrol_unasus_info_data d INNER JOIN   {$CFG->prefix}enrol_unasus_info_field f ON d.fieldid=f.id WHERE d.userid=$userid AND f.contextid= $contextid ";
		$r=get_record_sql($sql);
		return $r;
 	}
 */
function exist_by_user($userid,$fieldid) {
 		global $CFG;
		$sql ="SELECT  COUNT(id) AS qreg    FROM {$CFG->prefix}enrol_unasus_info_data WHERE userid=$userid AND fieldid=$fieldid";
		$r=get_record_sql($sql);
		return $r->qreg ;
		
 	}
 	
 	function get_id_by_user($userid,$fieldid) {
 		global $CFG;
		$sql ="SELECT id  FROM {$CFG->prefix}enrol_unasus_info_data WHERE userid=$userid AND fieldid=$fieldid";
		$r=get_record_sql($sql);
		return $r->id;
 	}
 	
 	function get_all_by_user($userid,$contextid) {
 	global $CFG;
		$sql ="SELECT f.name,d.data,f.datatype	 FROM {$CFG->prefix}enrol_unasus_info_field f INNER JOIN {$CFG->prefix}enrol_unasus_info_data d ON f.id=d.fieldid WHERE f.contextid=$contextid AND d.userid=$userid ";
		$r=get_records_sql($sql);
		return $r;
 	}
 	
 	   	/* converte array para string sql
    	 */
    	function conver_array_to_sql_in($fields_report){
    		$cont=0;
    		
    		$result="";
    		if(empty($fields_report)) return $result;
    		 foreach ($fields_report as $v){ 
    		 	$cont++;
    		 	if($cont==1){$result=$v;}
    		 	else{$result.=",".$v;}
    		 	
    		 }
    		 return $result;
    	}
    	
    	
    	/*
    	 * retorna a lista com os valores do perfil que devem ser exibidos no relat�rio
    	 */
    	function get_data_fields_user_profile($fields_report){
    			global $CFG;
    			
    			$fields_report_sql=$this->conver_array_to_sql_in($fields_report);
    			if(!empty($fields_report_sql)){
    				$sql =" SELECT  id,fieldid,userid,data FROM {$CFG->prefix}enrol_unasus_info_data  WHERE fieldid IN ($fields_report_sql) ";
    				return get_records_sql($sql);
    			}
    			
				
			    return NULL;
				
    	}	
    	/*
    	 * retorna a lista com os valores do perfil que devem ser exibidos no relat�rio em array
    	 */
    	function get_data_fields_user_profile_in_array($fields_report){
    			
				$rows=$this->get_data_fields_user_profile($fields_report);
				$a=array();
			 	if(empty($rows)) return $a;
    		  	foreach ($rows as $row){ 
    		  		$key=$row->fieldid."/".$row->userid;
    		  		$a[$key]=$row->data;
			  }
				return $a;
				
    	}
    	
    	 function get_sql_filter_user_data($fields,$param_fields,$sql_field='er.userid'){
    	$sql_filer="";
    	if(empty($param_fields)) return null;
    	 //faz loop nos em todos os fields
    	print_r($param_fields);
		foreach ($fields as $field){
			//verifique se tem par�metro 
			$param=null;
			if(array_key_exists ($field->id,$param_fields)){
				$param=$param_fields[$field->id];
			}
			
			if($param!=null || $param!=''){
				$sql_filer.=$this->get_usersid_by_data($field->datatype,$field->id,$param,$sql_field);
			}
		}
		return $sql_filer;		 	
    }
    
     /*
    	 * retorna lista de iduser com base  na pesquisa no no perfil
    	 */
    	function get_usersid_by_data($datatype,$fieldid,$data,$sql_field){
				global $CFG;
			 $wsql=" AND data LIKE '%".$data."%'";
			  if($datatype=='menu') $wsql=" AND data='".$data."'";
			  else if($datatype=='checkbox') {
			  	if($data==2){$data=0;}
			  		$wsql=" AND data= ".$data." ";
			  	}
			  $sql =" SELECT  userid FROM  {$CFG->prefix}enrol_unasus_info_data  WHERE fieldid=$fieldid $wsql";
			
			 $rows= get_records_sql($sql);
			 $sql_user=  $this->get_sql_in_userid($rows,$sql_field);
			
			  return $sql_user;
    	}	
    	
    	public function get_sql_in_userid($list,$sql_field){
    		$cont=0;
    		$result="";
    	
    		
    		if(empty($list)) return " AND $sql_field IN (-1) ";
    		
    		  foreach ($list as $v){ 
    		 	
    		 	$cont++;
    		    if($cont==1){$result=$v->userid;}
    		 	else{$result.=",".$v->userid."";}
    		 	
    		 }
    		 
    		 return " AND $sql_field IN ($result) ";
    	}
 }