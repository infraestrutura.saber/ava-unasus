CREATE TABLE IF NOT EXISTS `mdl_synciccarouca_course` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'chave primaria',
  `status` int(3) NOT NULL,
  `mdluserid` int(10) NOT NULL COMMENT 'Usuário do moodle',
  `mdlcourseid` int(10) NOT NULL COMMENT 'curso no mooodle',
  `arcidcurso` int(10) NOT NULL COMMENT 'id curso do arouca',
  `arcidoferta` int(10) NOT NULL COMMENT 'id orfeta do arouca',
  `dataingresso` int(10) NOT NULL COMMENT 'data de ingresso',
  `mdluseridaut` int(10) DEFAULT NULL,
  `dataconclusao` int(10) DEFAULT NULL COMMENT 'data de conclusão',
  `urlcertificado` varchar(255) DEFAULT NULL COMMENT 'url do certificado',
  `faultcodewsarouca` int(4) DEFAULT NULL,
  `faultstringwsarouca` varchar(255) DEFAULT NULL,
  `mdlenrol` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=47 ;
