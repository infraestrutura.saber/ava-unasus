<?php

require_once $CFG->libdir.'/formslib.php';

class form_edit extends moodleform {

    function definition() {
    	global $dto;
    	global $processmode;
    	 $options_mode=$processmode->get_cofig_html_option();
        $mform    =& $this->_form;
        $renderer =& $mform->defaultRenderer();

        $mform->addElement('header', '', get_string('cron_situation_review','enrol_unasus'), '');
         $mform->addElement('hidden', 'contextid', $dto->contextid);
        $mform->setType('contextid', PARAM_INT);
        
         $mform->addElement('hidden', 'id', $dto->id);
        $mform->setType('id', PARAM_INT);
        
         $mform->addElement('select', 'value', get_string('processmode','enrol_unasus'),$options_mode);
        $mform->setType('value', PARAM_TEXT);
        $mform->setDefault('value', $dto->value);
        $mform->setHelpButton('value', array('processing_mode', get_string('processmode', 'enrol_unasus'), 'enrol_unasus'));
       
       
        $this->add_action_buttons(true,get_string('edit','enrol_unasus'));
    }

    function validation($data, $files) {
       $errors = parent::validation($data, $files);
      
     return $errors;
    }

}

?>