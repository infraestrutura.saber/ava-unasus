<?php

require_once $CFG->libdir.'/formslib.php';

class form_edit extends moodleform {

    function definition() {
    	global $cron,$param,$config_process_item, $config_process_frequency;
    	
        $mform    =& $this->_form;
        $renderer =& $mform->defaultRenderer();

        $mform->addElement('header', '', get_string('cron_situation_review','enrol_unasus'), '');
         $mform->addElement('hidden', 'contextid', $param->contextid);
        $mform->setType('contextid', PARAM_INT);
        
        $mform->addElement('checkbox', 'erro_arouca_conn', get_string('msg_erro_arouca_conn','enrol_unasus'));
        $mform->setDefault('erro_arouca_conn', $cron->get_exist_param($config_process_item,cron::$ERRO_AROUCA_CONNECTION));
        
        $mform->addElement('checkbox', 'no_cbo_arouca', get_string('msg_no_cbo_arouca','enrol_unasus'));
        $mform->setDefault('no_cbo_arouca', $cron->get_exist_param($config_process_item,cron::$CBO_AROUCA_NOT_EXIST));
        
        $mform->addElement('checkbox', 'no_role_created', get_string('msg_no_role_created','enrol_unasus'));
        $mform->setDefault('no_role_created', $cron->get_exist_param($config_process_item,cron::$ROLE_NOT_CREATED));
        
        $mform->addElement('checkbox', 'no_cbo_in_role', get_string('msg_no_cbo_in_role','enrol_unasus'));
        $mform->setDefault('no_cbo_in_role', $cron->get_exist_param($config_process_item,cron::$CBO_NOT_IN_ROLE));
        
       $mform->addElement('header', '', get_string('cron_frequency_process','enrol_unasus'), '');
      $mform->addElement('text', 'interval_teme_process', get_string('cron_interval_teme_process','enrol_unasus'),'size="8"');
        $mform->setType('interval_teme_process', PARAM_INT);
        $mform->setDefault('interval_teme_process', $config_process_frequency->value);
       $mform->setHelpButton('interval_teme_process', array('cron_frequency_process', get_string('cron_frequency_process', 'enrol_unasus'), 'enrol_unasus'));
       
        $this->add_action_buttons(true,get_string('edit','enrol_unasus'));
    }

    function validation($data, $files) {
       $errors = parent::validation($data, $files);
       /* if(!is_int($data['interval_teme_process']) ){
        	$errors['interval_teme_process'] = get_string('required_int','enrol_unasus');
        }*/
     return $errors;
    }

}

?>