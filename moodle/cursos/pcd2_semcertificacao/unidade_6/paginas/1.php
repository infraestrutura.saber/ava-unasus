<?php
               
    $pg_atual=1;
    registro($id,unid6_pg,$pg_atual,unid6_ev);
?>
    <style type="text/css">
        #lista{
            list-style-type: disc; 
             margin-left: 30px;
        }
    </style>
 

   <h4 class="titulo">Estomas: o que são?</h4><br/>
   Ostomias, ostoma, estoma ou 
   estomia são palavras de origem grega que significam boca ou abertura cirúrgica 
   que permite a comunicação de um órgão interno com o meio externo. Neste curso, 
   usaremos o termo <b>estomas.</b> <br/><br/>
   Em outras palavras, um estoma 
   consiste na exteriorização do sistema (digestório, respiratório e urinário), 
   criando um orifício externo com as seguintes finalidades (SILVA, 2010; Portaria
    GM 793, 2012; Portaria GM 835, 2012):<br/>
<ol>
<li id="lista">nutrir e hidratar o paciente com distúrbios no sistema gastrointestinal;</li>
<li id="lista">drenar conteúdo fecal ou urinário;</li>
<li id="lista">desviar uma obstrução aguda ou crônica de vias aéreas superiores, a fim de 
	garantir uma boa ventilação;</li>
<li id="lista">administrar medicamentos.</li>
</ol>

A presença de estomas ligados a 
sondas e cateteres contribui para o estresse e reclusão social, gerando sofrimento
 e ansiedade para o paciente e a família, pois costuma comprometer a imagem corporal.
  Nessa perspectiva, a atuação da equipe multidisciplinar com paciente estomizado e
   seus familiares envolve a competência de promover a autonomia relacionada ao 
   autocuidado; sistematizar o seu processo de trabalho baseando-se nas necessidades 
   do paciente e da família; estimular a autoestima, a reinserção social e o 
   bem-estar das pessoas. 

<br><br>
<div class="box">
       <img src="../images/img_saibamais_ad.png" alt="Saiba mais" >
        <span class="titulo_box">Saiba mais...</span>
         <hr>


Orientações aos pacientes estomizados, clique <a target="blank" href= "http://www1.inca.gov.br/inca/Arquivos/cuidados_com_a_sua_estomia.pdf">aqui</a>.<br/>
Como cuidar da criança com estoma, clique <a target="blank" href= "http://www.abraso.org.br/Como%20cuidar%20da%20crian%C3%A7a%20com%20estoma%20(COR).pdf">aqui</a>.<br/>
O preparo do familiar para o cuidado à pessoa com estoma, clique <a target="blank" href="https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=4&cad=rja&ved=0CEgQFjAD&url=http%3A%2F%2Fwww.revista.ufpe.br%2Frevistaenfermagem%2Findex.php%2Frevista%2Farticle%2Fdownload%2F3731%2F5628&ei=3J-LUfnrJouE9QSQmoDoCg&usg=AFQjCNFIBRo9KbOahG32evnNlYyaH2bBpQ&sig2=jFyhoX1OSvuhuYleUrWP_A&bvm=bv.46226182,d.eWU">aqui</a>.<br/>

</div><br><br>

<div class="box">
        <img src="../images/img_vocesabiaque_ad.png" alt="Você sabia" ><span class="titulo_box">Você sabia que...</span>
        <hr>
        
        
        Com o Decreto N.º 5.296 de 02 de dezembro de 2004, a estoma foi considerada uma deficiência física, garantindo direito às pessoas estomizadas (BRASIL, 2004). </div>
