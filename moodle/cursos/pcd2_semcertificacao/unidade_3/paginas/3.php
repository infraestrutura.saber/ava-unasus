<?php
               
    $pg_atual=3;
     registro($id,unid3_pg,$pg_atual,unid3_ev);
?>
    
	<style type="text/css">
		
			#tab_3 img{
				width: 20%;
				margin-bottom: 5px;
				margin-top: 5px;
				margin-left: 5px;
			
			}
			
			#tab_3 p{
				margin: 0 0 10px;
				float: right;
				width: 362px;
				margin-top: 14px;
			}
			
			#linha{
				width: 468px;
				margin-left: 161px;
				background-color: rgb(229, 235, 255);
				cursor: pointer;
			}
			
			.fundo_claro{
				background-color: rgb(205, 219, 255) !important;
			}
			
			#zoom, #zoom1,#zoom2,#zoom3,#zoom4,#zoom5,#zoom6 { 
				margin: 3px;
				width: 462px;
				height: auto;
				display: none;
				float: left;
				background-color: #EFF7FA;
			}
			
			#zoom1 img,#zoom2 img,#zoom3 img,#zoom4 img,#zoom5 img,#zoom6 img{
				width: auto;
				height: auto;
				margin-left: 90px;
			}
			
			#zoom6{
				background-color: #EFF7FA !important;
				border-right: 3px solid rgb(205, 219, 255);
				border-left: 3px solid rgb(205, 219, 255);
				border-bottom: 3px solid rgb(205, 219, 255);
				margin: 0;
			
			}
			.josi{
				background-color:#f6f6f6;
				width: 100%;
			}

	</style>
   
<!-- <script src = "https://www.youtube.com/iframe_api"></script> -->
   <h4 class="titulo">Cuidados  na realização do procedimento</h4>
  
  Os cuidados de saúde realizados no domicílio são considerados 
   procedimentos para manter ou promover a saúde, no intuito de minimizar 
   os efeitos da doença e da incapacidade. <br><br>
Baseados nas necessidades do paciente e de sua família, os cuidados de 
saúde necessários devem ser planejados, coordenados e providenciados pela 
equipe multiprofissional que atua na assistência domiciliar.<br><br>
É importante lembrar que o plano de cuidados deve ser compartilhado entre 
a equipe, a família, o cuidador e o usuário, permitindo interações 
que auxiliam o compartilhamento e formação de compromissos para a 
melhoria da atenção (BRASIL, 2013). Alguns cuidados são fundamentais 
para o procedimento do enteroclisma e estão destacados na figura a seguir.
<br><br>
 <b>Infográfico dos cuidados para procedimento de enteroclisma</b><i class=" icon-hand-up" title="Essa figura possui interação."></i>
<br><br>
<div id="tab_3">
	<div id="linha"  onclick="esconder(1);" >
		<img src="paginas/images/Fig02.1_infografico-enteroclisma.jpg" alt="Lavar as mãos"/>
		<p>Lavar as mãos com sabão em água corrente.</p>
		<div id="zoom1"><img src="paginas/images/Fig02.1_infografico-enteroclisma.jpg"  /></div>
	</div>

	<div class="fundo_claro" id="linha"  onclick="esconder(2);">
		<img src="paginas/images/Fig02.2_infografico-enteroclisma.jpg" alt="Explicar o procedimento"/>
		<p>Explicar ao paciente e familiar o procedimento a ser realizado.</p>
		<div id="zoom2"><img  src="paginas/images/Fig02.2_infografico-enteroclisma.jpg" /></div>
	</div>
	
	<div  id="linha"  onclick="esconder(3);">	
		<img src="paginas/images/Fig02.3_infografico-enteroclisma.jpg" alt="Colocar o paciente em posição de Sims" />
		<p>Colocar o paciente em posição de Sims e durante o procedimento cobri-lo com lençol, expondo apenas a região glútea.</p>
		<div id="zoom3"><img  src="paginas/images/Fig02.3_infografico-enteroclisma.jpg"/></div>
	</div>
	
	<div class="fundo_claro" id="linha" onclick="esconder(4);">		
		<img src="paginas/images/Fig02.4_infografico-enteroclisma.jpg" alt="Colocar o paciente em posição de Fowler"/>
		<p>Após as refeições, colocar os pacientes acamados na posição de Fowler, se não houver contraindicação.</p>
		<div id="zoom4"><img  src="paginas/images/Fig02.4_infografico-enteroclisma.jpg"/></div>
	</div>
	
	<div id="linha"  onclick="esconder(5);">
		<img src="paginas/images/Fig02.5_infografico-enteroclisma.jpg" alt="Não forçar a introdução da sonda"/>
		<p>Não forçar a introdução da sonda.</p>
		<div id="zoom5"><img  src="paginas/images/Fig02.5_infografico-enteroclisma.jpg"/></div>
	</div>
		
	<div id="linha" class="fundo_claro"  onclick="esconder(6);">		
		<img src="paginas/images/Fig02.6_infografico-enteroclisma.jpg" alt="Investigar hábitos alimentares" />
		<p>Investigar hábitos alimentares.</p>
		<div id="zoom6" ><img  src="paginas/images/Fig02.6_infografico-enteroclisma.jpg"/></div>
		<div class="josi"><b >Fonte:</b> (BRUNI et al, 2004, adaptado).</div>
	</div>
	</div>
	<br>


	
<div class="box">


	<table >
		<tr>
			<td width="420"><div id="player"></div></td>
			<td class="josi2"> <h5>Manobra de Rosing</h5> É importante instruir o paciente e o cuidador acerca 
       da manobra de Rosing. Trata-se de uma massagem realizada várias vezes no abdômen, seguindo o sentido horário (da esquerda para a direita, e de baixo para cima) durante 20 a 30 minutos, após as refeições. <br>O paciente também deve ser 
       orientado a ficar sentado e, caso o equilíbrio permita, inclinado para 
       frente. Isso pode auxiliar no processo de defecação (BRUNI et al, 2004).

		Assista ao vídeo para entender melhor como fazer a manobra.</td>

		</tr>
	</table >
      



</div>

<br>
	<div class="box">
        <img src="../images/img_vocesabiaque_ad.png" alt="Você sabia">
        <span class="titulo_box">Você sabia que...</span>
        <br>
        <hr/>
A retirada de fecaloma é de competência privativa do médico. 
Saiba mais clicando <a href="http://www.cremesp.org.br/?siteAcao=Pareceres&dif=s&ficha=1&id=6566&tipo=PARECER&orgao=Conselho%20Regional%20de%20Medicina%20do%20Estado%20de%20S%E3o%20Paulo&numero=87507&situacao=&data=02-08-2006"  target="_blank">aqui </a>.
	</div>
	<script>
	
		
		function esconder(id){
		
			if ($("#zoom" + id).is(":hidden")) {
				$("#zoom" + id).slideDown("slow");
				
			} else{
			$('#zoom' + id).hide("slow");
			}
	
		}
		
		//onYouTubePlayerAPIReady
		/*p-zQcBD9m1Q
		  $.post( "../apresentacao/functions/chamaCadVideo.php", { video: 2}, function() {
						
				});
		  
		 */
// 2. This code loads the IFrame Player API code asynchronously.
      var tag = document.createElement('script');

      tag.src = "https://www.youtube.com/iframe_api";
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

      // 3. This function creates an <iframe> (and YouTube player)
      //    after the API code downloads.
      var player;
      function onYouTubeIframeAPIReady() {
        player = new YT.Player('player', {
          height: '250',
          width: '400',
          videoId: 'xG_4GpNIm6g',
          events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
          }
       
        });
      }
      // 4. The API will call this function when the video player is ready.
      function onPlayerReady(event) {
        //event.target.playVideo();
      }

      // 5. The API calls this function when the player's state changes.
      //    The function indicates that when playing a video (state=1),
      //    the player should play for six seconds and then stop.
      var done = false;
      function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.PLAYING && !done) {
      $.post( "../apresentacao/functions/chamaCadVideo.php", { video: 2}, function() {
						
				});
          done = true;
        }
      }
 
	</script>
	<style type="text/css">
	.josi2{
		line-height: 20px;

	}
	</style>
	
		<?php

	if ($_GET['video']==1){ 

		echo '<script type="text/javascript">$("html,body").animate({scrollTop: $("#player").offset().top}, 1500);</script>';
	}
?>
	
	
	





   
