<style type="text/css">
        #lista li, .box ul li{
            list-style-type: disc; 
             margin-left: 10px;
        }
     </style>
        <p>
            <h4 class='titulo'>Apresentação da Unidade</h4>
        
        </p>
		Durante o cuidado domiciliar, o paciente pode necessitar realizar alguns exames para monitoramento da sua situação de saúde sem precisar ser removido ou encaminhado para uma unidade de saúde. <br><br>
Nesta unidade, identificaremos os acessórios necessários para a coleta de material biológico no domicílio, além dos cuidados na execução do procedimento, acondicionamento, transporte do material biológico e descarte do material utilizado na coleta.<br>
		<br>
        <br>
        <b>Objetivos educacionais da unidade</b><br><br>
		<ul id="lista"><li>Identificar os acessórios necessários para a coleta de material biológico no domicílio, além dos cuidados na execução do procedimento, acondicionamento, transporte do material biológico e descarte do material utilizado na coleta.</li></ul><br>
		
<br><br>


        <img src="images/abertura.png" alt="Imagem de abertura Unidade 7">