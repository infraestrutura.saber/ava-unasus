﻿<?PHP 
$string['enrolname'] = 'Sistema de Matrícula UNA-SUS';
$string['description'] = 'Sistema de Gerenciamento da Matrícula Moodle da UNA-SUS .';

//general 
$string['save']='Salvar';
$string['delete']='Excluir';
$string['edit']='Editar';
$string['add']='Cadastar';
$string['view']='Visualizar';
$string['confirm']='Confirmar';
$string['cancel']='Cancelar';
$string['research']='Pesquisar';
$string['back']='Voltar';
$string['goon'] ='Avançar';
$string['descriptiont']='Descrição';
$string['id']='ID';
$string['requiredfield'] = 'Campo obrigatório'; 
$string['rowfilter'] = 'Registro Filtrado: ';
$string['rowcount'] = 'Quant. de Registro: ';
$string['userount'] = 'Quant. de Usuário';
$string['row_save_sucess'] ='Registro cadastrado com sucesso';
$string['row_edit_sucess'] ='Registro alterado com sucesso';
$string['row_delete_sucess'] ='Registro excluído com sucesso';
$string['manage_enrol'] ='Gerenciar Inscrição';

$string['cbo']='CBO';
$string['msg_erro_arouca_conn']='Erro de conexão com Arouca';
$string['msg_no_cbo_arouca']='CBO não consta na Plataforma Arouca';
$string['msg_no_role_created']='Não existe nenhuma regra de inscrição cadastrada na base de dados';
$string['msg_no_cbo_in_role']='CBO não consta na lista de requisitos';
$string['msg_cpf_not_valid'] = 'CPF inválido';
$string['msg_enrol_failed']='Ocorreu uma falha ao registrar inscrição na base de dados';
$string['yes']='Sim';
$string['not']='Não';
$string['duplication_record_db']='Duplicação de registro. Já existe um registro com o mesmo nome na base de dados. ';
$string['msg_connhttp_arouca_failed'] = 'Falha de conexão com a Plataforma Arouca/CNES';
#critica
$string['required_int']='Digite número inteiro';

$string['course'] ='Curso';
$string['category'] ='Categoria';
$string['course_category'] ='Categoria de Curso';
$string['value'] ='Valor';

$string['context_system'] ='Contexto de Sistema';
$string['config_global'] ='Configuração Global';

$string['aprove'] = 'Aceite';
$string['reject'] = 'Recusado';

$string['count_enrol_request'] = 'Quant. de Solicitação'; 
$string['count_enrol_request_aprove'] = 'Quant.  de Matrículas Aceites';
$string['count_enrol_request_reject'] = 'Quant.  de Matrículas Recusadas'; 


$string['automatic'] = 'Automático';
$string['manual'] = 'Manual';

$string['type'] = 'Tipo';
$string['enroldate'] = 'Data de Inscrição';
$string['enroldateupdate'] = 'Data de Atualização';
$string['status'] = 'Status';
$string['observation'] = 'Observação';
$string['name'] = 'Nome';
$string['user'] = 'Usuário';

$string['manual_aprove'] = 'Homologação Manual da Matrícula';
$string['manual_aprove_by'] = 'Homologado por';
$string['justify'] = 'Justificativa';

$string['select_course'] = 'Selecione o curso'; 

#wsqkey
$string['manegewskeyauth']='Gerenciar Chave de Autenticação de Webservice';
$string['wskeyauth']='Chave de Autenticação de Webservice';
$string['addnewwskeyauth']='Cadastrar Nova Chave de Autenticação de Webservice';
$string['addwskeyauth']='Cadastrar Chave de Autenticação de Webservice';
$string['editwskeyauth']='Editar Chave de Autenticação de Webservice';
$string['deletewskeyauth']='Apagar Chave de Autenticação de Webservice';
$string['wssystem']='Sistema de Webservice';
$string['wssystem_duplication']='Duplicação do sistema de Webservice';
$string['wsname']='Nome do Webservice';
$string['platarouca']='Plataforma Arouca';
$string['fileautkey']='Arquivo de certificado digital';
$string['authkeyname']='Arquivo com senha da chave privada';
$string['urlws']='Url do web service (WSDL)';
$string['urlnotvalid'] = 'URL inválido';
$string['unasus:wskeycreate']='Cadastrar Chave de Autenticação de Webservice';
$string['unasus:wskeyedit']='Editar Chave de Autenticação de Webservice';
$string['unasus:wskeydelete']='Apagar Chave de Autenticação de Webservice';
$string['unasus:wskeyview']='Visualizar Chave de Autenticação de Webservice';


#message
$string['message']='Mensagem';
$string['message_maneger']='Gerenciar Mensagens';
$string['message_add']='Cadastrar Mensagem';
$string['message_addnew']='Cadastrar Nova Mensagem';
$string['message_edit']='Editar Mensagem';
$string['message_view']='Visualizar Mensagem';
$string['message_delete']='Apagar Mensagem';
$string['message_view']='Visualizar Mensagem';
$string['message_presentation']='Mensagem de apresentação';
$string['message_approval']='Mensagem de aprovação';
$string['message_disapproval']='Mensagem de rejeição';
$string['message_autreview']='Mensagem de revisão automática';
$string['message_manualreview']='Mensagem de revisão manual';
$string['message_pendingapprovalmanager']='Mensagem pedindo para aguardar aprovação do gestor';

$string['message_enrol_aproved_subject'] = '$a->site: Pedido de inscrição aprovado no curso \"$a->course\"';

$string['unasus:messagecreate']='Cadastrar Mensagem';
$string['unasus:messageedit']='Editar Mensagem';
$string['unasus:messageview']='Visualizar Mensagem';

#processmode
$string['processmode']='Modo de Processamento';
$string['processmode_config']='Configurar Modo de Processamento';
$string['processmode_config_add']='Adicionar Configuração do  Modo de Processamento';
$string['processmode_config_edit']='Alterar Configuração do  Modo de Processamento';
$string['processmode_config_view']='Visualizar Configuração do  Modo de Processamento';
$string['processmode_manual']='Manual'; 
$string['processmode_auto']='Automático';
$string['processmode_manual_auto']='Manual e automático';  

$string['unasus:processmodeconfig']='Configurar Modo de Processamento';
$string['unasus:processmodeview']='Visualizar Modo de Processamento';

#role
$string['role']='Regra da Matrícula';
$string['role_manege']='Gerenciar Regra da Matrícula';
$string['role_add']='Cadastrar Regra da Matrícula';
$string['role_addnew']='Cadastrar Nova Regra da Matrícula';
$string['role_edit']='Editar Regra da Matrícula';
$string['role_delete']='Apagar Regra da Matrícula';
$string['role_view']='Visualizar Regra da Matrícula';
$string['role_type']='Tipo';
$string['role_title']='Título';
$string['role_norole_defined']='Ainda não foi definida nenhuma regra de matrícula nesse curso.';
$string['role_cbo_enrol']='Código CBO que homologou a matrícula';  

$string['unasus:rolecreate']='Cadastrar Regra da Matrícula';
$string['unasus:roleedit']='Editar Regra da Matrícula';
$string['unasus:roleview']='Visualizar Regra da Matrícula';

#cron
$string['cron']='Cron';
$string['cron_config']='Configurar Cron';
$string['cron_config_add']='Adicionar Configuração do Cron';
$string['cron_config_view']='Visualizar Configuração do Cron';
$string['cron_config_edit']='Editar Configuração do Cron';
$string['cron_review']='Revisão de matrícula';
$string['cron_situation_review']='Situações em que devem ocorrer o reprocessamento  da	 matrícula:';
$string['cron_frequency_process']='Frequência de processamento do cron';
$string['cron_interval_teme_process']='Intervalo de processamento em hora';
$string['cron_activated']='Ativado';
$string['cron_inactive']='Desativado';

$string['unasus:cronconfig']='Configurar Cron';
$string['unasus:cronview']='Visualizar Configuração do Cron';

#profilefield
$string['profilefield']='Campos do Perfil';
$string['profilefield_add']='Criar um novo campo de perfil';
$string['profilefieldcategory_add']='Criar uma nova categoria de perfil';
$string['profilefieldcategory_delete']='Apagar uma  categoria de perfil';
$string['profilefieldcategory_name']='Nome da categoria de perfil';

$string['profilefield_select_type']='Escolher...';
$string['profilefield_checkbox']='Caixa de seleção';
$string['profilefield_option']='Menu de escolhas';
$string['profilefield_text']='Texto de entrada';
$string['profilefield_textarea']='Área de texto';

$string['profilefield_checkbox_add']='Adicioanar caixa de seleção';
$string['profilefield_checkbox_edit']='Alterar caixa de seleção';
$string['profilefield_option_add']='Adicioanar menu de escolhas';
$string['profilefield_option_adit']='Alterar menu de escolhas';
$string['profilefield_text_add']='Adicioanar texto de entrada';
$string['profilefield_text_edit']='Alterar texto de entrada';
$string['profilefield_textarea_add']='Adicioanar área de texto';
$string['profilefield_textarea_edit']='Alterar área de texto';

$string['profilefield_view_form']='Visualização do forulário';


$string['profilefield_delete']='Apagar campo do perfil';

$string['profilefield_commun_config']='Configurações comuns';
$string['profilefield_data_profile']='Dados do perfil';

$string['profilefield_short_name']='Nome breve (deve ser único)';
$string['profilefield_name']='Nome';	
$string['profilefield_description']='Descrição do campo';	
$string['profilefield_required']='Este campo é obrigatório?';	
$string['profilefield_locked']='Este campo está trancado?';
$string['profilefield_show']='Este campo deve ser exibido?';		
$string['profilefield_unique']='A informação deve ser única?';	
$string['profilefield_category']='Categoria';	

$string['profilefield_espcific_config']='Configurações específicas';
$string['profilefield_default_value']='Valor padrão';	
$string['profilefield_size_show']='Tamanho de apresentação';
$string['profilefield_size_max']='Comprimento máximo';	
$string['profilefield_password']='Este campo é de senha?';
$string['profilefield_selected_default_value']='Selecionado por padrão';		
$string['profilefield_menu_options']='Menu de opções (um por linha)';		

$string['unasus:profilefieldcreate']='Cadastrar campo do perfil';
$string['unasus:profilefieldedit']='Editar campo do perfil';
$string['unasus:profilefielddelete']='Apagar campo do perfil';
$string['unasus:profilefieldview']='Visualizar campo do perfil';

$string['unasus:profilefieldcategorycreate']='Cadastrar categoria do perfil';
$string['unasus:profilefieldcategoryedit']='Editar categoria do perfil';
$string['unasus:profilefieldcategorydelete']='Apagar categoria do perfil';
$string['unasus:profilefieldcategoryview']='Visualizar categoria do perfil';

$string['unasus:manualenrol']='Efetuar inscrição manual';
$string['unasus:viewreporterenrol']='Consultar relatório de inscrição';
$string['unasus:configreporterenrol']='Configurar relatório';



$string['config']='Configuração';

$string['config_reporter_profile']='Relatório do perfil';
$string['config_reporter_profile_conf']='Configuração do relatório do perfil';
$string['config_reporter_profile_enrol']='Relatório do perfil da matrícula';
$string['config_reporter_profile_enrol_conf']='Configuração do relatório do perfil da matrícula';
$string['config_enable_field_filter']='Ativar campos de filtro do relatório ';
$string['config_enable_field_reporter']='Ativar campos de exibição de dados no relatório';


#reporter general
//$string['reporter']='Relatório';
#reporter general
//$string['general_reporter']='Relatório Geral';

$string['search']='Pesquisa';
$string['enrol_data']='Dados da Inscrição';
$string['filter_enrol_data']='Filtrar dados da Inscrição';
$string['filter_enrol_data']='Filtrar dados da Inscrição';
$string['filter_show_profile_data']='Filtrar/exibir dados do campos do perfil';
$string['filter_profile_data']='Filtrar dados do campos do perfil';
$string['show_profile_data_reporter']='Exibir dados dos campos do perfil no relatório ';

$string['reporter']='Relatório';
$string['reporter_consolidate_by_course']="Relatório consolidado por curso";
$string['reporter_enrol_data']='Relatório de dados da inscrição';

$string['reporter_consolidate_by_cbo']="Relatório consolidado por CBO";

?>
 
 