<div id="ex1"></div><span style="color: #009797; font-weight: bold; font-size: 20px;">Exercícios</span><br><br>
<!-- inicio exercicio 1 -->

	<b>1) De acordo com o que você aprendeu na unidade, marque a alternativa que apresenta uma das características do PEC:</b><br>

<br/>
<div id="unid3_ex1"></div>
<script src="../unidades/unidade3/script.js" type="text/javascript"></script>
<script src="../unidades/unidade3/unid3_ex1.js" type="text/javascript"></script>
<br /><br>
<div class="box" id="boxunid3_ex1"></div>
<div class="feedback feedbackunid3_ex1">
	<p class="MsoNormal"></p>
	<div class="feed">
		Para a utilização do PEC, há sim a necessidade do acesso à internet. Apenas por meio do CDS é possível a descrição utilizando formulários impressos.		<br />
	</div>
	<div class="feed">
		O PEC permite visualizar informações sobre o paciente, inclusive de atendimentos anteriores, caso o mesmo já esteja cadastrado.
		<br />
	</div>
	<div class="feed">
		Com o uso do PEC, há necessidade de acesso à internet, e também há interação entre áreas e profissionais responsáveis pelo atendimento ao usuário.
		<br />
	</div>
	<div class="feed">
		 Quando se opta pelo uso do PEC, é criado um perfil (e não um PEC) para cada profissional responsável pelo atendimento ao usuário. 
	</div>
</div>
<br>
<div id="ex2"></div>
<!-- inicio exercicio 2 -->

	<b>2) Os profissionais que atuam na Unidade Básica de Saúde (UBS) do município de Pontal dos Coqueirais não têm acesso à internet. O que fazer para utilizar o PEC? Marque (V) verdadeiro ou (F) falso nas sentenças a seguir: </b>


</p><p></p>
<br>
<div id="unid3_ex2"></div>

<script src="../unidades/unidade3/unid3_ex2.js" type="text/javascript"></script> 
<br /><br />
<div class="box" id="boxunid3_ex2"></div>
<div class="feedback feedbackunid3_ex2">
	<p class="MsoNormal"></p>

	Gabarito: V - V - F<br><br>

	I. Os dados podem ser descritos em fichas de papel, e as mesmas serão digitadas no sistema com instalação CDS ou serão digitados em outra máquina com o sistema PEC instalado, e esse sim irá enviar os dados para o Centralizador ou diretamente para o Ministério da Saúde.<br><br>
II. Se a UBS não tiver outra máquina com o PEC instalado, os dados das fichas de papel digitados no CDS serão enviados para o Centralizador e, posteriormente, para o SISAB.<br><br>
III. Caso sejam utilizadas as fichas de papel, as mesmas serão digitadas no sistema com instalação CDS, e, da própria UBS, esses dados podem ser enviados para outra máquina com o sistema PEC instalado, e esse sim irá enviar os dados para o Centralizador ou diretamente para o Ministério da Saúde.<br><br>

</div><br>
<div id="ex3"></div>
<!-- inicio exercicio 3 -->

	<b>3) Sobre o uso do protocolo SOAP e CIAP-2 no PEC, analise se sentenças são (V) verdadeiras ou (F) falsas:

</b><br><br>
I. O PEC utiliza o protocolo SOAP como um componente importante do Registro Clínico Orientado por Problemas (RCOP), permitindo a padronização das notas clínicas e potencializando o trabalho e a comunicação em equipe de saúde. Ele visa a simplificar o registro das informações, sistematizando o processo de coleta de dados.<br><br>
II. A Classificação Internacional e Atenção Primária (CIAP-2) consiste em um sistema de classificação de problemas que substitui a necessidade do CID-10 como indicador de morbimortalidade, possuindo a vantagem de poder ser utilizado por toda a equipe de saúde.<br><br>
III. A CIAP-2 aparece no e-SUS AB como uma ferramenta importante que permite classificar não só os problemas diagnosticados pelos profissionais de saúde, mas principalmente os motivos das consultas e as intervenções que foram acordadas utilizando o protocolo SOAP para sistematização dos registros.<br><br>
</p><p></p><br>
Agora marque a alternativa que apresenta a sequência <b>correta</b>:
<br><br/>
<div id="unid3_ex3"></div>

<script src="../unidades/unidade3/unid3_ex3.js" type="text/javascript"></script>
<br /><br />
<div class="box" id="boxunid3_ex3"></div>
<div class="feedback feedbackunid3_ex3">
	<p class="MsoNormal"></p>
	<div class="feed">
		a)	O item II é falso. A Classificação Internacional e Atenção Primária (CIAP-2) consiste em um sistema de classificação de problemas (e não de doenças), que pode ser utilizada por todos os profissionais da Atenção Básica. Faz o melhor atendimento da incerteza, porém não substitui o CID-10, que continua sendo importante indicador de morbimortalidade.
		<br />
	</div>
	<div class="feed">
		b)	Os itens I e III são verdadeiros e o II é falso.<br>
I) O PEC tem como objetivo simplificar o registro das informações, sistematizando o processo de coleta de dados dos profissionais de saúde. E, utiliza o protocolo SOAP como um componente importante do Registro Clínico Orientado por Problemas (RCOP), originalmente criado para o ambiente hospitalar, mas que pode ser adaptado e utilizado por todos os profissionais da Atenção Básica, incluindo os ACS, permitindo a padronização das notas clínicas e potencializando o trabalho e a comunicação em equipe.<br> 
II) A Classificação Internacional e Atenção Primária (CIAP-2) consiste em um sistema de classificação de problemas (e não de doenças), que pode ser utilizada por todos os profissionais da Atenção Básica. Faz o melhor atendimento da incerteza, porém não substitui o CID-10, que continua sendo importante indicador de morbimortalidade.<br>
III) A Classificação Internacional e Atenção Primária (CIAP-2) consiste em um sistema de classificação de problemas (e não de doenças), que pode ser utilizada por todos os profissionais da Atenção Básica. Faz o melhor atendimento da incerteza, porém não substitui o CID-10, que continua sendo importante indicador de morbimortalidade.

	
	</div>
	<div class="feed">
		c)	Apenas o item II é falso. A Classificação Internacional e Atenção Primária (CIAP-2) consiste em um sistema de classificação de problemas (e não de doenças), que pode ser utilizada por todos os profissionais da Atenção Básica. Faz o melhor atendimento da incerteza, porém não substitui o CID-10, que continua sendo importante indicador de morbimortalidade. 
		<br />
	</div>
	<div class="feed">
		d)	Os itens I e III são verdadeiros e o item II é falso. <br>
I) O PEC tem como objetivo simplificar o registro das informações, sistematizando o processo de coleta de dados dos profissionais de saúde. E, utiliza o protocolo SOAP como um componente importante do Registro Clínico Orientado por Problemas (RCOP), originalmente criado para o ambiente hospitalar, mas que pode ser adaptado e utilizado por todos os profissionais da Atenção Básica, incluindo os ACS, permitindo a padronização das notas clínicas e potencializando o trabalho e a comunicação em equipe. <br>
II) A Classificação Internacional e Atenção Primária (CIAP-2) consiste em um sistema de classificação de problemas (e não de doenças), que pode ser utilizada por todos os profissionais da Atenção Básica. Faz o melhor atendimento da incerteza, porém não substitui o CID-10, que continua sendo importante indicador de morbimortalidade.<br>
III) A Classificação Internacional e Atenção Primária (CIAP-2) consiste em um sistema de classificação de problemas (e não de doenças), que pode ser utilizada por todos os profissionais da Atenção Básica. Faz o melhor atendimento da incerteza, porém não substitui o CID-10, que continua sendo importante indicador de morbimortalidade. <br>

	</div>
</div>

<?php
	
	$ex = $_GET['ex'];

	if($ex == 1){

		echo '<script>$("html,body").animate({scrollTop: $("#ex1").offset().top}, 2500);</script>' ;


	}else if($ex == 2){

		echo '<script>$("html,body").animate({scrollTop: $("#ex2").offset().top}, 2500);</script>' ;

	}else if($ex == 3){
		echo '<script>$("html,body").animate({scrollTop: $("#ex3").offset().top}, 2500);</script>' ;
	}

?>
<style>
	
table tr td{font-size: 13px;}
table tr:first-child{
	font-size: 19px!important;
	
}
</style>