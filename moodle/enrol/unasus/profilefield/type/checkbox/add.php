<?php

   require("../../../../../config.php");
   require("$CFG->dirroot/enrol/unasus/lib/context.php");
   require("$CFG->dirroot/enrol/unasus/profilefield/category/lib.php");
   require("$CFG->dirroot/enrol/unasus/profilefield/lib.php");
   require("$CFG->dirroot/enrol/unasus/profilefield/type/checkbox/form_add.php");

require_login();

   //permiss�o
    require_capability('enrol/unasus:profilefieldcreate', get_context_instance(CONTEXT_SYSTEM), NULL, false);
    add_to_log(1, 'enrol_unasus', 'profilefieldcreate', "enrol/unasus/profilefield/type/checkbox/add.php?contextid=$contextid", getremoteaddr());    
    
    $param=  new object();
    $context =new context();
    $param->contextid= required_param('contextid',PARAM_INT);
    $context_name=$context->get_name($param->contextid); 
     $profilefield_category =new profilefield_category();
	$profilefield_field = new profilefield_field(); 
    $form= new form_add();
	
       if ($form->is_cancelled()) {
        redirect($CFG->httpswwwroot.'/enrol/unasus/profilefield/manage.php?contextid='.$param->contextid) ;

    } else if ($formdata = $form->get_data()) {
    	$profilefield_field->save($formdata);
    	redirect($CFG->httpswwwroot.'/enrol/unasus/profilefield/manage.php?contextid='.$param->contextid) ;
    	  
    	
    }
 
    //Navega��o
   $navigation = build_navigation(array(array('name' => get_string('enrolname','enrol_unasus'), 'link' => "$CFG->wwwroot/enrol/unasus/index.php", 'type' => 'misc'),
                                     array('name' => get_string('profilefield','enrol_unasus'), 'link' => "$CFG->wwwroot/enrol/unasus/profilefield/index.php", 'type' => 'misc'), array('name' => $context_name, 'link' => null, 'type' => 'misc'), array('name' => get_string('profilefield_checkbox_add','enrol_unasus'), 'link' => null, 'type' => 'misc')));
  
     print_header(get_string('enrolname','enrol_unasus'),get_string('applicationsenrolment','enrol_unasusem'), $navigation);
   
 	
    $form->display();
  
    print_footer();
    
?>